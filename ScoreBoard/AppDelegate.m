
#import "AppDelegate.h"

#import "SplashViewController.h"
#import "SharedComponent.h"

#import <Accounts/ACAccountType.h>
#import <Accounts/ACAccountStore.h>
#import "NSDate+Extensions.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation AppDelegate

@synthesize window;
@synthesize navigationController;
- (void
   )dealloc
{
    [self.window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
        
    globalAlert = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.

    self.navigationController = [[UINavigationController alloc] init];
    [self.navigationController setNavigationBarHidden:YES];
    
    SplashViewController *splashViewController = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
    
    [self.navigationController pushViewController:splashViewController animated:NO];
    [splashViewController release];
    self.window.rootViewController = self.navigationController;
    // important: to get popup transparent background
    self.window.rootViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [[SharedComponent getInstance] setNavigationController: self.navigationController];
    [[SharedComponent getInstance] setInterfaceOrientation: UIInterfaceOrientationPortrait];

    // https://developers.facebook.com/docs/ios/getting-started
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    /*[SharedComponent getInstance].facebook = [[Facebook alloc] initWithAppId:@"268459673271216" andDelegate:self];

    // Check and retrieve authorization information
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"]) {
        [SharedComponent getInstance].facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        [SharedComponent getInstance].facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
        
        [[SharedComponent getInstance] setIsLinkFacebook:YES];
    }*/
    
    [[SharedComponent getInstance] setIsLinkTwitter:NO];
    [[SharedComponent getInstance] setTwitterAccount:nil];
    
    //get Twitter username and store it
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    if (accountStore != nil) {
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
        [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
            if(granted) {
                NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                if (accountsArray != nil && [accountsArray count] > 0) {
                    ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                    [[SharedComponent getInstance] setTwitterAccount:twitterAccount]; //accountDescription
                }
                if ([[SharedComponent getInstance] twitterAccount] != nil){
                    [[SharedComponent getInstance] setIsLinkTwitter:YES];
                }
            }
        }];
    }
    
     
    [CommonFunction checkAndCreateDatabase];
    
    //register Push notification
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) { // iOS 8 and later
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeBadge];
    }
    
    NSDictionary *remoteNotification = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotification != nil)
        [self application:application didReceiveRemoteNotification:remoteNotification];
    
    ////
    //[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(simulatePushNotification) userInfo:nil repeats:YES];
    ////
    [self.window makeKeyAndVisible];
    return YES;
}

// https://developers.facebook.com/docs/ios/getting-started
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}

- (void) simulatePushNotification{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:@"1" forKey:@"gameid"];
    [dic setValue:@"11" forKey:@"eventid"];
    [self application:nil didReceiveRemoteNotification:dic];
    NSLog(@"simulate push notification");
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken { 
    
    //NSString *str = [NSString stringWithFormat:@"Device Token=%@",deviceToken];
    
    NSString *token = [[[[NSString stringWithFormat:@"%@", deviceToken] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[SharedComponent getInstance] setDeviceToken:token];
    
    NSLog(@"%@", [[SharedComponent getInstance] deviceToken]);
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err { 
    
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:str delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSString *gameId = nil;
    NSString *statusId = nil;
    NSString *requestScoreMessage = nil;
    
    for (id key in userInfo) {
        NSLog(@"key %@; value %@", key, [userInfo objectForKey:key]);
        
        /*NSString *message = [NSString stringWithFormat:@"key %@; value %@", key, [userInfo objectForKey:key]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
         */
        
        if ([key isEqualToString:@"aps"]){
            requestScoreMessage = [[userInfo objectForKey:key] objectForKey:@"alert"];
        }
        else if ([key isEqualToString:@"gameid"]){
            gameId = [NSString stringWithFormat:@"%@", [userInfo objectForKey:key]];
        }
        else if ([key isEqualToString:@"eventid"]){
            statusId = [NSString stringWithFormat:@"%@", [userInfo objectForKey:key]];
        }
    }
    
    if (gameId != nil && statusId != nil) {
        NSLog(@"gameId: %@, status: %@", gameId, statusId);
        
        if ([statusId isEqualToString:@"10"]){
            NSString *message = [NSString stringWithFormat:@"Please update score for game SBN#%@.", gameId];
            
            if (requestScoreMessage != nil){
                message = requestScoreMessage;
                requestScoreMessage = nil;
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        else if ([statusId isEqualToString:@"11"]) {
            [globalAlert dismissWithClickedButtonIndex:0 animated:NO];
            
            globalAlert = [[UIAlertView alloc] initWithTitle:@"Message" message:requestScoreMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [globalAlert show];
            [globalAlert release];
            
            for (UIViewController *v in [[[SharedComponent getInstance] navigationController] viewControllers]){
                if ([v isKindOfClass:[UpdateScoreViewController class]]){\
                    NSLog(@"Update score from notification at UpdateScoreViewController");
                    [[((UpdateScoreViewController*)v) scoreBoard] refresh];
                    break;
                }
                
                if ([v isKindOfClass:[GameFeedViewController class]]){
                    NSLog(@"Update score from notification at GameFeedViewController");
                    [[((GameFeedViewController*)v) scoreBoard] refresh];
                    break;
                }
            }
        }
        else if ([statusId isEqualToString:@"5"]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:requestScoreMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        else
        {
            GameBox *gameBox = nil;
            if ([[[SharedComponent getInstance] listCurrentGameBox] count] > 0) {
                gameBox = [[[SharedComponent getInstance] listCurrentGameBox] objectForKey:gameId];
                if (gameBox != nil) {
                    [gameBox.game setStatusID:statusId];
                    [gameBox setStatus:statusId.intValue];
                }
            }
            
            [Game updateGameStatus:gameId statusId:statusId];
            NSLog(@"Push notification set status done");
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GameStatusChanged" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:gameId, @"GameId", statusId, @"StatusId", nil]];
        }
        
        gameId = nil;
        statusId = nil;
    }
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (alertView == globalAlert){
        globalAlert = nil;
    }
}

/*- (void) fbDidLogin{
    NSLog(@"fbDidLogin");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[[SharedComponent getInstance].facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[[SharedComponent getInstance].facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your facebook account is linked." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
    [[SharedComponent getInstance] setIsLinkFacebook:YES];
    [[[SharedComponent getInstance] btnFacebook] setBackgroundImage:[UIImage imageNamed:@"BtnFacebook_Unlink.png"] forState:UIControlStateNormal];
}

- (void) fbDidLogout
{
    NSLog(@"fbDidLogout");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] && [defaults objectForKey:@"FBExpirationDateKey"]) {
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        [defaults synchronize];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your facebook account is unlinked." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];    
    
    [[SharedComponent getInstance] setIsLinkFacebook:NO];
    [[[SharedComponent getInstance] btnFacebook] setBackgroundImage:[UIImage imageNamed:@"BtnFacebook.png"] forState:UIControlStateNormal];
}

- (void)fbDidNotLogin:(BOOL)cancelled {
    
}

- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt {
    
}
- (void)fbSessionInvalidated {
    
}*/

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    [[SharedComponent getInstance] setIsLinkTwitter:NO];
    //get Twitter username and store it   
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    if (accountStore != nil) {
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
        [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
            if(granted) {
                NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                if (accountsArray != nil && [accountsArray count]>0) {
                    ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                    [[SharedComponent getInstance] setTwitterAccount: twitterAccount]; // accountDescription
                }
                if ([[SharedComponent getInstance] twitterAccount] != nil){
                    [[SharedComponent getInstance] setIsLinkTwitter:YES];
                }
                
                if ([[SharedComponent getInstance] btnTwitter] != nil)
                {
                    if ([[SharedComponent getInstance] isLinkTwitter]){
                        [[[SharedComponent getInstance] btnTwitter] setBackgroundImage:[UIImage imageNamed:@"BtnTwitter_Unlink.png"] forState:UIControlStateNormal];
                    }
                    else
                    {
                        [[[SharedComponent getInstance] btnTwitter] setBackgroundImage:[UIImage imageNamed:@"BtnTwitter.png"] forState:UIControlStateNormal];
                    }
                }
            }
        }];
        
        //[accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
            
        //}];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
