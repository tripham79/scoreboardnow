
#import <Foundation/Foundation.h>

#import "ASIHTTPRequest.h"
#import "Game.h"

@interface SBNRequest : NSObject

@property (retain) NSString *body;
@property (retain) ASIHTTPRequest *baseRequest;
@property (retain) NSMutableData *receivedData;

- (void) buildValidTokenBody:(NSString*)loginToken;

- (void) buildRegistrationBody:(NSString*)email firstName:(NSString*)firstName lastName:(NSString*)lastName password:(NSString*)password zipCode:(NSString*)zipCode;

- (void) buildLoginBody:(NSString*)email password:(NSString*)password;

- (void) buildLogoutBody:(NSString*)loginToken;

- (void) buildForgotPasswordBody:(NSString*)email;

- (void) buildGetClientConfigurationBody:(NSString*)loginToken;

- (void) buildSetClientConfigurationBody:(NSString*)loginToken scoreUpateOptionId:(NSString*)optionId;

- (void) buildGameScoreRefreshOptionsBody:(NSString*)loginToken;

- (void) buildSportBody:(NSString*)loginToken;

- (void) buildLeagueBody:(NSString*)loginToken sportId:(NSString *)sportId;

- (void) buildDivisionBody:(NSString*)loginToken leagueId:(NSString*)leagueId;

- (void) buildSearchTeamBody:(NSString*)loginToken sportId:(NSString*)sportId leagueId:(NSString*)leagueId divisionId:(NSString*)divisionId;

- (void) buildSearchGameBody:(NSString*)loginToken scheduledFromDate:(NSDate*)scheduledFromDate scheduledToDate:(NSDate*)scheduledToDate sportId:(NSString*)sportId teamId:(NSString*)teamId stadiumId:(NSString*)stadiumId;

- (void) buildGetGameStatusBody:(NSString *)loginToken listGameId:(NSArray*)listGameId;

- (void) buildPostShoutBody:(NSString*)loginToken gameId:(NSString*) gameId message:(NSString*)message;

- (void) buildGetScoreBody:(NSString*)loginToken listGameId:(NSArray*)listGameId;

- (void) buildUpdateScoreBody:(NSString*)loginToken game:(Game*)game endGame:(NSString*)endGame;

- (void) buildRequestScoreBody:(NSString*)loginToken gameId:(NSString*)gameId;

- (void) buildCheckInGameBody:(NSString*)loginToken gameId:(NSString*)gameId;

- (void) buildCheckOutGameBody:(NSString*)loginToken gameId:(NSString*)gameId;

- (void) buildSubscribeDeviceBody:(NSString*)loginToken deviceToken:(NSString*)deviceToken;

- (void) buildSubscribeGameBody:(NSString *)loginToken listGameId:(NSArray*)listGameId;

- (void) buildUnSubscribeGameBody:(NSString *)loginToken listGameId:(NSArray*)listGameId;

// NEW METHODS --------
- (void) buildSubscribeTeamBody:(NSString *)loginToken listTeamId:(NSArray*)listTeamId;
- (void) buildUnSubscribeTeamBody:(NSString *)loginToken listTeamId:(NSArray*)listTeamId;

- (void) buildGetSubscribedTeamsCountBody:(NSString*)loginToken;
- (void) buildGetSubscribedGamesCountBody:(NSString*)loginToken;

- (void) buildGetSubscribedTeamsBody:(NSString *)loginToken pageIndex:(int)pageIndex;
- (void) buildGetSubscribedGamesBody:(NSString *)loginToken pageIndex:(int)pageIndex;

- (void) buildGetNewGamesBody:(NSString *)loginToken listTeamId:(NSArray*)listTeamId pageIndex:(int)pageIndex;

@end


