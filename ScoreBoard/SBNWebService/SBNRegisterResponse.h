
#import "SBNBaseResponse.h"

@interface SBNRegisterResponse : SBNBaseResponse

- (void) parseResponseData:(NSString*) responseData;

@end
