//
//  BaseRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//

#import "BaseResponse.h"
#import "NSDate+Extensions.h"
@class BaseRequest;

@protocol ServiceRequestDelegate
-(void)requestDidFinish:(BaseRequest*)request withResult:(id)result withError:(NSError*)error;
@end

typedef void (^RequestCompletionBlock)(BaseRequest* request, id result, NSError *error);

@interface BaseRequest : NSObject

@property (nonatomic, assign) id<ServiceRequestDelegate> delegate;

@property (nonatomic, strong) void (^requestCompletionBlock)(BaseRequest* request, id result, NSError *error);

@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* uniqueId;

@property (nonatomic, strong) NSHTTPURLResponse* response;
@property (nonatomic, strong) NSMutableData *receivedData;

-(void)post:(NSString*)path body:(NSDictionary*)body withCallback:(RequestCompletionBlock)callback;
-(void)put:(NSString*)path body:(NSDictionary*)body withCallback:(RequestCompletionBlock)callback;
-(void)get:(NSString*)path withCallback:(RequestCompletionBlock)callback;

-(void)post:(NSString*)path body:(NSDictionary*)body withDelegate:(id<ServiceRequestDelegate>)delegate;
-(void)put:(NSString*)path body:(NSDictionary*)body withDelegate:(id<ServiceRequestDelegate>)delegate;
-(void)get:(NSString*)path withDelegate:(id<ServiceRequestDelegate>)delegate;


-(BOOL)handleHttpError:(NSInteger)code;
-(BOOL)handleHttpOK:(NSDictionary*)json;

-(void)cancel;

// build request body for post & put request
- (NSMutableDictionary*)buildRequestBody;

// parse json response to object, used to pass to callback
- (id)parseResponse:(NSDictionary *)json;

@end

