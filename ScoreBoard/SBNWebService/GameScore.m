
#import "GameScore.h"

@implementation GameScore
@synthesize gameId, team1Score, team2Score, qtr, lastUpdated, hours, minutes, seconds, numberCheckedInUser;
@synthesize outs, balls, strikes, halfTime, isTop, onPossition1, onPossition2, onPossition3;

- (id) initWithString:(NSString *)source {
    self = [super init];
    if (self) {
        self.gameId = [CommonFunction getInnerTextInSource:source byTagName:@"GameId"];
        self.team1Score = [CommonFunction getInnerTextInSource:source byTagName:@"Team1Score"];
        self.team2Score = [CommonFunction getInnerTextInSource:source byTagName:@"Team2Score"];
        self.qtr = [CommonFunction getInnerTextInSource:source byTagName:@"QTR"];
        
        NSString *text = [CommonFunction getInnerTextInSource:source byTagName:@"UpdatedOnUtc"];
        if (text.length > 0)
            self.lastUpdated = [CommonFunction convertStringToDate:text];
        
        self.hours = [CommonFunction getInnerTextInSource:source byTagName:@"Hours"];
        self.minutes = [CommonFunction getInnerTextInSource:source byTagName:@"Minutes"];
        self.seconds = [CommonFunction getInnerTextInSource:source byTagName:@"Seconds"];
        
        self.numberCheckedInUser = [CommonFunction getInnerTextInSource:source byTagName:@"CheckedInUsersCount"];
        
        self.outs = [CommonFunction getInnerTextInSource:source byTagName:@"Outs"];
        self.balls = [CommonFunction getInnerTextInSource:source byTagName:@"Balls"];
        self.strikes = [CommonFunction getInnerTextInSource:source byTagName:@"Strikes"];
        
        text = [CommonFunction getInnerTextInSource:source byTagName:@"HalfTime"];
        if (text.length > 0)
            self.halfTime = text.boolValue;
        
        text = [CommonFunction getInnerTextInSource:source byTagName:@"IsTop"];
        if (text.length > 0)
            self.isTop = text.boolValue;
        
        text = [CommonFunction getInnerTextInSource:source byTagName:@"OnPossition1"];
        if (text.length > 0)
            self.onPossition1 = text.boolValue;
        
        text = [CommonFunction getInnerTextInSource:source byTagName:@"OnPossition2"];
        if (text.length > 0)
            self.onPossition2 = text.boolValue;
        
        text = [CommonFunction getInnerTextInSource:source byTagName:@"OnPossition3"];
        if (text.length > 0)
            self.onPossition3 = text.boolValue;
        
        // this will cause crash due to double free
        //[text release];
    }
    return self;

}

- (void) dealloc{
    [gameId release];
    [team1Score release];
    [team2Score release];
    [qtr release];
    [lastUpdated release];
    [hours release];
    [minutes release];
    [seconds release];
    [numberCheckedInUser release];
    
    [outs release];
    [balls release];
    [strikes release];
    [super dealloc];
}

@end
