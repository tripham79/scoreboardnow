
#import "GameScoreRefreshOption.h"

@implementation GameScoreRefreshOption
@synthesize optionId, optionName, optionSeconds;

- (id) initWithString:(NSString *)source{
    self = [super init];
    if (self){
        self.optionId = [CommonFunction getInnerTextInSource:source byTagName:@"Id"];
        self.optionName = [CommonFunction getInnerTextInSource:source byTagName:@"Name"];
        self.optionSeconds = [CommonFunction getInnerTextInSource:source byTagName:@"Seconds"];
    }
    return self;
}

- (void) dealloc{
    [optionId release];
    [optionName release];
    [optionSeconds release];
    [super dealloc];
}
@end
