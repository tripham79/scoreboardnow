//
//  SBNService.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

//#import "BaseRequest.h"
#import "BaseResponse.h"
#import "SearchGameRequest.h"
@class Game;

@interface JsonRequest : NSObject
- (NSMutableDictionary*)toDictionary;
@end

@interface AuthenticatedJsonRequest : JsonRequest
@property (strong, nonatomic) NSString* LoginToken;
- (NSMutableDictionary*)toDictionary;
@end

@interface SuggestNewSportRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSString* SportName;
@property (strong, nonatomic) NSString* Comment;
@end

@interface GetGameStatusResult : BaseResponse
@property (strong, nonatomic) NSArray* games;
@end

@interface CreateLeagueRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* SportId;
@property (strong, nonatomic) NSString* LeagueName;
@property (strong, nonatomic) NSNumber* CountryId; // zero for USA as default.
@end

@interface CreateLeagueResult : BaseResponse
@property (strong, nonatomic) NSNumber* LeagueId;
@end

@interface ReportLeagueErrorRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* LeagueId;
@property (strong, nonatomic) NSString* Comment;
@end

@interface CreateDivisionRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* LeagueId;
@property (strong, nonatomic) NSString* DivisionName;
@end

@interface CreateDivisionResult : BaseResponse
@property (strong, nonatomic) NSNumber* DivisionId;
@end


@interface ReportDivisionErrorRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* DivisionId;
@property (strong, nonatomic) NSString* Comment;
@end

@interface CreateTeamRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* SportId;
@property (strong, nonatomic) NSNumber* LeagueId;
@property (strong, nonatomic) NSNumber* DivisionId;
@property (strong, nonatomic) NSString* TeamName;
@property (strong, nonatomic) NSNumber* CountryId; // zero for USA as default.
@property (strong, nonatomic) NSNumber* StateId;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSNumber* StadiumId;
@end

@interface CreateTeamResult : BaseResponse
@property (strong, nonatomic) NSNumber* TeamId;
@property (strong, nonatomic) NSString* TeamNumber;
@end


@interface ReportTeamErrorRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* TeamId;
@property (strong, nonatomic) NSString* Comment;
@end

@interface CreateStadiumRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSString* StadiumName;
@property (strong, nonatomic) NSString* Address;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* CountryCode;
@property (strong, nonatomic) NSNumber* Lat;
@property (strong, nonatomic) NSNumber* Lng;
@end

@interface CreateStadiumResult : BaseResponse
@property (strong, nonatomic) NSNumber* StadiumId;
@end

@interface ReportStadiumErrorRequest : AuthenticatedJsonRequest
@property (strong, nonatomic) NSNumber* StadiumId;
@property (strong, nonatomic) NSString* Comment;
@end

@interface SBNService : NSObject

+ (SBNService*)sharedInstance;

+ (void)downloadImageWithURLString:(NSString *)urlString completed:(void (^)(NSData *imageData))completed;

- (void)getGameStatus:(NSArray*)listGameId successHandler:(void (^)(GetGameStatusResult*))successHandler failHandler:(void (^)(NSError*))failHandler;

- (void)getMyCreatedGames:(int)fromIndex maxRecords:(int)maxRecords withCallback:(RequestCompletionBlock)callback;

- (void)addGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback;
- (void)updateGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback;
- (void)deleteGame:(int)gameId withCallback:(RequestCompletionBlock)callback;

- (void)searchTeamsWithStadium:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText sportId:(int)sportId withCallback:(RequestCompletionBlock)callback;
- (void)searchStadiums:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText withCallback:(RequestCompletionBlock)callback;

- (void)searchGames:(SearchGameRequest*)request withCallback:(RequestCompletionBlock)callback;
- (void)subscribeGames:(NSMutableArray*)arrGameIds withCallback:(RequestCompletionBlock)callback;

// Meta data methods
- (void)suggestNewSport:(SuggestNewSportRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)createLeague:(CreateLeagueRequest*)request successHandler:(void (^)(CreateLeagueResult*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)createDivision:(CreateDivisionRequest*)request successHandler:(void (^)(CreateDivisionResult*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)createTeam:(CreateTeamRequest*)request successHandler:(void (^)(CreateTeamResult*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)createStadium:(CreateStadiumRequest*)request successHandler:(void (^)(CreateStadiumResult*))successHandler failHandler:(void (^)(NSError*))failHandler;

- (void)reportLeagueError:(ReportLeagueErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)reportDivisionError:(ReportDivisionErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)reportTeamError:(ReportTeamErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler;
- (void)reportStadiumError:(ReportStadiumErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler;

// sync teams and games
- (void)syncData:(AuthenticatedJsonRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler;


@end
