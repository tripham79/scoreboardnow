
#import "Division.h"

@implementation Division
@synthesize divisionID, divisionName, leagueId;

- (id) initWithString:(NSString *)source{
    self = [super init];
    if (self)
    {
        self.divisionID = [CommonFunction getInnerTextInSource:source byTagName:@"Id"];
        self.divisionName = [CommonFunction getInnerTextInSource:source byTagName:@"Name"];
        self.leagueId = [CommonFunction getInnerTextInSource:source byTagName:@"LeagueId"];
    }
    return self;
}

- (void) dealloc{
    [divisionID release];
    [divisionName release];
    [leagueId release];
    [super dealloc];
}
@end
