//
//  BaseResponse.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseResponse.h"
#import "NSDictionary+Extensions.h"

@implementation BaseResponse

- (id)initWithDictionary:(NSDictionary*)dic {
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"%@ Miss Key: %@", NSStringFromClass(self.class), key);
}

@end

