//
//  Stadium.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "Stadium.h"
#import "NSDictionary+Extensions.h"

@implementation Stadium

- (id) initWithDictionary:(NSDictionary*)dict {
    self = [super init];
    if (self){
        if (dict != nil &&  (id)dict != [NSNull null]) {
            self.Id = [dict stringForKey:@"Id"];
            self.Name = [dict stringForKey:@"Name"];
            self.Address = [dict stringForKey:@"Address"];
            self.City = [dict stringForKey:@"City"];
            self.Lat = [dict objectForKey:@"Lat"];
            self.Lng = [dict objectForKey:@"Lng"];
        }
    }
    return self;
}

@end
