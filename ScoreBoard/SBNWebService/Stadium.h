//
//  Stadium.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stadium : NSObject

@property (retain) NSString *Id;
@property (retain) NSString *Name;
@property (retain) NSString *Address;
@property (retain) NSString *City;
@property (retain) NSString *Country;
@property (retain) NSString *CountryCode;
@property (retain) NSNumber *Lat;
@property (retain) NSNumber *Lng;

- (id) initWithDictionary:(NSDictionary*)dict;


@end
