//
//  SBNService.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SBNService.h"
#import "Config.h"
#import "NSDictionary+Extensions.h"
#import "NSString+Extensions.h"

#import "SharedComponent.h"
#import "GetMyCreatedGameRequest.h"
#import "CreateGameRequest.h"
#import "DeleteGameRequest.h"
#import "SearchTeamRequest.h"
#import "SearchStadiumRequest.h"
#import "SubscribeGamesRequest.h"

#import "Team.h"
#import "Game.h"

@implementation SBNService

+ (SBNService*)sharedInstance {
    //  Static local predicate must be initialized to 0
    static SBNService *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        if (sharedInstance == nil){
            sharedInstance = [[SBNService alloc] init];
            // Do any other initialisation stuff here
        }
    });
    return sharedInstance;
}

static NSOperationQueue* requestQueue;
+ (NSOperationQueue*)getRequestQueue {
    if (requestQueue == nil)
        requestQueue = [[NSOperationQueue alloc] init];
    return requestQueue;
}

+ (void)cancelCurrentRequests {
    [[SBNService getRequestQueue] cancelAllOperations];
}

+ (void)downloadImageWithURLString:(NSString *)urlString completed:(void (^)(NSData *imageData))completed
{
    NSURL *url = [NSURL URLWithString:[urlString urlEncodeDefault]];
    //dispatch_queue_t downloadQueue = dispatch_queue_create("com.refresh.processsmagequeue", NULL);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       NSData * imageData = [NSData dataWithContentsOfURL:url];
                       dispatch_async(dispatch_get_main_queue(), ^{
                           completed(imageData);
                       });
                   });
    //dispatch_release(downloadQueue);
}

- (void)getMyCreatedGames:(int)fromIndex maxRecords:(int)maxRecords withCallback:(RequestCompletionBlock)callback
{
    GetMyCreatedGameRequest *request = [[GetMyCreatedGameRequest alloc] init];
    request.loginToken = [SharedComponent getInstance].loginToken;
    request.fromIndex = fromIndex;
    request.maxRecords = maxRecords;
    [request post:@"GetMyCreatedGames" body:[request buildRequestBody] withCallback:callback];
}

- (void)addGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback {
    CreateGameRequest* request = [[CreateGameRequest alloc] init];
    request.ConfirmedCreate = confirmedDuplicated;
    request.game = game;
    [request post:@"CreateGame" body:[request buildRequestBody] withCallback:callback];
}

- (void)updateGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback {
    CreateGameRequest* request = [[CreateGameRequest alloc] init];
    request.ConfirmedCreate = confirmedDuplicated;
    request.game = game;
    [request post:@"UpdateGame" body:[request buildRequestBody] withCallback:callback];
}

- (void)deleteGame:(int)gameId withCallback:(RequestCompletionBlock)callback {
    DeleteGameRequest* request = [[DeleteGameRequest alloc] init];
    request.gameId = gameId;
    [request post:@"DeleteGame" body:[request buildRequestBody] withCallback:callback];
}

- (void)searchTeamsWithStadium:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText sportId:(int)sportId withCallback:(RequestCompletionBlock)callback
{
    SearchTeamRequest* request = [[SearchTeamRequest alloc] init];
    request.PageIndex = pageIndex;
    request.PageSize = pageSize;
    request.SearchTerms = searchText;
    request.SportId = sportId;
    [request post:@"SearchTeamsWithStadium" body:[request buildRequestBody] withCallback:callback];
}

- (void)searchStadiums:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText withCallback:(RequestCompletionBlock)callback
{
    SearchStadiumRequest* request = [[SearchStadiumRequest alloc] init];
    request.PageIndex = pageIndex;
    request.PageSize = pageSize;
    request.SearchTerms = searchText;
    [request post:@"SearchStadiums" body:[request buildRequestBody] withCallback:callback];
}

- (void)subscribeGames:(NSMutableArray*)arrGameIds withCallback:(RequestCompletionBlock)callback {
    SubscribeGamesRequest* request = [[SubscribeGamesRequest alloc] init];
    request.ListGameId = arrGameIds;
    [request post:@"SubscribeGamesJson" body:[request buildRequestBody] withCallback:callback];
}

- (void)searchGames:(SearchGameRequest*)request withCallback:(RequestCompletionBlock)callback {
    [request post:@"SearchGames" body:[request buildRequestBody] withCallback:callback];
}

- (void)suggestNewSport:(SuggestNewSportRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"SuggestNewSport" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             BaseResponse* result = [[BaseResponse alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)getGameStatus:(NSArray*)listGameId successHandler:(void (^)(GetGameStatusResult*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: [SharedComponent getInstance].loginToken, @"LoginToken", listGameId, @"ListGameId", nil];
    NSMutableURLRequest* urlRequest = [self createRequest:@"GetGameStatuses" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             GetGameStatusResult* result = [[GetGameStatusResult alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)createLeague:(CreateLeagueRequest*)request successHandler:(void (^)(CreateLeagueResult*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"CreateLeague" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             CreateLeagueResult* result = [[CreateLeagueResult alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)createDivision:(CreateDivisionRequest*)request successHandler:(void (^)(CreateDivisionResult*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"CreateDivision" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             CreateDivisionResult* result = [[CreateDivisionResult alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)createTeam:(CreateTeamRequest*)request successHandler:(void (^)(CreateTeamResult*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"CreateTeam" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             CreateTeamResult* result = [[CreateTeamResult alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)createStadium:(CreateStadiumRequest*)request successHandler:(void (^)(CreateStadiumResult*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"CreateStadium" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             CreateStadiumResult* result = [[CreateStadiumResult alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)reportLeagueError:(ReportLeagueErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"ReportLeagueError" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             BaseResponse* result = [[BaseResponse alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];

}
- (void)reportDivisionError:(ReportDivisionErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"ReportDivisionError" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             BaseResponse* result = [[BaseResponse alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}
- (void)reportTeamError:(ReportTeamErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"ReportTeamError" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             BaseResponse* result = [[BaseResponse alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)reportStadiumError:(ReportStadiumErrorRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [request toDictionary];
    NSMutableURLRequest* urlRequest = [self createRequest:@"ReportStadiumError" data:dict];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:[SBNService getRequestQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error == nil) {
             [self logResponse:data];
             NSError* err;
             NSDictionary* dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:data options: kNilOptions error:&err];
             
             BaseResponse* result = [[BaseResponse alloc] initWithDictionary:dict];
             dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
         }
         else
             dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
     }];
}

- (void)syncData:(AuthenticatedJsonRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
    ^{
        BOOL success = [self syncTeams:request successHandler:^(BaseResponse *result) {
            if (![result.IsSuccess boolValue])
                dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
        } failHandler:failHandler];
        
        if (!success) return;
        
        success = [self syncGames:request successHandler:^(BaseResponse *result) {
            if (![result.IsSuccess boolValue])
                dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
        } failHandler:failHandler];
        
        BaseResponse* result = [[BaseResponse alloc] init];
        result.IsSuccess = [NSNumber numberWithBool:YES];
        dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
    });
}

- (BOOL)syncTeams:(AuthenticatedJsonRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:request.LoginToken, @"LoginToken", nil];
    NSMutableURLRequest* urlRequest = [self createRequest:@"GetSubscribedTeamsCount" data:dict];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if ([data length] > 0 && error == nil) {
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        
        BaseResponse* result = [[BaseResponse alloc] initWithDictionary:json];
        // failed
        if (![result.IsSuccess boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
            return NO;
        }
        
        // success, download all subscribed teams
        int serverTeamsCount = [[json valueForKey:@"TotalRecords"] intValue];
        int teamCount = [Team getTeamsCount];
        if (serverTeamsCount > teamCount)
        {
            int pageIndex = 0;
            int pageSize = 20;
            int count = 0, downloadedTotal = 0;
            
            do {
                count = [self downloadTeams:request.LoginToken pageIndex:pageIndex pageSize:pageSize successHandler:successHandler failHandler:failHandler];
                downloadedTotal += count;
                pageIndex += 1;
            }
            while (count > 0 && downloadedTotal < serverTeamsCount);
        }
    }
    else if (error != nil) {
        // network error
        dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
        return NO;
    }
    return YES;
}

- (int)downloadTeams:(NSString*)loginToken pageIndex:(int)pageIndex pageSize:(int)pageSize successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:loginToken, @"LoginToken",
            [NSNumber numberWithInt:pageIndex], @"PageIndex",
            [NSNumber numberWithInt:pageSize], @"PageSize", nil];
    NSMutableURLRequest* urlRequest = [self createRequest:@"GetSubscribedTeams" data:dict];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if ([data length] > 0 && error == nil) {
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        
        BaseResponse* result = [[BaseResponse alloc] initWithDictionary:json];
        if (![result.IsSuccess boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
            return 0;
        }
        
        // insert teams to database if not exist
        NSArray* arrTeams = [json valueForKey:@"Teams"];
        for (NSDictionary* d in arrTeams) {
            Team* team = [[Team alloc] initWithDictionary:d];
            if (![Team hasTeam:team.teamID])
                [team insertDatabase];
        }
        return arrTeams.count;
    }
    else if (error != nil) {
        // network error
        dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
        return 0;
    }
    return 0;
}

- (BOOL)syncGames:(AuthenticatedJsonRequest*)request successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:request.LoginToken, @"LoginToken", nil];
    NSMutableURLRequest* urlRequest = [self createRequest:@"GetSubscribedGamesCount" data:dict];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if ([data length] > 0 && error == nil) {
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        
        BaseResponse* result = [[BaseResponse alloc] initWithDictionary:json];
        // failed
        if (![result.IsSuccess boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
            return NO;
        }
        
        // success, download all subscribed games
        int serverGamesCount = [[json valueForKey:@"TotalRecords"] intValue];
        int gameCount = [Game getGamesCount];
        if (serverGamesCount > gameCount)
        {
            int pageIndex = 0;
            int pageSize = 20;
            int count = 0, downloadedTotal = 0;
            
            do {
                count = [self downloadGames:request.LoginToken pageIndex:pageIndex pageSize:pageSize successHandler:successHandler failHandler:failHandler];
                downloadedTotal += count;
                pageIndex += 1;
            }
            while (count > 0 && downloadedTotal < serverGamesCount);
        }
    }
    else if (error != nil) {
        // network error
        dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
        return NO;
    }
    return YES;
}

- (int)downloadGames:(NSString*)loginToken pageIndex:(int)pageIndex pageSize:(int)pageSize successHandler:(void (^)(BaseResponse*))successHandler failHandler:(void (^)(NSError*))failHandler
{
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:loginToken, @"LoginToken",
                       [NSNumber numberWithInt:pageIndex], @"PageIndex",
                       [NSNumber numberWithInt:pageSize], @"PageSize", nil];
    NSMutableURLRequest* urlRequest = [self createRequest:@"GetSubscribedGames" data:dict];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if ([data length] > 0 && error == nil) {
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        
        BaseResponse* result = [[BaseResponse alloc] initWithDictionary:json];
        if (![result.IsSuccess boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{ successHandler(result); });
            return 0;
        }
        
        // insert games to database if not exist
        NSArray* arrGames = [json valueForKey:@"Games"];
        for (NSDictionary* d in arrGames) {
            Game* game = [[Game alloc] initWithDictionary:d];
            if (![Game hasGame:game.gameID])
                [game insertDatabase];
        }
        return arrGames.count;
    }
    else if (error != nil) {
        // network error
        dispatch_async(dispatch_get_main_queue(), ^{ failHandler(error); });
        return 0;
    }
    return 0;
}

// helper methods
- (NSMutableURLRequest*)createRequest:(NSString*)method data:(NSDictionary*)data {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", SBNEnpoint, method]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url ];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setTimeoutInterval:60];
    
    NSError *err;
    NSData* bodyData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&err];
    
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setHTTPBody: bodyData];
    
    return urlRequest;
}

- (void)logResponse:(NSData*)data {
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", jsonString);
}

@end


// Request & Response
@implementation JsonRequest

- (NSMutableDictionary *)toDictionary {
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    return dict;
}
@end

@implementation AuthenticatedJsonRequest

- (id) init {
    self = [super init];
    if (self) {
        self.LoginToken = [SharedComponent getInstance].loginToken;
    }
    return  self;
}

- (NSMutableDictionary *)toDictionary {    
    NSMutableDictionary* dict = [super toDictionary];
    [dict setObject:self.LoginToken forKey:@"LoginToken"];
    [dict addEntriesFromDictionary:[NSDictionary dictionaryWithPropertiesOfObject:self]];
    return dict;
}

@end


@implementation SuggestNewSportRequest
@end

@implementation GetGameStatusResult

- (id)initWithDictionary:(NSDictionary *)dic {
    [super initWithDictionary:dic];
    
    if ([self.IsSuccess boolValue]) {
        NSArray* arr = [dic valueForKey:@"Games"];
        NSMutableArray* arrGames = [[NSMutableArray alloc] init];
        for (NSDictionary* obj in arr) {
            Game* g = [[Game alloc] initWithDictionary:obj];
            [arrGames addObject:g];
        }
        self.games = arrGames;
    }
    return self;
}

@end

@implementation CreateLeagueRequest
@end

@implementation CreateLeagueResult
@end

@implementation ReportLeagueErrorRequest
@end

@implementation CreateDivisionRequest
@end

@implementation CreateDivisionResult
@end

@implementation ReportDivisionErrorRequest
@end

@implementation CreateTeamRequest
@end

@implementation CreateTeamResult
@end

@implementation ReportTeamErrorRequest
@end

@implementation CreateStadiumRequest
@end

@implementation CreateStadiumResult
@end

@implementation ReportStadiumErrorRequest
@end




