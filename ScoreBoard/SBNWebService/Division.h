
#import <Foundation/Foundation.h>
#import "CommonFunction.h"

@interface Division : NSObject

- (id) initWithString: (NSString*) source;

@property (retain) NSString *divisionID;
@property (retain) NSString *divisionName;
@property (retain) NSString *leagueId;

@end
