
#import "SBNResponse.h"

@implementation SBNResponse
@synthesize responseCode, errorMessage;

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
}

- (void) dealloc{
    [responseCode release];
    [errorMessage release];
    [super dealloc];
}
@end

#pragma SBNValidTokenResponse
@implementation SBNValidTokenResponse
@end

#pragma SBNRegisterResponse
@implementation SBNRegisterResponse

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
}

@end

#pragma SBNLoginResponse
@implementation SBNLoginResponse
@synthesize loginToken, canCreateGame;

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
    self.loginToken = [CommonFunction getInnerTextInSource:responseData byTagName: tagLoginToken];
    self.canCreateGame = [CommonFunction getInnerTextInSource:responseData byTagName: @"CanCreateGame"];
}

- (void) dealloc{
    [loginToken release];
    [super dealloc];
}
@end

#pragma SBNLogoutResponse
@implementation SBNLogoutResponse
@end

#pragma SBNForgotPasswordResponse
@implementation SBNForgotPasswordResponse
@end

#pragma SBNGetClientConfigurationResponse
@implementation SBNGetClientConfigurationResponse
@synthesize refreshSeconds;
- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    self.refreshSeconds = [CommonFunction getInnerTextInSource:responseData byTagName:@"a:RefreshSeconds"];
}
@end

#pragma SBNSetClientConfigurationResponse
@implementation SBNSetClientConfigurationResponse
@end

#pragma SBNGameScoreRefreshOptions
@implementation SBNGameScoreRefreshOptions
@synthesize listGameScoreRefreshOption;

- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    
    self.listGameScoreRefreshOption = [[NSMutableArray alloc] init];
    NSMutableArray *listGameScoreRefreshOptionString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"GameScoreRefreshOption"];
    
    for (NSString *optionString in listGameScoreRefreshOptionString){
        GameScoreRefreshOption *option = [[GameScoreRefreshOption alloc] initWithString:optionString];
        [self.listGameScoreRefreshOption addObject:option];
        [option release];
    }
}

- (void) dealloc{
    [listGameScoreRefreshOption release];
    [super dealloc];
}

@end

#pragma SBNSportResponse
@implementation SBNSportResponse
@synthesize listSport;

- (void) parseResponseData:(NSString*) responseData{
    [super parseResponseData:responseData];
    
    self.listSport = [[NSMutableArray alloc] init];
    
    NSMutableArray *listSportString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Sport"];
    
    for (NSString *sportString in listSportString){
        Sport *sport = [[Sport alloc] initWithString:sportString];
        [self.listSport addObject:sport];
        [sport release];
    }
}

- (void) dealloc{
    [listSport release];
    [super dealloc];
}
@end

#pragma SBNLeagueResponse
@implementation SBNLeagueResponse
@synthesize listLeague;

- (void) parseResponseData:(NSString*) responseData{
    [super parseResponseData:responseData];
    
    self.listLeague = [[NSMutableArray alloc] init];
    
    NSMutableArray *listLeagueString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"League"];
    
    for (NSString *leagueString in listLeagueString){
        League *league = [[League alloc] initWithString:leagueString];
        [self.listLeague addObject:league];
        [league release];
    }
}

- (void) dealloc{
    [listLeague release];
    [super dealloc];
}
@end

#pragma SBNDivisionResponse
@implementation SBNDivisionResponse
@synthesize listDivision;

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
    
    self.listDivision = [[NSMutableArray alloc] init];
    
    NSMutableArray *listDivisionString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Division"];
    
    for (NSString *divisionString in listDivisionString){
        Division *division = [[Division alloc] initWithString:divisionString];
        [self.listDivision addObject:division];
        [division release];
    }
}

- (void) dealloc{
    [listDivision release];
    [super dealloc];
}
@end

#pragma SBNSearchTeamResponse
@implementation SBNSearchTeamResponse
@synthesize listTeam;

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
    
    self.listTeam = [[NSMutableArray alloc] init];
    
    NSMutableArray *listTeamString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Team"];
    
    for (NSString *teamString in listTeamString){
        Team *team = [[Team alloc] initWithString:teamString];
        [self.listTeam addObject:team];
        [team release];
    }
}

- (void) dealloc{
    [listTeam release];
    [super dealloc];
}
@end

#pragma SBNSearchGameResponse
@implementation SBNSearchGameResponse
@synthesize listGame;

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
    
    self.listGame = [[NSMutableArray alloc] init];
    
    NSMutableArray *listGameString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Game"];
    
    for (NSString *gameString in listGameString){
        Game *game = [[Game alloc] initWithString:gameString];
        [self.listGame addObject:game];
        [game release];
    }
}

- (void) dealloc{
    [listGame release];
    [super dealloc];
}
@end

#pragma SBNGetGameStatusResponse
@implementation SBNGetGameStatusResponse
@synthesize listGameStatus;

- (void) parseResponseData:(NSString*) responseData{
    self.responseCode = [CommonFunction getInnerTextInSource:responseData byTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextInSource:responseData byTagName: tagErrorMessage];
    
    self.listGameStatus = [[NSMutableArray alloc] init];
    
    NSMutableArray *listGameString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Game"];
    
    for (NSString *gameString in listGameString){
        Game *game = [[Game alloc] initWithString:gameString];
        [self.listGameStatus addObject:game];
        [game release];
    }
}

- (void) dealloc{
    [listGameStatus release];
    [super dealloc];
}
@end

#pragma SBNPostShoutResponse
@implementation SBNPostShoutResponse

@end

#pragma SBNGameScoreResponse
@implementation SBNGameScoreResponse
@synthesize listGameScore;

- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    
    self.listGameScore = [[NSMutableArray alloc] init];
    
    NSMutableArray *listGameScoreString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"GameScore"];

    for (NSString *gameScoreString in listGameScoreString){
        GameScore *gameScore = [[GameScore alloc] initWithString:gameScoreString];
        [self.listGameScore addObject:gameScore];
        //[gameScore release];
    }
}

- (void) dealloc{
    //[listGameScore release];
    [super dealloc];
}
@end

#pragma SBNSetGameScoreResponse
@implementation SBNSetGameScoreResponse

@end

#pragma SBNRequestGameScoreResponse
@implementation SBNRequestGameScoreResponse

@end

#pragma SBNCheckInGameResponse
@implementation SBNCheckInGameResponse 
@end

#pragma SBNCheckOutGameResponse
@implementation SBNCheckOutGameResponse 
@end

#pragma SBNSubscribeDeviceResponse
@implementation SBNSubscribeDeviceResponse 

@end

#pragma SBNSubscribeGameResponse
@implementation SBNSubscribeGameResponse 
@end

#pragma SBNUnSubscribeGameResponse
@implementation SBNUnSubscribeGameResponse 
@end

// NEW METHODS -----------
@implementation SBNSubscribeTeamResponse
@end

@implementation SBNUnSubscribeTeamResponse
@end

@implementation SBNGetSubscribedTeamsCountResponse : SBNResponse
- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    
    NSString* total = [CommonFunction getInnerTextInSource:responseData byTagName: @"TotalRecords"];
    if (total != nil && ![total isEqualToString:@""])
        self.totalRecord = total.intValue;
}
@end

@implementation SBNGetSubscribedGamesCountResponse : SBNResponse
- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    
    NSString* total = [CommonFunction getInnerTextInSource:responseData byTagName: @"TotalRecords"];
    if (total != nil && ![total isEqualToString:@""])
        self.totalRecord = total.intValue;
}
@end

@implementation SBNGetSubscribedTeamsResponse
@synthesize listTeam;

- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    self.listTeam = [[NSMutableArray alloc] init];
    
    NSMutableArray *listTeamString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Team"];
    
    for (NSString *teamString in listTeamString){
        Team *team = [[Team alloc] initWithString:teamString];
        [self.listTeam addObject:team];
        [team release];
    }
    
    NSString* total = [CommonFunction getInnerTextInSource:responseData byTagName: @"TotalRecords"];
    if (total != nil && ![total isEqualToString:@""])
        self.totalRecord = total.intValue;
    
}

@end

@implementation SBNGetSubscribedGamesResponse
@synthesize listGame;

- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    self.listGame = [[NSMutableArray alloc] init];
    
    NSMutableArray *listGameString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Game"];
    
    for (NSString *gameString in listGameString){
        Game *game = [[Game alloc] initWithString:gameString];
        [self.listGame addObject:game];
        [game release];
    }
    
    NSString* total = [CommonFunction getInnerTextInSource:responseData byTagName: @"TotalRecords"];
    if (total != nil && ![total isEqualToString:@""])
        self.totalRecord = total.intValue;
    
}

@end

@implementation SBNGetNewGamesResponse
@synthesize listGame;

- (void) parseResponseData:(NSString *)responseData{
    [super parseResponseData:responseData];
    self.listGame = [[NSMutableArray alloc] init];
    
    NSMutableArray *listGameString = [CommonFunction getInnerTextsInSource:responseData byTagName:@"Game"];
    
    for (NSString *gameString in listGameString){
        Game *game = [[Game alloc] initWithString:gameString];
        [self.listGame addObject:game];
        [game release];
    }
    
    NSString* total = [CommonFunction getInnerTextInSource:responseData byTagName: @"TotalRecords"];
    if (total != nil && ![total isEqualToString:@""])
        self.totalRecord = total.intValue;
    
}

@end

