
#import "SBNRequest.h"
#import "Config.h"

#define kContentType @"Content-Type"
#define ContentType @"text/xml"

#define kHost @"Host" 
#define Host @"scoreboardnow.com"

#define kSoapAction @"SOAPAction"
#define RegisterAction @"http://tempuri.org/ISBNService/Register"
#define LoginAction @"http://tempuri.org/ISBNService/Login"

@implementation SBNRequest
@synthesize body = _body;
@synthesize baseRequest = _baseRequest;
@synthesize receivedData = _receivedData;

- (id) init{
    self = [super init];
    if (self){
        _receivedData = [[NSMutableData alloc] init];
    }
    return self;
}

- (void) dealloc{
    [_body release];
    [_baseRequest release];
    [_receivedData release];
    [super dealloc];
}

- (void) buildValidTokenBody:(NSString *)loginToken{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/IsTokenValid", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"IsTokenValidFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildRegistrationBody:(NSString *)email firstName:(NSString *)firstName lastName:(NSString *)lastName password:(NSString *)password zipCode:(NSString *)zipCode{
    
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/Register", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"RegistrationFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, email, firstName, lastName, password, zipCode];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildLoginBody:(NSString *)email password:(NSString *)password{
    
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/Login", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader:kContentType value:ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader:kHost value: Host];
    [_baseRequest setRequestMethod:@"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"LoginFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, email, password];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildLogoutBody:(NSString *)loginToken{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/Logout", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"LogoutFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildForgotPasswordBody:(NSString*)email{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/ForgotPassword", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ForgotPasswordFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, email];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGetClientConfigurationBody:(NSString*)loginToken{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetClientConfiguration", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GetClientConfigurationFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildSetClientConfigurationBody:(NSString*)loginToken scoreUpateOptionId:(NSString*)optionId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SetClientConfiguration", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SetClientConfigurationFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, optionId];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGameScoreRefreshOptionsBody:(NSString *)loginToken{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/ListGameScoreRefreshOptions", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ListGameScoreRefreshOptionsFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildSportBody:(NSString*)loginToken {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/ListSports", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ListSportFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildLeagueBody:(NSString *)loginToken sportId:(NSString *)sportId {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/ListLeagues", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"LeagueFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, sportId];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildDivisionBody:(NSString *)loginToken leagueId:(NSString *)leagueId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/ListDivisions", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DivisionFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, leagueId];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
}

- (void) buildSearchTeamBody:(NSString*)loginToken sportId:(NSString*)sportId leagueId:(NSString*)leagueId divisionId:(NSString*)divisionId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SearchTeams", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SearchTeamFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, divisionId, leagueId, sportId];
        NSLog(@"%@", self.body);
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildSearchGameBody:(NSString*)loginToken scheduledFromDate:(NSDate*)scheduledFromDate scheduledToDate:(NSDate*)scheduledToDate sportId:(NSString*)sportId teamId:(NSString*)teamId stadiumId:(NSString*)stadiumId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SearchGames", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SearchGameFormat" ofType:@"txt"];  
    if (filePath)
    {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        NSString *scheduledFromDateText = @"";
        NSString *scheduledToDateText = @"";
        if (scheduledFromDate!=nil)
            scheduledFromDateText = [CommonFunction convertDateToString:scheduledFromDate];
        if (scheduledToDate != nil)
            scheduledToDateText = [CommonFunction convertDateToString:scheduledToDate];
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, scheduledFromDateText, scheduledToDateText, sportId, teamId];
        NSLog(@"%@", self.body);
        
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGetGameStatusBody:(NSString *)loginToken listGameId:(NSArray*)listGameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetGameStatuses", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GetGameStatusFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *gameId in listGameId)
        {
            [params appendFormat:@"<a:int>%@</a:int>",gameId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params];
        NSLog(@"%@", self.body);
        
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildPostShoutBody:(NSString*)loginToken gameId:(NSString*) gameId message:(NSString*)message{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/Shout", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ShoutFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, gameId, message];
        NSLog(@"%@", self.body);
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGetScoreBody:(NSString *)loginToken listGameId:(NSArray *)listGameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetGameScores", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GetGameScoreFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *gameId in listGameId)
        {
            [params appendFormat:@"<a:int>%@</a:int>",gameId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params];
        NSLog(@"%@", self.body);
        
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildUpdateScoreBody:(NSString *)loginToken game:(Game*)game endGame:(NSString *)endGame{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SetGameScore", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SetGameScoreFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        NSString *halfTime = @"false";
        NSString *isTop = @"false";
        NSString *onPossition1 = @"false";
        NSString *onPossition2 = @"false";
        NSString *onPossition3 = @"false";
        
        if (game.halfTime)
            halfTime = @"true";
        if (game.isTop)
            isTop = @"true";
        if (game.onPossition1)
            onPossition1 = @"true";
        if (game.onPossition2)
            onPossition2 = @"true";
        if (game.onPossition3)
            onPossition3 = @"true";
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, game.balls, endGame, game.gameID, halfTime, isTop, game.minutes, onPossition1, onPossition2, onPossition3, game.outs, game.qtr, game.seconds, game.strikes, game.team1Score, game.team2Score];
                NSLog(@"%@", self.body);
        
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildRequestScoreBody:(NSString*)loginToken gameId:(NSString*)gameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/RequestGameScore", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"RequestGameScoreFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, gameId];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildCheckInGameBody:(NSString*)loginToken gameId:(NSString*)gameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/CheckInGame", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CheckInGameFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, gameId];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildCheckOutGameBody:(NSString*)loginToken gameId:(NSString*)gameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/CheckOutGame", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CheckOutGameFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, gameId];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildSubscribeDeviceBody:(NSString *)loginToken deviceToken:(NSString *)deviceToken{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SubscribeDevice", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SubscribeDeviceFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, deviceToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildSubscribeGameBody:(NSString *)loginToken listGameId:(NSArray *)listGameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SubscribeGames", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SubscribeGameFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *gameId in listGameId)
        {
            [params appendFormat:@"<a:int>%@</a:int>",gameId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildUnSubscribeGameBody:(NSString *)loginToken listGameId:(NSArray *)listGameId{
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/UnsubscribeGames", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UnSubscribeGameFormat" ofType:@"txt"];  
    if (filePath) {  
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil]; 
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *gameId in listGameId)
        {
            [params appendFormat:@"<a:int>%@</a:int>",gameId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

// NEW METHODS --------
- (void) buildSubscribeTeamBody:(NSString *)loginToken listTeamId:(NSArray*)listTeamId {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/SubscribeTeams", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SubscribeTeamFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *teamId in listTeamId)
        {
            [params appendFormat:@"<a:int>%@</a:int>", teamId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}
- (void) buildUnSubscribeTeamBody:(NSString *)loginToken listTeamId:(NSArray*)listTeamId {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/UnsubscribeTeams", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"UnSubscribeTeamFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *teamId in listTeamId)
        {
            [params appendFormat:@"<a:int>%@</a:int>", teamId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGetSubscribedTeamsCountBody:(NSString*)loginToken {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetSubscribedTeamsCount", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"BaseRequestFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}
- (void) buildGetSubscribedGamesCountBody:(NSString*)loginToken {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetSubscribedGamesCount", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"BaseRequestFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGetSubscribedTeamsBody:(NSString *)loginToken pageIndex:(int)pageIndex {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetSubscribedTeams", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GetSubscribedTeamFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, pageIndex];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}
- (void) buildGetSubscribedGamesBody:(NSString *)loginToken pageIndex:(int)pageIndex {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetSubscribedGames", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GetSubscribedGameFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, pageIndex];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

- (void) buildGetNewGamesBody:(NSString *)loginToken listTeamId:(NSArray*)listTeamId pageIndex:(int)pageIndex {
    _baseRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@/GetNewGames", SBNEnpoint]]];
    
    [_baseRequest addRequestHeader: kContentType value: ContentType];
    [_baseRequest addRequestHeader:@"Accept" value:ContentType];
    [_baseRequest addRequestHeader: kHost value: Host];
    [_baseRequest setRequestMethod: @"POST"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"GetNewGameFormat" ofType:@"txt"];
    if (filePath) {
        NSString *bodyFormat = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
        NSMutableString *params = [[NSMutableString alloc] init];
        for (NSString *teamId in listTeamId)
        {
            [params appendFormat:@"<a:int>%@</a:int>", teamId];
        }
        
        self.body = [NSString stringWithFormat: bodyFormat, loginToken, params, pageIndex];
        [_baseRequest appendPostData:[_body dataUsingEncoding:NSUTF8StringEncoding]];
    }
}

@end
