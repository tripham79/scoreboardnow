
#import <Foundation/Foundation.h>
#import "CommonFunction.h"

@interface League : NSObject

- (id) initWithString: (NSString*) source;

@property (retain) NSString *leagueId;
@property (retain) NSString *leagueName;
@property (retain) NSString *qtr;
@property (retain) NSString *qtrDuration;
@property (retain) NSString *sportId;

@end
