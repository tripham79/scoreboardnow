typedef enum GameStatus{
    PreStart = 1,
    Delayed,
    Process,
    Final,
} GameStatus;

typedef enum LocalGameStatus{
    CheckedIn = 1,
    End
}LocalGameStatus;

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "CommonFunction.h"
#import "GameScore.h"

@interface Game : NSObject

- (id) initFromDatabase:(sqlite3_stmt*)compiledStatement;
- (id) initWithString:(NSString*)source;
- (id) initWithDictionary:(NSDictionary*)dict;
- (NSDictionary*)toDictionary;

@property (retain) NSString *gameID;
@property (retain) NSString *gameNumber;
@property (retain) NSString *team1ID;
@property (retain) NSString *team1Name;
@property (retain) NSString *team2ID;
@property (retain) NSString *team2Name;
@property (retain) NSString *statusID;
@property (retain) NSString *statusName;
@property (retain) NSDate *scheduledStartTime;
@property (retain) NSDate *delayedStartTime;
@property (retain) NSString *duration;
@property (retain) NSDate *startTime;
@property (retain) NSString *sportID;
@property (retain) NSString *stadiumID;
@property (retain) NSString *stadiumName;

@property (retain) NSString *team1Score;
@property (retain) NSString *team2Score;
@property (retain) NSString *qtr;
@property (retain) NSDate *lastUpdated;

@property (retain) NSString *localStatusId;
@property (retain) NSString *user;

@property (retain) NSString *hours;
@property (retain) NSString *minutes;
@property (retain) NSString *seconds;

@property (retain) NSString *leagueId;
@property (retain) NSString *numberCheckedInUser;

@property (retain) NSString *outs;
@property (retain) NSString *balls;
@property (retain) NSString *strikes;

@property BOOL halfTime;
@property BOOL isTop;

@property BOOL onPossition1;
@property BOOL onPossition2;
@property BOOL onPossition3;

@property (retain) NSString *sportName;
@property (retain) NSString *leagueName;
@property (retain) NSString *divisionName;

@property BOOL addedManual;
@property BOOL isChecked;

- (void) updateScore:(GameScore*)score;
- (void) updateGameStatus:(Game*)gameStatus;

- (BOOL) insertDatabase;
- (BOOL) updateDatabase;
+ (BOOL) updateGameStatus:(NSString*)gameId statusId:(NSString*)statusId;

+ (BOOL) deleteDatabase:(NSString*)gameId;
+ (NSArray*) loadGames:(NSDate*)fromDate toDate:(NSDate*)toDate;
+ (NSMutableArray*) loadManuallyAddedGames;
+ (BOOL) hasGames;
+ (BOOL) hasGame:(NSString*)gameId;
+ (int) getGamesCount;

- (NSString*) buildInsertQuery;
- (NSString*) buildUpdateQuery;
@end
