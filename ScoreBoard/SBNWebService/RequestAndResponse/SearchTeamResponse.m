//
//  SearchTeamResponse.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SearchTeamResponse.h"
#import "Team.h"

@implementation SearchTeamResponse

-(id)initWithDictionary:(NSDictionary*)dic {
    self = [super initWithDictionary:dic];
    if (![self.IsSuccess boolValue])
        return self;
    
    self.totalRecords = [[dic objectForKey:@"TotalRecords"] intValue];
    self.teams = [[NSMutableArray alloc] init];
    
    NSArray* arr = [dic objectForKey:@"Teams"];
    for (NSDictionary* jsonDic in arr) {
        Team* team = [[Team alloc] initWithDictionary:jsonDic];
        [self.teams addObject:team];
    }
    return self;
}


@end
