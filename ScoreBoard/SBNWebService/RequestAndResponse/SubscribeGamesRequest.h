//
//  SubscribeGamesRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/31/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "AuthenticatedRequest.h"

@interface SubscribeGamesRequest : AuthenticatedRequest

// must be array of NSNumber
@property (nonatomic, strong) NSMutableArray* ListGameId;

@end
