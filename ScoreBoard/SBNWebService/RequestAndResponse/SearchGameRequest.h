//
//  SearchGameRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 11/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "AuthenticatedRequest.h"
#import "BaseResponse.h"

@interface SearchGameRequest : AuthenticatedRequest

@property int SportId;
@property int LeagueId;
@property int DivisionId;
@property int StatusId;
@property (strong, nonatomic) NSString* GameNumber;
@property int TeamId;
@property (strong, nonatomic) NSString* TeamNameOrNumber;
@property (strong, nonatomic) NSDate* ScheduledStartDateFromUtc;
@property (strong, nonatomic) NSDate* ScheduledStartDateToUtc;
@property int PageIndex;
@property int PageSize;
@property (strong, nonatomic) NSDate* UpdatedOnFromUtc;

@end

@interface SearchGameResponse : BaseResponse

@property int totalRecords;
@property (nonatomic, strong) NSMutableArray* games;

@end