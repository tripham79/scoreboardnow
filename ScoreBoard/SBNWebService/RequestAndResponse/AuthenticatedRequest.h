//
//  AuthenticatedRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseRequest.h"

@interface AuthenticatedRequest : BaseRequest

@property (nonatomic, strong) NSString *loginToken;

@end
