//
//  SearchTeamRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SearchTeamRequest.h"
#import "SearchTeamResponse.h"

@implementation SearchTeamRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    if (self.SearchTerms.length == 0)
        self.SearchTerms = @"";
    
    [bodyD setObject:self.SearchTerms forKey:@"SearchTerms"];
    [bodyD setObject:[NSNumber numberWithInt:self.PageIndex] forKey:@"PageIndex"];
    [bodyD setObject:[NSNumber numberWithInt:self.PageSize] forKey:@"PageSize"];
    [bodyD setObject:[NSNumber numberWithInt:self.SportId] forKey:@"SportId"];
    [bodyD setObject:[NSNumber numberWithInt:self.LeagueId] forKey:@"LeagueId"];
    [bodyD setObject:[NSNumber numberWithInt:self.DivisionId] forKey:@"DivisionId"];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"SearchTeam Response: %@", [json description]);
    
    // parse json dictionary to response object
    SearchTeamResponse* response = [[SearchTeamResponse alloc] initWithDictionary:json];
    return response;
}

@end
