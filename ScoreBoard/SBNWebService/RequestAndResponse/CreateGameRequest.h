//
//  CreateGameRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "AuthenticatedRequest.h"
@class Game;

@interface CreateGameRequest : AuthenticatedRequest

@property (nonatomic, strong) Game* game;
@property bool ConfirmedCreate;

@end
