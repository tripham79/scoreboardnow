//
//  SearchGameRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 11/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SearchGameRequest.h"
#import "Game.h"


@implementation SearchGameRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    
    [bodyD setObject:[NSNumber numberWithInt:self.SportId] forKey:@"SportId"];
    [bodyD setObject:[NSNumber numberWithInt:self.LeagueId] forKey:@"LeagueId"];
    [bodyD setObject:[NSNumber numberWithInt:self.DivisionId] forKey:@"DivisionId"];
    [bodyD setObject:[NSNumber numberWithInt:self.TeamId] forKey:@"TeamId"];
    
    [bodyD setObject:[NSNumber numberWithInt:self.StatusId] forKey:@"StatusId"];
    if (self.GameNumber.length > 0)
        [bodyD setObject:self.GameNumber forKey:@"GameNumber"];
    
    if (self.TeamNameOrNumber.length > 0)
        [bodyD setObject:self.TeamNameOrNumber forKey:@"TeamNameOrNumber"];
    
    NSString *jsonDate = @"";
    if (self.ScheduledStartDateFromUtc != nil) {
        jsonDate = [self.ScheduledStartDateFromUtc toJSONString];
        [bodyD setObject:jsonDate forKey:@"ScheduledStartDateFromUtc"];
    }
    
    if (self.ScheduledStartDateToUtc != nil) {
        jsonDate = [self.ScheduledStartDateToUtc toJSONString];
        [bodyD setObject:jsonDate forKey:@"ScheduledStartDateToUtc"];
    }
    
    if (self.UpdatedOnFromUtc != nil) {
        jsonDate = [self.UpdatedOnFromUtc toJSONString];
        [bodyD setObject:jsonDate forKey:@"UpdatedOnFromUtc"];
    }
   
    [bodyD setObject:[NSNumber numberWithInt:self.PageIndex] forKey:@"PageIndex"];
    [bodyD setObject:[NSNumber numberWithInt:self.PageSize] forKey:@"PageSize"];

    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"SearchGame Response: %@", [json description]);
    
    // parse json dictionary to response object
    SearchGameResponse* response = [[SearchGameResponse alloc] initWithDictionary:json];
    return response;
}

@end

@implementation SearchGameResponse

-(id)initWithDictionary:(NSDictionary*)dic {
    self = [super initWithDictionary:dic];
    if (![self.IsSuccess boolValue])
        return self;
    
    self.totalRecords = [[dic objectForKey:@"TotalRecords"] intValue];
    self.games = [[NSMutableArray alloc] init];
    
    NSArray* arr = [dic objectForKey:@"Games"];
    for (NSDictionary* jsonDic in arr) {
        Game* g = [[Game alloc] initWithDictionary:jsonDic];
        [self.games addObject:g];
    }
    return self;
}

@end