//
//  SearchTeamRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "AuthenticatedRequest.h"

@interface SearchTeamRequest : AuthenticatedRequest

@property (nonatomic, strong) NSString* SearchTerms;
@property int PageIndex;
@property int PageSize;
@property int SportId;
@property int LeagueId;
@property int DivisionId;

@end
