//
//  SearchStadiumRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SearchStadiumRequest.h"
#import "SearchStadiumResponse.h"

@implementation SearchStadiumRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    if (self.SearchTerms == nil)
        self.SearchTerms = @"";
    
    [bodyD setObject:self.SearchTerms forKey:@"SearchTerms"];
    [bodyD setObject:[NSNumber numberWithInt:self.PageIndex] forKey:@"PageIndex"];
    [bodyD setObject:[NSNumber numberWithInt:self.PageSize] forKey:@"PageSize"];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"SearchStadium Response: %@", [json description]);
    
    // parse json dictionary to response object
    SearchStadiumResponse* response = [[SearchStadiumResponse alloc] initWithDictionary:json];
    return response;
}

@end
