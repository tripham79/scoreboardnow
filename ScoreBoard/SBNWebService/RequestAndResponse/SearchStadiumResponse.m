//
//  SearchStadiumResponse.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SearchStadiumResponse.h"
#import "Stadium.h"

@implementation SearchStadiumResponse

-(id)initWithDictionary:(NSDictionary*)dic {
    self = [super initWithDictionary:dic];
    if (![self.IsSuccess boolValue])
        return self;
    
    self.totalRecords = [[dic objectForKey:@"TotalRecords"] intValue];
    self.stadiums = [[NSMutableArray alloc] init];
    
    NSArray* arr = [dic objectForKey:@"Stadiums"];
    for (NSDictionary* jsonDic in arr) {
        Stadium* s = [[Stadium alloc] initWithDictionary:jsonDic];
        [self.stadiums addObject:s];
    }
    return self;
}

@end
