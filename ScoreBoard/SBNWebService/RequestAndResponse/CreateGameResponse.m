//
//  CreateGameResponse.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateGameResponse.h"
#import "NSDictionary+Extensions.h"
#import "NSString+Extensions.h"

@implementation CreateGameResponse

-(id)initWithDictionary:(NSDictionary*)dic {
    self = [super initWithDictionary:dic];
    
    // if not success, still need to get duplicated game info
    //if (!self.isSuccess)
    //    return self;
    
    self.gameId = [[dic objectForKey:@"GameId"] intValue];
    self.gameNumber = [[dic objectForKey:@"GameNumber"] intValue];
    self.gameNumberText = [dic stringForKey:@"GameNumberText"];
    self.scheduledStartTime = [[dic stringForKey:@"ScheduledStartTime"] jsonStringToDateUtc];
    
    return self;
}


@end
