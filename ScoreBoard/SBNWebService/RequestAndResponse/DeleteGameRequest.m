//
//  DeleteGameRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "DeleteGameRequest.h"

@implementation DeleteGameRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    [bodyD setObject:[NSNumber numberWithInt:self.gameId] forKey:@"GameId"];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"DeleteGame Response: %@", [json description]);
    
    // parse json dictionary to response object
    BaseResponse* response = [[BaseResponse alloc] initWithDictionary:json];
    return response;
}

@end
