//
//  BaseResponse.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#define INVALID_LOGIN_TOKEN @"INVALID_LOGIN_TOKEN"
#define ERROR_DUPLICATED_GAME @"ERROR_DUPLICATED_GAME"

@interface BaseResponse : NSObject

@property (nonatomic, strong) NSNumber* IsSuccess;
@property (nonatomic, strong) NSString* ErrorMessage;
@property (nonatomic, strong) NSString* ErrorCode;

- (id)initWithDictionary:(NSDictionary*)dic;

@end

