
#import <Foundation/Foundation.h>
#import "CommonFunction.h"

@interface GameScore : NSObject

- (id) initWithString: (NSString*) source;

@property (retain) NSString *gameId;
@property (retain) NSString *team1Score;
@property (retain) NSString *team2Score;
@property (retain) NSString *qtr;
@property (retain) NSDate *lastUpdated;

@property (retain) NSString *hours;
@property (retain) NSString *minutes;
@property (retain) NSString *seconds;

@property (retain) NSString *numberCheckedInUser;

@property (retain) NSString *outs;
@property (retain) NSString *balls;
@property (retain) NSString *strikes;
@property BOOL halfTime;
@property BOOL isTop;

@property BOOL onPossition1;
@property BOOL onPossition2;
@property BOOL onPossition3;

@end
