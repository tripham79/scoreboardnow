//
//  BaseRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseRequest.h"
#import "NSHTTPURLResponse+StatusCodes.h"
#import "Config.h"

@implementation BaseRequest {
    NSURLConnection* _connection;
}

-(id)init {
	
	self = [super init];
	[self setUniqueId:[BaseRequest uuid]];
	_receivedData = [[NSMutableData alloc] initWithLength:0];
	return self;
}

+(NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge NSString *)uuidStringRef;
}

-(void)cancel {
    self.delegate = nil;
    // stop pending request
    [_connection cancel];
    _connection = nil;
}

-(void)get:(NSString*)path withDelegate:(id<ServiceRequestDelegate>)delegate {
    self.delegate = delegate;
    [self get:path];
}

-(void)get:(NSString*)path withCallback:(RequestCompletionBlock)callback {
    self.requestCompletionBlock = callback;
    [self get:path];
}

-(void)get:(NSString*)path {
    if ([path hasPrefix:@"http"])
        self.url = path;
    else
        self.url = [NSMutableString stringWithFormat:@"%@/%@", SBNEnpoint, path];
    
	NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:self.url]];
    [request setTimeoutInterval:30];
	[request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
	_connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

-(void)post:(NSString*)path body:(NSDictionary*)body withCallback:(RequestCompletionBlock)callback {
    self.requestCompletionBlock = callback;
	[self execute:@"POST" path:path body:body];
}

-(void)post:(NSString*)path body:(NSDictionary*)body withDelegate:(id<ServiceRequestDelegate>)delegate {
    self.delegate = delegate;
	[self execute:@"POST" path:path body:body];
}

-(void)put:(NSString*)path body:(NSDictionary*)body withCallback:(RequestCompletionBlock)callback {
    self.requestCompletionBlock = callback;
	[self execute:@"PUT" path:path body:body];
}

-(void)put:(NSString*)path body:(NSDictionary*)body withDelegate:(id<ServiceRequestDelegate>)delegate {
    self.delegate = delegate;
	[self execute:@"PUT" path:path body:body];
}

-(void)execute:(NSString*)method path:(NSString*)path body:(NSDictionary*)body
{
    if ([path hasPrefix:@"http"])
        self.url = path;
    else
        self.url = [NSMutableString stringWithFormat:@"%@/%@", SBNEnpoint, path];
    
	NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSData* bodyData = [NSJSONSerialization dataWithJSONObject:body options:kNilOptions error:nil];
    
    [request setHTTPBody:bodyData];
    _connection = [NSURLConnection connectionWithRequest:request delegate:self];
}

-(BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	
	[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
	
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response {
	
	[self setResponse:(NSHTTPURLResponse*)response];
	
	if (![[self response] isOK]) {
		[self handleHttpError:[[self response] statusCode]];
	}
}

-(void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data {
	
	if ([_response isOK]) {
		[_receivedData appendData:data];
	}
}

-(BOOL)handleHttpError:(NSInteger)code {
    // server error or network failure
    NSString *msg = @"Request to server failed. Please check internet connection and try again.";
    //showAlert(nil, msg, nil);
    
    NSError* error = [NSError errorWithDomain:self.url
                                         code:[[self response] statusCode]
                                     userInfo:[NSDictionary dictionaryWithObject:msg forKey:@"message"]];
    // call subclass for further processing if need
    if (self.delegate)
        [self.delegate requestDidFinish:self withResult:nil withError:error];
    else if (self.requestCompletionBlock)
        self.requestCompletionBlock(self, nil, error);
    
    return YES;
}

-(void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error {
	[self handleHttpError:[[self response] statusCode]];
}

-(void)connectionDidFinishLoading:(NSURLConnection*)connection {
    
	if ([[self response] isOK]) {
        //NSLog(@"Request Ok: %@",[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding]);
        
        NSDictionary* receivedJson = [NSJSONSerialization JSONObjectWithData:self.receivedData options:kNilOptions error:nil];
        
        [self handleHttpOK:receivedJson];
	} else {
        NSLog(@"Request Error: %d", [[self response] statusCode]);
		[self handleHttpError:[[self response] statusCode]];
	}
    _connection = nil;
}

-(BOOL)handleHttpOK:(NSDictionary*)data {
    id result = [self parseResponse:data];
    
    // call back to delegate
    if (self.delegate)
        [self.delegate requestDidFinish:self withResult:result withError:nil];
    else if (self.requestCompletionBlock)
        self.requestCompletionBlock(self, result, nil);
    
    return YES;
}

- (NSMutableDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [[NSMutableDictionary alloc] init];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    return json;
}

@end
