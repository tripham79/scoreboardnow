
#import "Notification.h"

@implementation Notification

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NotificationIcon.png"]];
        [backgroundImage setFrame:CGRectMake(0, 0, 21, 21)];
        numberTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 21, 21)];
        [numberTitle setTextAlignment:UITextAlignmentCenter];
        [numberTitle setBackgroundColor:[UIColor clearColor]];
        [numberTitle setFont:[UIFont fontWithName:@"Trebuchet MS" size:12]];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id) initWithNumber:(int)aNumber{
    self = [self initWithFrame:CGRectMake(-10, -10, 21, 21)];
    if (self) {
        number = aNumber;
        [numberTitle setText:[NSString stringWithFormat:@"%d", number]];
    }
    return self;
}

- (int) number{
    return number;
}

- (void) setNumber:(int)aNumber{
    number = aNumber;
    [numberTitle setText:[NSString stringWithFormat:@"%d", number]];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self addSubview:backgroundImage];
    [self addSubview:numberTitle];   
}

- (void) dealloc{
    [backgroundImage release];
    [numberTitle release];
    [super dealloc];
}
@end
