
#import <UIKit/UIKit.h>
#import "CommonFunction.h"
#import "GameBox.h"
#import "BaseScoreBoard.h"

@interface ScoreBoardFootball : BaseScoreBoard {
    
    UIButton *btnTeam1;
    UIButton *btnTeam2;
    UIButton *btnQtr;
    UIButton *btnSeconds;
    UIButton *btnMinutes;
    CCheckboxField *chkHalfTime;
    
    //NSString *score1;
    //NSString *score2;
    //NSString *scoreQtr;
    //NSString *seconds;
    //NSString *minutes;
    
    NSMutableString *currentScore;
    BOOL isEditScore1;
    BOOL isEditScore2;
    BOOL isEditQtr;
    
    BOOL isEditTime;
    
    int maxQtr;
    int maxQtrDurationSeconds;
    
    int maxLength;
}


@property (retain) IBOutlet UIView *portrait;
@property (retain) IBOutlet UIView *landscape;
@property (retain, nonatomic) IBOutlet UIImageView *imgSportLogo;
@property (retain, nonatomic) IBOutlet UILabel *lblSportName;
@property (retain, nonatomic) IBOutlet UILabel *lblTimeRemain;

//@property (retain) NSString *tempMinutes;
//@property (retain) NSString *tempSeconds;

//- (UIButton*) getTeam1Button;
//- (UIButton*) getTeam2Button;
//- (UIButton*) getQtrButton;
//- (UIButton*) getMinutesButton;
//- (UIButton*) getSecondsButton;

//- (NSString*) getMinutes;
//- (NSString*) getSeconds;

/*
- (void) changeOrientation: (int) orientation;

- (void) startTimer;
- (void) stopTimer;
- (void) stopTimeRemainTimer;
- (void) updateTeam1Score:(int) score;
- (void) updateTeam2Score:(int)score;
- (void) updateQtr:(int) qtr;
- (void) updateMinutes:(int)minutes;
- (void) updateSeconds:(int)seconds;
- (void) updateLastUpdated:(NSDate*) lastUpdated;

- (void) updateTimeElapsed:(int) seconds;

- (void) updateGameScoreFromGameBox: (GameBox*) gameBox;

- (void) activeScore:(int) kind;

- (void) refresh;*/
@end
