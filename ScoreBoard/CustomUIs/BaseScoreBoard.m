//
//  BaseScoreBoard.m
//  ScoreBoard
//
//  Created by Apple on 2/7/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import "BaseScoreBoard.h"

@implementation BaseScoreBoard
@synthesize currentGame;
@synthesize readOnly = _readOnly;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
// Drawing code
}
*/


- (void) showGame:(Game*)game {
    currentGame = game;
}

- (void) updateGameScore:(GameScore*)score {
    currentGame.team1Score = score.team1Score;
    currentGame.team2Score = score.team2Score;
    currentGame.qtr = score.qtr;
    currentGame.lastUpdated = score.lastUpdated;
    currentGame.minutes = score.minutes;
    currentGame.seconds = score.seconds;
    currentGame.numberCheckedInUser = score.numberCheckedInUser;
    
    currentGame.outs = score.outs;
    currentGame.balls = score.balls;
    currentGame.strikes = score.strikes;
    currentGame.halfTime = score.halfTime;
    currentGame.isTop = score.isTop;
    currentGame.onPossition1 = score.onPossition1;
    currentGame.onPossition2 = score.onPossition2;
    currentGame.onPossition3 = score.onPossition3;
}

- (BOOL) readOnly {
    return _readOnly;
}

- (void) setReadOnly:(BOOL)readOnly {
    _readOnly = readOnly;
}

- (void) changeOrientation: (int) orientation {
    
}

- (void) keyPadPressed:(KeyPadEnum)key {
    
}

- (void) refresh {
    if ([delegate respondsToSelector:@selector(refresh:)])
        [delegate refresh:self];
}

- (void) startTimer {
    if (refreshTimer == nil){
        refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    }
    
    [(UILabel*)[self viewWithTag:111] setText:[NSString stringWithFormat:@"Refreshing in %d sec", [[SharedComponent getInstance] refreshSeconds]]];
    tick = [[SharedComponent getInstance] refreshSeconds];
    isAlive = YES;
}

- (void) stopTimer {
    NSString *status = @"Getting score...";
    [(UILabel*)[self viewWithTag:111] setText:status];
    isAlive = NO;
}

- (void) killTimer {
    if (refreshTimer != nil) {
        [refreshTimer invalidate];
        refreshTimer = nil;
        
        [(UILabel*)[self viewWithTag:111] setText:@""];
    }
}

- (void) timerTick{
    if (isAlive){
        tick--;
        
        if (tick > 0)
        {
            NSString *status = [NSString stringWithFormat:@"Refreshing in %d sec", tick];
            [(UILabel*)[self viewWithTag:111] setText:status];
        }
        else if (tick == 0)
        {
            if ([self.delegate respondsToSelector:@selector(refresh:)])
                [self.delegate refresh:self];
        }
    }
}


@end
