
#import "ScoreBoardBaseball.h"

@implementation ScoreBoardBaseball
@synthesize portrait;
@synthesize landscape;
//@synthesize tempMinutes, tempSeconds;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"ScoreBoardBaseball" owner:self options:nil];
        self = (ScoreBoardBaseball*)[arr objectAtIndex:0];
        
        //timeRemain = -10;
        //isUpdateTimeRemain = YES;
        
        isAlive = NO;
        refreshTimer = nil;
        
        currentScore = [[NSMutableString alloc] init];
        
        //score1 = nil;
        //score2 = nil;
        //scoreQtr = nil;
        
        maxQtr = INT16_MAX;
        maxQtrDurationSeconds = INT16_MAX;
        
        lblSportName = (UILabel*)[self viewWithTag:130];
        
        btnTeam1 = [self getTeam1Button];
        btnTeam2 = [self getTeam2Button];
        btnQtr = [self getQtrButton];
        btnOuts = [self getOutsButton];
        btnBalls = [self getBallsButton];
        btnStrikes = [self getStrikesButton];
        btnTopBottom = (UIButton*)[self viewWithTag:131];
        imgTopBottom = (UIImageView*)[self viewWithTag:128];
        
        imgDiamond1 = (UIImageView*)[self viewWithTag:140];
        imgDiamond2 = (UIImageView*)[self viewWithTag:141];
        imgDiamond3 = (UIImageView*)[self viewWithTag:142];
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgDiamond1Tap:)];
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgDiamond2Tap:)];
        UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgDiamond3Tap:)];
        
        [imgDiamond1 addGestureRecognizer:tapGesture1];
        [imgDiamond2 addGestureRecognizer:tapGesture2];
        [imgDiamond3 addGestureRecognizer:tapGesture3];
        
        [btnTeam1.titleLabel setAdjustsFontSizeToFitWidth:YES];
        [btnTeam2.titleLabel setAdjustsFontSizeToFitWidth:YES];
        
        [btnTeam1 addTarget:self action:@selector(btnTeam1Tapped) forControlEvents:UIControlEventTouchUpInside];
        [btnTeam2 addTarget:self action:@selector(btnTeam2Tapped) forControlEvents:UIControlEventTouchUpInside];
        [btnQtr addTarget:self action:@selector(btnQtrTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnOuts addTarget:self action:@selector(btnOutsTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnBalls addTarget:self action:@selector(btnBallsTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnStrikes addTarget:self action:@selector(btnStrikesTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnTopBottom addTarget:self action:@selector(btnTopBottomTapped) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}

- (void)imgDiamond1Tap:(UITapGestureRecognizer *)recognizer {
    [self togglePosition:1];
}

- (void)imgDiamond2Tap:(UITapGestureRecognizer *)recognizer {
    [self togglePosition:2];
}

- (void)imgDiamond3Tap:(UITapGestureRecognizer *)recognizer {
    [self togglePosition:3];
}

- (void) togglePosition:(int)pos {
    if (pos == 1)
        [self updatePosition:pos isOn:!self.currentGame.onPossition1];
    if (pos == 2)
        [self updatePosition:pos isOn:!self.currentGame.onPossition2];
    if (pos == 3)
        [self updatePosition:pos isOn:!self.currentGame.onPossition3];
    
}

- (void) updatePosition:(int)pos isOn:(bool)isOn {
    UIImage *image;
    if (isOn)
        image = [UIImage imageNamed:@"diamond-on.png"];
    else
        image = [UIImage imageNamed:@"diamond-off.png"];
    
    if (pos == 1) {
        [imgDiamond1 setImage:image];
        self.currentGame.onPossition1 = isOn;
    }
    if (pos == 2) {
        [imgDiamond2 setImage:image];
        self.currentGame.onPossition2 = isOn;
    }
    if (pos == 3) {
        [imgDiamond3 setImage:image];
        self.currentGame.onPossition3 = isOn;
    }

}

- (void) showGame:(Game*)game {
    [super showGame:game];
    
    [self activeScore:NSNotFound];
        
    [btnTeam1 setTitle:game.team1Name forState:UIControlStateNormal];
    [btnTeam2 setTitle:game.team2Name forState:UIControlStateNormal];
    
    [self updateTeam1Score:game.team1Score.intValue];
    [self updateTeam2Score:game.team2Score.intValue];
    [self updateQtr:game.qtr.intValue];
    [self updateOuts:game.outs.intValue];
    [self updateBalls:game.balls.intValue];
    [self updateStrikes:game.strikes.intValue];
    [self updateTopBottomImage:game.isTop];
    [self updateLastUpdated:game.lastUpdated];
    [self updatePosition:1 isOn:game.onPossition1];
    [self updatePosition:2 isOn:game.onPossition2];
    [self updatePosition:3 isOn:game.onPossition3];
    
    //[self updateMinutes:game.minutes.intValue];
    //[self updateSeconds:game.seconds.intValue];
    
    for (League *league in [[SharedComponent getInstance] listLeague]){
        if ([self.currentGame.leagueId isEqualToString:league.leagueId]){
            maxQtr = league.qtr.intValue;
            maxQtrDurationSeconds = league.qtrDuration.intValue * 60;
            break;
        }
    }    
    
    Sport* sport = [[SharedComponent getInstance] getSport:game.sportID];
    if (sport != nil)
        lblSportName.text = sport.sportName;
}

- (void) updateGameScore:(GameScore*)score {
    [super updateGameScore:score];
    
    // update UI
    [self updateTeam1Score:score.team1Score.intValue];
    [self updateTeam2Score:score.team2Score.intValue];
    [self updateQtr:score.qtr.intValue];
    [self updateOuts:score.outs.intValue];
    [self updateBalls:score.balls.intValue];
    [self updateStrikes:score.strikes.intValue];
    [self updateTopBottomImage:score.isTop];
    [self updateLastUpdated:score.lastUpdated];
    
    [self updatePosition:1 isOn:score.onPossition1];
    [self updatePosition:2 isOn:score.onPossition2];
    [self updatePosition:3 isOn:score.onPossition3];
    
    //[self updateMinutes:score.minutes.intValue];
    //[self updateSeconds:score.seconds.intValue];
}

- (void) setReadOnly:(BOOL)readOnly {
    [super setReadOnly:readOnly];
    BOOL enabled = !readOnly;
    
    [btnTeam1 setUserInteractionEnabled:enabled];
    [btnTeam1 setUserInteractionEnabled:enabled];
    [btnQtr setUserInteractionEnabled:enabled];
    [btnOuts setUserInteractionEnabled:enabled];
    [btnBalls setUserInteractionEnabled:enabled];
    [btnStrikes setUserInteractionEnabled:enabled];
    [btnTopBottom setUserInteractionEnabled:enabled];
    
    [imgDiamond1 setUserInteractionEnabled:enabled];
    [imgDiamond2 setUserInteractionEnabled:enabled];
    [imgDiamond3 setUserInteractionEnabled:enabled];
    
    //[[self getSecondsButton] setUserInteractionEnabled:enabled];
    //[[self getMinutesButton] setUserInteractionEnabled:enabled];
    
    // restore UI state
    [self activeScore:NSNotFound];
}

- (void) updateTopBottomImage:(BOOL)isTop {
    if (isTop)
        [imgTopBottom setImage:[UIImage imageNamed:@"inning-top.png"]];
    else
        [imgTopBottom setImage:[UIImage imageNamed:@"inning-bottom.png"]];
}

- (void) changeOrientation:(int)orientation{
    if (orientation == Portrait)
    {
        for (int i = 100; i <= 142; i++)
        {
            if ([self viewWithTag:i] != nil) {
                [self viewWithTag:i].frame = [portrait viewWithTag:i].frame;
                if ([[self viewWithTag:i] respondsToSelector:@selector(setFont:)])
                {
                    [[self viewWithTag:i] setFont: [[portrait viewWithTag:i] font]];
                }
            
                if ( i == 100)
                {
                    UIImageView *background = (UIImageView*)[self viewWithTag:i];
                    [background setImage:[UIImage imageNamed:@"scoreboard-bg.png"]];
                }
            }
        }
    }
    else if (orientation == Landscape)
    {
        for (int i = 100; i <= 142; i++)
        {
            if ([self viewWithTag:i] != nil) {
                [self viewWithTag:i].frame = [landscape viewWithTag:i].frame;
                if ([[self viewWithTag:i] respondsToSelector:@selector(setFont:)])
                {
                    [[self viewWithTag:i] setFont: [[landscape viewWithTag:i] font]];
                }
            
                if ( i == 100)
                {
                    UIImageView *background = (UIImageView*)[self viewWithTag:i];
                    [background setImage:[UIImage imageNamed:@"scoreboard-landscape-bg.png"]];
                }
            }
        }
    }
}

- (void) keyPadPressed:(KeyPadEnum)key{
    switch (key) {
        case KeyPad0:
            [currentScore appendString:@"0"];
            [self updateScore];
            break;
        case KeyPad1:
            [currentScore appendString:@"1"];
            [self updateScore];
            break;
        case KeyPad2:
            [currentScore appendString:@"2"];
            [self updateScore];
            break;
        case KeyPad3:
            [currentScore appendString:@"3"];
            [self updateScore];
            break;
        case KeyPad4:
            [currentScore appendString:@"4"];
            [self updateScore];
            break;
        case KeyPad5:
            [currentScore appendString:@"5"];
            [self updateScore];
            break;
        case KeyPad6:
            [currentScore appendString:@"6"];
            [self updateScore];
            break;
        case KeyPad7:
            [currentScore appendString:@"7"];
            [self updateScore];
            break;
        case KeyPad8:
            [currentScore appendString:@"8"];
            [self updateScore];
            break;
        case KeyPad9:
            [currentScore appendString:@"9"];
            [self updateScore];
            break;
        case KeyPadDel:
            [currentScore setString:@""];
            [self updateScore];
            break;
        case KeyPadEnter:
            [self activeScore:NSNotFound];
            
            isEditScore1 = NO;
            isEditScore2 = NO;
            isEditQtr = NO;
            isEditOuts = NO;
            isEditBalls = NO;
            isEditStrikes = NO;
            break;
        default:
            break;
    }
}

- (void) updateScore {
    if (currentScore.length > maxLength) return;
    
    NSString *newScore = currentScore;
    
    if (isEditScore1)
    {
        NSString *score1 = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.team1Score = score1;
        [self updateTeam1Score:newScore.intValue];
    }
    else if (isEditScore2)
    {
        NSString *score2 = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.team2Score = score2;
        [self updateTeam2Score:newScore.intValue];
    }
    else if (isEditQtr)
    {
        NSString *scoreQtr = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        // don't need in phase 1
        //if (scoreQtr.intValue > maxQtr){
        //    [scoreQtr release];
        //    scoreQtr = [[NSString stringWithFormat:@"%d", maxQtr] retain];
        
        //    NSString *message = [NSString stringWithFormat:@"Max Qtr of game is %d", maxQtr];
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        //    [alert show];
        //    [alert release];
        
        //    [currentScore setString:@""];
        //}
        self.currentGame.qtr = scoreQtr;
        [self updateQtr:scoreQtr.intValue];
    }
    else if (isEditOuts)
    {
        NSString *outs = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.outs = outs;
        [self updateOuts:outs.intValue];
    }
    else if (isEditBalls)
    {
        NSString *balls = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.balls = balls;
        [self updateBalls:balls.intValue];
    }
    else if (isEditStrikes)
    {
        NSString *strikes = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.strikes = strikes;
        [self updateStrikes:strikes.intValue];
    }
}

- (void) updateTeam1Score:(int)score{
    //tag 108, 109
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)([self viewWithTag:108]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:109]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateTeam2Score:(int)score{
    //tag 108, 109
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)([self viewWithTag:102]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:103]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateQtr:(int) qtr{
    int tenNumber = qtr/10;
    int unitNumber = qtr%10;
    
    [(UIImageView*)([self viewWithTag:104]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:133]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateOuts:(int) outs{
    //int tenNumber = outs/10;
    //int unitNumber = outs%10;
    
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:outs]]];
    //[(UIImageView*)([self viewWithTag:135]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}
- (void) updateBalls:(int) balls{
    //int tenNumber = balls/10;
    //int unitNumber = balls%10;
    
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:balls]]];
    //[(UIImageView*)([self viewWithTag:137]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}
- (void) updateStrikes:(int) strikes{
    //int tenNumber = strikes/10;
    //int unitNumber = strikes%10;
    
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:strikes]]];
    //[(UIImageView*)([self viewWithTag:139]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateMinutes:(int)minutes{
    //tag 106 107
    //self.tempMinutes = [NSString stringWithFormat:@"%d", minutes];
    
    int tenNumber = minutes / 10;
    int unitNumber = minutes % 10;
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateSeconds:(int)seconds{
    //tag 101, 105
    //self.tempSeconds = [NSString stringWithFormat:@"%d", seconds];
    
    int tenNumber = seconds / 10;
    int unitNumber = seconds % 10;
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:101]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateTimeElapsed:(int)seconds{
    [self updateMinutes:seconds / 60];
    [self updateSeconds:seconds % 60];
    
    /*
    int tenNumber = hours / 10;
    int unitNumber = hours % 10;
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
    
    tenNumber = minutes / 10;
    unitNumber = minutes % 10;
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:101]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
    */
}

- (void) updateLastUpdated:(NSDate *)lastUpdated
{
    // display in user time zone
    NSString *desciptionString = @"";
    NSString *dateText = [CommonFunction toLocalTimeText:lastUpdated dateFormat:@"hh:mma yyyy-MM-dd"];
    
    if (dateText == nil || [dateText isEqualToString:@""])
        dateText = @"N/A";
    
    desciptionString = [NSString stringWithFormat:@"Last Update:\n%@", dateText];
    [(UILabel*)([self viewWithTag:110]) setText:desciptionString];
}

- (void) activeScore:(int)kind
{
    ((UIImageView*)[self viewWithTag:115]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:116]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:117]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:118]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:119]).highlighted = FALSE;
    
    ((UIImageView*)[self viewWithTag:121]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:122]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:124]).highlighted = FALSE;
    //((UIImageView*)[self viewWithTag:125]).highlighted = FALSE;
    
    ((UIImageView*)[self viewWithTag:132]).highlighted = FALSE;
    //((UIImageView*)[self viewWithTag:134]).highlighted = FALSE;
    //((UIImageView*)[self viewWithTag:136]).highlighted = FALSE;
    //((UIImageView*)[self viewWithTag:138]).highlighted = FALSE;
    
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOuts setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBalls setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnStrikes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (kind == 1){
        //team1
        ((UIImageView*)[self viewWithTag:115]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:116]).highlighted = TRUE;
        [btnTeam1 setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else if (kind == 2){
        //team2
        ((UIImageView*)[self viewWithTag:117]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:118]).highlighted = TRUE;
        [btnTeam2 setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else if (kind == 3){
        //qtr       
        ((UIImageView*)[self viewWithTag:119]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:132]).highlighted = TRUE;
        [btnQtr setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else if (kind == 4){
        ((UIImageView*)[self viewWithTag:121]).highlighted = TRUE;
        //((UIImageView*)[self viewWithTag:134]).highlighted = TRUE;
        [btnOuts setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else if (kind == 5){
        ((UIImageView*)[self viewWithTag:122]).highlighted = TRUE;
        //((UIImageView*)[self viewWithTag:136]).highlighted = TRUE;
        [btnBalls setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else if (kind == 6) {
        ((UIImageView*)[self viewWithTag:124]).highlighted = TRUE;
        //((UIImageView*)[self viewWithTag:138]).highlighted = TRUE;
        [btnStrikes setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
}

- (UIButton*) getTeam1Button{
    return (UIButton*)[self viewWithTag:112];
}

- (UIButton*) getTeam2Button{
    return (UIButton*)[self viewWithTag:113];
}

- (UIButton*) getQtrButton{
    return (UIButton*)[self viewWithTag:114];
}

- (UIButton*) getOutsButton{
    return (UIButton*)[self viewWithTag:120];
}

- (UIButton*) getBallsButton{
    return (UIButton*)[self viewWithTag:123];
}

- (UIButton*) getStrikesButton{
    return (UIButton*)[self viewWithTag:126];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) btnTeam1Tapped {
    [self activeScore:1];
    
    isEditScore1 = YES;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditOuts = NO;
    isEditBalls = NO;
    isEditStrikes = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnTeam2Tapped {    
    [self activeScore:2];
    
    isEditScore1 = NO;
    isEditScore2 = YES;
    isEditQtr = NO;
    isEditOuts = NO;
    isEditBalls = NO;
    isEditStrikes = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnQtrTapped {
    [self activeScore:3];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = YES;
    isEditOuts = NO;
    isEditBalls = NO;
    isEditStrikes = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnOutsTapped {
    [self activeScore:4];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditOuts = YES;
    isEditBalls = NO;
    isEditStrikes = NO;
    
    [currentScore setString:@""];
    
    maxLength = 1;
}

- (void) btnBallsTapped {
    [self activeScore:5];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditOuts = NO;
    isEditBalls = YES;
    isEditStrikes = NO;
    
    [currentScore setString:@""];
    
    maxLength = 1;
}

- (void) btnStrikesTapped {
    [self activeScore:6];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditOuts = NO;
    isEditBalls = NO;
    isEditStrikes = YES;
    
    [currentScore setString:@""];
    
    maxLength = 1;
}

- (void) btnTopBottomTapped {
    [self currentGame].isTop = ![self currentGame].isTop;
    [self updateTopBottomImage:[self currentGame].isTop];
}

- (void)dealloc {
    NSLog(@"ScoreBoardBaseball dealloc");
    // don't release any control here or error will occur
    //[portrait release];
    //[landscape release];
    
    [btnTeam1 removeTarget:self action:@selector(btnTeam1Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnTeam2 removeTarget:self action:@selector(btnTeam2Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnQtr removeTarget:self action:@selector(btnQtrTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnOuts removeTarget:self action:@selector(btnOutsTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnBalls removeTarget:self action:@selector(btnBallsTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnStrikes removeTarget:self action:@selector(btnStrikesTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnTopBottom removeTarget:self action:@selector(btnTopBottomTapped) forControlEvents:UIControlEventTouchUpInside];
    
    /*
    [btnTeam1 release];
    [btnTeam2 release];
    [btnQtr release];
    [btnOuts release];
    [btnBalls release];
    [btnStrikes release];*/
    
    [super dealloc];
}
@end
