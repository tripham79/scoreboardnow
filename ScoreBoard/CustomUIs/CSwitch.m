
#import "CSwitch.h"

@implementation CSwitch
@synthesize delegate;
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        
        imageSwitch = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 135, 35)];
        [imageSwitch setImage:[UIImage imageNamed:@"BackgroundSwitch.png"]];

        status = NO;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        
        imageSwitch = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width*3/2, self.frame.size.height)];
        [imageSwitch setImage:[UIImage imageNamed:@"BackgroundSwitch.png"]];
        [self setBackgroundColor:[UIColor clearColor]];
        status = NO;
    }
    return self;
}

- (void) layoutSubviews{
    [self addSubview:imageSwitch];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code

}*/

- (BOOL) status{
    return status;
}

- (void) setStatus:(BOOL)aStatus{
    status = aStatus;
    if (status == YES)
    {
        imageSwitch.frame = CGRectMake(-self.frame.size.width/2, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
    }
    else
    {
        imageSwitch.frame = CGRectMake(0, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    startPoint = [[[touches allObjects] objectAtIndex:0] locationInView:self];
    
    if (status == YES)
    {
        if (startPoint.x < self.frame.size.width/2)
        {
            isDrag = YES;
        }
        else
        {
            isDrag = NO;
        }
    }
    else
    {
        if (startPoint.x > self.frame.size.width/2)
        {
            isDrag = YES;
        }
        else
        {
            isDrag = NO;
        }
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isDrag == YES)
    {
        CGPoint currentPoint = [[[touches allObjects] objectAtIndex:0] locationInView:self];
        int movement = currentPoint.x - startPoint.x;
        imageSwitch.frame = CGRectOffset(imageSwitch.frame, movement, 0);
        startPoint = currentPoint;
        totalMovement += movement;
        
        if (imageSwitch.frame.origin.x > 0)
        {
            imageSwitch.frame = CGRectMake(0, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
            totalMovement -= movement;
        }
        
        if (imageSwitch.frame.origin.x < -self.frame.size.width/2) 
        {
            imageSwitch.frame = CGRectMake(-self.frame.size.width/2, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
            totalMovement -= movement;
        }
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (isDrag == NO)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.1];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        if (status == YES)
        {
            status = NO;
            imageSwitch.frame = CGRectMake(0, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
        }
        else
        {
            status = YES;
            imageSwitch.frame = CGRectMake(-self.frame.size.width/2, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
        }
        
        [UIView commitAnimations];
    }
    else
    {
        int offsetX = NSNotFound;
        
        if (totalMovement > self.frame.size.width / 4)
        {
            offsetX = 0;
            status = NO;
        }
        else if (totalMovement > 0)
        {
            offsetX = -self.frame.size.width / 2;
            status = YES;
        }
        
        if (totalMovement < -self.frame.size.width / 4)
        {
            offsetX = -self.frame.size.width / 2;
            status = YES;
        }
        else if (totalMovement < 0)
        {
            offsetX = 0;
            status = NO;
        }
        
        NSLog(@"%d %d", totalMovement, offsetX);
        
        if (offsetX != NSNotFound)
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.1];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            imageSwitch.frame = CGRectMake(offsetX, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
            [UIView commitAnimations];
        }
        else
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.1];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            if (status == YES)
            {
                status = NO;
                imageSwitch.frame = CGRectMake(0, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
            }
            else
            {
                status = YES;
                imageSwitch.frame = CGRectMake(-self.frame.size.width/2, 0, imageSwitch.frame.size.width, imageSwitch.frame.size.height);
            }
            
            [UIView commitAnimations];
        }
        
        totalMovement = 0;
        isDrag = NO;
    }

    if (delegate != nil){
        [delegate switchChangeStatus:status];
    }
}
@end
