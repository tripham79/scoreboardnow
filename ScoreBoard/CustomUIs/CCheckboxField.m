
#import "CCheckboxField.h"

@implementation CCheckboxField
@synthesize isChecked;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        isChecked = NO;
        
        imageCheckBox = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox.png"]];
        
        label = [[UILabel alloc] init];
        [label setMinimumFontSize:10];
        [label setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setText:@"Facebook"];
        [label setTextColor:[UIColor whiteColor]];
        [label setAdjustsFontSizeToFitWidth:YES];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        isChecked = NO;
        
        imageCheckBox = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox.png"]];
        
        label = [[UILabel alloc] init];
        [label setMinimumFontSize:10];
        [label setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor blackColor]];
        [label setAdjustsFontSizeToFitWidth:YES];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (NSString*) text{
    NSString *result = [NSString stringWithString:_text];
    return result;
}

- (void) setText:(NSString *)text{
    [_text release];
    _text = [text retain];
    [label setText:text];
}

- (void) setStatus:(BOOL)checked{
    [self setIsChecked:checked];
    if (checked){
        [imageCheckBox setImage:[UIImage imageNamed:@"checkbox_Active.png"]];   
    }
    else{
        [imageCheckBox setImage:[UIImage imageNamed:@"checkbox.png"]];
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [imageCheckBox setFrame:CGRectMake(0, (self.frame.size.height - 20)/2, 20, 20)];
    [label setFrame:CGRectMake(22, 0, self.frame.size.width - 22, self.frame.size.height)];
    
    [self addSubview:imageCheckBox];
    [self addSubview:label];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (isChecked)
    {
        isChecked = NO;
        [imageCheckBox setImage:[UIImage imageNamed:@"checkbox.png"]];
    }
    else
    {
        isChecked = YES;
        [imageCheckBox setImage:[UIImage imageNamed:@"checkbox_Active.png"]];        
    }
    [super touchesEnded:touches withEvent:event];
}

- (void) dealloc{
    //[self.text release];
    [super dealloc];
}
@end
