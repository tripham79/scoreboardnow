//
//  MKCustomAnnotation.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/11/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKCustomAnnotation : MKPointAnnotation

@property (retain, nonatomic) id userData;

@end
