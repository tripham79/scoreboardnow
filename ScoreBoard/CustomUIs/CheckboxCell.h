//
//  CheckboxCell.h
//  ScoreBoard
//
//  Created by Apple on 7/12/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckboxCell : UITableViewCell {
}
@property (retain, nonatomic) IBOutlet UIImageView *checkBoxField;
@property (retain, nonatomic) IBOutlet UILabel *labelField;
@property (retain, nonatomic) IBOutlet UILabel *detailField;

@property (nonatomic) BOOL isChecked;
@end
