
#import "DropDownList.h"
#import "SelectItemCell.h"
#import "UIView+Extensions.h"

@implementation DropDownList
@synthesize btnBack;
@synthesize source, table, delegate, owner;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // hide default status bar
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    }
    
    [table setDataSource:self];
    [table setDelegate:self];
    
    if (self.title.length > 0)
        self.lblTitle.text = self.title;
    
    [btnBack setBackgroundImage:[UIImage imageNamed:@"btnBack.png"] forState:UIControlStateNormal];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"btnBack_Active.png"] forState:UIControlStateHighlighted];
    [btnBack addTarget:self action:@selector(btnBackTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    // iOS 5 and later
    if ([self.table respondsToSelector:@selector(registerNib:forCellReuseIdentifier:)])
        [self.table registerNib:[UINib nibWithNibName:@"SelectItemCell" bundle:nil] forCellReuseIdentifier:@"SelectItemCell"];
    
    if (!self.showAddButton)
        [self.btnAdd setHidden:YES];
}

- (void)viewDidUnload
{
    [self setTable:nil];
    [self setSource:nil];
    [self setBtnBack:nil];
    [self setLblTitle:nil];
    [self setBtnAdd:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [table release];
    [source release];
    [btnBack release];
    [_lblTitle release];
    [_btnAdd release];
    [super dealloc];
}

- (void) reloadData{
    [table reloadData];
}

- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [source count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"SelectItemCell";
    SelectItemCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // If there is no cell to reuse, create a new one
    if(cell == nil) {
        cell = (SelectItemCell*)[[[UINib nibWithNibName:@"SelectItemCell" bundle:nil] instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    
    if (!self.showReplyButton)
        [cell.btnReply setHidden:YES];
    else
        [cell.btnReply addTarget:self action:@selector(btnReplyClick:) forControlEvents:UIControlEventTouchUpInside];
    
    id obj = [source objectAtIndex:indexPath.row];
    if (self.textField)
        cell.lblLabel.text = [obj valueForKey:self.textField];
    else
        cell.lblLabel.text = [NSString stringWithFormat:@"%@", obj];
    
    //if (self.detailField)
    //    cell.detailTextLabel.text = [obj valueForKey:self.detailField];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [delegate dropDownList:self.owner didSelectedRowAtIndex:(int)indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddClicked:(id)sender {
    if (self.delegate)
        [self.delegate dropDownList:self addButtonClick:sender];
}
                
- (IBAction)btnReplyClick:(id)sender {
    UITableViewCell *cell = (id)[sender findSuperViewWithClass:[UITableViewCell class]];
    NSIndexPath *indexPath = [self.table indexPathForCell:cell];
    //NSLog(@"%ld", indexPath.row);
    
    if (self.delegate)
        [self.delegate dropDownList:self replyButtonClick:indexPath];
}

@end
