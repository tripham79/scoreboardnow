
#import "ScoreBoardFootball.h"

@implementation ScoreBoardFootball
@synthesize portrait;
@synthesize landscape;
//@synthesize tempMinutes, tempSeconds;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"ScoreBoardFootball" owner:self options:nil];
        self = (ScoreBoardFootball*)[arr objectAtIndex:0];
        
        //timeRemain = -10;
        //isUpdateTimeRemain = YES;
        
        isAlive = NO;
        refreshTimer = nil;
        
        currentScore = [[NSMutableString alloc] init];
        
        //score1 = nil;
        //score2 = nil;
        //scoreQtr = nil;
        
        maxQtr = INT16_MAX;
        maxQtrDurationSeconds = INT16_MAX;
        
        btnTeam1 = [self getTeam1Button];
        btnTeam2 = [self getTeam2Button];
        btnQtr = [self getQtrButton];
        btnSeconds = [self getSecondsButton];
        btnMinutes = [self getMinutesButton];
        chkHalfTime = (CCheckboxField*)[self viewWithTag:127];
        
        [btnTeam1 addTarget:self action:@selector(btnTeam1Tapped) forControlEvents:UIControlEventTouchUpInside];
        [btnTeam2 addTarget:self action:@selector(btnTeam2Tapped) forControlEvents:UIControlEventTouchUpInside];
        [btnQtr addTarget:self action:@selector(btnQtrTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnSeconds addTarget:self action:@selector(btnSecondsTapped) forControlEvents:UIControlEventTouchUpInside];
        [btnMinutes addTarget:self action:@selector(btnMinutesTapped) forControlEvents:UIControlEventTouchUpInside];
        [chkHalfTime addTarget:self action:@selector(chkHaldTimeTapped) forControlEvents:UIControlEventTouchUpInside];        
        
        [chkHalfTime setText:@""];
    }
    return self;
}

- (void) showGame:(Game*)game {
    [super showGame:game];
    
    [self activeScore:NSNotFound];
    
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam1.titleLabel setAdjustsFontSizeToFitWidth:YES];
    [btnTeam2.titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    [btnTeam1 setTitle:game.team1Name forState:UIControlStateNormal];
    [btnTeam2 setTitle:game.team2Name forState:UIControlStateNormal];
    
    [chkHalfTime setStatus:game.halfTime];
    
    [self updateTeam1Score:game.team1Score.intValue];
    [self updateTeam2Score:game.team2Score.intValue];
    [self updateQtr:game.qtr.intValue];
    [self updateLastUpdated:game.lastUpdated];
    
    [self updateMinutes:game.minutes.intValue];
    [self updateSeconds:game.seconds.intValue];
    
    for (League *league in [[SharedComponent getInstance] listLeague]){
        if ([self.currentGame.leagueId isEqualToString:league.leagueId]){
            maxQtr = league.qtr.intValue;
            maxQtrDurationSeconds = league.qtrDuration.intValue * 60;
            break;
        }
    }
    
    // this control is used for a few sports  (with small differents)
    SportTypeEnum sportType = [[SharedComponent getInstance] getSportType:game.sportID];    
    switch (sportType) {
        case SportBaseball:
            // never go here
            break;
        case SportFootball:
            self.imgSportLogo.image = [UIImage imageNamed:@"football.png"];
            self.lblSportName.text = @"Football";
            self.lblTimeRemain.text = @"Time Remaining";
            break;
        case SportSoccer:
            self.imgSportLogo.image = [UIImage imageNamed:@"soccer.png"];
            self.lblSportName.text = @"Soccer";
            self.lblTimeRemain.text = @"Time";
            [btnQtr setTitle:@"Half" forState:UIControlStateNormal];
            break;
        case SportBasketball:
            self.imgSportLogo.image = [UIImage imageNamed:@"basketball.png"];
            self.lblSportName.text = @"Basketball";
            self.lblTimeRemain.text = @"Time Remaining";
            break;
        case SportUnknown:            
            break;
    }
    
    Sport* sport = [[SharedComponent getInstance] getSport:game.sportID];
    if (sport != nil)
        self.lblSportName.text = sport.sportName;
}

- (void) updateGameScore:(GameScore*)score {
    [super updateGameScore:score];
    
    // update UI
    [self updateTeam1Score:score.team1Score.intValue];
    [self updateTeam2Score:score.team2Score.intValue];
    [self updateQtr:score.qtr.intValue];
    [self updateLastUpdated:score.lastUpdated];
    
    [self updateMinutes:score.minutes.intValue];
    [self updateSeconds:score.seconds.intValue];
    
    [chkHalfTime setStatus:score.halfTime];
}

- (void) setReadOnly:(BOOL)readOnly {
    [super setReadOnly:readOnly];
    BOOL enabled = !readOnly;
    
    [[self getTeam1Button] setUserInteractionEnabled:enabled];
    [[self getTeam2Button] setUserInteractionEnabled:enabled];
    [[self getQtrButton] setUserInteractionEnabled:enabled];
    [[self getSecondsButton] setUserInteractionEnabled:enabled];
    [[self getMinutesButton] setUserInteractionEnabled:enabled];
    [chkHalfTime setUserInteractionEnabled:enabled];
    
    // restore UI state
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self activeScore:NSNotFound];
}

- (UIButton*) getTeam1Button{
    return (UIButton*)[self viewWithTag:112];
}

- (UIButton*) getTeam2Button{
    return (UIButton*)[self viewWithTag:113];
}

- (UIButton*) getQtrButton{
    return (UIButton*)[self viewWithTag:114];
}

- (UIButton*) getMinutesButton{
    return (UIButton*)[self viewWithTag:120];
}

- (UIButton*) getSecondsButton{
    return (UIButton*)[self viewWithTag:123];
}

- (void) changeOrientation:(int)orientation{
    if (orientation == Portrait)
    {
        for (int i = 100; i <= 131; i++)
        {
            [self viewWithTag:i].frame = [portrait viewWithTag:i].frame;
            if ([[self viewWithTag:i] respondsToSelector:@selector(setFont:)])
            {       
                [[self viewWithTag:i] setFont: [[portrait viewWithTag:i] font]];
            }
            
            if ( i == 100)
            {
                UIImageView *background = (UIImageView*)[self viewWithTag:i];
                [background setImage:[UIImage imageNamed:@"scoreboard-bg.png"]];
            }
        }
    }
    else if (orientation == Landscape)
    {
        for (int i = 100; i <= 131; i++)
        {
            [self viewWithTag:i].frame = [landscape viewWithTag:i].frame;
            if ([[self viewWithTag:i] respondsToSelector:@selector(setFont:)])
            {
                [[self viewWithTag:i] setFont: [[landscape viewWithTag:i] font]];
            }
            
            if ( i == 100)
            {
                UIImageView *background = (UIImageView*)[self viewWithTag:i];
                [background setImage:[UIImage imageNamed:@"scoreboard-landscape-bg.png"]];
            }
        }
    }
}

- (void) keyPadPressed:(KeyPadEnum)key{
    switch (key) {
        case KeyPad0:
            [currentScore appendString:@"0"];
            [self updateScore];
            break;
        case KeyPad1:
            [currentScore appendString:@"1"];
            [self updateScore];
            break;
        case KeyPad2:
            [currentScore appendString:@"2"];
            [self updateScore];
            break;
        case KeyPad3:
            [currentScore appendString:@"3"];
            [self updateScore];
            break;
        case KeyPad4:
            [currentScore appendString:@"4"];
            [self updateScore];
            break;
        case KeyPad5:
            [currentScore appendString:@"5"];
            [self updateScore];
            break;
        case KeyPad6:
            [currentScore appendString:@"6"];
            [self updateScore];
            break;
        case KeyPad7:
            [currentScore appendString:@"7"];
            [self updateScore];
            break;
        case KeyPad8:
            [currentScore appendString:@"8"];
            [self updateScore];
            break;
        case KeyPad9:
            [currentScore appendString:@"9"];
            [self updateScore];
            break;
        case KeyPadDel:
            [currentScore setString:@""];
            [self updateScore];
            break;
        case KeyPadEnter:
            [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            [self activeScore:NSNotFound];
            
            isEditScore1 = NO;
            isEditScore2 = NO;
            isEditQtr = NO;
            isEditTime = NO;
            break;
        default:
            break;
    }
}

- (void) updateScore {
    if (currentScore.length > maxLength) return;
    
    NSString *newScore = currentScore;
    
    if (isEditScore1)
    {
        NSString *score1 = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.team1Score = score1;
        [self updateTeam1Score:newScore.intValue];
    }
    else if (isEditScore2)
    {
        NSString *score2 = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        self.currentGame.team2Score = score2;
        [self updateTeam2Score:newScore.intValue];
    }
    else if (isEditQtr)
    {
        NSString *scoreQtr = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        // don't need in phase 1
        //if (scoreQtr.intValue > maxQtr){
        //    [scoreQtr release];
        //    scoreQtr = [[NSString stringWithFormat:@"%d", maxQtr] retain];
        
        //    NSString *message = [NSString stringWithFormat:@"Max Qtr of game is %d", maxQtr];
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        //    [alert show];
        //    [alert release];
        
        //    [currentScore setString:@""];
        //}
        self.currentGame.qtr = scoreQtr;
        [self updateQtr:scoreQtr.intValue];
    }
    else if (isEditTime)
    {
        int loopTimes = maxLength - newScore.length;
        for (int i=0; i< loopTimes;i++){
            newScore = [newScore stringByAppendingString:@"0"];
        }
        
        int time = newScore.intValue;
        
        NSString *minutes = [[NSString stringWithFormat:@"%d", time/100] retain];
        NSString *seconds = [[NSString stringWithFormat:@"%d", time%100] retain];
        
        if (seconds.intValue >= 60){
            [seconds release];
            seconds = [[NSString stringWithFormat:@"%d", 59] retain];
        }
        
        //check max qtr duration
        int currentQtrDurationSeconds = minutes.intValue*60 + seconds.intValue;
        if (currentQtrDurationSeconds > maxQtrDurationSeconds) {
            [self updateTimeElapsed:maxQtrDurationSeconds];
            
            NSString *message = [NSString stringWithFormat:@"Max Qtr duration of game is %d:00", maxQtrDurationSeconds / 60];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            
            minutes = [[NSString stringWithFormat:@"%d", maxQtrDurationSeconds/60] retain];
            seconds = [[NSString stringWithFormat:@"%d", 0] retain];
            
            [currentScore setString:@""];
        }
        
        self.currentGame.minutes = minutes;
        self.currentGame.seconds = seconds;
        [self updateMinutes:minutes.intValue];
        [self updateSeconds:seconds.intValue];
        [minutes release];
        [seconds release];
    }
}

- (void) btnTeam1Tapped{
    [btnTeam1 setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self activeScore:1];
    
    isEditScore1 = YES;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditTime = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnTeam2Tapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self activeScore:2];
    
    isEditScore1 = NO;
    isEditScore2 = YES;
    isEditQtr = NO;
    isEditTime = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnQtrTapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    
    [self activeScore:3];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = YES;
    isEditTime = NO;
    
    [currentScore setString:@""];
    
    maxLength = 1;
}

- (void) btnMinutesTapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self activeScore:4];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditTime = YES;
    
    [currentScore setString:@""];
    
    maxLength = 4;
}

- (void) btnSecondsTapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self activeScore:4];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditTime = YES;
    
    [currentScore setString:@""];
    
    maxLength = 4;
}

- (void) chkHaldTimeTapped {
    self.currentGame.halfTime = chkHalfTime.isChecked;
}

- (void) updateTeam1Score:(int)score{
    //tag 108, 109
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)([self viewWithTag:108]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:109]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateTeam2Score:(int)score{
    //tag 108, 109
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)([self viewWithTag:102]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:103]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateQtr:(int) qtr{
    [(UIImageView*)([self viewWithTag:104]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:qtr]]];
}

- (void) updateMinutes:(int)minutes{
    //tag 106 107
    //self.tempMinutes = [NSString stringWithFormat:@"%d", minutes];
    
    int tenNumber = minutes / 10;
    int unitNumber = minutes % 10;
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateSeconds:(int)seconds{
    //tag 101, 105
    //self.tempSeconds = [NSString stringWithFormat:@"%d", seconds];
    
    int tenNumber = seconds / 10;
    int unitNumber = seconds % 10;
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:101]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateTimeElapsed:(int)seconds{
    [self updateMinutes:seconds / 60];
    [self updateSeconds:seconds % 60];
    
    /*
    int tenNumber = hours / 10;
    int unitNumber = hours % 10;
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
    
    tenNumber = minutes / 10;
    unitNumber = minutes % 10;
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:101]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
    */
}

- (void) updateLastUpdated:(NSDate *)lastUpdated {
    // display in user time zone
    NSString *desciptionString = @"";
    NSString *dateText = [CommonFunction toLocalTimeText:lastUpdated dateFormat:@"hh:mma yyyy-MM-dd"];
    
    if (dateText == nil || [dateText isEqualToString:@""])
        dateText = @"N/A";
    
    desciptionString = [NSString stringWithFormat:@"Last Update:\n%@", dateText];
    [(UILabel*)([self viewWithTag:110]) setText:desciptionString];
}

- (void) activeScore:(int)kind{
    ((UIImageView*)[self viewWithTag:115]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:116]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:117]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:118]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:119]).highlighted = FALSE;
    
    ((UIImageView*)[self viewWithTag:121]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:122]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:124]).highlighted = FALSE;
    ((UIImageView*)[self viewWithTag:125]).highlighted = FALSE;

    //[[self viewWithTag:115] setHidden:YES];;
    //[[self viewWithTag:116] setHidden:YES];
    //[[self viewWithTag:117] setHidden:YES];
    //[[self viewWithTag:118] setHidden:YES];
    //[[self viewWithTag:119] setHidden:YES];

    //[[self viewWithTag:121] setHidden:YES];
    //[[self viewWithTag:122] setHidden:YES];
    //[[self viewWithTag:124] setHidden:YES];
    //[[self viewWithTag:125] setHidden:YES];
    
    if (kind == 1){
        //team1
        //[[self viewWithTag:115] setHidden:NO];
        //[[self viewWithTag:116] setHidden:NO];
        ((UIImageView*)[self viewWithTag:115]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:116]).highlighted = TRUE;
    }
    else if (kind == 2){
        //team2
        //[[self viewWithTag:117] setHidden:NO];
        //[[self viewWithTag:118] setHidden:NO];
        ((UIImageView*)[self viewWithTag:117]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:118]).highlighted = TRUE;
    }
    else if (kind == 3){
        //qtr
        //[[self viewWithTag:119] setHidden:NO];
        ((UIImageView*)[self viewWithTag:119]).highlighted = TRUE;
    }
    else if (kind == 4){
        //minutes
        //[[self viewWithTag:121] setHidden:NO];
        //[[self viewWithTag:122] setHidden:NO];
        ((UIImageView*)[self viewWithTag:121]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:122]).highlighted = TRUE;
        
        //seconds
        //[[self viewWithTag:124] setHidden:NO];
        //[[self viewWithTag:125] setHidden:NO];
        ((UIImageView*)[self viewWithTag:124]).highlighted = TRUE;
        ((UIImageView*)[self viewWithTag:125]).highlighted = TRUE;
    }
    else if (kind == 5){
        
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    NSLog(@"ScoreBoardFootball dealloc");
    
    // don't release any control here or error will occur
    //[portrait release];
    //[landscape release];
    
    [btnTeam1 removeTarget:self action:@selector(btnTeam1Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnTeam2 removeTarget:self action:@selector(btnTeam2Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnQtr removeTarget:self action:@selector(btnQtrTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnSeconds removeTarget:self action:@selector(btnSecondsTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnMinutes removeTarget:self action:@selector(btnMinutesTapped) forControlEvents:UIControlEventTouchUpInside];
    [chkHalfTime removeTarget:self action:@selector(chkHaldTimeTapped) forControlEvents:UIControlEventTouchUpInside];
    
    // don't release any control here or error will occur
    /*
    [btnTeam1 release];
    [btnTeam2 release];
    [btnQtr release];
    [btnSeconds release];
    [btnMinutes release];
    [chkHalfTime release];    
    
    [_imgSportLogo release];
    [_lblSportName release];
    [_lblTimeRemain release];*/
    [super dealloc];
}
@end
