//
//  SelectItemCell.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectItemCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblLabel;
@property (retain, nonatomic) IBOutlet UIButton *btnReply;

@end
