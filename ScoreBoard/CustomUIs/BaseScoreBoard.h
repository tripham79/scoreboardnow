//
//  BaseScoreBoard.h
//  ScoreBoard
//
//  Created by Apple on 2/7/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonFunction.h"
#import "GameBox.h"
#import "League.h"
#import "CCheckboxField.h"

@class BaseScoreBoard;

@protocol BaseScoreBoardDelegate <NSObject>

- (void) refresh:(BaseScoreBoard*) scoreBoard;

@end

@interface BaseScoreBoard : UIView {
    BOOL _readOnly;
    
    BOOL isGettingScore;
    NSTimer *refreshTimer;
    int tick;
    BOOL isAlive;
}

@property (assign) id<BaseScoreBoardDelegate> delegate;
@property (assign) Game* currentGame;
@property BOOL readOnly;

- (void) changeOrientation: (int) orientation;
- (void) showGame:(Game*)game;
- (void) updateGameScore:(GameScore*)score;
- (void) keyPadPressed:(KeyPadEnum)key;

- (void) startTimer;
- (void) stopTimer;
// need to call this method for this control to release (deadloc)
- (void) killTimer;

//- (void) stopTimeRemainTimer;
/*
- (void) updateTeam1Score:(int) score;
- (void) updateTeam2Score:(int)score;
- (void) updateQtr:(int) qtr;
- (void) updateMinutes:(int)minutes;
- (void) updateSeconds:(int)seconds;

- (void) updateOuts:(int)value;
- (void) updateBalls:(int)value;
- (void) updateStrikes:(int)value;

- (void) updateLastUpdated:(NSDate*) lastUpdated;

- (void) updateTimeElapsed:(int) seconds;

- (void) updateGameScoreFromGameBox: (GameBox*) gameBox;
 // hilight specific score value
 - (void) activeScore:(int) kind;
*/

// raise refresh score event manually (instead of timer)
- (void) refresh;

@end


