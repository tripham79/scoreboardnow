

#import <UIKit/UIKit.h>
#import "CommonFunction.h"
#import "GameBox.h"

@class ScoreBoard2;

@protocol ScoreBoardDelegate <NSObject>

- (void) refresh:(ScoreBoard2*) scoreBoard;

@end

@interface ScoreBoard2 : UIView{
    BOOL isGettingScore;
    NSTimer *refreshTimer;
    int tick;
    
    int timeRemain;
    NSTimer *timeRemainTimer;
    BOOL isUpdateTimeRemain;
    
    BOOL isAlive;
}

@property (assign) id<ScoreBoardDelegate> delegate;
@property (retain) IBOutlet UIView *portrait;
@property (retain) IBOutlet UIView *landscape;
@property (retain) NSString *tempMinutes;
@property (retain) NSString *tempSeconds;

- (UIButton*) getTeam1Button;
- (UIButton*) getTeam2Button;
- (UIButton*) getQtrButton;
- (UIButton*) getMinutesButton;
- (UIButton*) getSecondsButton;

//- (NSString*) getMinutes;
//- (NSString*) getSeconds;

- (void) changeOrientation: (int) orientation;

- (void) startTimer;
- (void) stopTimer;
- (void) stopTimeRemainTimer;
- (void) updateTeam1Score:(int) score;
- (void) updateTeam2Score:(int)score;
- (void) updateQtr:(int) qtr;
- (void) updateMinutes:(int)minutes;
- (void) updateSeconds:(int)seconds;
- (void) updateLastUpdated:(NSDate*) lastUpdated;

- (void) updateTimeElapsed:(int) seconds;

- (void) updateGameScoreFromGameBox: (GameBox*) gameBox;

- (void) activeScore:(int) kind;

- (void) refresh;
@end
 

