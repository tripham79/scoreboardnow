
#import <UIKit/UIKit.h>
#import "Notification.h"
#import "Game.h"

@class GameBox;
@protocol GameBoxDelegate <NSObject>

- (void) performClick:(GameBox*) gameBox;

@end

@interface GameBox : UIView{
    int status;
    UIImageView *backgroundImage;
    UIImageView *sportLogo;
    Notification *notification;
    
    UILabel *homeTeamName;
    UILabel *homeTeamScore;
    
    UILabel *awayTeamName;
    UILabel *awayTeamScore;
    
    UILabel *desciption;
    
    UIColor *textColor;
    int _logoSize;
}

- (void) setStatus:(int)aStatus;
- (void) setNumberNorification:(int)number;

@property int gameId;
@property int logoSize;
@property (retain) Game *game;
@property (assign) id<GameBoxDelegate> delegate;
@property (retain) UILabel *homeTeamName;
@property (retain) UILabel *homeTeamScore;
@property (retain) UILabel *awayTeamName;
@property (retain) UILabel *awayTeamScore;
@property (retain) UILabel *desciption;

- (void) updateGameScore;
- (void) updateGameStatus;
@end
