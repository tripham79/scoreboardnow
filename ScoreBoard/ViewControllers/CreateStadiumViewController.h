//
//  CreateStadiumViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"
#import "MyDelegates.h"
#import "SelectLocationViewController.h"

@interface CreateStadiumViewController : BaseViewController<SelectLocationDelegate>

@property (retain, nonatomic) Stadium* stadium;
@property (retain, nonatomic) IBOutlet UITextField *txtStadiumName;
@property (retain, nonatomic) IBOutlet UITextField *txtAddress;
@property (retain, nonatomic) IBOutlet UITextField *txtCity;
@property (retain, nonatomic) IBOutlet UILabel *lblGPSLocation;
@property (assign) id<StadiumDelegate> delegate;

- (IBAction)btnAddClick:(id)sender;
- (IBAction)btnMapClick:(id)sender;

@end
