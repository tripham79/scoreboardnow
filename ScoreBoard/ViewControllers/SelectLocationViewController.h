//
//  SelectLocationViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/8/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BaseViewController.h"

@class SelectLocationViewController;

@protocol SelectLocationDelegate <NSObject>
//- (void) selectedLocation:(NSString*)address lat:(double)lat lng:(double)lng;
- (void) selectedLocation:(SelectLocationViewController*)vc;
@end

@interface SelectLocationViewController : BaseViewController<UIGestureRecognizerDelegate, MKMapViewDelegate>

@property (retain, nonatomic) NSString* locationAddress;
@property (retain, nonatomic) NSString* locationCity;
@property (retain, nonatomic) NSString* locationCountry;
@property (retain, nonatomic) NSString* locationCountryCode;
@property double locationLat;
@property double locationLng;

@property (assign) id<SelectLocationDelegate> delegate;
@property (retain, nonatomic) IBOutlet UILabel *lblGPSLocation;
@property (retain, nonatomic) IBOutlet MKMapView *map;
@property (retain, nonatomic) IBOutlet UITextField *txtAddress;
- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnDoneClick:(id)sender;
- (IBAction)btnSearchClick:(id)sender;

@end
