//
//  CreateGameViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateGameViewController.h"
#import "MyCreatedGameViewController.h"
#import "SearchTeamResponse.h"
#import "SearchStadiumResponse.h"
#import "SelectTeamViewController.h"
#import "SelectStadiumViewController.h"
#import "CreateGameResponse.h"

@interface CreateGameViewController ()

@end

@implementation CreateGameViewController {
    UITextField* _editingTextField;
    
    // changes are only apply when save
    Team* selectedHomeTeam;
    Team* selectedGuestTeam;
    Stadium* selectedStadium;
    
    NSDateFormatter *dateFormatter;
    bool confirmedCreateDuplicatedGame;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.dtpViewContainer.hidden = YES;
    
    confirmedCreateDuplicatedGame = NO;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    //UIImage* img = [self.btnSave backgroundImageForState:UIControlStateNormal];
    //img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    //[self.btnSave setBackgroundImage:img forState:UIControlStateNormal];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)];
    [cancelButton setTintColor:[UIColor whiteColor]];
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           cancelButton,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.txtDuration.inputAccessoryView = numberToolbar;
    self.txtDuration.delegate = self;
    
    [self createDatePicker];
    self.txtScheduledStartTime.inputView = datePicker;
    self.txtScheduledStartTime.inputAccessoryView = datePickerToolbar;
    self.txtScheduledStartTime.delegate = self;
    
    [self.txtHomeTeam setReturnKeyType:UIReturnKeyDone];
    self.txtHomeTeam.delegate = self;
    [self.txtGuestTeam setReturnKeyType:UIReturnKeyDone];
    self.txtGuestTeam.delegate = self;
    [self.txtStadium setReturnKeyType:UIReturnKeyDone];
    self.txtStadium.delegate = self;

    if (self.game.gameID.length == 0)
        self.btnDelete.hidden = YES;
    
    [self registerForKeyboardNotifications];
    [self.mainScrollView setContentSize:CGSizeMake(self.mainScrollView.frame.size.width, self.mainScrollView.frame.size.height)];
    
    [self showGameInfo];
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.mainScrollView addGestureRecognizer:tapGes];
}

- (void)hideKeyboard {
    [self.mainScrollView endEditing:YES];
}

- (void)showGameInfo {
    if (self.game == nil)
        return;
    
    if (self.game.gameNumber.length > 0)
        self.lblGameNumber.text = self.game.gameNumber;
    
    self.lblHomeTeam.text = @"";
    [self.btnHomeTeam setTitle:self.game.team1Name forState:UIControlStateNormal];
    
    self.lblGuestTeam.text = @"";
    [self.btnGuestTeam setTitle:self.game.team2Name forState:UIControlStateNormal];
    
    self.lblStadium.text = @"";
    [self.btnStadium setTitle:self.game.stadiumName forState:UIControlStateNormal];
    
    self.txtDuration.text = self.game.duration;
    
    //NSDate* localDate;
    if (self.game.scheduledStartTime == nil)
        self.game.scheduledStartTime = [NSDate date];
        
    if (self.game.scheduledStartTime) {
        //localDate = [self.game.scheduledStartTime toLocalDate];
        self.txtScheduledStartTime.text = [dateFormatter stringFromDate:self.game.scheduledStartTime];
    }
}

- (void)cancelNumberPad {
    [_editingTextField resignFirstResponder];

    // restore old value
    if (_editingTextField == self.txtDuration)
        [self.txtDuration setText:self.game.duration];
}

- (void)doneWithNumberPad{
    [_editingTextField resignFirstResponder];
    
    // apply new value
    if (_editingTextField == self.txtDuration) {
        //self.game.duration = self.txtDuration.text;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [super textFieldDidBeginEditing:textField];
    _editingTextField = textField;
    
    if (textField == self.txtScheduledStartTime) {
        // this will cause textFieldDidEndEditing run, which set activeField to null
        //[self.txtScheduledStartTime resignFirstResponder];
        activeField = textField;
        
        // show date picker (textField.text in LocalTimeZone already)
        datePicker.date = [dateFormatter dateFromString:textField.text];
        
        //if (self.game.scheduledStartTime)
        //    self.dtpDatePicker.date = [self.game.scheduledStartTime toLocalDate];
        
        //self.dtpViewContainer.hidden = NO;
        
        // to adjust scroll view content offset
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
        [userInfo setValue:[NSValue valueWithCGRect:datePicker.frame] forKey:UIKeyboardFrameBeginUserInfoKey];
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:UIKeyboardWillShowNotification object:nil userInfo:userInfo]];
    }
}

- (void)datePickerCancel {
    [super datePickerCancel];
    
    // to adjust scroll view content offset
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:UIKeyboardWillHideNotification object:nil userInfo:nil]];
}

- (void)datePickerDone {
    [super datePickerDone];
    
    NSString* dateText = [dateFormatter stringFromDate:datePicker.date];
    if (_editingTextField == self.txtScheduledStartTime) {
        [self.txtScheduledStartTime setText:dateText];
    }
    
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:UIKeyboardWillHideNotification object:nil userInfo:nil]];
}

/*- (IBAction)btnCalendarCancel:(id)sender {
    [super textFieldDidEndEditing:self.txtScheduledStartTime];
    
    // hide date picker
    self.dtpViewContainer.hidden = YES;
    // to adjust scroll view content offset
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:UIKeyboardWillHideNotification object:nil userInfo:nil]];
}

- (IBAction)btnCalendarDone:(id)sender {
    [super textFieldDidEndEditing:self.txtScheduledStartTime];
    NSString* dateText = [dateFormatter stringFromDate:self.dtpDatePicker.date];
    
    if (_editingTextField == self.txtScheduledStartTime) {
        [self.txtScheduledStartTime setText:dateText];
        //self.scheduledStartTime = [self.dtpDatePicker.date toUTCDate];
        //NSLog(@"%@", self.scheduledStartTime);
    }
    
    // hide date picker
    self.dtpViewContainer.hidden = YES;
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:UIKeyboardWillHideNotification object:nil userInfo:nil]];

}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_lblGameNumber release];
    [_lblHomeTeam release];
    [_lblGuestTeam release];
    [_lblStadium release];
    [_txtHomeTeam release];
    [_txtGuestTeam release];
    [_txtStadium release];
    [_txtScheduledStartTime release];
    [_txtDuration release];
    //[_dtpDatePicker release];
    //[_dtpViewContainer release];
    [_btnSave release];
    [_btnDelete release];
    [_btnHomeTeam release];
    [_btnGuestTeam release];
    [_btnStadium release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLblGameNumber:nil];
    [self setLblHomeTeam:nil];
    [self setLblGuestTeam:nil];
    [self setLblStadium:nil];
    [self setTxtHomeTeam:nil];
    [self setTxtGuestTeam:nil];
    [self setTxtStadium:nil];
    [self setTxtScheduledStartTime:nil];
    [self setTxtDuration:nil];
    //[self setDtpDatePicker:nil];
    //[self setDtpViewContainer:nil];
    [self setBtnSave:nil];
    [self setBtnDelete:nil];
    [self setBtnHomeTeam:nil];
    [self setBtnGuestTeam:nil];
    [self setBtnStadium:nil];
    [super viewDidUnload];
}

- (void) selectItemViewController:(id)sender didSelectedRowAtIndex:(int)index {
    if ([sender isKindOfClass:[SelectTeamViewController class]])
    {
        SelectTeamViewController* vc = (id)sender;
        id sourceView = vc.sourceView;
        
        if (sourceView == self.txtHomeTeam) {
            selectedHomeTeam = [vc.teams objectAtIndex:index];
            self.lblHomeTeam.text = selectedHomeTeam.teamName;
            
            // set stadium to home team's stadium
            if (selectedHomeTeam.stadium) {
                selectedStadium = selectedHomeTeam.stadium;
                
                self.lblStadium.text = selectedStadium.Address;
                [self.btnStadium setTitle:selectedStadium.Name forState:UIControlStateNormal];
            }
        }
        else if (sourceView == self.txtGuestTeam) {
            selectedGuestTeam = [vc.teams objectAtIndex:index];
            self.lblGuestTeam.text = selectedGuestTeam.teamName;
        }
        else if (sourceView == self.btnHomeTeam) {
            selectedHomeTeam = [vc.teams objectAtIndex:index];
            [self.btnHomeTeam setTitle:selectedHomeTeam.teamName forState:UIControlStateNormal];
            self.lblHomeTeam.text = selectedHomeTeam.teamNumber;
            
            // set stadium to home team's stadium
            if (selectedHomeTeam.stadium) {
                selectedStadium = selectedHomeTeam.stadium;
                
                self.lblStadium.text = selectedStadium.Address;
                [self.btnStadium setTitle:selectedStadium.Name forState:UIControlStateNormal];
            }
        }
        else if (sourceView == self.btnGuestTeam) {
            selectedGuestTeam = [vc.teams objectAtIndex:index];
            [self.btnGuestTeam setTitle:selectedGuestTeam.teamName forState:UIControlStateNormal];
            self.lblGuestTeam.text = selectedGuestTeam.teamNumber;
        }
    }
    else if ([sender isKindOfClass:[SelectStadiumViewController class]]) {
        SelectStadiumViewController* vc = (id)sender;
        id sourceView = vc.sourceView;
        
        if (sourceView == self.btnStadium) {
            selectedStadium = [vc.stadiums objectAtIndex:index];
            
            self.lblStadium.text = selectedStadium.Address;
            [self.btnStadium setTitle:selectedStadium.Name forState:UIControlStateNormal];
        }
    }
}

- (IBAction)btnSearchHomeTeamClick:(id)sender {
    [self hideKeyboard];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"Searching" animated:YES];
    NSString* searchText = self.txtHomeTeam.text;
    
    [[SBNService sharedInstance] searchTeamsWithStadium:0 pageSize:15 searchText:searchText sportId:0 withCallback:^(BaseRequest *request, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        SearchTeamResponse* response = (id)result;
        if ([response.teams count] == 0) {
            showAlert(@"No team found.", @"", nil);
            return;
        }
        
        SelectTeamViewController* vc = [[SelectTeamViewController alloc] init];
        vc.teams = response.teams;
        vc.totalRecords = response.totalRecords;
        vc.delegate = self;
        vc.sourceView = self.txtHomeTeam;
        vc.searchText = searchText;
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

- (IBAction)btnSearchGuestTeamClick:(id)sender {
    [self hideKeyboard];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"Searching" animated:YES];
    NSString* searchText = self.txtGuestTeam.text;
    
    [[SBNService sharedInstance] searchTeamsWithStadium:0 pageSize:15 searchText:searchText sportId:0 withCallback:^(BaseRequest *request, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        SearchTeamResponse* response = (id)result;
        if ([response.teams count] == 0) {
            showAlert(@"No team found.", @"", nil);
            return;
        }
        
        SelectTeamViewController* vc = [[SelectTeamViewController alloc] init];
        vc.teams = response.teams;
        vc.totalRecords = response.totalRecords;
        vc.delegate = self;
        vc.sourceView = self.txtGuestTeam;
        vc.searchText = searchText;
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

- (IBAction)btnSearchStadiumClick:(id)sender {
    [self hideKeyboard];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"Searching" animated:YES];
    NSString* searchText = self.txtStadium.text;
    
    [[SBNService sharedInstance] searchStadiums:0 pageSize:15 searchText:searchText withCallback:^(BaseRequest *request, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        SearchStadiumResponse* response = (id)result;
        if ([response.stadiums count] == 0) {
            showAlert(@"No stadium found.", @"", nil);
            return;
        }
        
        SelectStadiumViewController* vc = [[SelectStadiumViewController alloc] init];
        vc.stadiums = response.stadiums;
        vc.totalRecords = response.totalRecords;
        vc.delegate = self;
        vc.sourceView = self.txtStadium;
        vc.searchText = searchText;
        [self.navigationController pushViewController:vc animated:YES];
    }];
}

- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSaveClick:(id)sender {
    
    // if user set/change home team
    if (selectedHomeTeam) {
        self.game.team1ID = selectedHomeTeam.teamID;
        self.game.team1Name = selectedHomeTeam.teamName;
        
        self.game.sportID = selectedHomeTeam.sportID;
        self.game.sportName = selectedHomeTeam.sportName;
        self.game.leagueName = selectedHomeTeam.leagueName;
        self.game.divisionName = selectedHomeTeam.divisionName;
    }
    
    // if user set/change guest team
    if (selectedGuestTeam) {
        self.game.team2ID = selectedGuestTeam.teamID;
        self.game.team2Name = selectedGuestTeam.teamName;
    }
    
    self.game.scheduledStartTime = [dateFormatter dateFromString:self.txtScheduledStartTime.text];
    
    // if user set/change stadium
    if (selectedStadium) {
        self.game.stadiumID = selectedStadium.Id;
        self.game.stadiumName = selectedStadium.Name;
    }
    
    self.game.duration = self.txtDuration.text;
    
    // check if user has enter all information
    if (self.game.team1ID.length == 0 || self.game.team2ID.length == 0 || self.game.stadiumID.length == 0 || self.game.scheduledStartTime == nil || self.game.duration.length == 0) {
        showAlert(@"", @"Please enter all information to create game.", nil);
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"Saving" animated:YES];
    
    if ([self.game.gameID intValue] > 0) {
        // update game
        [[SBNService sharedInstance] updateGame:self.game confirmedDuplicated:confirmedCreateDuplicatedGame withCallback:^(BaseRequest *request, id result, NSError *error)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            // if has error then stop
            if ([self handleServiceError:result error:error])
                return;
            
            CreateGameResponse* response = (id)result;
            
            if ([response.ErrorCode isEqualToString:ERROR_DUPLICATED_GAME] && !confirmedCreateDuplicatedGame) {
                [self confirmDuplicate:response];
                return;
            }
            
            //save change
            [self.game updateDatabase];
            
            //back to my games
            MyCreatedGameViewController* listContoller = (id)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
            [listContoller gameSaved:self.game isAdded:NO];
            
            [self.navigationController popViewControllerAnimated:YES];

        }];
    }
    else {
        // add new game
        [[SBNService sharedInstance] addGame:self.game confirmedDuplicated:confirmedCreateDuplicatedGame withCallback:^(BaseRequest *request, id result, NSError *error)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            // if has error then stop
            if ([self handleServiceError:result error:error])
                return;
            
            CreateGameResponse* response = (id)result;
            
            if ([response.ErrorCode isEqualToString:ERROR_DUPLICATED_GAME] && !confirmedCreateDuplicatedGame) {
                [self confirmDuplicate:response];
                return;
            }
            
            // set new game ID & number
            self.game.gameID = [NSString stringWithFormat:@"%d", response.gameId];
            self.game.gameNumber = response.gameNumberText;
            
            // subscribe game to self-created game
            NSMutableArray* arrGameIds = [[NSMutableArray alloc] init];
            [arrGameIds addObject:[NSNumber numberWithInt:response.gameId]];
            
            [[SBNService sharedInstance] subscribeGames:arrGameIds withCallback:^(BaseRequest *request, id result, NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                // if has error then stop
                if ([self handleServiceError:result error:error])
                    return;
                
                // insert database
                self.game.addedManual = TRUE;
                self.game.user = [CommonFunction getDataWithKey:kUser];
                
                [self.game insertDatabase];
                
                // back to my created game list page
                MyCreatedGameViewController* listContoller = (id)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
                [listContoller gameSaved:self.game isAdded:YES];
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
        }];
    }
}

// confirm if user still want to create or update game
- (void)confirmDuplicate:(CreateGameResponse*)response {
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateStyle:NSDateFormatterNoStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
    
    //NSString* msg = response.errorMessage;
    NSString* msg = [NSString stringWithFormat:@"Another game (number #%@) between these two teams has already scheduled on the same day at %@.", response.gameNumberText, [df stringFromDate:response.scheduledStartTime]];
    
    msg = [msg stringByAppendingString:@" Do you still want to continue?"];
    
    showQuestionWithBlock(@"", msg, ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0)
            return;
        
        confirmedCreateDuplicatedGame = YES;
        [self btnSaveClick:self.btnSave];
    });
}

- (IBAction)btnDeleteClick:(id)sender {
    showQuestionWithBlock(@"", @"Are you sure you want to delete this game?", ^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0)
            return;
        
        [MBProgressHUD showHUDAddedTo:self.view text:@"Deleting" animated:YES];
        
        [[SBNService sharedInstance] deleteGame:[self.game.gameID intValue] withCallback:^(BaseRequest *request, id result, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            // if has error then stop
            if ([self handleServiceError:result error:error])
                return;
            
            MyCreatedGameViewController* listContoller = (id)[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count]-2];
            [listContoller gameDeleted:self.game];
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
    });
}

- (IBAction)btnHomeTeamClick:(id)sender {
    [self hideKeyboard];
    
    SelectTeamViewController* vc = [[SelectTeamViewController alloc] init];
    vc.delegate = self;
    vc.sourceView = self.btnHomeTeam;
    
    if (selectedGuestTeam != nil)
        vc.sportName = selectedGuestTeam.sportName;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnGuestTeamClick:(id)sender {
    [self hideKeyboard];
    
    SelectTeamViewController* vc = [[SelectTeamViewController alloc] init];
    vc.delegate = self;
    vc.sourceView = self.btnGuestTeam;
    
    if (selectedHomeTeam != nil)
        vc.sportName = selectedHomeTeam.sportName;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnStadiumClick:(id)sender {
    [self hideKeyboard];
    
    SelectStadiumViewController* vc = [[SelectStadiumViewController alloc] init];
    vc.delegate = self;
    vc.sourceView = self.btnStadium;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
