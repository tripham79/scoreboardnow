//
//  CreateDivisionViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateDivisionViewController.h"

@interface CreateDivisionViewController ()

@end

@implementation CreateDivisionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerForKeyboardNotifications];
    
    self.lblSportName.text = self.sport.sportName;
    self.lblLeagueName.text = self.league.leagueName;
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)btnAddClick:(id)sender {
    NSString* name = [self.txtDivisionName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (name.length == 0) {
        showAlert(@"Please enter division name.", @"", nil);
        return;
    }
    
    CreateDivisionRequest* req = [[CreateDivisionRequest alloc] init];
    req.DivisionName = name;
    req.LeagueId = [NSNumber numberWithInt:[self.league.leagueId intValue]];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
    [[SBNService sharedInstance] createDivision:req successHandler:^(CreateDivisionResult *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:nil])
            return;
        
        Division* div = [[Division alloc] init];
        div.divisionID = [result.DivisionId stringValue];
        div.leagueId = self.league.leagueId;
        div.divisionName = name;
        if (self.delegate != nil)
            [self.delegate createdDivision:div];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } failHandler:^(NSError *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self handleServiceError:nil error:err];
    }];
}

- (void)dealloc {
    [_lblSportName release];
    [_lblLeagueName release];
    [_txtDivisionName release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setLblSportName:nil];
    [self setLblLeagueName:nil];
    [self setTxtDivisionName:nil];
    [super viewDidUnload];
}
@end
