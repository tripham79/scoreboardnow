
#import "AboutViewController.h"

@implementation AboutViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) sendMailTo:(NSString *)address subject:(NSString *)subject{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            
            [picker setSubject:subject];
            
            // Set up recipients
            NSArray *toRecipients =[NSArray arrayWithObject: address]; 
            [picker setToRecipients:toRecipients];
            
            // Fill out the email body text 
            NSString *emailBody = @"";
            [picker setMessageBody:emailBody isHTML:YES];
            [self presentModalViewController:picker animated:YES];
            [picker release];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] 
                                  initWithTitle:@"Message" 
                                  message:@"Please config email in Mail Application first" 
                                  delegate:nil 
                                  cancelButtonTitle:@"Ok!" 
                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle:@"Message" 
                              message:@"Please config email in Mail Application first" 
                              delegate:nil 
                              cancelButtonTitle:@"Ok!" 
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) openURL:(NSString *)url{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (IBAction)btnInfoTapped:(id)sender {
    [self openURL:@"http://www.scoreboardnow.com"];
}

- (IBAction)btnContactTapped:(id)sender {
    [self sendMailTo:@"info@scoreboardnow.com" subject:@""];
}

- (IBAction)btnSupportTapped:(id)sender {
    [self sendMailTo:@"support@scoreboardnow.com" subject:@"SBN Help Request"];
}

- (IBAction)btnRequestTapped:(id)sender {
    [self sendMailTo:@"schedules@scoreboardnow.com" subject:@"SBN Schedule Request"];
}

- (IBAction)btnRateItTapped:(id)sender {
    [self openURL:@"http://www.scoreboardnow.com/ratesbn.aspx"];
}

- (IBAction)btnFAQTapped:(id)sender {
    [self openURL:@"http://www.scoreboardnow.com/faqs.aspx"];
}

- (IBAction)btnTermTapped:(id)sender {
    [self openURL:@"http://www.scoreboardnow.com/terms.aspx"];
}

- (IBAction)btnPolicyTapped:(id)sender {
    [self openURL:@"http://www.scoreboardnow.com/privacy.aspx"];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissModalViewControllerAnimated:YES];
}

@end
