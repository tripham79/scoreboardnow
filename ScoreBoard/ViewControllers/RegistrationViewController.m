
#import "RegistrationViewController.h"

@implementation RegistrationViewController
@synthesize firstNameField;
@synthesize lastNameField;
@synthesize emailField;
@synthesize zipCodeField;
@synthesize passwordField;
@synthesize confirmPasswordField;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setFirstNameField:nil];
    [self setLastNameField:nil];
    [self setEmailField:nil];
    [self setZipCodeField:nil];
    [self setPasswordField:nil];
    [self setConfirmPasswordField:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [firstNameField release];
    [lastNameField release];
    [emailField release];
    [zipCodeField release];
    [passwordField release];
    [confirmPasswordField release];
    [super dealloc];
}

- (IBAction)btnCreateAccountTapped:(id)sender {
    if (focusedField != nil)
    {
        [focusedField resignFirstResponder];
        [self slideFrameWhenUnFocus:focusedField];
    }
    
    NSString *firstName = firstNameField.text;
    NSString *lastName = lastNameField.text;
    NSString *email = emailField.text;
    NSString *zipCode = zipCodeField.text;
    NSString *password = passwordField.text;
    NSString *confirmPassword = confirmPasswordField.text;
    
    if ([firstName isEqualToString:@""] || [lastName isEqualToString:@""] || [email isEqualToString:@""] || [zipCode isEqualToString:@""] || [password isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"All field are required." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    else if (![CommonFunction validateEmail:email])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email is invalid." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    else if (![password isEqualToString:confirmPassword])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Confirm password is not match." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildRegistrationBody:email firstName:firstName lastName:lastName password:password zipCode:zipCode];
    [self sendRequest];
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNRegisterResponse *response = [[SBNRegisterResponse alloc] init];
    [response parseResponseData:responseData];
    
    if ([response.responseCode isEqualToString:@""]) return;
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]){
        [[[SharedComponent getInstance] navigationController] popViewControllerAnimated:YES];
    }
    else
    {
        NSString *errorMessage = @"Error";
        if (![response.errorMessage isEqualToString:@""])
        {
            errorMessage = response.errorMessage;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }

    [responseData release];
    [response release];
}

@end
