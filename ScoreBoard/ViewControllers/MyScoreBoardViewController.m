typedef enum MyScoreBoardScreenStep{
    GetGameScore = 0,
    GetGameStatus
} MyScoreBoardScreenStep;

#import "MyScoreBoardViewController.h"
#import "GameListViewController.h"
#import "NSEnumerator+Linq.h"

@implementation MyScoreBoardViewController
@synthesize alertSetting;
@synthesize dateLabel;
@synthesize refreshStatus;
@synthesize listGamebox;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //insert option "Never" for refreshOption
    GameScoreRefreshOption *neverOption = [[GameScoreRefreshOption alloc] init];
    [neverOption setOptionId:@"0"];
    [neverOption setOptionName:@"Never"];
    [neverOption setOptionSeconds:@"0"];
    [[[SharedComponent getInstance] listGameScoreRefreshOption] addObject:neverOption];
    [neverOption release];
    
    NSLog(@"refresh seconds %d", [[SharedComponent getInstance] refreshSeconds]);
    NSLog(@"RefreshOptions %d", [[SharedComponent getInstance] listGameScoreRefreshOption].count);
    NSLog(@"Leafue %d", [[SharedComponent getInstance] listLeague].count);
    
    y = 0;
    x = 15;
    
    isGettingScore = NO;
    
    NSDate *now = [NSDate date];
    NSLog(@"%@", now);
    
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"yyyyMMdd hh:mm:ss a";
    
    //Get UTC time from Now
    /*
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *utcNow = [dateFormatter dateFromString:[dateFormatter stringFromDate:now]];
    NSString *utcString = [dateFormatter stringFromDate: utcNow];
    NSLog(@"UCT Now %@", [dateFormatter stringFromDate: utcNow]);
    */
    
    //Get local time from now
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *cstNow = [dateFormatter dateFromString: [dateFormatter stringFromDate:now]];
    
    /*
    NSString *message = [NSString stringWithFormat:@"CST %@", cstString];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    [alert release];
    */
     
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    NSDateComponents *components = [calendar components:( NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekCalendarUnit ) fromDate:cstNow];
    
    [components setDay:([components day] - ([components weekday] - 1))]; 
    
    weekNumber = [components week];
    fromDate  = [[calendar dateFromComponents:components] retain];
    toDate = [[fromDate dateByAddingTimeInterval:604799] retain];
    
    
    [self changeDateLabel];
    
    self.listGamebox = [[NSMutableDictionary alloc] init];
    [[SharedComponent getInstance] setListCurrentGameBox: self.listGamebox];
    
    //load status On/Off Alert
    [alertSetting setDelegate:self];
    if ([[CommonFunction getDataWithKey:Notification_Alert] isEqualToString:@"YES"]){
        [alertSetting setStatus:YES];
    }
    else
    {
        [alertSetting setStatus:NO];
    }
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    //tick = [[SharedComponent getInstance] gameScoreRefreshSeconds];
    tick = [[SharedComponent getInstance] refreshSeconds];
    
    [self syncData];
    
    [self synchNewGames:FALSE];
}

- (void)syncData {
    [[SBNService sharedInstance] syncData:[[AuthenticatedJsonRequest alloc] init] successHandler:^(BaseResponse *result)
     {
         if (![result.IsSuccess boolValue] && [result.ErrorMessage isEqualToString:INVALID_LOGIN_TOKEN])
         {
             // show login page
             [self logout];
         }
     } failHandler:^(NSError *err) {
         showAlert(@"", [NSString stringWithFormat:@"Synchronize new teams and games failed: %@", err.localizedDescription], nil);
     }];
}

- (void) switchChangeStatus:(BOOL)status{
    if (status)
        [CommonFunction saveData:@"YES" withKey:Notification_Alert];
    else
        [CommonFunction saveData:@"NO" withKey:Notification_Alert];
}

- (void) changeDateLabel{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormatter.dateFormat = @"MM/dd/yyyy";
    
    NSString *dateTitle = [NSString stringWithFormat:@"Week%d: %@ - %@", weekNumber, [dateFormatter stringFromDate:fromDate], [dateFormatter stringFromDate: toDate]];
    [dateLabel setText:dateTitle];
}

- (IBAction)previousWeekTapped:(id)sender {
    fromDate = [[fromDate dateByAddingTimeInterval:-604800] retain];
    toDate = [[fromDate dateByAddingTimeInterval:604799] retain];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:( NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekCalendarUnit ) fromDate:fromDate];
    weekNumber = [components week];
    [self changeDateLabel];
    [self reloadGamebox];
}

- (IBAction)nextWeekTapped:(id)sender {
    fromDate = [[fromDate dateByAddingTimeInterval:604800] retain];
    toDate = [[fromDate dateByAddingTimeInterval:604799] retain];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:( NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSWeekCalendarUnit ) fromDate:fromDate];
    weekNumber = [components week];
    
    [self changeDateLabel];
    [self reloadGamebox];
}

- (void)reloadGamebox {
    for (NSString *key in self.listGamebox){
        GameBox *gamebox = [self.listGamebox objectForKey:key];
        [gamebox removeFromSuperview];
    }
    
    y = 0;
    x = 10;
    [self.mainScrollView setContentSize:CGSizeMake(320, 354)];
    [self.listGamebox removeAllObjects];
    
    NSArray *list = [Game loadGames:fromDate toDate:toDate];
    for (Game *game in list)
        [self insertNewGame:game];

    /*
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *startDate = [dateFormatter stringFromDate:fromDate];
    NSString *endDate = [dateFormatter stringFromDate:toDate];
    
    NSLog(@"reloadGamebox %@ - %@", startDate, endDate);
    // Open the database from the users filessytem
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        NSString *selectedSportId = [CommonFunction getDataWithKey:MyScoreBoard_ShowSport];
        if (selectedSportId == nil)
            selectedSportId = @"";
        
        if (![selectedSportId isEqualToString:@""])
            selectedSportId = [NSString stringWithFormat:@" AND SportId = '%@' ", selectedSportId];
                                       
        //check game is exist
        query = [NSString stringWithFormat: @"select * from games where ScheduledStartTime>='%@' AND ScheduledStartTime<'%@' AND User='%@' %@ ORDER BY ScheduledStartTime ASC", startDate, endDate, [CommonFunction getDataWithKey:kUser], selectedSportId];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                Game *game = [[Game alloc] initFromDatabase:compiledStatement];
                [self insertNewGame:game];
                [game release];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);*/
}

- (void) timerTick{

    if (isActive){
        tick--;
        NSString *status = [NSString stringWithFormat:@"Refreshing\nin %d sec", tick];
        [refreshStatus setText:status];
        
        if (tick == 0)
        {
            [self getGameScore];
            [self synchNewGames:FALSE];
        }
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isActive = YES;
    
    [self reloadGamebox];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    isActive = NO;
}

- (void) performClick:(GameBox *)gameBox{

    GameFeedViewController *viewController = [[GameFeedViewController alloc] initWithNibName:@"GameFeedViewController" bundle:nil];
    //[viewController setSelectedGameBox:gameBox];
    [viewController setSelectedGame:gameBox.game];
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (void) insertNewGame:(Game *)game{
    if (game.scheduledStartTime == nil || [game.scheduledStartTime compare:fromDate] ==  NSOrderedAscending || [game.scheduledStartTime compare:toDate] == NSOrderedDescending) return;
    
    int topLogoSize = 20;
    GameBox *box = [[GameBox alloc] initWithFrame:CGRectMake(x, y, 145, 105 + topLogoSize)];
    
    [self.mainScrollView addSubview:box];
    [box setDelegate:self];
    [box setGame: game];
    [box updateGameScore];
    
    if (x <= 15) 
    {
        x = 165;
        [self.mainScrollView setContentSize:CGSizeMake(320, y + 118 + topLogoSize)];
    }
    else
    {
        x = 10;
        y = y  + 118 + topLogoSize;
        [self.mainScrollView setContentSize:CGSizeMake(320, y)];
    }
    

    
    [self.listGamebox setObject:box forKey:game.gameID];
}

- (void) removeGame:(NSString*)gameId{
    for (NSString *gameBoxId in listGamebox){
        if ([gameBoxId isEqualToString:gameId]){
            [[listGamebox objectForKey:gameBoxId] removeFromSuperview];
            [listGamebox removeObjectForKey:gameBoxId];
        }
    }
}

- (void) getGameScore
{
    if (isGettingScore) return;
    if ([[self.listGamebox allKeys] count] == 0)
    {
        tick = [[SharedComponent getInstance] refreshSeconds];
        return;
    }
    
    isGettingScore = YES;
    isActive = NO;
    [refreshStatus setText:@"Getting score...."];
    
    currentStep = GetGameScore;
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    
    if (sbnGetScoreRequest == nil){
        sbnGetScoreRequest = [[SBNRequest alloc] init];
    }
    
    NSMutableArray *listGameId = [[NSMutableArray alloc] init]; //autorelease

    for (NSString* key in self.listGamebox) {
        GameBox *gameBox = [self.listGamebox objectForKey:key];
        [listGameId addObject:gameBox.game.gameID];
    }

    [sbnGetScoreRequest buildGetScoreBody:loginToken listGameId:listGameId];
    [self sendGetScoreRequest];
}

- (void) handleGeScoreResponse
{
    if ([sbnGetScoreRequest.receivedData length] <= 0){
        isGettingScore = NO;        
        return;
    }
    
    NSString *responseData = [[NSString alloc] initWithData: sbnGetScoreRequest.receivedData encoding:NSUTF8StringEncoding];
    
    [sbnGetScoreRequest release];
    sbnGetScoreRequest = nil;
    
    
    SBNResponse *response;
    switch (currentStep) {
        case GetGameScore:
            response = [[SBNGameScoreResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetGameStatus:
            response = [[SBNGetGameStatusResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        default:
            response = [[SBNResponse alloc] init];
            [response parseResponseData:responseData];
            break;
    }
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL])
    {
        switch (currentStep) {
            case GetGameScore:
                {
                    SBNGameScoreResponse *gamScoreResponse = (SBNGameScoreResponse*)response;
                    NSMutableArray *listGameScore = [gamScoreResponse listGameScore];
                    for (GameScore *gameScore in listGameScore)
                    {
                        GameBox *gameBox = [listGamebox objectForKey:gameScore.gameId];
                        if (gameBox != nil){
                            //[gameBox.game updateScore:gameScore.team1Score team2Score:gameScore.team2Score qtr:gameScore.qtr lastUpdated:gameScore.lastUpdated Hours:gameScore.hours Minutes:gameScore.minutes Seconds:gameScore.seconds numberCheckedInUser:gameScore.numberCheckedInUser];
                            [gameBox.game updateScore:gameScore];
                            [gameBox updateGameScore];
                            
                            //update game in database
                            [gameBox.game updateDatabase];
                            
                            /*
                            NSString *databasePath = [[SharedComponent getInstance] databasePath];
                            sqlite3 *database;
                            const char *sqlStatement;
                            sqlite3_stmt *compiledStatement;
                            NSString *query = @"";
                            
                            // Open the database from the users filessytem
                            if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK){
                                //check game is exist
                                query = [NSString stringWithFormat:@"update games set team1Score='%@', team2Score='%@', qtr='%@', lastupdated='%@', hours='%@', minutes='%@', seconds='%@', NumberCheckedInUser='%@' where id='%@' AND User='%@'", gameScore.team1Score, gameScore.team2Score, gameScore.qtr, gameScore.lastUpdated, gameScore.hours, gameScore.minutes, gameScore.seconds, gameScore.numberCheckedInUser, gameScore.gameId, [CommonFunction getDataWithKey:kUser]];
                                sqlStatement = [query UTF8String];
                                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
                                    if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                                        NSLog(@"OK");
                                    }
                                }
                                sqlite3_finalize(compiledStatement);
                            }
                            
                            sqlite3_close(database);
                            */
                        }
                    }
                    
                    NSString *loginToken = [[SharedComponent getInstance] loginToken];
                    
                    if (sbnGetScoreRequest == nil){
                        sbnGetScoreRequest = [[SBNRequest alloc] init];
                    }
                    currentStep = GetGameStatus;
                    
                    NSMutableArray *listGameId = [[NSMutableArray alloc] init]; //autorelease
                    
                    for (NSString* key in self.listGamebox) {
                        GameBox *gameBox = [self.listGamebox objectForKey:key];
                        [listGameId addObject:gameBox.game.gameID];
                    }
                    
                    [sbnGetScoreRequest buildGetGameStatusBody:loginToken listGameId:listGameId];
                    [self sendGetScoreRequest];
                    break;
                }   
            case GetGameStatus:
                {
                    currentStep = -1;
                    SBNGetGameStatusResponse *gamStatusResponse = (SBNGetGameStatusResponse*)response;
                    NSMutableArray *listGameStatus = [gamStatusResponse listGameStatus];
                    for (Game *gameStatus in listGameStatus)
                    {
                        GameBox *gameBox = [listGamebox objectForKey:gameStatus.gameID];
                        if (gameBox != nil){
                            [gameBox.game updateGameStatus:gameStatus];
                            [gameBox updateGameStatus];
                            
                            //update game in database
                            [gameBox.game updateDatabase];
                        }
                    }
                    
                    // delete games that are removed on server
                    BOOL hasRemovedGames = false;
                    for (NSString* key in self.listGamebox) {
                        BOOL exist = [[listGameStatus objectEnumerator] any:^BOOL(id object) {
                            return [((Game*)object).gameID isEqualToString:key];
                        }];
                        
                        if (!exist) {
                            hasRemovedGames = true;
                            [Game deleteDatabase:key];
                        }
                    }
                    
                    if (hasRemovedGames)
                        [self reloadGamebox];

                    break;
                }
            default:
                break;
        }
    }
    else
    {
        if ([response.errorMessage isEqualToString:INVALID_LOGIN_TOKEN]) {
            [self logout];
            return;
        }
        
        NSString *errorMessage = @"Invalid Server Response.";
        if (response.errorMessage.length > 0)
            errorMessage = response.errorMessage;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    [responseData release];
    [response release];
    
    if (currentStep == -1)
    {
        isGettingScore = NO;
        isActive = YES;
        tick = [[SharedComponent getInstance] refreshSeconds];
    }
}

- (void) handleFailRequest{
    [super handleFailRequest];
    
    isGettingScore = NO;
    isActive = YES;
    tick = [[SharedComponent getInstance] refreshSeconds];
}

- (void) newGamesDownloaded:(int)count
{
    if (count > 0)
        [self reloadGamebox];
}

- (IBAction)btnRefreshTapped:(id)sender {
    [self getGameScore];
}

- (IBAction)btnManageGameTapped:(id)sender {
    GameListViewController *viewController = [[GameListViewController alloc] initWithNibName:@"GameListViewController" bundle:nil];
    //[viewController setDelegate:self];
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}


- (void)viewDidUnload
{
    [self setRefreshStatus:nil];
    [self setDateLabel:nil];
    [self setAlertSetting:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [listGamebox release];
    [refreshStatus release];
    [dateLabel release];
    [alertSetting release];
    [super dealloc];
}
@end
