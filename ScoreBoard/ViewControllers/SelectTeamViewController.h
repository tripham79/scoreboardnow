//
//  SelectTeamViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface SelectTeamViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (assign) id<SelectItemViewControllerDelegate> delegate;
@property (assign) id sourceView;
@property (assign) NSString* searchText;
// can only select team in the same sport
@property (assign) NSString* sportName;

@property (retain, nonatomic) IBOutlet UITextField *txtSearchText;
@property (retain, nonatomic) IBOutlet UIButton *btnSearch;
- (IBAction)btnSearchClick:(id)sender;

@property (nonatomic, retain) NSMutableArray* teams;
// use to show & enable load more
@property int totalRecords;
@property (retain, nonatomic) IBOutlet UITableView *tableTeams;

- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnAddClick:(id)sender;

@end
