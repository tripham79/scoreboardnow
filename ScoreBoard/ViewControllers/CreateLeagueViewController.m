//
//  CreateLeagueViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateLeagueViewController.h"

@interface CreateLeagueViewController ()

@end

@implementation CreateLeagueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerForKeyboardNotifications];
    
    self.lblSport.text = self.sport.sportName;
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_lblSport release];
    [_txtLeagueName release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setLblSport:nil];
    [self setTxtLeagueName:nil];
    [super viewDidUnload];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)btnAddClick:(id)sender {
    NSString* name = [self.txtLeagueName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([self.sport.sportId intValue] == 0) {
        showAlert(@"Please select sport first.", @"", nil);
        return;
    }
    if (name.length == 0) {
        showAlert(@"Please enter league name.", @"", nil);
        return;
    }
    
    CreateLeagueRequest* req = [[CreateLeagueRequest alloc] init];
    req.LeagueName = name;
    req.SportId = [NSNumber numberWithInt:[self.sport.sportId intValue]];
    req.CountryId = [NSNumber numberWithInt:0];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
    [[SBNService sharedInstance] createLeague:req successHandler:^(CreateLeagueResult *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:nil])
            return;
        
        League* l = [[League alloc] init];
        l.leagueName = name;
        l.leagueId = [result.LeagueId stringValue];
        if (self.delegate)
            [self.delegate createdLeague:l];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } failHandler:^(NSError *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self handleServiceError:nil error:err];
    }];
}

@end
