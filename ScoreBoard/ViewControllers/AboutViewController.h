
#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutViewController : BaseViewController<MFMailComposeViewControllerDelegate>

- (void) sendMailTo:(NSString*)address subject:(NSString*)subject;
- (void) openURL:(NSString*)url;

- (IBAction)btnInfoTapped:(id)sender;
- (IBAction)btnContactTapped:(id)sender;
- (IBAction)btnSupportTapped:(id)sender;
- (IBAction)btnRequestTapped:(id)sender;
- (IBAction)btnRateItTapped:(id)sender;
- (IBAction)btnFAQTapped:(id)sender;
- (IBAction)btnTermTapped:(id)sender;
- (IBAction)btnPolicyTapped:(id)sender;

@end
