//
//  CreateTeamViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface CreateTeamViewController : BaseViewController

@property (retain, nonatomic) Sport* sport;
@property (retain, nonatomic) League* league;
@property (retain, nonatomic) Division* division;
@property (assign) id<TeamDelegate> delegate;

@property (retain, nonatomic) IBOutlet UILabel *lblSportName;
@property (retain, nonatomic) IBOutlet UILabel *lblLeagueName;
@property (retain, nonatomic) IBOutlet UILabel *lblDivisionName;
@property (retain, nonatomic) IBOutlet UITextField *txtTeamName;
- (IBAction)btnAddClick:(id)sender;

@end
