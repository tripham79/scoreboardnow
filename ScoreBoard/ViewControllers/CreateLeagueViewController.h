//
//  CreateLeagueViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"
#import "MyDelegates.h"

@interface CreateLeagueViewController : BaseViewController

@property (retain, nonatomic) Sport* sport;
@property (assign) id<LeagueDelegate> delegate;

@property (retain, nonatomic) IBOutlet UILabel *lblSport;
@property (retain, nonatomic) IBOutlet UITextField *txtLeagueName;
- (IBAction)btnAddClick:(id)sender;

@end
