//
//  CreateSportViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface CreateSportViewController : BaseViewController

@property (retain, nonatomic) IBOutlet UITextField *txtName;
@property (retain, nonatomic) IBOutlet UITextView *txtComment;
@property (retain, nonatomic) IBOutlet UITextField *txtCommentBorder;
- (IBAction)btnAddClick:(id)sender;

@end
