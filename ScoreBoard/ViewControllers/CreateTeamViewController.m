//
//  CreateTeamViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateTeamViewController.h"

@interface CreateTeamViewController ()

@end

@implementation CreateTeamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerForKeyboardNotifications];
    
    self.lblSportName.text = self.sport.sportName;
    self.lblLeagueName.text = self.league.leagueName;
    self.lblDivisionName.text = self.division.divisionName;
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_lblSportName release];
    [_lblLeagueName release];
    [_lblDivisionName release];
    [_txtTeamName release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLblSportName:nil];
    [self setLblLeagueName:nil];
    [self setLblDivisionName:nil];
    [self setTxtTeamName:nil];
    [super viewDidUnload];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)btnAddClick:(id)sender {
    NSString* name = [self.txtTeamName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (name.length == 0) {
        showAlert(@"Please enter team name.", @"", nil);
        return;
    }
    
    CreateTeamRequest* req = [[CreateTeamRequest alloc] init];
    req.TeamName = name;
    req.SportId = [NSNumber numberWithInt:[self.sport.sportId intValue]];
    req.LeagueId = [NSNumber numberWithInt:[self.league.leagueId intValue]];
    req.DivisionId = [NSNumber numberWithInt:[self.division.divisionID intValue]];
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
    [[SBNService sharedInstance] createTeam:req successHandler:^(CreateTeamResult *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:nil])
            return;
        
        Team* t = [[Team alloc] init];
        t.teamID = [result.TeamId stringValue];
        t.teamNumber = result.TeamNumber;
        t.teamName = name;
        t.user = [CommonFunction getDataWithKey:kUser];
        t.stadiumID = @"0";
        
        t.sportID = self.sport.sportId;
        t.sportName = self.sport.sportName;
        t.leagueName = self.league.leagueName;
        t.divisionID = self.division.divisionID;
        t.divisionName = self.division.divisionName;
        
        if (self.delegate != nil)
            [self.delegate createdTeam:t];
            
        [self.navigationController popViewControllerAnimated:YES];
        
    } failHandler:^(NSError *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self handleServiceError:nil error:err];
    }];
}

@end
