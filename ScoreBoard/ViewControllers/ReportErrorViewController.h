//
//  ReportErrorViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/11/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface ReportErrorViewController : BaseViewController

@property (retain, nonatomic) League* league;
@property (retain, nonatomic) Division* division;
@property (retain, nonatomic) Team* team;
@property (retain, nonatomic) Stadium* stadium;

@property (retain, nonatomic) IBOutlet UILabel *lblData;
@property (retain, nonatomic) IBOutlet UILabel *lblDataText;
@property (retain, nonatomic) IBOutlet UITextView *txtComment;
@property (retain, nonatomic) IBOutlet UITextField *txtCommentBorder;
- (IBAction)btnSendClick:(id)sender;

@end
