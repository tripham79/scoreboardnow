
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "BaseViewController.h"

/*
 * GetClientConfig, GetGameScoreOption
 * required: CheckLoginToken
 * if (token valid)
 *      {go to MyScoreBoard}
 * else
 *      {go to Sign In}
 */

@interface SplashViewController : BaseViewController<AVAudioPlayerDelegate>{
    int tick;
    NSTimer *timer;
    AVAudioPlayer *playerTemp;
    
    BOOL isNeedCheckToken;
    BOOL isCheckTokenComplete;
    BOOL isValidToken;
    
    BOOL isGetConfigComplete;
    
    int waitingTime;
}

@property (retain, nonatomic) IBOutlet UIView *portrait;
@property (retain, nonatomic) IBOutlet UIView *landscape;

@property (retain, nonatomic) IBOutlet UIImageView *nImageView;
@property (retain, nonatomic) IBOutlet UIImageView *oImageView;
@property (retain, nonatomic) IBOutlet UIImageView *wImageView;


- (void) goNext;
- (void) goLogin;
- (void) goMyScoreBoard;
@end
