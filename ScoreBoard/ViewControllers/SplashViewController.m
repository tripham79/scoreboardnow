typedef enum SplashScreenStep{
    CheckTokenStep = 0,
    GetClientConfigStep,
    GetGameScoreOptionStep,
    GetSportStep,
} SplashScreenStep;

#import "SplashViewController.h"
#import "SetupViewController.h"

@implementation SplashViewController
@synthesize nImageView;
@synthesize oImageView;
@synthesize wImageView;

@synthesize portrait, landscape;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    self.view.frame = [UIScreen mainScreen].bounds;
    [self.portrait setFrame:[UIScreen mainScreen].bounds];
    [self.landscape setFrame:[UIScreen mainScreen].bounds];
    NSLog(@"W: %f, H: %f", self.view.frame.size.width, self.view.frame.size.height);
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[SharedComponent getInstance] setRefreshSeconds:60];
    
#if !TARGET_IPHONE_SIMULATOR
    //prepare to play sound
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Memo" ofType: @"m4a"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    NSError *error;

    playerTemp = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
    playerTemp.numberOfLoops = 0;
    [playerTemp prepareToPlay];
    [playerTemp setDelegate:self];
#endif
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    
    int homeScore = [components year] / 100;
    int guessScore = [components year] % 100;
    [self setHomeScore:homeScore];
    [self setGuessScore:guessScore];
    
    // test date convertion
    //NSDate* dt = [CommonFunction convertStringToDate:@"2013-07-13T12:11:26"];
    //NSString* text = [CommonFunction convertDateToString:dt];
    //text = [CommonFunction toLocalTimeText:dt];
    
    
    //count down
    tick = 3;
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    
    waitingTime = 0;
    
    //check login token
    isNeedCheckToken = NO;
    isCheckTokenComplete = NO;
    isValidToken = NO;
    
    isGetConfigComplete = NO;

    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    if (loginToken == nil || [loginToken isEqualToString:@""]) return;
    isNeedCheckToken = YES;
    
    currentStep = CheckTokenStep;
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildValidTokenBody:loginToken];
    [self sendRequest];
    
}

- (void)viewWillLayoutSubviews {
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self layoutSubviews];
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;

    SBNResponse *response = [[SBNResponse alloc] init];
    switch (currentStep) {
        case CheckTokenStep:
            response = [[SBNValidTokenResponse alloc] init];
            [response parseResponseData:responseData];
            isCheckTokenComplete = YES;
            break;
        case GetClientConfigStep:
            response = [[SBNGetClientConfigurationResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetGameScoreOptionStep:
            response = [[SBNGameScoreRefreshOptions alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetSportStep:
            response = [[SBNSportResponse alloc] init];
            [response parseResponseData:responseData];
        default:
            break;
    }
    
    if ([response.responseCode isEqualToString:@""]) return;

    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]){
        switch (currentStep) {
            case CheckTokenStep:
                {
                    isValidToken = YES;
                    
                    //get client config
                    currentStep = GetClientConfigStep;
                    
                    NSString *loginToken = [[SharedComponent getInstance] loginToken];
                    if (sbnRequest == nil){
                        sbnRequest = [[SBNRequest alloc] init];
                    }
                    
                    [sbnRequest buildGetClientConfigurationBody:loginToken];
                    [self sendRequest];
                    break;
                }
            case GetClientConfigStep:
                {
                    SBNGetClientConfigurationResponse *getClientConfigurationResponse = (SBNGetClientConfigurationResponse*) response;
                    [[SharedComponent getInstance] setRefreshSeconds:getClientConfigurationResponse.refreshSeconds.intValue];

                    currentStep = GetGameScoreOptionStep;
                    
                    NSString *loginToken = [[SharedComponent getInstance] loginToken];
                    if (sbnRequest == nil){
                        sbnRequest = [[SBNRequest alloc] init];
                    }
                    
                    [sbnRequest buildGameScoreRefreshOptionsBody:loginToken];
                    [self sendRequest];
                    break;
                }
            case GetGameScoreOptionStep:
                {
                    SBNGameScoreRefreshOptions *gameScoreRefreshOptions = (SBNGameScoreRefreshOptions*) response;
                    [[SharedComponent getInstance] setListGameScoreRefreshOption:[gameScoreRefreshOptions listGameScoreRefreshOption]];
                    
                    //set game score refresh time, if first time, set option 1
                    //if from second lauch time, get from local data
                    NSString *optionName = [CommonFunction getDataWithKey:Notification_ScoreUpdate];
                    if (optionName == nil || [optionName isEqualToString:@""]){
                        GameScoreRefreshOption *firstOption = [[[SharedComponent getInstance] listGameScoreRefreshOption] objectAtIndex:0];
                        [CommonFunction saveData:firstOption.optionName withKey:Notification_ScoreUpdate];
                        [[SharedComponent getInstance] setGameScoreRefreshSeconds:firstOption.optionSeconds.intValue];
                    }
                    else{
                        BOOL isStop = NO;
                        for (GameScoreRefreshOption *option in [[SharedComponent getInstance] listGameScoreRefreshOption]){
                            if (!isStop){
                                if ([optionName isEqualToString:option.optionName]){
                                    [[SharedComponent getInstance] setGameScoreRefreshSeconds:option.optionSeconds.intValue];
                                    isStop = YES;
                                }   
                            }
                        }
                        
                        //if don't match to every option, set default value: 120 seconds
                        if (!isStop){
                            [[SharedComponent getInstance] setGameScoreRefreshSeconds:120];
                        }
                    }
                    
                    currentStep = GetSportStep;
                    
                    NSString *loginToken = [[SharedComponent getInstance] loginToken];
                    if (sbnRequest == nil){
                        sbnRequest = [[SBNRequest alloc] init];
                    }
                    
                    [sbnRequest buildSportBody:loginToken];
                    [self sendRequest];
                    break;
                }
            case GetSportStep:
                {
                    SBNSportResponse *sportResponse = (SBNSportResponse*) response;
                    [[SharedComponent getInstance] setListSport:sportResponse.listSport];
                    
                    isGetConfigComplete = YES;
                    break;
                }
            default:
                break;
        }
    }
    else
    {
        NSString *errorMessage = @"Error";
        if (currentStep == CheckTokenStep){
            errorMessage = @"You have been signed out because you signed in on a different computer or device";
        }
        else
        {
            if (![response.errorMessage isEqualToString:@""])
            {
                errorMessage = response.errorMessage;
            }
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    [responseData release];
    [response release];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    [[SharedComponent getInstance] setInterfaceOrientation:toInterfaceOrientation];
    return YES;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self layoutSubviews];
}

- (void) layoutSubviews {
    if ([CommonFunction checkPortraitOrientation])
    {
        for (int i = 1; i <= 9; i++)
        {
            [self.view viewWithTag:i].frame = [portrait viewWithTag:i].frame;
            
            if ( i == 1)
            {
                UIImageView *background = (UIImageView*)[self.view viewWithTag:i];
                [background setImage:[UIImage imageNamed:@"SplashScreen_Portrait.png"]];
            }
        }
    }
    else if ([CommonFunction checkLandscapeOrientation])
    {
        for (int i = 1; i <= 9; i++)
        {
            [self.view viewWithTag:i].frame = [landscape viewWithTag:i].frame;
            
            if ( i == 1)
            {
                UIImageView *background = (UIImageView*)[self.view viewWithTag:i];
                [background setImage:[UIImage imageNamed:@"SplashScreen.png"]];
            }
        }
    }
}

- (void) setHomeScore:(int)score {
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)[self.view viewWithTag:6] setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)[self.view viewWithTag:7] setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) setGuessScore:(int)score {
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)[self.view viewWithTag:8] setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)[self.view viewWithTag:9] setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) timerTick{
    tick--;
    switch (tick) {
        case 3:
            [wImageView setImage:[UIImage imageNamed:@"number3.png"]];
            break;
        case 2:
            [wImageView setImage:[UIImage imageNamed:@"number2.png"]];
            break;
        case 1:
            [wImageView setImage:[UIImage imageNamed:@"number1.png"]];
            break;
        case 0:
            [nImageView setImage:[UIImage imageNamed:@"N.png"]];
            [wImageView setImage:[UIImage imageNamed:@"W.png"]];
            [timer invalidate];
            
#if !TARGET_IPHONE_SIMULATOR
            [playerTemp play];
#else
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(goNext) userInfo:nil repeats:NO];
#endif
            break;
        default:
            break;
    }
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [self goNext];
}

- (void) goNext{
    if (waitingTime == 2){
        [self goLogin];
        return;
    }
    
    if (isNeedCheckToken){
        if (isCheckTokenComplete){
            if (isValidToken){
                //waiting application get Config and League
                if (isGetConfigComplete){
                    [self goMyScoreBoard];
                }
                else{
                    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(goNext) userInfo:nil repeats:NO];
                    waitingTime++;
                }
            }
            else{
                [self goLogin];
            }
        }
        else{
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(goNext) userInfo:nil repeats:NO];
            waitingTime++;
        }
    }
    else {
        [self goLogin];
    }
}

- (void) goMyScoreBoard{
    MyScoreBoardViewController *viewController = [[MyScoreBoardViewController alloc] initWithNibName:@"MyScoreBoardViewController" bundle:nil];
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];

}

- (void) goLogin{   
    SignInViewController *viewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    [[[SharedComponent getInstance] navigationController] pushViewController:viewController animated:YES];
    [viewController release];
}

- (void)dealloc {
    [portrait release];
    [landscape release];
    [playerTemp release];
    [nImageView release];
    [oImageView release];
    [wImageView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setNImageView:nil];
    [self setOImageView:nil];
    [self setWImageView:nil];
    [super viewDidUnload];
}
@end
