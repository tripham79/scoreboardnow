//
//  MyCreatedGameViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface MyCreatedGameViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableGames;

- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnAddClick:(id)sender;

- (void)gameSaved:(Game*)game isAdded:(BOOL)isAdded;
- (void)gameDeleted:(Game*)game;
@property (retain, nonatomic) IBOutlet UIButton *btnAdd;

@end
