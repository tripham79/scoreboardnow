//
//  SelectLocationViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/8/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SelectLocationViewController.h"
#import "UIAlertView+Extensions.h"
#import "NSDictionary+Extensions.h"

@interface SelectLocationViewController ()

@end

@implementation SelectLocationViewController {
    MKPointAnnotation *mapAnnotation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txtAddress.delegate = self;
    
    // long press map to add pin
    UILongPressGestureRecognizer* lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleMapLongPress:)];
    lpgr.minimumPressDuration = 1.0;
    lpgr.delegate = self;
    [self.map addGestureRecognizer:lpgr];
    self.map.delegate = self;
    
    // show address
    if (self.locationAddress.length > 0) {
        NSString* address = [self getFormattedAddress];
        self.txtAddress.text = address;
    
        // show location on map if already set
        if (self.locationLat != 0 || self.locationLng != 0) {
            self.lblGPSLocation.text = [NSString stringWithFormat:@"%.5f, %.5f", self.locationLat, self.locationLng];
            
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.locationLat, self.locationLng);
            [self addMapPinAt:coordinate];
            
            // move map to pin
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000);
            [self.map setRegion:region animated:YES];
        }
        else {
            // try to locate from address
            [self searchAddressLocation:address];
        }
    }
    else {
        // center map to current country (already done by iOS Map)
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)addMapPinAt:(CLLocationCoordinate2D)coordinate {
    // remove current pin if any
    if (mapAnnotation != nil)
        [self.map removeAnnotation:mapAnnotation];
    
    // add a pin at found location
    mapAnnotation = [[MKPointAnnotation alloc] init];
    mapAnnotation.coordinate = coordinate;
    [self.map addAnnotation:mapAnnotation];
}

- (NSString*)getFormattedAddress {
    NSString* address = self.locationAddress;
    
    if (self.locationCity.length > 0)
        address = [[address stringByAppendingString:@", "] stringByAppendingString:self.locationCity];
    if (self.locationCountry.length > 0)
        address = [[address stringByAppendingString:@", "] stringByAppendingString:self.locationCountry];
    
    return address;
}

- (void)searchAddressLocation:(NSString*)address {
    if (address.length == 0)
        return;
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:address
                 completionHandler:^(NSArray* placemarks, NSError* error)
     {
         if (placemarks && placemarks.count > 0) {
             CLPlacemark *topResult = [placemarks objectAtIndex:0];
             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
             
             [self addMapPinAt:placemark.coordinate];
             
             //MKCoordinateRegion region = self.map.region;
             //region.center = placemark.region.center;
             //region.span.longitudeDelta /= 8.0;
             //region.span.latitudeDelta /= 8.0;

             // move map to pin
             MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(placemark.coordinate, 5000, 5000);
             [self.map setRegion:region animated:YES];

             // set as current address
             self.locationAddress = [placemark.addressDictionary stringForKey:@"Street"];
             if (self.locationAddress.length == 0)
                 self.locationAddress = address;
             
             self.locationCity = [placemark.addressDictionary stringForKey:@"City"];
             self.locationCountry = [placemark.addressDictionary stringForKey:@"Country"];
             self.locationCountryCode = [placemark.addressDictionary stringForKey:@"CountryCode"];
             
             // set as address's location
             self.locationLat = placemark.coordinate.latitude;
             self.locationLng = placemark.coordinate.longitude;
             self.lblGPSLocation.text = [NSString stringWithFormat:@"%.5f, %.5f", placemark.coordinate.latitude, placemark.coordinate.longitude];
         }
         else {
             showAlert(@"", @"Could not find address location on map.", nil);
         }
     }];
}

- (void) handleMapLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        /*
         Only handle state as the touches began set the location of the annotation
         */
        CGPoint touchPoint = [gestureRecognizer locationInView:self.map];
        CLLocationCoordinate2D coordinate =[self.map convertPoint:touchPoint toCoordinateFromView:self.map];
        
        // add a pin at long press location
        [self addMapPinAt:coordinate];

        // set as address's location
        self.locationLat = coordinate.latitude;
        self.locationLng = coordinate.longitude;
        self.lblGPSLocation.text = [NSString stringWithFormat:@"%.5f, %.5f", coordinate.latitude, coordinate.longitude];
        
        // look up address for the new location
        [self getAddressFromLatitude:coordinate.latitude Longitude:coordinate.longitude];
    }
}

- (IBAction)btnBackClick:(id)sender {
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDoneClick:(id)sender {
    [self.view endEditing:YES];
    
    if (self.locationAddress.length == 0 || self.locationLat == 0 || self.locationLng == 0) {
        showAlert(@"", @"Please enter an address and search/select a location on map.", nil);
        return;
    }
    
    if (self.delegate)
        [self.delegate selectedLocation:self];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSearchClick:(id)sender {
    [self.view endEditing:YES];
    
    NSString* address = [self.txtAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (address.length == 0) {
        showAlert(@"", @"Please enter an address.", nil);
        return;
    }
    [self searchAddressLocation:address];
}

- (void)getAddressFromLatitude:(double)lat Longitude:(double)lng
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation* loc = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    
    [geocoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks && placemarks.count > 0) {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            
            /*NSLog(@"placemark %@", placemark);
            NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
            NSLog(@"addressDictionary %@", placemark.addressDictionary);
            NSLog(@"region: %@",placemark.region);
            NSLog(@"country: %@",placemark.country);  // Give Country Name
            NSLog(@"locality: %@",placemark.locality); // Extract the city name
            NSLog(@"name: %@",placemark.name);
            NSLog(@"ocean: %@",placemark.ocean);
            NSLog(@"postalCode: %@",placemark.postalCode);
            NSLog(@"subLocality: %@",placemark.subLocality);
            
            NSLog(@"location %@",placemark.location);
            NSLog(@"I am currently at %@", locatedAt);*/
            
            NSString* address = [placemark.addressDictionary stringForKey:@"Street"];
            if (address.length > 0) {
                self.locationAddress = address;
                self.txtAddress.text = address;
            }
            
            self.locationCity = [placemark.addressDictionary stringForKey:@"City"];
            self.locationCountry = [placemark.addressDictionary stringForKey:@"Country"];
            self.locationCountryCode = [placemark.addressDictionary stringForKey:@"CountryCode"];
        }
        else {
            self.locationAddress = @"";
            self.locationCity = @"";
            self.locationCountry = @"";
            self.locationCountryCode = @"";
            
            NSLog(@"Could not locate address.");
        }
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation
{
    MKPinAnnotationView *pin = (MKPinAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier: @"myPin"];
    if (pin == nil) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"myPin"] ;
    }
    else {
        pin.annotation = annotation;
    }
    pin.animatesDrop = YES;
    pin.draggable = YES;
    return pin;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView
    didChangeDragState:(MKAnnotationViewDragState)newState
            fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        [annotationView.annotation setCoordinate:droppedAt];
        //NSLog(@"Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
        
        // user move pin, change location only, keep current address
        self.locationLat = droppedAt.latitude;
        self.locationLng = droppedAt.longitude;
        self.lblGPSLocation.text = [NSString stringWithFormat:@"%.5f, %.5f", droppedAt.latitude, droppedAt.longitude];
    }
}

- (void)dealloc {
    [_txtAddress release];
    [_lblGPSLocation release];
    [_map release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTxtAddress:nil];
    [self setLblGPSLocation:nil];
    [self setMap:nil];
    [super viewDidUnload];
}
@end
