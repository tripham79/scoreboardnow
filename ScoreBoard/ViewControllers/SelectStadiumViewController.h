//
//  SelectStadiumViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "BaseViewController.h"
#import "SelectStadiumInfoViewController.h"
#import "MyDelegates.h"

@interface SelectStadiumViewController : BaseViewController<MKMapViewDelegate, StadiumDelegate>

@property (assign) id<SelectItemViewControllerDelegate> delegate;
@property (assign) id sourceView;
@property (assign) NSString* searchText;
@property (retain, nonatomic) IBOutlet UITextField *txtSearchText;
@property (retain, nonatomic) IBOutlet UIButton *btnSearch;
- (IBAction)btnSearchClick:(id)sender;

@property (retain, nonatomic) IBOutlet MKMapView *map;
@property (nonatomic, retain) NSMutableArray* stadiums;
// use to show & enable load more
@property int totalRecords;

//@property (retain, nonatomic) IBOutlet UITableView *tableStadiums;
- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnAddClick:(id)sender;

@end
