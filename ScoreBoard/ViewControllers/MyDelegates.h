//
//  MyDelegates.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/5/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//
#import "League.h"
#import "Division.h"
#import "Team.h"
#import "Stadium.h"

@protocol LeagueDelegate <NSObject>
@optional
- (void) createdLeague:(League*)league;
@end

@protocol DivisionDelegate <NSObject>
@optional
- (void) createdDivision:(Division*)division;
@end

@protocol TeamDelegate <NSObject>
@optional
- (void) createdTeam:(Team*)team;
@end

@protocol StadiumDelegate <NSObject>
@optional
- (void) createdStadium:(Stadium*)stadium;
- (void) selectedStadium:(Stadium*)stadium;
- (void) cancelSelecteStadium:(Stadium*)stadium;
- (void) reportStadiumError:(Stadium*)stadium;
@end


