//
//  SelectStadiumViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SelectStadiumViewController.h"
#import "Stadium.h"
#import "SearchStadiumResponse.h"
#import "CreateStadiumViewController.h"
#import "ReportErrorViewController.h"
#import "MKCustomAnnotation.h"

@interface SelectStadiumViewController ()

@end

@implementation SelectStadiumViewController {
    NSMutableArray* mapAnnotations;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.tableStadiums.delegate = self;
    //self.tableStadiums.dataSource = self;
    self.map.delegate = self;
    mapAnnotations = [[NSMutableArray alloc] init];
    
    self.txtSearchText.delegate = self;
    self.txtSearchText.text = self.searchText;
    // search all by default
    [self doSearch:@""];
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.map addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == self.txtSearchText) {
        NSString* searchText = [self.txtSearchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (searchText.length > 0)
            [self doSearch:searchText];
    }
    return YES;
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddClick:(id)sender {
    CreateStadiumViewController* vc = [[CreateStadiumViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)createdStadium:(Stadium *)stadium {
    // stay on this list
    //[self.stadiums removeAllObjects];
    //self.totalRecords = 0;
    //[self doSearch:@""];
    
    // select the new created stadium
    [self.stadiums insertObject:stadium atIndex:0];
    if (self.delegate)
        [self.delegate selectItemViewController:self didSelectedRowAtIndex:0];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selectedStadium:(Stadium *)stadium {
    NSUInteger index = [self.stadiums indexOfObject:stadium];
    if (self.delegate)
        [self.delegate selectItemViewController:self didSelectedRowAtIndex:(int)index];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelSelecteStadium:(Stadium *)stadium {
    for (MKCustomAnnotation* obj in self.map.selectedAnnotations) {
        [self.map deselectAnnotation:obj animated:NO];
    }
}

- (void)dealloc {
    //[_tableStadiums release];
    [_txtSearchText release];
    [_btnSearch release];
    [_map release];
    [super dealloc];
}
- (void)viewDidUnload {
    //[self setTableStadiums:nil];
    [self setTxtSearchText:nil];
    [self setBtnSearch:nil];
    [self setMap:nil];
    [super viewDidUnload];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.stadiums count]) {
        NSString *cellIdentifier = @"UITableViewCell";
	
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
        if(cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
            [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.textLabel setMinimumFontSize:10];
            [cell.textLabel setTextColor:[UIColor blackColor]];
        }
    
        Stadium* obj = [self.stadiums objectAtIndex:indexPath.row];
        cell.textLabel.text = obj.Name;
    
        NSMutableString* detail = [[NSMutableString alloc] initWithString:obj.Address];
        if ([obj.City length] > 0) {
            [detail appendString:@", "];
            [detail appendString:obj.City];
        }
        cell.detailTextLabel.text = detail;
        return cell;
    }
    else {
        NSString *cellIdentifier = @"UITableViewCellLoadMore";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault    reuseIdentifier:cellIdentifier] autorelease];
            [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.textLabel setMinimumFontSize:10];
            [cell.textLabel setTextColor:[UIColor blackColor]];
            [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"Load More (%d/%d)", [self.stadiums count], self.totalRecords];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.stadiums count])
    {
        if (self.delegate)
            [self.delegate selectItemViewController:self didSelectedRowAtIndex:(int)indexPath.row];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        // load more
        [MBProgressHUD showHUDAddedTo:self.view text:@"Loading" animated:YES];
        int pageIndex = (int)[self.stadiums count] / 15;
        
        [[SBNService sharedInstance] searchStadiums:pageIndex pageSize:15 searchText:self.searchText withCallback:^(BaseRequest *request, id result, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            // if has error then stop
            if ([self handleServiceError:result error:error])
                return;
            
            SearchStadiumResponse* response = (id)result;
            [self.stadiums addObjectsFromArray:response.stadiums];
            self.totalRecords = response.totalRecords;
            
            //[self.tableStadiums reloadData];
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.totalRecords > [self.stadiums count])
        return [self.stadiums count] + 1;
    else
        return [self.stadiums count];
}

- (IBAction)btnSearchClick:(id)sender {
    NSString* searchText = [self.txtSearchText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [self doSearch:searchText];
}

- (void)doSearch:(NSString*)searchText {
    // search for all stadiums
    [MBProgressHUD showHUDAddedTo:self.view text:@"Searching" animated:YES];
    
    [[SBNService sharedInstance] searchStadiums:0 pageSize:0 searchText:searchText withCallback:^(BaseRequest *request, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        SearchStadiumResponse* response = (id)result;
        if ([response.stadiums count] == 0) {
            showAlert(@"No stadium found.", @"", nil);
            return;
        }
        
        self.stadiums = response.stadiums;
        self.totalRecords = response.totalRecords;
        //[self.tableStadiums reloadData];
        [self reloadStadiumOnMap];
        
        // if user do a search, select and move map to first stadium
        if (searchText.length > 0 && mapAnnotations.count > 0)
        {
            Stadium* s = (id)((MKCustomAnnotation*)[mapAnnotations firstObject]).userData;
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([s.Lat doubleValue], [s.Lng doubleValue]);
            [self.map setCenterCoordinate:coordinate animated:YES];
            [self.map selectAnnotation:[mapAnnotations firstObject] animated:YES];
        }
    }];
}

- (void)reloadStadiumOnMap {
    // clear existing annotations
    [self.map removeAnnotations:mapAnnotations];
    [mapAnnotations removeAllObjects];
    
    // reload new annotations
    for (Stadium* s in self.stadiums) {
        if ([s.Lat doubleValue] == 0 && [s.Lng doubleValue] == 0)
            continue;
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([s.Lat doubleValue], [s.Lng doubleValue]);
        MKCustomAnnotation* mapAnnotation = [[MKCustomAnnotation alloc] init];
        mapAnnotation.coordinate = coordinate;
        mapAnnotation.title = s.Name;
        mapAnnotation.userData = s;
        
        [self.map addAnnotation:mapAnnotation];
        [mapAnnotations addObject:mapAnnotation];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id)annotation
{
    MKPinAnnotationView *pin = (MKPinAnnotationView *)[self.map dequeueReusableAnnotationViewWithIdentifier: @"myPin"];
    if (pin == nil) {
        pin = [[MKPinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"myPin"];
    }
    else {
        pin.annotation = annotation;
    }
    
    pin.canShowCallout = YES;
    pin.animatesDrop = NO;
    pin.draggable = NO;
    return pin;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    MKCustomAnnotation* mapAnnotation = (id)view.annotation;
    Stadium* stadium = (id)mapAnnotation.userData;
    
    SelectStadiumInfoViewController* vc = [[SelectStadiumInfoViewController alloc] init];
    vc.stadium = stadium;
    vc.delegate = self;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)reportStadiumError:(Stadium *)stadium {
    ReportErrorViewController* vc = [[ReportErrorViewController alloc] init];
    vc.stadium = stadium;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
