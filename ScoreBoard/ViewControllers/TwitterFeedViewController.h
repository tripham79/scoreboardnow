
#import "BaseViewController.h"

@interface TwitterFeedViewController : BaseViewController<UITableViewDataSource>{
    //NSMutableData *responseData;
    
}

@property (retain) UIFont *font;
@property (retain) NSMutableDictionary *cachedImages;
@property (retain) NSString *hashTag;
@property (retain, nonatomic) IBOutlet UITableView *twitterFeedTable;
@property (nonatomic, retain) NSArray *tweets;

- (void) loadTweetWithHashTag;

@end
