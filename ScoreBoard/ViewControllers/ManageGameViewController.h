
#import "BaseViewController.h"
#import "MyDelegates.h"

@interface ManageGameViewController : BaseViewController <DropDownListDelegate, UITableViewDataSource, UITableViewDelegate,
    LeagueDelegate, DivisionDelegate, TeamDelegate>
{
    NSString *selectedSeason;
    Sport *selectedSport;
    League *selectedLeague;
    Division *selectedDivision;
    Team *selectedTeam;
    
    NSMutableArray *listSeason;
    NSMutableArray *listLeague;
    NSMutableArray *listDivision;
    NSMutableArray *listTeam;
    
    NSMutableArray *listGame;
    
    NSMutableArray *listSelectedGameId;
}

//@property (assign) id delegate;

@property (retain, nonatomic) IBOutlet UIButton *seasonDropDownList;
@property (retain, nonatomic) IBOutlet UIButton *sportDropDownList;
@property (retain, nonatomic) IBOutlet UIButton *leagueDropDownList;
@property (retain, nonatomic) IBOutlet UIButton *divisionDropDownList;
@property (retain, nonatomic) IBOutlet UIButton *teamDropDownList;
- (void) showDropDownList:(id)sender;

@property (retain, nonatomic) IBOutlet UITableView *tableGame;

@property (retain, nonatomic) IBOutlet UIButton *btnAddGame;
- (void) btnAddGameTapped:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnAddTeam;
- (void) btnAddTeamTapped:(id)sender;

//- (void) loadSavedTeam;
//- (void) addGames:(NSMutableArray*)listSelectedGame addedManual:(BOOL)addedManual;
//- (void) sendSubscribeGame;
//- (void) sendUnSubscribeGame;

@end
