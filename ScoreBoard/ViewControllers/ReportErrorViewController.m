//
//  ReportErrorViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/11/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "ReportErrorViewController.h"

@interface ReportErrorViewController ()

@end

@implementation ReportErrorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    
    self.txtCommentBorder.borderStyle = UITextBorderStyleRoundedRect;
    [self showErrorReport];
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_lblDataText release];
    [_txtComment release];
    [_txtCommentBorder release];
    [_lblData release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setLblDataText:nil];
    [self setTxtComment:nil];
    [self setTxtCommentBorder:nil];
    [self setLblData:nil];
    [super viewDidUnload];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (void)showErrorReport {
    if (self.league != nil) {
        self.lblData.text = @"League Name:";
        self.lblDataText.text = self.league.leagueName;
    } else if (self.division != nil) {
        self.lblData.text = @"Division Name:";
        self.lblDataText.text = self.division.divisionName;
    } else if (self.team != nil) {
        self.lblData.text = @"Team Name:";
        self.lblDataText.text = self.team.teamName;
    } else if (self.stadium != nil) {
        self.lblData.text = @"Stadium Name:";
        self.lblDataText.text = self.stadium.Name;
    } else {
        showAlert(@"", @"Report data is not set yet.", nil);
    }
}

- (IBAction)btnSendClick:(id)sender {
    NSString* comment = [self.txtComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (comment.length == 0) {
        showAlert(@"", @"Please enter a comment.", nil);
        return;
    }
    
    if (self.league != nil) {
        ReportLeagueErrorRequest* req = [[ReportLeagueErrorRequest alloc] init];
        req.LeagueId = [NSNumber numberWithInt:[self.league.leagueId intValue]];
        req.Comment = comment;
        
        [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
        [[SBNService sharedInstance] reportLeagueError:req successHandler:^(BaseResponse *result) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([self handleServiceError:result error:nil])
                return;
            
            [self.navigationController popViewControllerAnimated:YES];
        } failHandler:^(NSError *err) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self handleServiceError:nil error:err];
        }];
    }
    else if (self.division != nil) {
        ReportDivisionErrorRequest* req = [[ReportDivisionErrorRequest alloc] init];
        req.DivisionId = [NSNumber numberWithInt:[self.division.divisionID intValue]];
        req.Comment = comment;
        
        [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
        [[SBNService sharedInstance] reportDivisionError:req successHandler:^(BaseResponse *result) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([self handleServiceError:result error:nil])
                return;
            
            [self.navigationController popViewControllerAnimated:YES];
        } failHandler:^(NSError *err) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self handleServiceError:nil error:err];
        }];
    }
    else if (self.team != nil) {
        ReportTeamErrorRequest* req = [[ReportTeamErrorRequest alloc] init];
        req.TeamId = [NSNumber numberWithInt:[self.team.teamID intValue]];
        req.Comment = comment;
        
        [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
        [[SBNService sharedInstance] reportTeamError:req successHandler:^(BaseResponse *result) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([self handleServiceError:result error:nil])
                return;
            
            [self.navigationController popViewControllerAnimated:YES];
        } failHandler:^(NSError *err) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self handleServiceError:nil error:err];
        }];
    }
    else if (self.stadium != nil) {
        ReportStadiumErrorRequest* req = [[ReportStadiumErrorRequest alloc] init];
        req.StadiumId = [NSNumber numberWithInt:[self.stadium.Id intValue]];
        req.Comment = comment;
        
        [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
        [[SBNService sharedInstance] reportStadiumError:req successHandler:^(BaseResponse *result) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([self handleServiceError:result error:nil])
                return;
            
            [self.navigationController popViewControllerAnimated:YES];
        } failHandler:^(NSError *err) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self handleServiceError:nil error:err];
        }];
    }
}

@end
