//
//  CreateDivisionViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface CreateDivisionViewController : BaseViewController

@property (retain, nonatomic) Sport* sport;
@property (retain, nonatomic) League* league;
@property (assign) id<DivisionDelegate> delegate;

@property (retain, nonatomic) IBOutlet UILabel *lblSportName;
@property (retain, nonatomic) IBOutlet UILabel *lblLeagueName;
@property (retain, nonatomic) IBOutlet UITextField *txtDivisionName;
- (IBAction)btnAddClick:(id)sender;

@end
