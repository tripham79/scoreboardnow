
#import "BaseViewController.h"

@interface MyScoreBoardViewController : BaseViewController<GameBoxDelegate, CSwitchDelegate>{
    int x;
    int y;
    
    BOOL isGettingScore;
    
    NSTimer *refreshTimer;
    BOOL isActive;

    int tick;
    
    NSDate *fromDate;
    NSDate *toDate;
    int weekNumber;
}
@property (retain) IBOutlet CSwitch *alertSetting;
@property (retain) IBOutlet UILabel *dateLabel;
@property (retain) NSMutableDictionary *listGamebox;

- (void) insertNewGame: (Game*) game;
- (void) removeGame:(NSString*)gameId;
- (void) getGameScore;
- (void) changeDateLabel;
- (void) reloadGamebox;

@property (retain, nonatomic) IBOutlet UILabel *refreshStatus;
- (IBAction)btnRefreshTapped:(id)sender;
- (IBAction)btnManageGameTapped:(id)sender;
- (IBAction)previousWeekTapped:(id)sender;
- (IBAction)nextWeekTapped:(id)sender;
@end
