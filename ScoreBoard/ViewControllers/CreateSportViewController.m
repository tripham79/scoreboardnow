//
//  CreateSportViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateSportViewController.h"

@interface CreateSportViewController ()

@end

@implementation CreateSportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    self.txtCommentBorder.borderStyle = UITextBorderStyleRoundedRect;
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)btnAddClick:(id)sender {
    NSString* name = [self.txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString* comment = self.txtComment.text;
    if (name.length == 0) {
        showAlert(@"Please enter sport name.", @"", nil);
        return;
    }
    
    SuggestNewSportRequest* req = [[SuggestNewSportRequest alloc] init];
    req.SportName = name;
    req.Comment = comment;
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
    [[SBNService sharedInstance] suggestNewSport:req successHandler:^(BaseResponse *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:nil])
            return;
        
        [self.navigationController popViewControllerAnimated:YES];

    } failHandler:^(NSError *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self handleServiceError:nil error:err];
    }];
}

- (void)dealloc {
    [_txtName release];
    [_txtComment release];
    [_txtCommentBorder release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setTxtName:nil];
    [self setTxtComment:nil];
    [self setTxtCommentBorder:nil];
    [super viewDidUnload];
}
@end
