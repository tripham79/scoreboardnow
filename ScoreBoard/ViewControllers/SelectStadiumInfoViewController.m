//
//  SelectStadiumInfoViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/11/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SelectStadiumInfoViewController.h"

@interface SelectStadiumInfoViewController ()

@end

@implementation SelectStadiumInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopup:)];
    [self.bgView addGestureRecognizer:tapGes];

    self.lblStadiumName.text = self.stadium.Name;
    self.lblStadiumAddress.text = self.stadium.Address;
    self.lblStadiumCity.text = [self.lblStadiumCity.text stringByAppendingString:self.stadium.City];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_lblStadiumName release];
    [_lblStadiumAddress release];
    [_lblStadiumCity release];
    [_bgView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLblStadiumName:nil];
    [self setLblStadiumAddress:nil];
    [self setLblStadiumCity:nil];
    [self setBgView:nil];
    [super viewDidUnload];
}

- (void)closePopup:(UITapGestureRecognizer *)gesture {
    [self hideChildController:self];
    
    if (self.delegate != nil)
        [self.delegate cancelSelecteStadium:self.stadium];
}

- (IBAction)btnCancelClick:(id)sender {
    [self hideChildController:self];
    
    if (self.delegate != nil)
        [self.delegate cancelSelecteStadium:self.stadium];
}

- (IBAction)btnSelectClick:(id)sender {
    [self hideChildController:self];
    
    if (self.delegate != nil)
        [self.delegate selectedStadium:self.stadium];
}

- (IBAction)btnReportErrorClick:(id)sender {
    [self hideChildController:self];
    
    if (self.delegate != nil)
        [self.delegate reportStadiumError:self.stadium];
}

@end
