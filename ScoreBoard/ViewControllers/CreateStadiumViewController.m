//
//  CreateStadiumViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateStadiumViewController.h"
#import "SelectLocationViewController.h"

@interface CreateStadiumViewController ()

@end

@implementation CreateStadiumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txtStadiumName.delegate = self;
    self.txtAddress.delegate = self;
    self.txtCity.delegate = self;
    
    // create new stadium if it's null
    if (self.stadium == nil)
        self.stadium = [[Stadium alloc] init];
    else
    {
        self.txtStadiumName.text = self.stadium.Name;
        self.txtAddress.text = self.stadium.Address;
        self.lblGPSLocation.text = [NSString stringWithFormat:@"%.5f, %.5f", [self.stadium.Lat doubleValue], [self.stadium.Lng doubleValue]];
    }
    
    UITapGestureRecognizer* tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)btnAddClick:(id)sender {
    NSString* name = [self.txtStadiumName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString* address = [self.txtAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString* city = [self.txtCity.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (name.length == 0 || address.length == 0 || [self.stadium.Lat doubleValue] == 0 || [self.stadium.Lng doubleValue] == 0) {
        showAlert(@"Please enter stadium name, address and select a location on map.", @"", nil);
        return;
    }
    
    self.stadium.Name = name;
    self.stadium.Address = address;
    self.stadium.City = city;
    
    CreateStadiumRequest *req = [[CreateStadiumRequest alloc] init];
    req.StadiumName = self.stadium.Name;
    req.Address = self.stadium.Address;
    req.City = self.stadium.City;
    req.Lat = self.stadium.Lat;
    req.Lng = self.stadium.Lng;
    
    [MBProgressHUD showHUDAddedTo:self.view text:@"" animated:YES];
    [[SBNService sharedInstance] createStadium:req successHandler:^(CreateStadiumResult *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:nil])
            return;

        self.stadium.Id = [result.StadiumId stringValue];
        
        if (self.delegate != nil)
            [self.delegate createdStadium:self.stadium];
        
        [self.navigationController popViewControllerAnimated:YES];
    } failHandler:^(NSError *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self handleServiceError:nil error:err];
    }];
}

- (IBAction)btnMapClick:(id)sender {
    // select address on map
    SelectLocationViewController* vc = [[SelectLocationViewController alloc] init];
    vc.locationAddress = self.stadium.Address;
    vc.locationCity = self.stadium.City;
    vc.locationLat = [self.stadium.Lat doubleValue];
    vc.locationLng = [self.stadium.Lng doubleValue];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

//- (void)selectedLocation:(NSString *)address lat:(double)lat lng:(double)lng {
- (void)selectedLocation:(SelectLocationViewController*)vc {
    self.stadium.Address = vc.locationAddress;
    self.stadium.City = vc.locationCity;
    self.stadium.Country = vc.locationCountry;
    self.stadium.CountryCode = vc.locationCountryCode;
    self.stadium.Lat = [NSNumber numberWithDouble:vc.locationLat];
    self.stadium.Lng = [NSNumber numberWithDouble:vc.locationLng];
    
    self.txtAddress.text = self.stadium.Address;
    self.txtCity.text = self.stadium.City;
    self.lblGPSLocation.text = [NSString stringWithFormat:@"%.5f, %.5f", [self.stadium.Lat doubleValue], [self.stadium.Lng doubleValue]];
}

- (void)dealloc {
    [_txtStadiumName release];
    [_txtAddress release];
    [_lblGPSLocation release];
    [_txtCity release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTxtStadiumName:nil];
    [self setTxtAddress:nil];
    [self setLblGPSLocation:nil];
    [self setTxtCity:nil];
    [super viewDidUnload];
}
@end
