
#import "BaseViewController.h"

@interface RecoveryPasswordViewController : BaseViewController

@property (retain, nonatomic) IBOutlet CTextField *emailTextField;
- (IBAction)btnResetPasswordTapped:(id)sender;
@end
