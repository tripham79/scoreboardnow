typedef enum GameFeedScreenStep{
    PostShout = 0,
    RequestScore,
    CheckIn,
    CheckOut
} GameFeedScreenStep;

#import "GameFeedViewController.h"

@implementation GameFeedViewController
//@synthesize selectedGameBox;
@synthesize selectedGame;
@synthesize scoreBoard;
@synthesize btnRequestScore;
@synthesize btnUpdateScore;
@synthesize shoutMessage;
@synthesize fbCheckbox;
@synthesize twitterCheckbox;
@synthesize btnRefreshNow;
@synthesize btnCheckIn;
@synthesize btnCheckOut;
@synthesize btnVewiGameFeed;
@synthesize btnPostShout;
@synthesize numberCheckedInUser;
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[SharedComponent getInstance] setCurrentViewController:self];
    
    SportTypeEnum sportType = [[SharedComponent getInstance] getSportType:selectedGame.sportID];
    
    switch (sportType) {
        case SportBaseball:
            scoreBoard = [[ScoreBoardBaseball alloc] initWithFrame:CGRectMake(0, 0, 279, 135)];
            break;
        case SportFootball:
        case SportSoccer:
        case SportBasketball:
        case SportUnknown:
            scoreBoard = [[ScoreBoardFootball alloc] initWithFrame:CGRectMake(0, 0, 279, 135)];
            break;
    }
    
    [scoreBoard setDelegate:self];    
    
    //[[scoreBoard getTeam1Button].titleLabel setAdjustsFontSizeToFitWidth:YES];
    //[[scoreBoard getTeam2Button].titleLabel setAdjustsFontSizeToFitWidth:YES];
    
    //[[scoreBoard getTeam1Button] setTitle:selectedGameBox.game.team1Name forState:UIControlStateNormal];
    //[[scoreBoard getTeam2Button] setTitle:selectedGameBox.game.team2Name forState:UIControlStateNormal];
    
    [fbCheckbox setText:@"Facebook"];
    //[fbCheckbox setStatus:YES];
    //[fbCheckbox  setUserInteractionEnabled:FALSE];

    // shout to Twitter as default
    [twitterCheckbox setText:@"Twitter"];
    [twitterCheckbox setStatus:YES];
    
    [btnUpdateScore addTarget:self action:@selector(btnUpdateScoreTapped:) forControlEvents:UIControlEventTouchUpInside];
    [btnRequestScore addTarget:self action:@selector(btnRequestScoreTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [shoutMessage setDelegate:self];
    
    isGettingScore = NO;
    
    [self updateUIStatus];
    
    if (selectedGame.localStatusId.intValue == CheckedIn){
        [btnRequestScore setHidden:YES];
        [btnUpdateScore setHidden:NO];
        
        [btnCheckIn setHidden:YES];
        [btnCheckOut setHidden:NO];
    }
    
    [numberCheckedInUser setText:[NSString stringWithFormat:@"%@ checked-in users", selectedGame.numberCheckedInUser]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameStatusChanged:) name:@"GameStatusChanged" object:nil];
    
    // live update game status
    [self performSelector:@selector(refreshGameStatus) withObject:nil afterDelay:7];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [scoreBoard setFrame:CGRectMake(21, 90, 279, 135)];
    [self.mainScrollView addSubview:scoreBoard];
    
    [scoreBoard setReadOnly:TRUE];
    
    //[scoreBoard updateGameScoreFromGameBox:self.selectedGameBox];
    [scoreBoard showGame:selectedGame];
    
    [self changeOrientation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GameStatusChanged" object:nil];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshGameStatus) object:nil];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self setFbCheckbox:nil];
    [self setTwitterCheckbox:nil];
    [self setShoutMessage:nil];
    [self setBtnRequestScore:nil];
    [self setBtnUpdateScore:nil];
    [self setScoreBoard:nil];
    [self setBtnRefreshNow:nil];
    [self setBtnCheckIn:nil];
    [self setBtnVewiGameFeed:nil];
    [self setBtnPostShout:nil];
    [self setBtnCheckOut:nil];
    [self setNumberCheckedInUser:nil];
}

- (void)dealloc
{
    //[selectedGameBox release];
    // call this make the control to release instead
    [scoreBoard killTimer];
    // release this cause error, due to using multiple view in one xib
    //[scoreBoard release];
    
    [fbCheckbox release];
    [twitterCheckbox release];
    [shoutMessage release];
    [btnRequestScore release];
    [btnUpdateScore release];
    [btnRefreshNow release];
    [btnCheckIn release];
    [btnVewiGameFeed release];
    [btnPostShout release];
    [btnCheckOut release];
    [numberCheckedInUser release];
    [super dealloc];
}

- (void)gameStatusChanged:(NSNotification*)notification {
    NSDictionary* userInfo = notification.userInfo;
    NSString* gameId = [userInfo valueForKey:@"GameId"];
    NSString* statusId = [userInfo valueForKey:@"StatusId"];
    
    if ([gameId isEqualToString:self.selectedGame.gameID]) {
        self.selectedGame.statusID = statusId;
        [self updateUIStatus];
    }
}

- (void)refreshGameStatus {
    // update game status from server to auto start / end game
    [[SBNService sharedInstance] getGameStatus:@[self.selectedGame.gameID] successHandler:^(GetGameStatusResult *result) {
        if ([result.IsSuccess boolValue]) {
            Game* game = [result.games firstObject];
            if (game != nil && ![game.statusID isEqualToString:self.selectedGame.statusID]) {
                self.selectedGame.statusID = game.statusID;
                [self.selectedGame updateDatabase];
                [self updateUIStatus];
            }
        }
        [self performSelector:@selector(refreshGameStatus) withObject:nil afterDelay:7];
    } failHandler:^(NSError *err) {
        // network error, try again later
        [self performSelector:@selector(refreshGameStatus) withObject:nil afterDelay:7];
    }];
}

- (void)updateUIStatus {
    if (selectedGame.statusID.intValue != Process || selectedGame.localStatusId.intValue == End){
        [btnCheckIn setEnabled:NO];
        [btnCheckOut setEnabled:NO];
        [btnRefreshNow setEnabled:NO];
        [btnRequestScore setEnabled:NO];
        [btnUpdateScore setEnabled:NO];
        [btnPostShout setEnabled:NO];
        [shoutMessage setEditable:NO];
        
        [scoreBoard killTimer];
    }
    else
    {
        [btnCheckIn setEnabled:YES];
        [btnCheckOut setEnabled:YES];
        [btnRefreshNow setEnabled:YES];
        [btnRequestScore setEnabled:YES];
        [btnUpdateScore setEnabled:YES];
        [btnPostShout setEnabled:YES];
        [shoutMessage setEditable:YES];
        
        [scoreBoard startTimer];
    }
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        [[SharedComponent getInstance] setInterfaceOrientation:toInterfaceOrientation];
        return YES;
    }
    
    return NO;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self changeOrientation];
}

- (void) changeOrientation
{
    if ([CommonFunction checkPortraitOrientation])
    {
        [self.view setFrame:CGRectMake(0, 0, 320, 480)];
        [self.mainScrollView setFrame:CGRectMake(0, 0, 320, 480)];
        [self.mainScrollView setContentSize:CGSizeMake(320, 480)];
        
        [scoreBoard setFrame:CGRectMake(21, 90, 279, 135)];
        [scoreBoard changeOrientation:Portrait];
        
        [btnRefreshNow setFrame:CGRectMake(16, 245, 83, 23)];
        [btnRequestScore setFrame:CGRectMake(207, 245, 93, 23)];
        [btnUpdateScore setFrame:CGRectMake(207, 245, 93, 23)];
    }
    else if ([CommonFunction checkLandscapeOrientation])
    {
        [self.view setFrame:CGRectMake(0, 0, 480, 320)];
        [self.mainScrollView setFrame:CGRectMake(0, 0, 480, 320)];
        [self.mainScrollView setContentSize:CGSizeMake(480, 320)];
        
        [scoreBoard setFrame:CGRectMake(0, 0, 480, 320)];
        [scoreBoard changeOrientation:Landscape];
        
        [btnRefreshNow setFrame:CGRectMake(17, 279, 83, 23)];
        [btnRequestScore setFrame:CGRectMake(377, 279, 93, 23)];
        [btnUpdateScore setFrame:CGRectMake(377, 279, 93, 23)];
    }
    
    [self.mainScrollView bringSubviewToFront:btnRefreshNow];
    [self.mainScrollView bringSubviewToFront:btnRequestScore];
    [self.mainScrollView bringSubviewToFront:btnUpdateScore];
}

- (void) textViewDidBeginEditing:(UITextView *)textView{
    if ([[textView text] isEqualToString:@"Write something ..."]){
        [textView setText:@""];
    }
    
    [self slideFrameWhenFocus:textView];
}

- (void) textViewDidEndEditing:(UITextView *)textView{
    if ([[textView text] isEqualToString:@""]){
        [textView setText:@"Write something ..."];
    }
}

- (void) btnRequestScoreTapped:(id)sender{
    if (focusedField != nil)
    {
        [focusedField resignFirstResponder];
        [self slideFrameWhenUnFocus:focusedField];
    }
    
    currentStep = RequestScore;
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildRequestScoreBody:loginToken gameId:selectedGame.gameID];
    [self sendRequest];
}

- (void) btnUpdateScoreTapped:(id)sender{
    UpdateScoreViewController *viewController = [[UpdateScoreViewController alloc] initWithNibName:@"UpdateScoreViewController" bundle:nil];
    
    [viewController setScoreBoard:scoreBoard];
    //[viewController setSelectedGameBox:selectedGameBox];
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (IBAction)btnRefreshTapped:(id)sender {
    [self getGameScore];
}

- (IBAction)hideKeyboard:(id)sender {
    if (focusedField == shoutMessage){
        [shoutMessage resignFirstResponder];
        [self slideFrameWhenUnFocus:shoutMessage];
    }
}

- (void) refresh:(BaseScoreBoard *)scoreBoard{
    [self getGameScore];
}

- (void) getGameScore
{
    if (isGettingScore) return;
    
    isGettingScore = YES;
    [self.scoreBoard stopTimer];
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    
    if (sbnGetScoreRequest == nil){
        sbnGetScoreRequest = [[SBNRequest alloc] init];
    }
    
    [sbnGetScoreRequest buildGetScoreBody:loginToken listGameId:[NSArray arrayWithObject:selectedGame.gameID]];
    [self sendGetScoreRequest];
}

- (IBAction)btnTwitterFeedTapped:(id)sender {
    if ([[SharedComponent getInstance] getTwitterAccount] == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please login to your Twitter account first." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    TwitterFeedViewController *viewController = [[TwitterFeedViewController alloc] initWithNibName:@"TwitterFeedViewController" bundle:nil];
    
    [viewController setHashTag:[hashTagPrefix stringByAppendingString: selectedGame.gameNumber]];
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (IBAction)btnPostShoutTapped:(id)sender {
    if (focusedField != nil)
    {
        [focusedField resignFirstResponder];
        [self slideFrameWhenUnFocus:focusedField];
    }
    
    messageShout = [shoutMessage.text retain];    
    if ([messageShout isEqualToString:@"Write something ..."]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please input shout message." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }    
        
    isShouldPostToFB = [fbCheckbox isChecked];
    isShouldPostToTwitter = [twitterCheckbox isChecked];
    
    if (!isShouldPostToFB && !isShouldPostToTwitter) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please select where to post shout (Facebook or Twitter)." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    // auto append current score.
    messageShout = [messageShout stringByAppendingFormat:@". %@ %@ - %@ %@.", selectedGame.team1Name, selectedGame.team1Score, selectedGame.team2Score, selectedGame.team2Name];
   
    //currentStep = PostShout;
    //[fbCheckbox setStatus:NO];
    //[twitterCheckbox setStatus:NO];
    
    //NSString *message = messageShout;
    //NSString *loginToken = [[SharedComponent getInstance] loginToken];
    
    //if (sbnRequest == nil){
    //    sbnRequest = [[SBNRequest alloc] init];
    //}
    
    //[sbnRequest buildPostShoutBody:loginToken gameId:selectedGameBox.game.gameID message:message];
    //[self sendRequest];
    
    if (isShouldPostToFB)
        [self PostShoutFB];
    else if (isShouldPostToTwitter)
        [self PostShoutTwitter];
    
    [shoutMessage setText:@"Write something ..."];
}

- (IBAction)checkInGameTapped:(id)sender {
    if (focusedField != nil)
    {
        [focusedField resignFirstResponder];
        [self slideFrameWhenUnFocus:focusedField];
    }
    
    currentStep = CheckIn;
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildCheckInGameBody:loginToken gameId:selectedGame.gameID];
    [self sendRequest];
}

- (IBAction)btnCheckOutTapped:(id)sender {
    if (focusedField != nil)
    {
        [focusedField resignFirstResponder];
        [self slideFrameWhenUnFocus:focusedField];
    }
    
    currentStep = CheckOut;
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildCheckOutGameBody:loginToken gameId:selectedGame.gameID];
    [self sendRequest];
}

- (void) PostShoutFB {
    /*NSArray *permissions = [[NSArray alloc] initWithObjects:@"offline_access", nil];
    NSMutableDictionary *params =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     @"Score Board Now", @"name",
     messageShout, @"caption",
     @"", @"description",
     @"http://scoreboardnow.com", @"link",
     @"http://scoreboardnow.com/images/SBNLogo.png", @"picture",
     nil];
    
    if (![[[SharedComponent getInstance] facebook] isSessionValid]) {
        [[[SharedComponent getInstance] facebook] authorize:permissions];
        [[[SharedComponent getInstance] facebook] dialog:@"feed"
                                               andParams:params
                                             andDelegate:self];
    }
    else
    {
        [[[SharedComponent getInstance] facebook] dialog:@"feed"
                                               andParams:params
                                             andDelegate:self];
    }*/
    
    // iOS 6 Social
    /*SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller setInitialText:messageShout]; // not work anymore
    [controller addURL:[NSURL URLWithString:@"http://scoreboardnow.com"]];
    [controller addImage:[UIImage imageNamed:@"SBNLogo.png"]];
    [self presentViewController:controller animated:YES completion:nil];*/
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL
                          URLWithString:@"http://scoreboardnow.com"];
    //content.contentTitle = title;
    content.contentDescription = messageShout;
    content.imageURL = [NSURL URLWithString:@"http://scoreboardnow.com/images/SBNLogo.png"];
    
    FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeNative;
    dialog.shareContent = content;
    dialog.fromViewController = self;
    dialog.delegate = self;
    [dialog show];
    
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([twitterCheckbox isChecked])
            [self PostShoutTwitter];
    });
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([twitterCheckbox isChecked])
            [self PostShoutTwitter];
    });
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    
}

- (void) PostShoutTwitter {
    /*TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];
    NSString *postData = [NSString stringWithFormat:@"%@ %@%@ %@", hashTagSBN, hashTagPrefix2, selectedGame.gameNumber, messageShout];    
    
    if ([postData length] > 140)
    {
        postData = [postData substringToIndex:140];
    }
    
    [twitter setInitialText:[NSString stringWithFormat:@"%@", postData]];
    
    [self presentViewController:twitter animated:YES completion:nil];
    twitter.completionHandler = ^(TWTweetComposeViewControllerResult res) {
        if(res == TWTweetComposeViewControllerResultDone) {
            [twitter dismissModalViewControllerAnimated:YES];
        }
        if(res == TWTweetComposeViewControllerResultCancelled) {
            [twitter dismissModalViewControllerAnimated:YES];
        }
    };*/
    
    // iOS 6 Social
    //if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    //{
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *postData = [NSString stringWithFormat:@"%@ %@%@ %@", hashTagSBN, hashTagPrefix2, selectedGame.gameNumber, messageShout];
        
        if ([postData length] > 140)
            postData = [postData substringToIndex:140];
        
        [tweetSheet setInitialText:postData];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    //}
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNResponse *response = nil;
    switch (currentStep) {
        case PostShout:
            response = [[SBNPostShoutResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case RequestScore:
            response = [[SBNRequestGameScoreResponse alloc] init];
            [response parseResponseData:responseData];    
            break;
        case CheckIn:
            response = [[SBNCheckInGameResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case CheckOut:
            response = [[SBNCheckOutGameResponse alloc] init];
            [response parseResponseData:responseData];
            break;            
        default:
            break;
    }
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]){
        switch (currentStep) {
            case PostShout:
            {
                /*
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Shout has been posted successfully." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }*/
                break;
            }
            case RequestScore:
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Request score is sent successful" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                break;
            }
            case CheckIn:
            {
                [btnRequestScore setHidden:YES];
                [btnUpdateScore setHidden:NO];
                
                [btnCheckIn setHidden:YES];
                [btnCheckOut setHidden:NO];
                
                selectedGame.localStatusId = [NSString stringWithFormat:@"%d",CheckedIn];
                
                //update in local data
                NSString *databasePath = [[SharedComponent getInstance] databasePath];
                sqlite3 *database;
                const char *sqlStatement;
                sqlite3_stmt *compiledStatement;
                NSString *query = @"";
                
                // Open the database from the users filessytem
                if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK){
                    //check game is exist
                    query = [NSString stringWithFormat:@"update games set LocalStatusId='%d' where Id='%@' AND User='%@'", CheckedIn, selectedGame.gameID, [CommonFunction getDataWithKey:kUser]];
                    
                    sqlStatement = [query UTF8String];
                    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
                        if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                            NSLog(@"OK");
                        }
                    }
                    sqlite3_finalize(compiledStatement);
                }
                
                sqlite3_close(database);
                
                // refresh game score
                [self getGameScore];
                
                break;
            }
            case CheckOut:
            {
                [btnRequestScore setHidden:NO];
                [btnUpdateScore setHidden:YES];
                
                [btnCheckIn setHidden:NO];
                [btnCheckOut setHidden:YES];
                
                selectedGame.localStatusId = [NSString stringWithFormat:@"%d",-1];
                
                //update in local data
                NSString *databasePath = [[SharedComponent getInstance] databasePath];
                sqlite3 *database;
                const char *sqlStatement;
                sqlite3_stmt *compiledStatement;
                NSString *query = @"";
                
                // Open the database from the users filessytem
                if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK){
                    //check game is exist
                    query = [NSString stringWithFormat:@"update games set LocalStatusId='%d' where Id='%@' AND User='%@'", -1, selectedGame.gameID, [CommonFunction getDataWithKey:kUser]];
                    
                    sqlStatement = [query UTF8String];
                    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
                        if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                            NSLog(@"OK");
                        }
                    }
                    sqlite3_finalize(compiledStatement);
                }
                
                sqlite3_close(database);
                
                // refresh game score
                [self getGameScore];
                
                break;
            }
            default:
                break;
        }
    }
    else
    {
        NSString *errorMessage = @"Invalid Server Response.";
        if (![response.errorMessage isEqualToString:@""])
            errorMessage = response.errorMessage;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    [responseData release];
    [response release];
}

- (void) handleGeScoreResponse {
    if ([sbnGetScoreRequest.receivedData length] <= 0){
        isGettingScore = NO;
        return;
    }
    
    NSString *responseData = [[NSString alloc] initWithData: sbnGetScoreRequest.receivedData encoding:NSUTF8StringEncoding];
    
    [sbnGetScoreRequest release];
    sbnGetScoreRequest = nil;
    
    SBNResponse *response = [[SBNGameScoreResponse alloc] init];
    [response parseResponseData:responseData];
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL])
    {
        SBNGameScoreResponse *gamScoreResponse = (SBNGameScoreResponse*)response;
        NSMutableArray *listGameScore = [gamScoreResponse listGameScore];
        if ([listGameScore count] == 1)
        {
            GameScore *gameScore = [listGameScore objectAtIndex:0];
            [scoreBoard updateGameScore:gameScore];
            [selectedGame updateScore:gameScore];
            
            /*
            GameBox *gameBox = selectedGameBox;
            if (gameBox != nil){
                [gameBox.game  updateScore:gameScore];
                [gameBox updateGameScore];            }*/
            
            [numberCheckedInUser setText:[NSString stringWithFormat:@"%@ checked-in users", gameScore.numberCheckedInUser]];
            
            // update game in database
            [[scoreBoard currentGame] updateDatabase];
            
            /*
            NSString *databasePath = [[SharedComponent getInstance] databasePath];
            sqlite3 *database;
            const char *sqlStatement;
            sqlite3_stmt *compiledStatement;
            NSString *query = @"";
            
            // Open the database from the users filessytem
            if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK){
                //check game is exist
                query = [NSString stringWithFormat:@"update games set team1Score='%@', team2Score='%@', qtr='%@', lastupdated='%@', hours='%@', minutes='%@', seconds='%@', NumberCheckedInUser='%@' where id='%@' AND User='%@'", gameScore.team1Score, gameScore.team2Score, gameScore.qtr, gameScore.lastUpdated, gameScore.hours, gameScore.minutes, gameScore.seconds, gameScore.numberCheckedInUser, gameScore.gameId, [CommonFunction getDataWithKey:kUser]];
                sqlStatement = [query UTF8String];
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
                    if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                        NSLog(@"OK");
                    }
                }
                sqlite3_finalize(compiledStatement);
            }
            sqlite3_close(database);
            */
        }
    }
    else
    {
        if ([response.errorMessage isEqualToString:INVALID_LOGIN_TOKEN]) {
            [self logout];
            return;
        }
        
        NSString *errorMessage = @"Invalid Server Response.";
        if (response.errorMessage.length > 0)
            errorMessage = response.errorMessage;
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    
    [responseData release];
    [response release];
    isGettingScore = NO;
    
    [scoreBoard startTimer];
}

- (void) handleFailRequest{
    [super handleFailRequest];
    isGettingScore = NO;
    [scoreBoard startTimer];
}

- (void) dialogDidNotComplete:(FBDialog *)dialog{
    NSLog(@"Post FB not completed."); 
    
    /*if (isShouldPostToTwitter)
    {
        TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];
        NSString *postData = [hashTagPrefix2 stringByAppendingFormat:@"%@ %@", selectedGameBox.game.gameNumber, messageShout];
        
        if ([postData length] > 140)
        {
            postData = [postData substringToIndex:140];
        }
        
        [twitter setInitialText:[NSString stringWithFormat:@"%@", postData]];
        
        [self presentViewController:twitter animated:YES completion:nil];
        twitter.completionHandler = ^(TWTweetComposeViewControllerResult res) {
            if(res == TWTweetComposeViewControllerResultDone) {
                [twitter dismissModalViewControllerAnimated:YES];
            }
            if(res == TWTweetComposeViewControllerResultCancelled) {
                [twitter dismissModalViewControllerAnimated:YES];
            }
        };
    }*/
    //[dialog dismissWithSuccess:YES animated:YES];

    if (isShouldPostToTwitter)
        [self PostShoutTwitter];
}

-(void) dialogDidComplete:(FBDialog *)dialog{
    NSLog(@"Post FB completed.");
    
    /*if (isShouldPostToTwitter)
    {
        TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];
        NSString *postData = [hashTagPrefix2 stringByAppendingFormat:@"%@ %@", selectedGameBox.game.gameNumber, messageShout];

        if ([postData length] > 140)
        {
            postData = [postData substringToIndex:140];
        }
        
        [twitter setInitialText:[NSString stringWithFormat:@"%@", postData]];
        
        [self presentViewController:twitter animated:YES completion:nil];
        twitter.completionHandler = ^(TWTweetComposeViewControllerResult res) {
            if(res == TWTweetComposeViewControllerResultDone) {
                [twitter dismissModalViewControllerAnimated:YES];
            }
            if(res == TWTweetComposeViewControllerResultCancelled) {
                [twitter dismissModalViewControllerAnimated:YES];
            }
        };
    }*/
    
    //[dialog dismissWithSuccess:YES animated:YES];
    if (isShouldPostToTwitter)
        [self PostShoutTwitter];
}

@end
