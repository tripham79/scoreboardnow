//
//  SelectStadiumInfoViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/11/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"
#import "MyDelegates.h"

@interface SelectStadiumInfoViewController : BaseViewController<StadiumDelegate>

@property (assign) id<StadiumDelegate> delegate;
@property (retain, nonatomic) Stadium* stadium;
@property (retain, nonatomic) IBOutlet UIView *bgView;
@property (retain, nonatomic) IBOutlet UILabel *lblStadiumName;
@property (retain, nonatomic) IBOutlet UILabel *lblStadiumAddress;
@property (retain, nonatomic) IBOutlet UILabel *lblStadiumCity;
- (IBAction)btnCancelClick:(id)sender;
- (IBAction)btnSelectClick:(id)sender;
- (IBAction)btnReportErrorClick:(id)sender;

@end
