//
//  UIView+Extensions.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "UIView+Extensions.h"

@implementation UIView (Extensions)

- (UIView *)findSuperViewWithClass:(Class)superViewClass {
    
    UIView *superView = self.superview;
    UIView *foundSuperView = nil;
    
    while (nil != superView && nil == foundSuperView) {
        if ([superView isKindOfClass:superViewClass]) {
            foundSuperView = superView;
        } else {
            superView = superView.superview;
        }
    }
    return foundSuperView;
}

+ (UIView *)showWaiting:(NSString *)msg parentView:(UIView *)parentView {
    if (msg.length == 0)
        msg = @"Processing...";
    
    CGRect frame = CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height);
    UIView* waitingView = [[UIView alloc] initWithFrame: frame];
    [waitingView setBackgroundColor:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.5]];
    
    //NSLog(@"x: %f, y: %f, h: %f", frame.origin.x, frame.origin.y, frame.size.height);
    
    CGPoint center = CGPointMake(parentView.frame.size.width/2, parentView.frame.size.height/2);
    
    UILabel *label = [[UILabel alloc] init];
    [label setText:msg];
    [label setTextColor:[UIColor darkGrayColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFrame:CGRectMake(0, center.y - 25, frame.size.width, 21)];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
    [activity setFrame:CGRectMake(center.x-15, center.y, 30, 30)];
    
    [activity startAnimating];
    [waitingView addSubview:activity];
    [waitingView addSubview:label];
    
    [parentView addSubview:waitingView];
    return waitingView;
}

+ (UIView *)showWaiting:(NSString *)msg parentView:(UIView *)parentView tapAction:(SEL)tapAction target:(id)target;
{
    UIView* waitingView = [UIView showWaiting:msg parentView:parentView];
    CGPoint center = CGPointMake(parentView.frame.size.width/2, parentView.frame.size.height/2);
    
    if (tapAction != nil) {
        UILabel *labelCancel = [[UILabel alloc] init];
        [labelCancel setText:@"(Tap to cancel)"];
        [labelCancel setTag:1];
        [labelCancel setTextColor:[UIColor darkGrayColor]];
        [labelCancel setBackgroundColor:[UIColor clearColor]];
        [labelCancel setTextAlignment:NSTextAlignmentCenter];
        [labelCancel setFrame:CGRectMake(0, center.y + 30 + 25, parentView.frame.size.width, 21)];
        [waitingView addSubview:labelCancel];
        
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:target action:tapAction];
        [waitingView addGestureRecognizer:tapGesture];
    }
    
    return waitingView;
}

+ (void)removeWaiting:(UIView *)waitingView {
    [waitingView removeFromSuperview];
}


@end
