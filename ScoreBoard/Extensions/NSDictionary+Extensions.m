//
//  NSDictionary+Extensions.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "NSDictionary+Extensions.h"
#import <objc/runtime.h>

@implementation NSDictionary (Extensions)

// can convert back to dictionary this way but we need to provide an string array for object properties
// NSDictionary *properties = [self dictionaryWithValuesForKeys:[self allKeys]];
+ (NSMutableDictionary*)dictionaryWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        Class classObject = NSClassFromString([key capitalizedString]);
        if (classObject) {
            id subObj = [self dictionaryWithPropertiesOfObject:[obj valueForKey:key]];
            [dict setObject:subObj forKey:key];
        }
        else
        {
            id value = [obj valueForKey:key];
            if(value)
                [dict setObject:value forKey:key];
            //else
            //    [dict setObject:[NSNull null] forKey:key];
        }
    }
    
    free(properties);
    //return [NSDictionary dictionaryWithDictionary:dict];
    
    // return NSMutableDictionary so user can append more values to the output
    return dict;
}

- (NSString*)stringForKey:(NSString*)key {
    id value = [self objectForKey:key];
    if (value == nil || value == [NSNull null])
        return @"";
    
    return [NSString stringWithFormat:@"%@", value];
}

@end