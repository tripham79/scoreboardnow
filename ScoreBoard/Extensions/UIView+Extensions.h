//
//  UIView+Extensions.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 12/4/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extensions)

- (UIView *)findSuperViewWithClass:(Class)superViewClass;

+ (UIView *)showWaiting:(NSString *)msg parentView:(UIView *)parentView;
+ (UIView *)showWaiting:(NSString*)msg parentView:(UIView*)parentView tapAction:(SEL)tapAction target:(id)target;
+ (void) removeWaiting:(UIView*)waitingView;


@end
