//
//  UIViewController+Extensions.m
//  GasketMaster
//
//  Created by Tri Pham Minh on 4/15/14.
//  Copyright (c) 2014 Flexitallic. All rights reserved.
//

#import "UIViewController+Extensions.h"

@implementation UIViewController (Extensions)

- (void)displayChildController:(UIViewController*)child
{
    UINavigationController *frontController = (id)self.navigationController;
    //NSLog(@"%@", frontController);
    
    // this work
    [frontController presentViewController:child animated:YES completion:nil];
    
    // this not work
    //[frontNavigationController pushViewController:child animated:YES];
    
    // this work
    /*UIViewController *parentController = frontNavigationController.topViewController;
     [parentController addChildViewController:child];
     child.view.frame = parentController.view.bounds;
     [parentController.view addSubview:child.view];
     [child didMoveToParentViewController:parentController];*/
}

- (void)hideChildController:(UIViewController*)child
{
    //UIViewController *frontController = self.slidingViewController.topViewController;
    //UINavigationController *frontNavigationController = (id)revealController.frontViewController;
    //NSLog(@"%@", frontController);
    
    // this also work
    //[frontNavigationController.topViewController dismissViewControllerAnimated:YES completion:nil];
    [child.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
    // this not work
    //[frontNavigationController popViewControllerAnimated:YES];
    
    // this work
    /*UIViewController *parentController = frontNavigationController.topViewController;
     [child willMoveToParentViewController:parentController];
     [child.view removeFromSuperview];
     [child removeFromParentViewController];*/
}

- (void)openURL:(NSString *)url {
    if (url.length == 0)
        return;
    
    if (![url hasPrefix:@"http"])
        url = [@"http://" stringByAppendingString:url];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)callPhoneNumber:(NSString*)number {
    if (number.length == 0)
        return;
    
    NSString *url = [@"telprompt://" stringByAppendingString:number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

@end
