//
//  UIViewController+Extensions.h
//  GasketMaster
//
//  Created by Tri Pham Minh on 4/15/14.
//  Copyright (c) 2014 Flexitallic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Extensions)

- (void)displayChildController:(UIViewController*)child;
- (void)hideChildController:(UIViewController*)child;

- (void)openURL:(NSString *)url;
- (void)callPhoneNumber:(NSString*)number;

@end
