//
//  NSHTTPURLResponse+StatusCodes.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHTTPURLResponse (StatusCodes)
-(BOOL)is200OK;
-(BOOL)isOK;
-(BOOL)isRedirect;
-(BOOL)isClientError;
-(BOOL)isServerError;
@end
