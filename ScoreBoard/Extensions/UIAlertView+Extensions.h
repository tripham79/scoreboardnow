//
//  UIAlertView+Extensions.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Extensions)

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end


inline static void showAlert(NSString* title, NSString *message, id delegate)
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

inline static void showQuestion(NSString* title, NSString *message, id delegate)
{
    NSString* yesText = @"Yes";
    NSString* noText = @"No";
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate  cancelButtonTitle:noText otherButtonTitles:yesText, nil];
    [alert show];
}

inline static void showAlertWithBlock(NSString* title, NSString *message, void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex))
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alert showWithCompletion:completionBlock];
}


inline static void showQuestionWithBlock(NSString* title, NSString *message, void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex))
{
    NSString* yesText = @"Yes";
    NSString* noText = @"No";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:noText
                                              otherButtonTitles:yesText, nil];
    
    [alertView showWithCompletion:completionBlock];
}
