//
//  NSString+Extensions.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "NSString+Extensions.h"

@implementation NSString (Extensions)

- (NSDate *)jsonStringToDateUtc {
    if ([self isEqual:[NSNull null]] || self == nil || [self isEqual:@""])
        return nil;
    
    static NSRegularExpression *dateRegEx = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateRegEx = [[NSRegularExpression alloc] initWithPattern:@"^\\/date\\((-?\\d++)(?:([+-])(\\d{2})(\\d{2}))?\\)\\/$" options:NSRegularExpressionCaseInsensitive error:nil];
    });
    
    NSTextCheckingResult *regexResult = [dateRegEx firstMatchInString:self options:0 range:NSMakeRange(0, [self length])];
    
    if (regexResult) {
        // milliseconds
        NSTimeInterval seconds = [[self substringWithRange:[regexResult rangeAtIndex:1]] doubleValue] / 1000.0;
        
        // dont add timezone offset to return UTC date time.
        // timezone offset
        /*if ([regexResult rangeAtIndex:2].location != NSNotFound) {
         NSString *sign = [self substringWithRange:[regexResult rangeAtIndex:2]];
         // hours
         double offset = [[NSString stringWithFormat:@"%@%@", sign, [self substringWithRange:[regexResult rangeAtIndex:3]]] doubleValue];
         seconds += offset * 60.0 * 60.0;
         
         // minutes
         offset = [[NSString stringWithFormat:@"%@%@", sign, [self substringWithRange:[regexResult rangeAtIndex:4]]] doubleValue];
         seconds +=  offset * 60.0;
         }*/
        
        NSDate* date = [NSDate dateWithTimeIntervalSince1970:seconds];
        //NSLog(@"%@", date);
        return date;
    }
    return nil;
}

- (NSString *)urlEncodeDefault {
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,                                                                                                      (CFStringRef)self,                                                                                                    NULL,                                                                                                    NULL,                                                                                                    kCFStringEncodingUTF8 ));
    return encodedString;
}

@end
