//
//  Config.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//
#import "UIAlertView+Extensions.h"

#define SBNEnpoint @"http://scoreboardnow.com/Services/SBNService.svc"
//#define SBNEnpoint @"http://192.168.0.101/SBN/Services/SBNService.svc"
//#define SBNEnpoint @"http://10.0.0.190/SBN/Services/SBNService.svc"

#define INVALID_LOGIN_TOKEN @"INVALID_LOGIN_TOKEN"
#define PAGING_RECORD 15

//#define DONTHAVELOGS
#ifdef  DONTHAVELOGS
#define NSLog(...)
#endif


// Check if the "thing" pass'd is empty
static inline BOOL isEmpty(id thing) {
    return thing == nil
    || [thing isKindOfClass:[NSNull class]]
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}


