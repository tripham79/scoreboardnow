//
//  Sport.h
//  ScoreBoard
//
//  Created by Apple on 2/6/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sport : NSObject

- (id) initWithString: (NSString*) source;

@property (retain) NSString *sportId;
@property (retain) NSString *sportTypeId;
@property (retain) NSString *sportName;

@end
