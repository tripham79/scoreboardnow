
#import "Team.h"
#import "NSDictionary+Extensions.h"

@implementation Team
@synthesize teamID, divisionID, teamName, teamNumber, stadiumID, user, sportID, sportName, leagueName, divisionName;
@synthesize isChecked;

- (id) initFromDatabase:(sqlite3_stmt *)compiledStatement{
    self = [super init];
    if (self){
        self.teamID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
        self.divisionID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
        self.teamName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
        self.teamNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
        self.stadiumID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
        self.user = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
        
        if (sqlite3_column_text(compiledStatement, 6) != nil)
            self.sportName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
        
        if (sqlite3_column_text(compiledStatement, 6) != nil)
            self.leagueName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
        
        if (sqlite3_column_text(compiledStatement, 8) != nil)
            self.divisionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];

    }
    return self;
}

- (id) initWithString:(NSString *)source{
    self = [super init];
    if (self){
        self.teamID = [CommonFunction getInnerTextInSource:source byTagName:@"Id"];
        self.divisionID = [CommonFunction getInnerTextInSource:source byTagName:@"DivisionId"];
        self.teamName = [CommonFunction getInnerTextInSource:source byTagName:@"Name"];
        self.teamNumber = [CommonFunction getInnerTextInSource:source byTagName:@"Number"];
        self.stadiumID = [CommonFunction getInnerTextInSource:source byTagName:@"StadiumId"];
        self.user = [CommonFunction getDataWithKey:kUser];
        
        self.sportName = [CommonFunction getInnerTextInSource:source byTagName:@"SportName"];
        self.leagueName = [CommonFunction getInnerTextInSource:source byTagName:@"LeagueName"];
        self.divisionName = [CommonFunction getInnerTextInSource:source byTagName:@"DivisionName"];
    }
    return self;
}

- (id) initWithDictionary:(NSDictionary*)dict {
    self = [super init];
    if (self){
        self.teamID = [dict stringForKey:@"Id"];
        self.divisionID = [dict stringForKey:@"DivisionId"];
        self.teamName = [dict stringForKey:@"Name"];
        self.teamNumber = [dict stringForKey:@"Number"];
        self.stadiumID = [dict stringForKey:@"StadiumId"];
        self.sportID = [dict stringForKey:@"SportId"];
        self.sportName = [dict stringForKey:@"SportName"];
        self.leagueName = [dict stringForKey:@"LeagueName"];
        self.divisionName = [dict stringForKey:@"DivisionName"];
        
        NSDictionary* jsonDict = [dict objectForKey:@"Stadium"];
        if (jsonDict != nil &&  (id)jsonDict != [NSNull null]) {
            self.stadium = [[Stadium alloc] initWithDictionary:jsonDict];
        }
        
        self.user = [CommonFunction getDataWithKey:kUser];
    }
    return self;
}

- (NSString*) buildInsertQuery {
    NSString *query = [NSString stringWithFormat:@"insert into Teams (Id, DivisionId, Name, Number, StadiumId, User, SportName, LeagueName, DivisionName) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@')", teamID, divisionID, teamName, teamNumber, stadiumID, user, sportName, leagueName, divisionName];
    NSLog(@"%@", query);
    return query;
}

- (BOOL) insertDatabase {
    //insert game to database
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    BOOL success = FALSE;
    
    NSString *query = self.buildInsertQuery;
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            sqlite3_bind_text(compiledStatement, 1, [self.teamName UTF8String], -1, SQLITE_STATIC);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                //while (sqlite3_step(compiledStatement) == SQLITE_DONE){
                NSLog(@"Insert Team OK.");
                success = TRUE;
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    //[query release];
    return success;
}

+ (BOOL) deleteTeam:(NSString*)teamId {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement, *compiledStatement2;
    NSString *query = @"";
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK){
        query = [NSString stringWithFormat:@"delete from teams where Id='%@' AND User='%@'", teamId, [CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
            if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                NSLog(@"Delete team %@ successful!!!", teamId);
            }
        }
        sqlite3_finalize(compiledStatement);
        
        // delete all related games
        query = [NSString stringWithFormat:@"delete from games where (Team1Id='%@' OR Team2Id='%@') AND User='%@'", teamId, teamId, [CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement2, NULL) == SQLITE_OK){
            if (sqlite3_step(compiledStatement2) == SQLITE_DONE){
                NSLog(@"Delete games for team %@ successful!!!", teamId);
            }
        }
        sqlite3_finalize(compiledStatement2);
    }
    
    sqlite3_close(database);
    return TRUE;

}

- (BOOL) deleteDatabase {
    return [Team deleteTeam:self.teamID];
}

+ (NSMutableArray*) loadTeams {
    NSMutableArray *listTeamInLocal = [[NSMutableArray alloc] init];
    
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK){
        query = [NSString stringWithFormat:@"select * from teams where User='%@' order by Name", [CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                Team *team = [[Team alloc] initFromDatabase:compiledStatement];
                [listTeamInLocal addObject:team];
                [team release];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return listTeamInLocal;
}

+ (NSArray*) loadTeamIds {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    NSMutableArray *list = [[[NSMutableArray alloc] init] autorelease];
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {        
        query = [NSString stringWithFormat: @"select Id from teams where User='%@' ORDER BY Id",[CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                //Team *team = [[Team alloc] initFromDatabase:compiledStatement];
                NSString* teamID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                [list addObject:teamID];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return list;
}

+ (BOOL) hasTeam:(NSString*)teamId {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from teams where Id='%@' and User='%@'", teamId, [CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count > 0;
}

+ (BOOL) hasTeams {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";    
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from teams where User='%@'",[CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count > 0;
}

+ (int) getTeamsCount {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from teams where User='%@'", [CommonFunction getDataWithKey:kUser]];
        
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count;
}

- (int) getGamesCount {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from games where (Team1Id='%@' or Team2Id='%@') and User='%@'",self.teamID, self.teamID, [CommonFunction getDataWithKey:kUser]];
        
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count;
}

- (void) dealloc{
    [teamID release];
    [divisionID release];
    [teamName release];
    [teamNumber release];
    [stadiumID release];
    [sportName release];
    [leagueName release];
    [divisionName release];
    
    [super dealloc];
}
@end
