//
//  GetMyCreatedGameResponse.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseResponse.h"

@interface GetMyCreatedGameResponse : BaseResponse

@property int totalRecords;
@property (nonatomic, strong) NSMutableArray* games;

@end
