//
//  SubscribeGamesRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/31/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SubscribeGamesRequest.h"
#import "BaseResponse.h"

@implementation SubscribeGamesRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];    
    [bodyD setObject:self.ListGameId forKey:@"ListGameId"];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"SubscribeGames Response: %@", [json description]);
    
    // parse json dictionary to response object
    BaseResponse* response = [[BaseResponse alloc] initWithDictionary:json];
    return response;
}

@end
