//
//  GetMyCreatedGameResponse.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "GetMyCreatedGameResponse.h"
#import "Game.h"

@implementation GetMyCreatedGameResponse

-(id)initWithDictionary:(NSDictionary*)dic {
    self = [super initWithDictionary:dic];
    if (!self.isSuccess)
        return self;
    
    self.totalRecords = [[dic objectForKey:@"TotalRecords"] intValue];
    self.games = [[NSMutableArray alloc] init];
    
    NSArray* arr = [dic objectForKey:@"Games"];
    for (NSDictionary* jsonDic in arr) {
        Game* game = [[Game alloc] initWithDictionary:jsonDic];
        [self.games addObject:game];
    }
    return self;
}

@end
