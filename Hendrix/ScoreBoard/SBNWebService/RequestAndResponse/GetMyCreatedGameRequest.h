//
//  GetMyCreatedGameRequest.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "AuthenticatedRequest.h"

@interface GetMyCreatedGameRequest : AuthenticatedRequest

@property int fromIndex;
@property int maxRecords;

@end
