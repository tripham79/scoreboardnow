//
//  CreateGameRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "CreateGameRequest.h"
#import "CreateGameResponse.h"
#import "Game.h"

@implementation CreateGameRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    [bodyD setObject:[self.game toDictionary] forKey:@"Game"];
    [bodyD setObject:[NSNumber numberWithBool:self.ConfirmedCreate] forKey:@"ConfirmedCreate"];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"CreateGame Response: %@", [json description]);
    
    // parse json dictionary to response object
    CreateGameResponse* response = [[CreateGameResponse alloc] initWithDictionary:json];
    return response;
}

@end
