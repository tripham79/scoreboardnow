//
//  CreateGameResponse.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseResponse.h"

@interface CreateGameResponse : BaseResponse

@property int gameId;
@property int gameNumber;
@property (nonatomic, strong) NSString* gameNumberText;
@property (nonatomic, strong) NSDate *scheduledStartTime;

@end
