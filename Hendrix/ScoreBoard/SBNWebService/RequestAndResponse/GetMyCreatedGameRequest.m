//
//  GetMyCreatedGameRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "GetMyCreatedGameRequest.h"
#import "GetMyCreatedGameResponse.h"

@implementation GetMyCreatedGameRequest

- (NSDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    [bodyD setObject:[NSNumber numberWithInt:self.fromIndex] forKey:@"FromIndex"];
    [bodyD setObject:[NSNumber numberWithInt:self.maxRecords] forKey:@"MaxRecord"];
    return bodyD;
}

- (id)parseResponse:(NSDictionary *)json {
    NSLog(@"GetMyCreatedGame Response: %@", [json description]);
    
    // parse json dictionary to response object
    GetMyCreatedGameResponse* response = [[GetMyCreatedGameResponse alloc] initWithDictionary:json];
    return response;
}

@end
