//
//  AuthenticatedRequest.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "AuthenticatedRequest.h"
#import "SharedComponent.h"

@implementation AuthenticatedRequest

-(NSString*)loginToken {
    return [SharedComponent getInstance].loginToken;
}

- (NSMutableDictionary*)buildRequestBody {
    NSMutableDictionary* bodyD = [super buildRequestBody];
    [bodyD setObject:self.loginToken forKey:@"LoginToken"];
    return bodyD;
}

@end
