
#import "SBNBaseResponse.h"

@implementation SBNBaseResponse
@synthesize responseCode, errorMessage;

- (id) init{
    self = [super init];
    if (self){
        responseCode = @"";
        errorMessage = @"";
    }
    return self;
}

- (void) dealloc{
    [responseCode release];
    [errorMessage release];
    [super dealloc];
}
@end
