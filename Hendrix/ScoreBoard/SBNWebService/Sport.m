//
//  Sport.m
//  ScoreBoard
//
//  Created by Apple on 2/6/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import "Sport.h"
#import "CommonFunction.h"

@implementation Sport
@synthesize sportId, sportTypeId, sportName;

- (id) initWithString:(NSString *)source{
    self = [super init];
    if (self)
    {
        self.sportId = [CommonFunction getInnerTextInSource:source byTagName:@"Id"];
        self.sportTypeId = [CommonFunction getInnerTextInSource:source byTagName:@"TypeId"];
        self.sportName = [CommonFunction getInnerTextInSource:source byTagName:@"Name"];
    }
    return self;
}

- (void) dealloc{
    [sportId release];
    [sportTypeId release];
    [sportName release];
    [super dealloc];
}
@end
