//
//  BaseResponse.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseResponse.h"
#import "NSDictionary+Extensions.h"

@implementation BaseResponse

-(id)initWithDictionary:(NSDictionary*)dic {
    self = [super init];
    self.json = dic;
    
    self.isSuccess = [[self.json objectForKey:@"IsSuccess"] boolValue];
    self.errorMessage = [self.json stringForKey:@"ErrorMessage"];
    self.errorCode = [self.json stringForKey:@"ErrorCode"];
    
    return self;
}

@end

