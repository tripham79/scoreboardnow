
#import <Foundation/Foundation.h>
#import "CommonFunction.h"
#import "Stadium.h"

@interface Team : NSObject

- (id) initFromDatabase: (sqlite3_stmt*)compiledStatement;
- (id) initWithString: (NSString*) source;
- (id) initWithDictionary:(NSDictionary*)dict;

@property (retain) NSString *teamID;
@property (retain) NSString *divisionID;
@property (retain) NSString *teamName;
@property (retain) NSString *teamNumber;
@property (retain) NSString *stadiumID;
@property (retain) NSString *user;

@property (retain) NSString *sportID;
@property (retain) NSString *sportName;
@property (retain) NSString *leagueName;
@property (retain) NSString *divisionName;

@property (retain) Stadium* stadium;

@property BOOL isChecked;

- (BOOL) insertDatabase;
- (BOOL) deleteDatabase;
- (NSString*) buildInsertQuery;

+ (BOOL) deleteTeam:(NSString*)teamId;
+ (BOOL) hasTeam:(NSString*)teamId;
+ (NSArray*) loadTeamIds;
+ (NSMutableArray*) loadTeams;
+ (BOOL) hasTeams;
+ (int) getTeamsCount;
- (int) getGamesCount;

@end
