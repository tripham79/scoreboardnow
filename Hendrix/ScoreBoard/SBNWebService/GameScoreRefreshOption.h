
#import <Foundation/Foundation.h>
#import "CommonFunction.h"

@interface GameScoreRefreshOption : NSObject

- (id) initWithString: (NSString*) source;

@property (retain) NSString *optionId;
@property (retain) NSString *optionName;
@property (retain) NSString *optionSeconds;

@end
