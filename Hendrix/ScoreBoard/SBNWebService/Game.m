
#import "Game.h"
#import "NSDictionary+Extensions.h"
#import "NSString+Extensions.h"
#import "NSDate+Extensions.h"
#import "Config.h"

@implementation Game
@synthesize gameID, gameNumber, team1ID, team1Name, team2ID, team2Name, statusID, delayedStartTime, scheduledStartTime, duration, startTime, sportID, stadiumID, stadiumName, localStatusId, user, sportName, leagueName, divisionName, addedManual, isChecked;

@synthesize team1Score, team2Score, qtr, lastUpdated;
@synthesize hours, minutes, seconds;
@synthesize leagueId;
@synthesize numberCheckedInUser;
@synthesize outs, balls, strikes, halfTime, isTop, onPossition1, onPossition2, onPossition3;

- (id)init {
    self = [super init];
    if (self) {
        self.statusID = @"1"; // scheduled
        
        self.team1Score = @"0";
        self.team2Score = @"0";
        self.qtr = @"0";
        self.lastUpdated = [NSDate date];
        self.hours = @"0";
        self.minutes = @"0";
        self.seconds = @"0";
        self.numberCheckedInUser = @"0";
        
        // existing logics
        self.localStatusId = @"-1";
        self.user = [CommonFunction getDataWithKey:kUser];
    }
    return self;
}
- (id) initFromDatabase:(sqlite3_stmt *)compiledStatement{
    self = [super init];
    if (self){
        self.gameID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
        
        if (self.gameID == nil)
            return self;
        
        self.gameNumber = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
        
        self.team1ID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
        if (sqlite3_column_text(compiledStatement, 3) != nil)
            self.team1Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
        
        self.team2ID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
        if (sqlite3_column_text(compiledStatement, 5) != nil)
            self.team2Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
        
        self.statusID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
        self.sportID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
        self.stadiumID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
        if (sqlite3_column_text(compiledStatement, 9) != nil)
            self.stadiumName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
        
        if (sqlite3_column_text(compiledStatement, 12) != nil)
            self.duration = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
        
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        dateFormatter.dateFormat = DateTimeFormat;
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        
        NSString *scheduledStartTimeString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
        self.scheduledStartTime = [dateFormatter dateFromString:scheduledStartTimeString];
        
        NSString *startTimeString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
        self.startTime = [dateFormatter dateFromString:startTimeString];
        
        NSString* delayedStartTimeString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
        self.delayedStartTime = [dateFormatter dateFromString:delayedStartTimeString];

        
        self.team1Score = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
        self.team2Score = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
        self.qtr = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
        
        NSString *lastUpdatedString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
        self.lastUpdated = [dateFormatter dateFromString:lastUpdatedString];
        
        self.localStatusId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
        self.user = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 19)];
        
        self.hours = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 20)];
        self.minutes = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 21)];
        self.seconds = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 22)];
        
        self.leagueId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 23)];
        
        self.numberCheckedInUser = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 24)];
        
        if (sqlite3_column_text(compiledStatement, 25) != nil)
            self.outs = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 25)];
        
        if (sqlite3_column_text(compiledStatement, 26) != nil)
            self.balls = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 26)];
        if (sqlite3_column_text(compiledStatement, 27) != nil)
            self.strikes = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 27)];
        
        NSString *text;
        if (sqlite3_column_text(compiledStatement, 28) != nil) {
            text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 28)];
            self.halfTime = text.boolValue;
        }
        
        if (sqlite3_column_text(compiledStatement, 29) != nil) {
            text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 29)];
            self.isTop = text.boolValue;
        }
        
        if (sqlite3_column_text(compiledStatement, 30) != nil) {
            text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 30)];
            self.onPossition1 = text.boolValue;
        }
        if (sqlite3_column_text(compiledStatement, 31) != nil) {
            text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 31)];
            self.onPossition2 = text.boolValue;
        }
        if (sqlite3_column_text(compiledStatement, 32) != nil) {
            text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 32)];
            self.onPossition3 = text.boolValue;
        }
        
        if (sqlite3_column_text(compiledStatement, 33) != nil) {
            self.sportName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 33)];
        }
        if (sqlite3_column_text(compiledStatement, 34) != nil) {
            self.leagueName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 34)];
        }
        if (sqlite3_column_text(compiledStatement, 35) != nil) {
            self.divisionName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 35)];
        }
        
        if (sqlite3_column_text(compiledStatement, 36) != nil) {
            text = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 36)];
            self.addedManual = text.boolValue;
        }
        
        NSLog(@"gameID %@", self.gameID);
        NSLog(@"gameNumer %@", self.gameNumber);
        NSLog(@"team1ID %@", self.team1ID);
        NSLog(@"team1Name %@", self.team1Name);
        NSLog(@"team2ID %@", self.team2ID);
        NSLog(@"team2Name %@", self.team2Name);
        NSLog(@"statusID %@", self.statusID);
        NSLog(@"delayedStartTime %@", self.delayedStartTime);
        NSLog(@"scheduledStartTime %@", scheduledStartTime);
        NSLog(@"startTime %@", startTime);
        NSLog(@"lastUpdated %@", self.lastUpdated);
    }
    return self;
}

- (id) initWithString:(NSString *)source{
    self = [super init];
    if (self){
        self.gameID = [CommonFunction getInnerTextInSource:source byTagName:@"Id"];
        self.gameNumber = [CommonFunction getInnerTextInSource:source byTagName:@"Number"];
        self.team1ID = [CommonFunction getInnerTextInSource:source byTagName:@"Team1Id"];
        self.team1Name = [CommonFunction getInnerTextInSource:source byTagName:@"Team1Name"];
        self.team2ID = [CommonFunction getInnerTextInSource:source byTagName:@"Team2Id"];
        self.team2Name = [CommonFunction getInnerTextInSource:source byTagName:@"Team2Name"];
        self.statusID = [CommonFunction getInnerTextInSource:source byTagName:@"StatusId"];
        self.sportID = [CommonFunction getInnerTextInSource:source byTagName:@"SportId"];
        self.stadiumID = [CommonFunction getInnerTextInSource:source byTagName:@"StadiumId"];
        self.stadiumName = [CommonFunction getInnerTextInSource:source byTagName:@"StadiumName"];
        if ([self.stadiumName isEqualToString:@""]){
             self.stadiumName = [CommonFunction getInnerTextInSource:source byTagName:@"Stadium"];   
        }
        
        self.duration = [CommonFunction getInnerTextInSource:source byTagName:@"Duration"];
        
        NSString *scheduledStartTimeString = [CommonFunction getInnerTextInSource:source byTagName:@"ScheduledStartTime"];
        NSString *startTimeString = [CommonFunction getInnerTextInSource:source byTagName:@"StartTime"];
        NSString* delayedStartTimeString = [CommonFunction getInnerTextInSource:source byTagName:@"DelayedStartTime"];
        
        if ([startTimeString isEqualToString:@""]){
            startTimeString = scheduledStartTimeString;
            
            if (![delayedStartTimeString isEqualToString:@""]){
                startTimeString = delayedStartTimeString;
            }
        }
        
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        
        self.delayedStartTime = [dateFormatter dateFromString:delayedStartTimeString];
        self.scheduledStartTime = [dateFormatter dateFromString:scheduledStartTimeString];
        self.startTime = [dateFormatter dateFromString:startTimeString];
        
        self.localStatusId = @"-1";
        self.user = [CommonFunction getDataWithKey:kUser];
        
        self.hours = [CommonFunction getInnerTextInSource:source byTagName:@"Hours"];
        self.minutes = [CommonFunction getInnerTextInSource:source byTagName:@"Minutes"];
        self.seconds = [CommonFunction getInnerTextInSource:source byTagName:@"Seconds"];
        
        self.leagueId = [CommonFunction getInnerTextInSource:source byTagName:@"LeagueId"]; 
        
        self.sportName = [CommonFunction getInnerTextInSource:source byTagName:@"SportName"];
        self.leagueName = [CommonFunction getInnerTextInSource:source byTagName:@"LeagueName"];
        self.divisionName = [CommonFunction getInnerTextInSource:source byTagName:@"DivisionName"];
        
        // set default scores
        self.team1Score = @"0";
        self.team2Score = @"0";
        self.qtr = @"0";
        self.hours = @"0";
        self.minutes = @"0";
        self.seconds = @"0";
        self.numberCheckedInUser = @"0";
        self.outs = @"0";
        self.balls = @"0";
        self.strikes = @"0";
        self.halfTime = FALSE;
        
        // existing logics
        self.localStatusId = @"-1";
        self.user = [CommonFunction getDataWithKey:kUser];
        
        NSLog(@"gameID %@", self.gameID);
        NSLog(@"gameNumer %@", self.gameNumber);
        NSLog(@"team1ID %@", self.team1ID);
        NSLog(@"team1Name %@", self.team1Name);
        NSLog(@"team2ID %@", self.team2ID);
        NSLog(@"team2Name %@", self.team2Name);
        NSLog(@"statusID %@", self.statusID);
        NSLog(@"delayedStartTime %@", self.delayedStartTime);
        NSLog(@"scheduledStartTimeString %@", self.scheduledStartTime);
        NSLog(@"startTimeString %@", self.startTime);
    }
    return self;
}

- (id) initWithDictionary:(NSDictionary*)dict {
    self = [super init];
    if (self) {
        self.gameID = [dict stringForKey:@"Id"];
        self.gameNumber = [dict stringForKey:@"Number"];
        self.team1ID = [dict stringForKey:@"Team1Id"];
        self.team1Name = [dict stringForKey:@"Team1Name"];
        self.team2ID = [dict stringForKey:@"Team2Id"];
        self.team2Name = [dict stringForKey:@"Team2Name"];
        self.statusID = [dict stringForKey:@"StatusId"];
        self.statusName = [dict stringForKey:@"Status"];
        self.sportID = [dict stringForKey:@"SportId"];
        self.stadiumID = [dict stringForKey:@"StadiumId"];
        self.stadiumName = [dict stringForKey:@"Stadium"];
        
        self.duration = [dict stringForKey:@"Duration"];
        self.scheduledStartTime = [[dict stringForKey:@"ScheduledStartTime"] jsonStringToDateUtc];
        self.startTime = [[dict stringForKey:@"StartTime"] jsonStringToDateUtc];
        self.delayedStartTime = [[dict stringForKey:@"DelayedStartTime"] jsonStringToDateUtc];
        
        self.leagueId = [dict stringForKey:@"LeagueId"];
        self.sportName = [dict stringForKey:@"SportName"];
        self.leagueName = [dict stringForKey:@"LeagueName"];
        self.divisionName = [dict stringForKey:@"DivisionName"];
        self.lastUpdated = [[dict stringForKey:@"UpdatedOnUtc"] jsonStringToDateUtc];
        
        // set default time & scores
        self.hours = @"0";
        self.minutes = @"0";
        self.seconds = @"0";
        
        self.team1Score = @"0";
        self.team2Score = @"0";
        self.qtr = @"0";
        self.hours = @"0";
        self.minutes = @"0";
        self.seconds = @"0";
        self.numberCheckedInUser = @"0";
        self.outs = @"0";
        self.balls = @"0";
        self.strikes = @"0";
        self.halfTime = FALSE;
        // existing logics
        self.localStatusId = @"-1";
        self.user = [CommonFunction getDataWithKey:kUser];
        
    }
    return self;
}

- (NSDictionary*)toDictionary {
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    if (!isEmpty(self.gameID))
        [dic setValue:self.gameID forKey:@"Id"];
    if (!isEmpty(self.gameNumber))
        [dic setValue:self.gameNumber forKey:@"Number"];
    if (!isEmpty(self.team1ID))
        [dic setValue:self.team1ID forKey:@"Team1Id"];
    if (!isEmpty(self.team1Name))
        [dic setValue:self.team1Name forKey:@"Team1Name"];
    if (!isEmpty(self.team2ID))
        [dic setValue:self.team2ID forKey:@"Team2Id"];
    if (!isEmpty(self.team2Name))
        [dic setValue:self.team2Name forKey:@"Team2Name"];
    if (!isEmpty(self.statusID))
        [dic setValue:self.statusID forKey:@"StatusId"];
    if (!isEmpty(self.statusName))
        [dic setValue:self.statusName forKey:@"Status"];
    if (!isEmpty(self.sportID))
        [dic setValue:self.sportID forKey:@"SportId"];
    if (!isEmpty(self.stadiumID))
        [dic setValue:self.stadiumID forKey:@"StadiumId"];
    if (!isEmpty(self.stadiumName))
        [dic setValue:self.stadiumName forKey:@"Stadium"];
    
    if (!isEmpty(self.duration))
        [dic setValue:self.duration forKey:@"Duration"];
    
    if (!isEmpty(self.scheduledStartTime)) {
        // user enter time in local time zone, need to convert to UTC when upload to server
        NSLog(@"%@", self.scheduledStartTime);
        NSDate* scheduledDateUTC = [self.scheduledStartTime toUTCDate];
        NSLog(@"%@", scheduledDateUTC);
        [dic setValue:[scheduledDateUTC toJSONString] forKey:@"ScheduledStartTime"];
    }
    
    if (!isEmpty(self.startTime))
        [dic setValue:[self.startTime toJSONString] forKey:@"StartTime"];
    
    if (!isEmpty(self.delayedStartTime)) {
        [dic setValue:[self.delayedStartTime toJSONString] forKey:@"DelayedStartTime"];
    }
    
    if (!isEmpty(self.leagueId))
        [dic setValue:self.leagueId forKey:@"LeagueId"];
    if (!isEmpty(self.sportName))
        [dic setValue:self.sportName forKey:@"SportName"];
    if (!isEmpty(self.leagueName))
        [dic setValue:self.leagueName forKey:@"LeagueName"];
    if (!isEmpty(self.divisionName))
        [dic setValue:self.divisionName forKey:@"DivisionName"];
    if (!isEmpty(self.sportName))
        [dic setValue:self.sportName forKey:@"SportName"];
    if (!isEmpty(self.leagueName))
        [dic setValue:self.leagueName forKey:@"LeagueName"];
    if (!isEmpty(self.divisionName))
        [dic setValue:self.divisionName forKey:@"DivisionName"];
    
    if (self.lastUpdated)
        [dic setValue:[self.lastUpdated toJSONString] forKey:@"UpdatedOnUtc"];
    
    return dic;
}

- (void) updateScore:(GameScore*)score
{
    self.team1Score = score.team1Score;
    self.team2Score = score.team2Score;
    self.qtr = score.qtr;
    self.lastUpdated = score.lastUpdated;
    
    self.hours = score.hours;
    self.minutes = score.minutes;
    self.seconds = score.seconds;
    
    self.numberCheckedInUser = score.numberCheckedInUser;
    
    // support multi-sports
    self.outs = score.outs;
    self.balls = score.balls;
    self.strikes = score.strikes;
    self.halfTime = score.halfTime;
    self.isTop = score.isTop;
    self.onPossition1 = score.onPossition1;
    self.onPossition2 = score.onPossition2;
    self.onPossition3 = score.onPossition3;
}

- (void) updateGameStatus:(Game *)gameStatus{
    self.sportID = gameStatus.sportID;
    self.duration = gameStatus.duration;
    self.gameNumber = gameStatus.gameNumber;
    self.scheduledStartTime = gameStatus.scheduledStartTime;
    self.statusID = gameStatus.statusID;
    self.stadiumName = gameStatus.stadiumName;
    self.startTime = gameStatus.startTime;
    self.statusID = gameStatus.statusID;
    self.team1ID = gameStatus.team1ID;
    self.team1Name = gameStatus.team1Name;
    self.team2ID = gameStatus.team2ID;
    self.team2Name = gameStatus.team2Name;
    self.delayedStartTime = gameStatus.delayedStartTime;
}

- (BOOL) insertDatabase {
    //insert game to database
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = [self buildInsertQuery];
    BOOL success = FALSE;
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {   
            sqlite3_bind_text(compiledStatement, 1, [self.team1Name UTF8String], -1, SQLITE_STATIC);
            sqlite3_bind_text(compiledStatement, 2, [self.team2Name UTF8String], -1, SQLITE_STATIC);
            sqlite3_bind_text(compiledStatement, 3, [self.stadiumName UTF8String], -1, SQLITE_STATIC);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE){
            //while (sqlite3_step(compiledStatement) == SQLITE_DONE){
                NSLog(@"Insert Game OK.");
                success = TRUE;
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    //[query release];
    return success;
}

- (BOOL) updateDatabase {
    //update game in database
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = [self buildUpdateQuery];
    BOOL success = FALSE;
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            sqlite3_bind_text(compiledStatement, 1, [self.team1Name UTF8String], -1, SQLITE_STATIC);
            sqlite3_bind_text(compiledStatement, 2, [self.team2Name UTF8String], -1, SQLITE_STATIC);
            sqlite3_bind_text(compiledStatement, 3, [self.stadiumName UTF8String], -1, SQLITE_STATIC);
            
            if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                NSLog(@"Update Game OK.");
                success = TRUE;
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    //[query release];
    return success;
}

+ (BOOL) deleteDatabase:(NSString *)gameId {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    BOOL success = FALSE;
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat:@"delete from games where id='%@' and user='%@'", gameId, [CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatement) == SQLITE_DONE)
            {
                success = TRUE;
                NSLog(@"Delete game %@ complete", gameId);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    //[query release];
    return  success;
}

+ (NSMutableArray*) loadManuallyAddedGames {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select * from games where AddedManual=1 and User='%@' order by ScheduledStartTime desc",[CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                Game *game = [[Game alloc] initFromDatabase:compiledStatement];
                [list addObject:game];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return list;
}

+ (NSArray*) loadGames:(NSDate*)fromDate toDate:(NSDate*)toDate {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    NSMutableArray *list = [[[NSMutableArray alloc] init] autorelease];
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *startDate = [dateFormatter stringFromDate:fromDate];
    NSString *endDate = [dateFormatter stringFromDate:toDate];
    
    NSLog(@"Load games from %@ to %@", startDate, endDate);
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        NSString *selectedSportId = [CommonFunction getDataWithKey:MyScoreBoard_ShowSport];
        if (selectedSportId == nil)
            selectedSportId = @"";
        
        if (![selectedSportId isEqualToString:@""])
            selectedSportId = [NSString stringWithFormat:@" AND SportId = '%@' ", selectedSportId];
        
        //check game is exist
        query = [NSString stringWithFormat: @"select * from games where ScheduledStartTime>='%@' AND ScheduledStartTime<'%@' AND User='%@' %@ ORDER BY ScheduledStartTime ASC", startDate, endDate, [CommonFunction getDataWithKey:kUser], selectedSportId];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                Game *game = [[Game alloc] initFromDatabase:compiledStatement];
                [list addObject:game];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return list;
}

+ (BOOL) hasGame:(NSString*)gameId {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from games where Id='%@' and User='%@'",gameId, [CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count > 0;
}

+ (BOOL) hasGames {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from games where User='%@'",[CommonFunction getDataWithKey:kUser]];
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count > 0;
}

+ (int) getGamesCount {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *query = @"";
    
    int count = 0;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        query = [NSString stringWithFormat: @"select count(1) from games where User='%@'", [CommonFunction getDataWithKey:kUser]];
        
        sqlStatement = [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW){
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    
    sqlite3_close(database);
    return count;
}

- (NSString*) buildInsertQuery
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *query = [NSString stringWithFormat:@"insert into Games (Id, Number, Team1Id, Team1Name, Team2Id, Team2Name, StatusId, SportId, StadiumId, StadiumName, ScheduledStartTime, DelayedStartTime, Duration, StartTime, Team1Score, Team2Score, Qtr, LastUpdated, LocalStatusId, User, Hours, Minutes, Seconds, LeagueId, NumberCheckedInUser, Outs, Balls, Strikes, HalfTime, IsTop, OnPossition1, OnPossition2, OnPossition3, SportName, LeagueName, DivisionName, AddedManual) values('%@', '%@', '%@', ?, '%@', ?, '%@', '%@', '%@', ?, '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%d', '%d', '%d', '%d', '%d','%@','%@','%@','%d')", gameID, gameNumber, team1ID, team2ID, statusID, sportID, stadiumID, [dateFormatter stringFromDate:scheduledStartTime], delayedStartTime, duration, [dateFormatter stringFromDate:startTime], team1Score, team2Score, qtr, [dateFormatter stringFromDate:lastUpdated], localStatusId, user, hours, minutes, seconds, leagueId, numberCheckedInUser, outs, balls, strikes, halfTime, isTop, onPossition1, onPossition2, onPossition3, sportName, leagueName, divisionName, addedManual];
    NSLog(@"%@", query);
    return query;
}

- (NSString*) buildUpdateQuery{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *query = [NSString stringWithFormat:@"update Games set Number='%@', Team1Id='%@', Team1Name=?, Team2Id='%@', Team2Name=?, StatusId='%@', LocalStatusId='%@', SportId='%@', StadiumId='%@', StadiumName=?, ScheduledStartTime='%@', DelayedStartTime='%@', Duration='%@', StartTime='%@',  Team1Score='%@', Team2Score='%@', Qtr='%@', LastUpdated='%@', Hours='%@', Minutes='%@', Seconds='%@', NumberCheckedInUser='%@', Outs='%@', Balls='%@', Strikes='%@', HalfTime='%d', IsTop='%d', OnPossition1='%d', OnPossition2='%d',OnPossition3='%d', SportName='%@', LeagueName='%@', DivisionName='%@', AddedManual='%d' where Id='%@' AND User='%@'", gameNumber, team1ID, team2ID, statusID, localStatusId, sportID, stadiumID, [dateFormatter stringFromDate:scheduledStartTime], delayedStartTime, duration, [dateFormatter stringFromDate:startTime], team1Score, team2Score, qtr, [dateFormatter stringFromDate:lastUpdated], hours, minutes, seconds, numberCheckedInUser, outs, balls, strikes, halfTime,  isTop, onPossition1, onPossition2, onPossition3, sportName, leagueName, divisionName, addedManual, gameID, user];
    NSLog(@"%@", query);
    return query;
}

- (void) dealloc{
    [gameID release];
    [gameNumber release];
    [team1ID release];
    [team1Name release];
    [team2ID release];
    [team2Name release];
    [statusID release];
    [delayedStartTime release];
    [scheduledStartTime release];
    [duration release];
    [startTime release];
    [sportID release];
    [stadiumID release];
    [stadiumName release];
    [sportName release];
    [leagueName release];
    [divisionName release];
    
    [team1Score release];
    [team2Score release];
    [qtr release];
    [lastUpdated release];
    [outs release];
    [balls release];
    [strikes release];
    [super dealloc];
}

@end
