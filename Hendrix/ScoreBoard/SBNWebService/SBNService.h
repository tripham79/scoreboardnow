//
//  SBNService.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseRequest.h"
@class Game;

@interface SBNService : NSObject

+ (SBNService*)sharedInstance;

- (void)getMyCreatedGames:(int)fromIndex maxRecords:(int)maxRecords withCallback:(RequestCompletionBlock)callback;

- (void)addGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback;
- (void)updateGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback;
- (void)deleteGame:(int)gameId withCallback:(RequestCompletionBlock)callback;

- (void)searchTeamsWithStadium:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText sportId:(int)sportId withCallback:(RequestCompletionBlock)callback;
- (void)searchStadiums:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText withCallback:(RequestCompletionBlock)callback;

- (void)subscribeGames:(NSMutableArray*)arrGameIds withCallback:(RequestCompletionBlock)callback;

@end
