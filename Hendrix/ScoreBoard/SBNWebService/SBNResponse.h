
#import <Foundation/Foundation.h>
#import "CommonFunction.h"
#import "Sport.h"
#import "League.h"
#import "Division.h"
#import "Team.h"
#import "Game.h"
#import "GameScore.h"
#import "GameScoreRefreshOption.h"

#define RESPONSE_SUCCESSFULL @"true"
#define RESPONSE_ERROR @"false"

#define tagResponseCode @"IsSuccess"
#define tagErrorMessage @"ErrorMessage"
#define tagLoginToken @"LoginToken"


@interface SBNResponse : NSObject
@property (retain) NSString *responseCode;
@property (retain) NSString *errorMessage;
- (void) parseResponseData:(NSString*) responseData;
@end

#pragma SBNValidTokenResponse
@interface SBNValidTokenResponse : SBNResponse 
@end

#pragma SBNRegisterResponse
@interface SBNRegisterResponse : SBNResponse 
@end

#pragma SBNLoginResponse
@interface SBNLoginResponse : SBNResponse
@property (retain) NSString *loginToken;
@property (retain) NSString *canCreateGame;
@end

#pragma SBNLogoutResponse
@interface SBNLogoutResponse : SBNResponse
@end

#pragma SBNForgotPasswordResponse
@interface SBNForgotPasswordResponse : SBNResponse
@end

#pragma SBNGetClientConfigurationResponse
@interface SBNGetClientConfigurationResponse : SBNResponse
@property (retain) NSString *refreshSeconds;
@end

#pragma SBNSetClientConfigurationResponse
@interface SBNSetClientConfigurationResponse : SBNResponse
@end

#pragma SBNGameScoreRefreshOptions
@interface SBNGameScoreRefreshOptions : SBNResponse
@property (retain) NSMutableArray *listGameScoreRefreshOption;
@end

#pragma SBNLeagueResponse
@interface SBNSportResponse : SBNResponse
@property (retain) NSMutableArray *listSport;
@end

#pragma SBNLeagueResponse
@interface SBNLeagueResponse : SBNResponse 
@property (retain) NSMutableArray *listLeague;
@end

#pragma SBNDivisionResponse
@interface SBNDivisionResponse : SBNResponse 
@property (retain) NSMutableArray *listDivision;
@end

#pragma SBNSearchTeamResponse
@interface SBNSearchTeamResponse : SBNResponse 
@property (retain) NSMutableArray *listTeam;
@end

#pragma SBNSearchGameResponse
@interface SBNSearchGameResponse : SBNResponse 
@property (retain) NSMutableArray *listGame;
@end

#pragma SBNGetGameStatusResponse
@interface SBNGetGameStatusResponse : SBNResponse 
@property (retain) NSMutableArray *listGameStatus;
@end

#pragma SBNPostShoutResponse
@interface SBNPostShoutResponse : SBNResponse 
@end

#pragma SBNGameScoreResponse
@interface SBNGameScoreResponse : SBNResponse 
@property (retain) NSMutableArray *listGameScore;
@end

#pragma SBNSetGameScoreResponse
@interface SBNSetGameScoreResponse : SBNResponse 
@end

#pragma SBNRequestGameScoreResponse
@interface SBNRequestGameScoreResponse : SBNResponse 
@end

#pragma SBNCheckInGameResponse
@interface SBNCheckInGameResponse : SBNResponse 
@end

#pragma SBNCheckOutGameResponse
@interface SBNCheckOutGameResponse : SBNResponse 
@end

#pragma SBNSubscribeDeviceResponse
@interface SBNSubscribeDeviceResponse : SBNResponse 
@end

#pragma SBNSubscribeGameResponse
@interface SBNSubscribeGameResponse : SBNResponse 
@end

#pragma SBNUnSubscribeGameResponse
@interface SBNUnSubscribeGameResponse : SBNResponse 
@end

// NEW METHODS ---------
@interface SBNSubscribeTeamResponse : SBNResponse
@end
@interface SBNUnSubscribeTeamResponse : SBNResponse
@end

@interface SBNGetSubscribedTeamsCountResponse : SBNResponse
@property int totalRecord;
@end

@interface SBNGetSubscribedGamesCountResponse : SBNResponse
@property int totalRecord;
@end

@interface SBNGetSubscribedTeamsResponse : SBNResponse
@property (retain) NSMutableArray *listTeam;
@property int totalRecord;
@end

@interface SBNGetSubscribedGamesResponse : SBNResponse
@property (retain) NSMutableArray *listGame;
@property int totalRecord;
@end

@interface SBNGetNewGamesResponse : SBNResponse
@property (retain) NSMutableArray *listGame;
@property int totalRecord;
@end

