
#import <Foundation/Foundation.h>
#import "CommonFunction.h"

#define RESPONSE_SUCCESSFULL @"true"
#define RESPONSE_ERROR @"false"

#define tagResponseCode @"a:IsSuccess"
#define tagErrorMessage @"a:ErrorMessage"

@interface SBNBaseResponse : NSObject{

}

@property (retain) NSString *responseCode;
@property (retain) NSString *errorMessage;

@end
