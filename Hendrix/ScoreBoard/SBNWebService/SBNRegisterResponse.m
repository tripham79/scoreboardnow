
#import "SBNRegisterResponse.h"

@implementation SBNRegisterResponse

- (void) parseResponseData:(NSString *)responseData{
    self.responseCode = [CommonFunction getInnerTextIn:responseData withTagName: tagResponseCode];
    self.errorMessage = [CommonFunction getInnerTextIn:responseData withTagName: tagErrorMessage];
}

@end
