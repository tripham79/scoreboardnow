
#import "League.h"

@implementation League
@synthesize leagueId, leagueName, qtr, qtrDuration, sportId;

- (id) initWithString:(NSString *)source{
    self = [super init];
    if (self)
    {
        self.leagueId = [CommonFunction getInnerTextInSource:source byTagName:@"Id"];
        self.leagueName = [CommonFunction getInnerTextInSource:source byTagName:@"Name"];
        self.qtr = [CommonFunction getInnerTextInSource:source byTagName:@"QTR"];
        self.qtrDuration = [CommonFunction getInnerTextInSource:source byTagName:@"QTRDuration"];
        self.sportId = [CommonFunction getInnerTextInSource:source byTagName:@"SportId"];
    }
    return self;
}

- (void) dealloc{
    [leagueId release];
    [leagueName release];
    [sportId release];
    [super dealloc];
}
@end
