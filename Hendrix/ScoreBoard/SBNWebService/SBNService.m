//
//  SBNService.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SBNService.h"
#import "SharedComponent.h"
#import "GetMyCreatedGameRequest.h"
#import "CreateGameRequest.h"
#import "DeleteGameRequest.h"
#import "SearchTeamRequest.h"
#import "SearchStadiumRequest.h"
#import "SubscribeGamesRequest.h"

@implementation SBNService

+ (SBNService*)sharedInstance {
    //  Static local predicate must be initialized to 0
    static SBNService *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        if (sharedInstance == nil){
            sharedInstance = [[SBNService alloc] init];
            // Do any other initialisation stuff here
        }
    });
    return sharedInstance;
}

- (void)getMyCreatedGames:(int)fromIndex maxRecords:(int)maxRecords withCallback:(RequestCompletionBlock)callback
{
    GetMyCreatedGameRequest *request = [[GetMyCreatedGameRequest alloc] init];
    request.loginToken = [SharedComponent getInstance].loginToken;
    request.fromIndex = fromIndex;
    request.maxRecords = maxRecords;
    [request post:@"GetMyCreatedGames" body:[request buildRequestBody] withCallback:callback];
}

- (void)addGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback {
    CreateGameRequest* request = [[CreateGameRequest alloc] init];
    request.ConfirmedCreate = confirmedDuplicated;
    request.game = game;
    [request post:@"CreateGame" body:[request buildRequestBody] withCallback:callback];
}

- (void)updateGame:(Game*)game confirmedDuplicated:(BOOL)confirmedDuplicated withCallback:(RequestCompletionBlock)callback {
    CreateGameRequest* request = [[CreateGameRequest alloc] init];
    request.ConfirmedCreate = confirmedDuplicated;
    request.game = game;
    [request post:@"UpdateGame" body:[request buildRequestBody] withCallback:callback];
}

- (void)deleteGame:(int)gameId withCallback:(RequestCompletionBlock)callback {
    DeleteGameRequest* request = [[DeleteGameRequest alloc] init];
    request.gameId = gameId;
    [request post:@"DeleteGame" body:[request buildRequestBody] withCallback:callback];
}

- (void)searchTeamsWithStadium:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText sportId:(int)sportId withCallback:(RequestCompletionBlock)callback
{
    SearchTeamRequest* request = [[SearchTeamRequest alloc] init];
    request.PageIndex = pageIndex;
    request.PageSize = pageSize;
    request.SearchTerms = searchText;
    request.SportId = sportId;
    [request post:@"SearchTeamsWithStadium" body:[request buildRequestBody] withCallback:callback];
}

- (void)searchStadiums:(int)pageIndex pageSize:(int)pageSize searchText:(NSString*)searchText withCallback:(RequestCompletionBlock)callback
{
    SearchStadiumRequest* request = [[SearchStadiumRequest alloc] init];
    request.PageIndex = pageIndex;
    request.PageSize = pageSize;
    request.SearchTerms = searchText;
    [request post:@"SearchStadiums" body:[request buildRequestBody] withCallback:callback];
}

- (void)subscribeGames:(NSMutableArray*)arrGameIds withCallback:(RequestCompletionBlock)callback {
    SubscribeGamesRequest* request = [[SubscribeGamesRequest alloc] init];
    request.ListGameId = arrGameIds;
    [request post:@"SubscribeGamesJson" body:[request buildRequestBody] withCallback:callback];
}

@end
