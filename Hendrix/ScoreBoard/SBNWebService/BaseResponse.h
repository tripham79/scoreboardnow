//
//  BaseResponse.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#define INVALID_LOGIN_TOKEN @"INVALID_LOGIN_TOKEN"
#define ERROR_DUPLICATED_GAME @"ERROR_DUPLICATED_GAME"

@interface BaseResponse : NSObject

@property (nonatomic, retain) NSDictionary *json;
@property bool isSuccess;
@property (nonatomic, strong) NSString* errorMessage;
@property (nonatomic, strong) NSString* errorCode;

-(id)initWithDictionary:(NSDictionary*)dic;

@end

