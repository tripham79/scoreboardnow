
#import <UIKit/UIKit.h>
#import "FBConnect.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, FBSessionDelegate, UIAlertViewDelegate>{
    //NSString *databasePath;
    //NSString *databaseName;
    
    UIAlertView *globalAlert;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;

//- (void) writeSomething;

@end
