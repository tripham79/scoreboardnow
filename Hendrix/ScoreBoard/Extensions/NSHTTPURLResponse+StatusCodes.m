//
//  NSHTTPURLResponse+StatusCodes.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "NSHTTPURLResponse+StatusCodes.h"

@implementation NSHTTPURLResponse (StatusCodes)

-(BOOL)is200OK {
	return [self statusCode] == 200;
}
-(BOOL)isOK {
	return 200 <= [self statusCode] && [self statusCode] <= 299;
}
-(BOOL)isRedirect {
	return 300 <= [self statusCode] && [self statusCode] <= 399;
}
-(BOOL)isClientError {
	return 400 <= [self statusCode] && [self statusCode] <= 499;
}
-(BOOL)isServerError {
	return 500 <= [self statusCode] && [self statusCode] <= 599;
}

@end
