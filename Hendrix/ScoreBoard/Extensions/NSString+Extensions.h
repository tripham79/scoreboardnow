//
//  NSString+Extensions.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)

- (NSDate *)jsonStringToDateUtc;

@end
