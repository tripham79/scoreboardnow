//
//  NSDictionary+Extensions.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extensions)

+ (NSMutableDictionary*)dictionaryWithPropertiesOfObject:(id) obj;
- (NSString*)stringForKey:(NSString*)key;

@end
