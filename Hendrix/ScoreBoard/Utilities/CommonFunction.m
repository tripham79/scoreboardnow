
#import "CommonFunction.h"
#define FILE_NAME @"data.plist"

@implementation CommonFunction

+ (void)logResponse:(NSData*)data {
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSString *content =[ NSString stringWithCString:[data bytes] encoding:NSUTF8StringEncoding];
    NSLog(@"%@", jsonString);
}

+ (NSString*) htmlEncode: (NSString*) xmlSource
{
	return [[[[[xmlSource stringByReplacingOccurrencesOfString: @"&amp;" withString: @"&"] stringByReplacingOccurrencesOfString: @"&quot;" withString: @"\""] stringByReplacingOccurrencesOfString: @"&#39;" withString: @"'"] stringByReplacingOccurrencesOfString: @"&gt;" withString: @">"] stringByReplacingOccurrencesOfString: @"&lt;" withString: @"<"];
}

+ (NSString*) getImageNameWithNumber:(int)number{
    NSString *imageName = @"";
    if (number >= 0 && number <= 9)
    {
        imageName = [NSString stringWithFormat:@"number%d", number];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"number%d", 0];
    }
    return  imageName;
}

+ (NSString*) getWeekDayFromId:(int)dayId{
    NSString *result = @"";
    switch (dayId) {
        case 0:
            result = @"Sun";
            break;
        case 1:
            result = @"Mon";
            break;
        case 2:
            result = @"Tue";
            break;
        case 3:
            result = @"Wed";
            break;
        case 4:
            result = @"Thu";
            break;
        case 5:
            result = @"Fri";
            break;
        case 6:
            result = @"Sat";
            break;
        default:
            break;
    }
    return  result;
}

+ (int) checkInterfaceOrientation{
    int interfaceOrientation = [[SharedComponent getInstance] interfaceOrientation];
    
    if (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        return Portrait;
    }
    else if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        return Landscape;
    }
    
    return UnknownOrientaion;
}

+ (BOOL) checkPortraitOrientation{
    int interfaceOrientation = [[SharedComponent getInstance] interfaceOrientation];
    
    if (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL) checkLandscapeOrientation{
    int interfaceOrientation = [[SharedComponent getInstance] interfaceOrientation];
    
    if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL) validateEmail:(NSString *)emailString{
    NSString *emailRegExString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegExString];
    return [regExPredicate evaluateWithObject:emailString];
}

+ (NSDate*) convertStringToDate:(NSString *)dateText {
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate* date = [dateFormatter dateFromString:dateText];
    return date;
}

+ (NSDate*) convertStringToDate:(NSString *)dateText dateFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    dateFormatter.dateFormat = format;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate* date = [dateFormatter dateFromString:dateText];
    return date;
}

+ (NSString*) convertDateToString:(NSDate*)date {
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString* text = [dateFormatter stringFromDate:date];
    return text;
}

+ (NSString*) convertDateToString:(NSDate*)date dateFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = format;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSString* text = [dateFormatter stringFromDate:date];
    return text;
}

+ (NSString*)toLocalTimeText:(NSDate*)utcDate
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString* dateText = [dateFormatter stringFromDate:utcDate];
    return dateText;
}

+ (NSString *)toLocalTimeText:(NSDate*)utcDate dateFormat:(NSString*)format {
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = format;
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString* dateText = [dateFormatter stringFromDate:utcDate];
    return dateText;
}

+ (NSString*) toUTCTimeText:(NSDate*)localDate
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = DateTimeFormat;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString* dateText = [dateFormatter stringFromDate:localDate];
    return dateText;
}

+ (NSString*) toUTCTimeText:(NSDate*)localDate dateFormat:(NSString*)format
{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = format;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString* dateText = [dateFormatter stringFromDate:localDate];
    return dateText;
}

+ (NSString*) getInnerTextInSource:(NSString*)xmlSource byTagName:(NSString*)tagName{
    NSString* openTag = [NSString stringWithFormat:@"<%@", tagName];
    NSString* closeTag = [NSString stringWithFormat:@"</%@>", tagName];
    
    int beginIndex = [xmlSource rangeOfString:openTag options:NSCaseInsensitiveSearch].location;
    if (beginIndex < 0 || beginIndex > [xmlSource length]) { return @""; }
    
    int endIndex = [xmlSource rangeOfString:closeTag options:NSCaseInsensitiveSearch].location;
    if (endIndex < 0 || endIndex > [xmlSource length]) {
        /*endIndex = [xmlSource rangeOfString:@"/>" options:NSCaseInsensitiveSearch].location;
        if (endIndex < 0 || endIndex > [xmlSource length]){
            return @""; 
        }*/
        
        return  @"";
    }
    
    beginIndex += [openTag length];
    
    NSString *result = @"";
    if (endIndex >= beginIndex)
    {
        result = [xmlSource substringWithRange:NSMakeRange(beginIndex, endIndex - beginIndex)];
        int unexpect = [result rangeOfString:@">" options:NSCaseInsensitiveSearch].location;
        if (unexpect >= 0 && unexpect < result.length)
        {
            result = [result substringWithRange:NSMakeRange(unexpect + 1, result.length - unexpect - 1)];
        }
    }
    
    result = [CommonFunction htmlEncode:result];
    return result;
}

+ (NSMutableArray*) getInnerTextsInSource:(NSString *)xmlSource byTagName:(NSString *)tagName
{
    NSMutableArray *innerTestList = [[NSMutableArray alloc] init];
    NSString* closeTag = [NSString stringWithFormat:@"</%@>", tagName];
    int length = 0;
    NSString *innerText = @"-1";
    
    while (![innerText isEqualToString:@""]) {
        innerText = [CommonFunction getInnerTextInSource:xmlSource byTagName:tagName];
        if (![innerText isEqualToString:@""])
        {
            int endIndex = [xmlSource rangeOfString:closeTag options: NSCaseInsensitiveSearch].location;
            length = endIndex + [closeTag length];
            xmlSource = [xmlSource substringFromIndex:length];
            [innerTestList addObject:innerText];
        }
    }
    return innerTestList;// autorelease];
}


+ (NSString*) getDataWithKey:(NSString *)key{
	NSMutableDictionary* dataFromFile = [CommonFunction applicationDataFromFile: FILE_NAME];
	return [dataFromFile objectForKey:key];
}

+ (void) saveData:(NSString*)valueString withKey:(NSString*)keyString{
	NSMutableDictionary* dataFromFile = [CommonFunction applicationDataFromFile: FILE_NAME];
	[dataFromFile setValue:valueString forKey:keyString];
	[CommonFunction writeApplicationData: dataFromFile toFile:FILE_NAME];
}

+ (NSMutableDictionary*) applicationDataFromFile:(NSString*)fileName {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	NSMutableDictionary *myData = nil;
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:appFile])	
		myData = [[[NSMutableDictionary alloc] initWithContentsOfFile:appFile] autorelease];
	else
		myData = [[[NSMutableDictionary alloc] init] autorelease];	
	
    return myData; 
	
}

+ (BOOL) writeApplicationData:(NSMutableDictionary*)data toFile:(NSString*)fileName {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return NO;	
    }
    
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
	
    return ([data writeToFile:appFile atomically:YES]);
}

+ (void) checkAndCreateDatabase
{
    BOOL success;
    NSString *databasePath = @"";
    NSString *databaseName = @"localData";
    
    // Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    
    [[SharedComponent getInstance] setDatabasePath:databasePath];
    
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
	
	// If the database already exists, check and update schema if needed
	if(success) {
        [CommonFunction updateDatabaseSchema];
        return;
    }
	
	// If not then proceed to copy the database from the application to the users filesystem
	
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
	
	//[fileManager release];
}

+ (void) executeDatabaseCommand:(NSString *)commandText {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        sqlStatement = [commandText UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
            if (sqlite3_step(compiledStatement) == SQLITE_DONE){
                NSLog(@"Executed command successfully: %@", commandText);
            }
        }
        sqlite3_finalize(compiledStatement);
    }    
    sqlite3_close(database);
}

+ (NSString*) getDatabaseSchemaVersion {
    NSString *databasePath = [[SharedComponent getInstance] databasePath];
    sqlite3 *database;
    const char *sqlStatement;
    sqlite3_stmt *compiledStatement;
    NSString *commandText = @"select Value from setting where Name = 'SchemaVersion'";
    NSString *version = @"";
    
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        sqlStatement = [commandText UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK){
            if (sqlite3_step(compiledStatement) == SQLITE_ROW){
                version =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];                
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    return version;
}

+ (void) updateDatabaseSchema {
    NSString *version = [self getDatabaseSchemaVersion];
    NSString *scriptFilePath = @"";
    NSString *script = @"";
    
    if (version == @"") {
        scriptFilePath = [[NSBundle mainBundle] pathForResource:@"SchemaUpdate1.1" ofType: @"txt"];
        script = [NSString stringWithContentsOfFile:scriptFilePath encoding:NSUTF8StringEncoding error:nil];        
        
        [CommonFunction executeScript:script];
        version = @"1.1";
    }
    if ([version isEqualToString:@"1.1"]) {
        scriptFilePath = [[NSBundle mainBundle] pathForResource:@"SchemaUpdate1.2" ofType: @"txt"];
        script = [NSString stringWithContentsOfFile:scriptFilePath encoding:NSUTF8StringEncoding error:nil];
        
        [CommonFunction executeScript:script];
        version = @"1.2";
    }
    if ([version isEqualToString:@"1.2"]) {
        scriptFilePath = [[NSBundle mainBundle] pathForResource:@"SchemaUpdate1.3" ofType: @"txt"];
        script = [NSString stringWithContentsOfFile:scriptFilePath encoding:NSUTF8StringEncoding error:nil];
        
        [CommonFunction executeScript:script];
        version = @"1.3";
    }
    if ([version isEqualToString:@"1.3"]) {
        scriptFilePath = [[NSBundle mainBundle] pathForResource:@"SchemaUpdate1.4" ofType: @"txt"];
        script = [NSString stringWithContentsOfFile:scriptFilePath encoding:NSUTF8StringEncoding error:nil];
        
        [CommonFunction executeScript:script];
        version = @"1.4";
    }
}

+ (void) executeScript:(NSString*)script {
    NSArray *list = [script componentsSeparatedByString:@";"];
    for (NSString* sql in list) {
        if (sql != @"") {
            sql =  [sql stringByAppendingString:@";"];
            [CommonFunction executeDatabaseCommand:sql];
        }
    }
}

@end
