
#import <Foundation/Foundation.h>
#import "SharedComponent.h"
#import <sqlite3.h>

@interface CommonFunction : NSObject

+ (void)logResponse:(NSData*)data;
+ (NSString*) htmlEncode: (NSString*) xmlSource;

+ (NSString*) getImageNameWithNumber:(int) number;
+ (NSString*) getWeekDayFromId:(int) dayId;

+ (int) checkInterfaceOrientation;
+ (BOOL) checkPortraitOrientation;
+ (BOOL) checkLandscapeOrientation;

+ (BOOL) validateEmail:(NSString *)emailString;

+ (NSDate*) convertStringToDate:(NSString*)dateText;
+ (NSDate*) convertStringToDate:(NSString*)dateText dateFormat:(NSString*)format;
+ (NSString*) convertDateToString:(NSDate*)date;
+ (NSString*) convertDateToString:(NSDate*)date dateFormat:(NSString *)format;

+ (NSString *)toLocalTimeText:(NSDate*)utcDate;
+ (NSString *)toLocalTimeText:(NSDate*)utcDate dateFormat:(NSString*)format;
+ (NSString *) toUTCTimeText:(NSDate*)localDate;
+ (NSString *) toUTCTimeText:(NSDate*)localDate dateFormat:(NSString*)format;

+ (NSString*) getInnerTextInSource:(NSString*)xmlSource byTagName:(NSString*)tagName;
+ (NSMutableArray*) getInnerTextsInSource:(NSString *)xmlSource byTagName:(NSString *)tagName;

+ (NSString*) getDataWithKey:(NSString *)key;
+ (void) saveData:(NSString*)valueString withKey:(NSString*)keyString;
+ (NSMutableDictionary*) applicationDataFromFile:(NSString*)fileName;
+ (BOOL) writeApplicationData:(NSMutableDictionary*)data toFile:(NSString*)fileName;

+ (void) checkAndCreateDatabase;
+ (void) executeDatabaseCommand:(NSString*)commandText;
//+ (NSString*) getDatabaseSchemaVersion;
+ (void) updateDatabaseSchema;


@end
