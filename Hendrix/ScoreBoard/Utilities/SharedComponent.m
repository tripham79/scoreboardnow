
#import "SharedComponent.h"
#import "CommonFunction.h"

@implementation SharedComponent
@synthesize interfaceOrientation, navigationController, loginToken, deviceToken, databasePath, listSport, listLeague, listGameScore, listCurrentGameBox, facebook, btnFacebook, btnTwitter, twitterAccount, refreshSeconds, currentViewController;
@synthesize isLinkFacebook, isLinkTwitter, isCheckingNewGame;

@synthesize gameScoreRefreshSeconds, listGameScoreRefreshOption;

@synthesize selectedGame;

static SharedComponent *sharedObject;

+ (SharedComponent*) getInstance {
    @synchronized (self) {
        if (sharedObject == nil) {
            sharedObject = [[SharedComponent alloc] init];
            
            //load local data
            NSString *loginToken = [CommonFunction getDataWithKey:kLoginToken];
            if (loginToken != nil && ![loginToken isEqualToString:@""])
                [sharedObject setLoginToken:loginToken];
            else
                [sharedObject setLoginToken:nil];
            
            NSString* value = [CommonFunction getDataWithKey:kCanCreateGame];
            if (value != nil && ![value isEqualToString:@""])
                [sharedObject setCanCreateGame:[value boolValue]];
            else
                [sharedObject setCanCreateGame:NO];
        }
        return sharedObject;
    }
}

- (Sport*) getSport:(NSString*)sportId {
    if (listSport == nil || listSport.count == 0)
        return nil;
    
    Sport *sport = nil;
    for (int i=0; i<listSport.count; i++) {
        sport = [listSport objectAtIndex:i];
        if ([sport.sportId  isEqualToString:sportId])
            return sport;
    }
    return nil;
}

- (SportTypeEnum) getSportType:(NSString*)sportId {
    if (listSport == nil || listSport.count == 0)
         return SportUnknown;
    
    Sport *sport = nil;
    for (int i=0; i<listSport.count; i++) {
        sport = [listSport objectAtIndex:i];
        if ([sport.sportId  isEqualToString:sportId])
            break;
    }
    
    if (sport != nil) {
        if ([sport.sportTypeId isEqualToString:@"1"])
            return SportFootball;
        else if ([sport.sportTypeId isEqualToString:@"2"])
            return SportSoccer;
        else if ([sport.sportTypeId isEqualToString:@"3"])
            return SportBaseball;
        else if ([sport.sportTypeId isEqualToString:@"4"])
            return SportBasketball;
        else
            return SportUnknown;
    }
    return SportUnknown;
}

- (ACAccount *)getTwitterAccount
{
    if (self.twitterAccount == nil) {
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        if (accountStore != nil) {
            ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
            [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
                if(granted) {
                    NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                    if (accountsArray != nil && [accountsArray count]>0) {
                        ACAccount *acc = [accountsArray objectAtIndex:0];
                        self.twitterAccount = acc;
                        self.isLinkTwitter = YES;
                    }
                }
            }];
        }
    }
    return self.twitterAccount;
}

@end
