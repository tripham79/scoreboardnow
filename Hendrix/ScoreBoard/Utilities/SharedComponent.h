
#import <Foundation/Foundation.h>
#import "FBConnect.h"
#import "Sport.h"
//#import "Game.h"
#import <Accounts/ACAccountType.h>
#import <Accounts/ACAccountStore.h>

typedef enum : NSInteger {
    SportUnknown = 0,
    SportFootball,
    SportSoccer,
    SportBasketball,
    SportBaseball
} SportTypeEnum;

typedef enum : NSInteger {
    KeyPad0 = 0,
    KeyPad1,
    KeyPad2,
    KeyPad3,
    KeyPad4,
    KeyPad5,
    KeyPad6,
    KeyPad7,
    KeyPad8,
    KeyPad9,
    KeyPadDel,
    KeyPadEnter
} KeyPadEnum;

@interface SharedComponent : NSObject

@property int interfaceOrientation;
@property (assign) UINavigationController *navigationController;
@property (assign) UIViewController *currentViewController;

@property int refreshSeconds;
@property BOOL isCheckingNewGame;

@property (retain) NSMutableArray *listGameScoreRefreshOption;
@property int gameScoreRefreshSeconds;


@property (retain) NSString* loginToken;
@property BOOL canCreateGame;

@property (retain) NSString *deviceToken;
@property (retain) NSString *databasePath;
@property (retain) NSMutableArray *listSport;
@property (retain) NSMutableArray *listLeague;
@property (retain) NSMutableArray *listGameScore;
@property (assign) NSMutableDictionary *listCurrentGameBox;

@property (retain) Facebook *facebook;
@property (retain) UIButton *btnFacebook;
@property (retain) UIButton *btnTwitter;
@property (retain) ACAccount* twitterAccount;
@property BOOL isLinkFacebook;
@property BOOL isLinkTwitter;

- (SportTypeEnum) getSportType:(NSString*)sportId;
- (Sport*) getSport:(NSString*)sportId;
- (ACAccount*) getTwitterAccount;

@property (retain) NSArray *selectedGame;

+ (SharedComponent*) getInstance;

@end


