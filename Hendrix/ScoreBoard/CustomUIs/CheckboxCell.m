//
//  CheckboxCell.m
//  ScoreBoard
//
//  Created by Apple on 7/12/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import "CheckboxCell.h"

@implementation CheckboxCell
@synthesize checkBoxField, labelField, detailField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _isChecked = FALSE;
        
        //UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkBoxFieldTapped:)];
        //[self.checkBoxField addGestureRecognizer:tapRecognizer];
        //[self.labelField addGestureRecognizer:tapRecognizer];
        
        [labelField setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [labelField setAdjustsFontSizeToFitWidth:YES];
        [labelField setMinimumFontSize:10];
        [detailField setAdjustsFontSizeToFitWidth:YES];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return self;
}

- (BOOL)getIsChecked {
    return _isChecked;
}

- (void)setIsChecked:(BOOL)checked {
    _isChecked = checked;
    if (!_isChecked)
        [checkBoxField setImage:[UIImage imageNamed:@"checkbox.png"]];
    else 
        [checkBoxField setImage:[UIImage imageNamed:@"checkbox_Active.png"]];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)checkBoxFieldTapped:(UITapGestureRecognizer*)recognizer
//{
//    if (recognizer.state == UIGestureRecognizerStateEnded)
//    {
//    }
//}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setIsChecked:!_isChecked];
    
    [super touchesEnded:touches withEvent:event];
}

- (void)dealloc {
    [checkBoxField release];
    [labelField release];
    [detailField release];
    [super dealloc];
}
@end
