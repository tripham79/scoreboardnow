
#import "GameBox.h"

@implementation GameBox
@synthesize gameId;
@synthesize game, delegate;
@synthesize homeTeamName, homeTeamScore, awayTeamName, awayTeamScore, desciption;
@synthesize logoSize = _logoSize;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GreenBox.png"]];
        
        _logoSize = 20;
        int left = (frame.size.width - _logoSize)/2;
        //int top =(frame.size.height - _logoSize)/2;
        sportLogo = [[UIImageView alloc] initWithFrame:CGRectMake(left, 6, _logoSize, _logoSize)];
        notification = [[Notification alloc] initWithNumber:0];
        
        homeTeamName = [[UILabel alloc] initWithFrame:CGRectMake(10, 6 + _logoSize, 90, 21)];
        [homeTeamName setMinimumFontSize:8];
        [homeTeamName setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [homeTeamName setAdjustsFontSizeToFitWidth:YES];
        [homeTeamName setBackgroundColor:[UIColor clearColor]];
        
        homeTeamScore = [[UILabel alloc] initWithFrame:CGRectMake(110, 6 + _logoSize, 19, 21)];
        [homeTeamScore setMinimumFontSize:10];
        [homeTeamScore setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [homeTeamScore setBackgroundColor:[UIColor clearColor]];
        [homeTeamScore setTextAlignment:NSTextAlignmentRight];
        
        awayTeamName = [[UILabel alloc] initWithFrame:CGRectMake(10, 30 + _logoSize, 90, 21)];
        [awayTeamName setMinimumFontSize:8];
        [awayTeamName setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [awayTeamName setAdjustsFontSizeToFitWidth:YES];
        [awayTeamName setBackgroundColor:[UIColor clearColor]];
        
        awayTeamScore = [[UILabel alloc] initWithFrame:CGRectMake(110, 30 + _logoSize, 19, 21)];
        [awayTeamScore setMinimumFontSize:10];
        [awayTeamScore setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [awayTeamScore setBackgroundColor:[UIColor clearColor]];
        [awayTeamScore setTextAlignment:NSTextAlignmentRight];
        
        desciption = [[UILabel alloc] initWithFrame:CGRectMake(8, 54 + _logoSize, self.frame.size.width - 16, 46)];
        [desciption setFont:[UIFont fontWithName:@"Trebuchet MS" size:11]];
        [desciption setLineBreakMode:NSLineBreakByCharWrapping];
        [desciption setNumberOfLines:0];
        [desciption setBackgroundColor:[UIColor clearColor]];
        [desciption setTextAlignment:NSTextAlignmentCenter];
        [desciption setAdjustsFontSizeToFitWidth:YES];
        [self setBackgroundColor:[UIColor clearColor]];
        
        
        //test        
        [homeTeamName setText:@"Cy Falls V"];
        [homeTeamScore setText:@"21"];
        
        [awayTeamName setText:@"Cy Falls V"];
        [awayTeamScore setText:@"21"];
        
        [desciption setText:@""];
    }
    return self;
}

- (void) setStatus:(int)aStatus{
    status = aStatus;
    NSString *desciptionString = nil;
    
    switch (status) {
        case PreStart:
            [backgroundImage setImage:[UIImage imageNamed:@"GrayBox.png"]];
            textColor = [UIColor blackColor];
            
            NSString* localDateText = [CommonFunction toLocalTimeText:game.scheduledStartTime dateFormat:@"hh:mma yyyy-MM-dd"];
            
            if (localDateText != nil)
            {
                NSCalendar *calendar = [NSCalendar currentCalendar];
                // display in user time zone
                [calendar setTimeZone:[NSTimeZone systemTimeZone]]; // [NSTimeZone timeZoneWithName:@"UTC"]];
                NSDateComponents *components = [calendar components:NSWeekdayCalendarUnit fromDate:game.scheduledStartTime];
                NSLog(@"%d", [components weekday]);
                
                //NSString *weekDay = [dateCST descriptionWithCalendarFormat:@"%w" timeZone:[NSTimeZone timeZoneWithAbbreviation:@"CST"] locale:nil];
                //NSLog(@"%@ %d", weekDay, weekDay.intValue);
                localDateText = [NSString stringWithFormat:@"%@, %@", [CommonFunction getWeekDayFromId:[components weekday] - 1], localDateText];
            }
            
            desciptionString = [NSString stringWithFormat:@"%@\n%@", localDateText, game.stadiumName];
            [desciption setText:desciptionString];
            
            break;
        case Process:
        {
            [backgroundImage setImage:[UIImage imageNamed:@"GreenBox.png"]];
            textColor = [UIColor whiteColor];
            
            NSString *timeRemain = [NSString stringWithFormat:@"%d:%02d", self.game.minutes.intValue, self.game.seconds.intValue];
            
            // display in user time zone
            NSString *lastUpdated = [CommonFunction toLocalTimeText:game.lastUpdated dateFormat:@"hh:mma yyyy-MM-dd"];
            
            if (lastUpdated == nil)
                lastUpdated = @"(N/A)";
            
            
            NSString *szTopBottom = @"Top";
            if (!self.game.isTop)
                szTopBottom = @"Bottom";
            
            SportTypeEnum sportType = [[SharedComponent getInstance] getSportType:self.game.sportID];            
            switch (sportType) {
                case SportBaseball:
                    desciptionString = [NSString stringWithFormat:@"Inning %@ %02d,\n%d-%d-%d, Last Update\n%@", szTopBottom, self.game.qtr.intValue, self.game.balls.intValue, self.game.strikes.intValue, self.game.outs.intValue, lastUpdated];
                    break;
                case SportSoccer:
                    if (self.game.halfTime)
                        desciptionString = [NSString stringWithFormat:@"%@ Elapsed, Halftime\nLast Update:\n%@", timeRemain, lastUpdated];
                    else
                        desciptionString = [NSString stringWithFormat:@"%@ Elapsed, Half %@\nLast Update:\n%@", timeRemain, self.game.qtr, lastUpdated];
                    break;
                case SportFootball:
                case SportBasketball:
                case SportUnknown:
                    if (self.game.halfTime)
                        desciptionString = [NSString stringWithFormat:@"%@ Remain, Halftime\nLast Update:\n%@", timeRemain, lastUpdated];
                    else
                        desciptionString = [NSString stringWithFormat:@"%@ Remain, Qtr %@\nLast Update:\n%@", timeRemain, self.game.qtr, lastUpdated];
                    break;
            }
            
            [self.desciption setText: desciptionString];
            break;
        }
        case Final:
        {
            [backgroundImage setImage:[UIImage imageNamed:@"RedBox.png"]];
            textColor = [UIColor blackColor];
            [desciption setText:@"Final"];
            break;
        }
        default: {
            [backgroundImage setImage:[UIImage imageNamed:@"GrayBox.png"]];
            textColor = [UIColor blackColor];
            break;
        }
    }
    
    [homeTeamName setTextColor: textColor];
    [homeTeamScore setTextColor: textColor];
    [awayTeamName setTextColor: textColor];
    [awayTeamScore setTextColor: textColor];
    [desciption setTextColor: textColor];
}

- (void) setNumberNorification:(int)number{
    notification.number = number;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [backgroundImage setFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:backgroundImage];
    
    SportTypeEnum sportType = [[SharedComponent getInstance] getSportType:game.sportID];
    switch (sportType) {
        case SportBaseball:
            sportLogo.image = [UIImage imageNamed:@"baseball.png"];
            break;
        case SportFootball:
            sportLogo.image = [UIImage imageNamed:@"football.png"];
            break;
        case SportSoccer:
            sportLogo.image = [UIImage imageNamed:@"soccer.png"];
            break;
        case SportBasketball:
            sportLogo.image = [UIImage imageNamed:@"basketball.png"];
            break;
        case SportUnknown:
            break;
    }
    [self addSubview:sportLogo];
    
    [self addSubview:notification];
    if (notification.number <= 0)
        [notification setHidden:YES];
    else
        [notification setHidden:NO];    
    
    [self addSubview: homeTeamName];
    [self addSubview:homeTeamScore];
    
    [self addSubview:awayTeamName];
    [self addSubview:awayTeamScore];
    
    [self addSubview:desciption];
}

- (void) updateGameScore
{
    if (game != nil)
    {
        [self.homeTeamName setText:game.team1Name];
        [self.homeTeamScore setText:game.team1Score];
        [self.awayTeamName setText:game.team2Name];
        [self.awayTeamScore setText:game.team2Score];
        
        [self setStatus:game.statusID.intValue];
    
        if (game.localStatusId.intValue == End){
            [self setStatus:Final];
        }
    }
}

- (void) updateGameStatus
{
    if (game != nil)
    {
        [self.homeTeamName setText:game.team1Name];
        [self.awayTeamName setText:game.team2Name];
        [self setStatus:game.statusID.intValue];
        
        if (game.localStatusId.intValue == End){
            [self setStatus:Final];
        }
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [delegate performClick: self];
}

- (void) dealloc{
    [game release];
    [backgroundImage release];
    [sportLogo release];
    [notification release];
    [super dealloc];
}
@end
