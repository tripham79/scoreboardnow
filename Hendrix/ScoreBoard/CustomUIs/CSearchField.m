
#import "CSearchField.h"

@implementation CSearchField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width - 50, bounds.size.height);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width - 50, bounds.size.height);
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor blackColor] setFill];
    [[self placeholder] drawInRect:rect withFont:self.font];
}

@end
