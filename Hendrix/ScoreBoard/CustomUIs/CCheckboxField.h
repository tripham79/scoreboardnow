
#import <UIKit/UIKit.h>

@interface CCheckboxField : UIControl {
    UIImageView *imageCheckBox;
    UILabel *label;
    NSString *_text;
}

@property BOOL isChecked;
@property (retain) NSString *text;

- (void) setStatus:(BOOL)checked;

@end
