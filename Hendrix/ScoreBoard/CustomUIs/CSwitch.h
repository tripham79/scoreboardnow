
#import <UIKit/UIKit.h>
@class CSwitch;
@protocol CSwitchDelegate <NSObject>

@optional
- (void) switchChangeStatus:(BOOL)status;

@end

@interface CSwitch : UIView{
    UIImageView *imageSwitch;
    BOOL status;
    
    CGPoint startPoint;
    int totalMovement;
    
    BOOL isDrag;
}
@property (assign) id<CSwitchDelegate> delegate;

- (BOOL) status;
- (void) setStatus:(BOOL)aStatus;

@end
