

#import "ScoreBoard.h"

@implementation ScoreBoard2
@synthesize portrait;
@synthesize landscape;
@synthesize delegate;
@synthesize tempMinutes, tempSeconds;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        
        NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"ScoreBoard" owner:self options:nil];
        self = (ScoreBoard2*)[arr objectAtIndex:0];
        
        timeRemain = -10;
        isUpdateTimeRemain = YES;
        
        isAlive = NO;
        refreshTimer = nil;
    }
    return self;
}

- (void) startTimer{
    if (refreshTimer == nil){
        refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    }
    
    [(UILabel*)[self viewWithTag:111] setText:[NSString stringWithFormat:@"Refreshing\nin %d sec", [[SharedComponent getInstance] refreshSeconds]]];
    tick = [[SharedComponent getInstance] refreshSeconds];
    isAlive = YES;
}

- (void) stopTimer{
    NSString *status = @"Getting score...";
    [(UILabel*)[self viewWithTag:111] setText:status];
    isAlive = NO;
}

- (void) stopTimeRemainTimer{
    if ([timeRemainTimer respondsToSelector:@selector(invalidate)])
    {
       [timeRemainTimer invalidate];
    }
}

- (void) timerTick{
    if (isAlive){
        tick--;
        
        if (tick > 0)
        {
            NSString *status = [NSString stringWithFormat:@"Refreshing\nin %d sec", tick];
            [(UILabel*)[self viewWithTag:111] setText:status];
        }
        else if (tick == 0)
        {
            if ([delegate respondsToSelector:@selector(refresh:)])
            {
                [delegate refresh:self];
            }
        }
    }
}

- (void) changeOrientation:(int)orientation{
    if (orientation == Portrait)
    {
        for (int i = 100; i <= 125; i++)
        {
            [self viewWithTag:i].frame = [portrait viewWithTag:i].frame;
            if ([[self viewWithTag:i] respondsToSelector:@selector(setFont:)])
            {       
                [[self viewWithTag:i] setFont: [[portrait viewWithTag:i] font]];
            }
            
            if ( i == 100)
            {
                UIImageView *background = (UIImageView*)[self viewWithTag:i];
                [background setImage:[UIImage imageNamed:@"GameFeedBoard.png"]];
            }
        }
    }
    else if (orientation == Landscape)
    {
        for (int i = 100; i <= 125; i++)
        {
            [self viewWithTag:i].frame = [landscape viewWithTag:i].frame;
            if ([[self viewWithTag:i] respondsToSelector:@selector(setFont:)])
            {       
                [[self viewWithTag:i] setFont: [[landscape viewWithTag:i] font]];
            }
            
            if ( i == 100)
            {
                UIImageView *background = (UIImageView*)[self viewWithTag:i];
                [background setImage:[UIImage imageNamed:@"GameFeedBoard_landscape.png"]];
            }
        }
    }
}

- (void) updateTeam1Score:(int)score{
    //tag 108, 109
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)([self viewWithTag:108]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:109]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateTeam2Score:(int)score{
    //tag 108, 109
    int tenNumber = score/10;
    int unitNumber = score%10;
    
    [(UIImageView*)([self viewWithTag:102]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:103]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateQtr:(int) qtr{
    [(UIImageView*)([self viewWithTag:104]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:qtr]]];
}

- (void) updateMinutes:(int)minutes{
    //tag 106 107
    self.tempMinutes = [NSString stringWithFormat:@"%d", minutes];
    
    int tenNumber = minutes / 10;
    int unitNumber = minutes % 10;
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateSeconds:(int)seconds{
    //tag 101, 105
    self.tempSeconds = [NSString stringWithFormat:@"%d", seconds];
    
    int tenNumber = seconds / 10;
    int unitNumber = seconds % 10;
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:101]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
}

- (void) updateTimeElapsed:(int)seconds{
    [self updateMinutes:seconds / 60];
    [self updateSeconds:seconds % 60];
    
    /*
    int tenNumber = hours / 10;
    int unitNumber = hours % 10;
    [(UIImageView*)([self viewWithTag:107]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:106]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
    
    tenNumber = minutes / 10;
    unitNumber = minutes % 10;
    [(UIImageView*)([self viewWithTag:105]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:tenNumber]]];
    [(UIImageView*)([self viewWithTag:101]) setImage:[UIImage imageNamed:[CommonFunction getImageNameWithNumber:unitNumber]]];
    */
}

- (void) updateLastUpdated:(NSDate *)lastUpdated{
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFormatter.dateFormat = @"hh:mma yyyy-MM-dd";
    //[dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"CST"]];
    
    NSString *desciptionString = @"";
    
    if ([dateFormatter stringFromDate:lastUpdated] == nil){
        desciptionString = [NSString stringWithFormat:@"Last Update:\n%@", @"N/A"];
    }
    else
    {
        desciptionString = [NSString stringWithFormat:@"Last Update:\n%@", [dateFormatter stringFromDate:lastUpdated]];
    }
    
    [(UILabel*)([self viewWithTag:110]) setText:desciptionString];
}

- (void) updateGameScoreFromGameBox:(GameBox*)gameBox{
    [self updateTeam1Score:gameBox.game.team1Score.intValue];
    [self updateTeam2Score:gameBox.game.team2Score.intValue];
    [self updateQtr:gameBox.game.qtr.intValue];
    [self updateLastUpdated:gameBox.game.lastUpdated];
    
    [self updateMinutes:gameBox.game.minutes.intValue];
    [self updateSeconds:gameBox.game.seconds.intValue];
    
    /*if (([gameBox.game.seconds isEqualToString:@""] || [gameBox.game.seconds isEqualToString:@"0"]) &&
        ([gameBox.game.minutes isEqualToString:@""] || [gameBox.game.minutes isEqualToString:@"0"])){
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        dateFormatter.dateFormat = DateTimeFormat;
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
        
        NSDate *utcNow = [dateFormatter dateFromString:timeStamp];
        NSDate *startTime = gameBox.game.startTime;
        
        NSTimeInterval timeElapsed = [utcNow timeIntervalSinceDate:startTime];
        if (timeElapsed >= 0){
            [self updateTimeElapsed:timeElapsed];
        }
    }
    else{
        [self updateMinutes:gameBox.game.minutes.intValue];
        [self updateSeconds:gameBox.game.seconds.intValue];
    }*/
    
    /*if (timeRemain < 0)
    {
        int duration = gameBox.game.duration.intValue * 60;
        
        NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        dateFormatter.dateFormat = DateTimeFormat;
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
        
        NSDate *utcNow = [dateFormatter dateFromString:timeStamp];
        NSDate *startTime = gameBox.game.startTime;
        
        if (startTime != nil && duration > 0)
        {
            NSTimeInterval timeElapsed = [utcNow timeIntervalSinceDate:startTime];
            if (timeElapsed > 0)
            {
                timeRemain = duration - timeElapsed;
                if (timeRemain < 0) return;
                
                [self updateTimeRemain:timeRemain];
                
                timeRemainTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick2) userInfo:nil repeats:YES];
            }

        }
    }*/
}

- (void) activeScore:(int)kind{
    [[self viewWithTag:115] setHidden:YES];;
    [[self viewWithTag:116] setHidden:YES];
    [[self viewWithTag:117] setHidden:YES];
    [[self viewWithTag:118] setHidden:YES];
    [[self viewWithTag:119] setHidden:YES];

    [[self viewWithTag:121] setHidden:YES];
    [[self viewWithTag:122] setHidden:YES];
    [[self viewWithTag:124] setHidden:YES];
    [[self viewWithTag:125] setHidden:YES];
    
    if (kind == 1){
        //team1
        [[self viewWithTag:115] setHidden:NO];
        [[self viewWithTag:116] setHidden:NO];
    }
    else if (kind == 2){
        //team2
        [[self viewWithTag:117] setHidden:NO];
        [[self viewWithTag:118] setHidden:NO];
    }
    else if (kind == 3){
        //qtr
        [[self viewWithTag:119] setHidden:NO];
    }
    else if (kind == 4){
        //minutes
        [[self viewWithTag:121] setHidden:NO];
        [[self viewWithTag:122] setHidden:NO];
        
        //seconds
        [[self viewWithTag:124] setHidden:NO];
        [[self viewWithTag:125] setHidden:NO];
    }
    else if (kind == 5){
        
    }
}
- (void) timerTick2{
    timeRemain--;
    if (timeRemain < 0){
        [timeRemainTimer invalidate];
        return;
    }
    
    if (isUpdateTimeRemain){

    }
}

- (UIButton*) getTeam1Button{
    return (UIButton*)[self viewWithTag:112];
}

- (UIButton*) getTeam2Button{
    return (UIButton*)[self viewWithTag:113];
}

- (UIButton*) getQtrButton{
    return (UIButton*)[self viewWithTag:114];
}

- (UIButton*) getMinutesButton{
    return (UIButton*)[self viewWithTag:120];
}

- (UIButton*) getSecondsButton{
    return (UIButton*)[self viewWithTag:123];
}

- (void) refresh{
    if ([delegate respondsToSelector:@selector(refresh:)])
    {
        [delegate refresh:self];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    NSLog(@"ScoreBoard dealloc");
    [portrait release];
    [landscape release];
    [super dealloc];
}
@end
