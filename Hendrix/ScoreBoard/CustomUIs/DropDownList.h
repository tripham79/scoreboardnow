
#import <UIKit/UIKit.h>

@class DropDownList;

@protocol DropDownListDelegate <NSObject>
- (void) dropDownList:(id)owner didSelectedRowAtIndex:(int) index;
@end

@interface DropDownList : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (assign) id<DropDownListDelegate> delegate;
@property (assign) id owner;
@property (retain) NSMutableArray *source;
@property (retain) NSString* textField;
@property (retain) NSString* detailField;

@property (retain, nonatomic) IBOutlet UITableView *table;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;

- (void) reloadData;
@property (retain, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)btnBackTapped:(id)sender;

@end
