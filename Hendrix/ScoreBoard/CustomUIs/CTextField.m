
#import "CTextField.h"

@implementation CTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 10 );
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 10 );
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor lightGrayColor] setFill];
    //[[self placeholder] drawInRect:rect withFont:self.font];
    [self.placeholder drawInRect:rect withFont:self.font lineBreakMode:NSLineBreakByTruncatingTail alignment:self.textAlignment];
}

@end
