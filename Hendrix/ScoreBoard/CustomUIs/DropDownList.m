
#import "DropDownList.h"

@implementation DropDownList
@synthesize btnBack;
@synthesize source, table, delegate, owner;
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    }
    
    [table setDataSource:self];
    [table setDelegate:self];
    
    if (self.title.length > 0)
        self.lblTitle.text = self.title;
    
    [btnBack setBackgroundImage:[UIImage imageNamed:@"btnBack.png"] forState:UIControlStateNormal];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"btnBack_Active.png"] forState:UIControlStateHighlighted];
    [btnBack addTarget:self action:@selector(btnBackTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidUnload
{
    [self setTable:nil];
    [self setSource:nil];
    [self setBtnBack:nil];
    [self setLblTitle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [table release];
    [source release];
    [btnBack release];
    [_lblTitle release];
    [super dealloc];
}

- (void) reloadData{
    [table reloadData];
}

- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [source count];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"UITableViewCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell == nil){
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [cell.textLabel setMinimumFontSize:10];
        [cell.textLabel setTextColor:[UIColor blackColor]];
	}
    
    id obj = [source objectAtIndex:indexPath.row];
    if (self.textField)
        cell.textLabel.text = [obj valueForKey:self.textField];
    else
        cell.textLabel.text = [NSString stringWithFormat:@"%@", obj];
    
    if (self.detailField)
        cell.detailTextLabel.text = [obj valueForKey:self.detailField];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [delegate dropDownList:self.owner didSelectedRowAtIndex:indexPath.row];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
