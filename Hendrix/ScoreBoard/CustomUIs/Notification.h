
#import <UIKit/UIKit.h>

@interface Notification : UIView{
    UIImageView *backgroundImage;
    UILabel *numberTitle;
    int number;
}

@property int number;

- (id) initWithNumber:(int)aNumber;

@end
