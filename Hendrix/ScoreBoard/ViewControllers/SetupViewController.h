
#import "BaseViewController.h"

@interface SetupViewController : BaseViewController<DropDownListDelegate>{
    NSString *selectedScoreUpdateId;
    NSString *selectedSportId;
}

@property (retain, nonatomic) IBOutlet CSwitch *startGameSetting;
@property (retain, nonatomic) IBOutlet CSwitch *endGameSetting;
@property (retain, nonatomic) IBOutlet UIButton *scoreUpdateDropDownList;
@property (retain, nonatomic) IBOutlet CSwitch *scoreRequestSetting;
@property (retain, nonatomic) IBOutlet UIButton *selectSportDropDownList;

- (IBAction)btnDoneTapped:(id)sender;
- (IBAction)btnLogoutTapped:(id)sender;
- (void) showDropDownList:(id)sender;

@end
