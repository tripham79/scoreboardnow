typedef enum LoginScreenStep{
    LoginStep = 0,
    SubscribeDeviceStep,
    GetClientConfigStep,
    GetGameScoreOptionStep,
    GetSportStep,
    GetSubscribedTeamCount,
    GetSubscribedGameCount
} LoginScreenStep;

#import "SignInViewController.h"

@implementation SignInViewController
@synthesize userNameField;
@synthesize passwordField;
@synthesize btnFacebook;
@synthesize btnTwitter;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isLinkTwitter = [[SharedComponent getInstance] isLinkTwitter];
    
    [[SharedComponent getInstance] setBtnTwitter:btnTwitter];
    
    if ([[SharedComponent getInstance] isLinkFacebook])
    {
        [btnFacebook setBackgroundImage:[UIImage imageNamed:@"BtnFacebook_Unlink.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btnFacebook setBackgroundImage:[UIImage imageNamed:@"BtnFacebook.png"] forState:UIControlStateNormal];        
    }
    
    if ([[SharedComponent getInstance] isLinkTwitter])
    {
        [btnTwitter setBackgroundImage:[UIImage imageNamed:@"BtnTwitter_Unlink.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btnTwitter setBackgroundImage:[UIImage imageNamed:@"BtnTwitter.png"] forState:UIControlStateNormal];        
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.userNameField.text = @"";
    self.passwordField.text = @"";
}

- (void)viewDidUnload
{
    [self setUserNameField:nil];
    [self setPasswordField:nil];
    [self setBtnFacebook:nil];
    [self setBtnTwitter:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [userNameField release];
    [passwordField release];
    [btnFacebook release];
    [btnTwitter release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
        return false;
    
    return true;
}

- (IBAction) textFieldEndEditting:(id)sender{
    [super textFieldEndEditting:sender];
    
    if (![userNameField.text isEqualToString:@""] &&
        ![passwordField.text isEqualToString:@""]){
        [self doLogIn];
    }
}

- (IBAction)btnRegistrationTapped:(id)sender {
    RegistrationViewController *viewController = [[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil];
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (IBAction)btnDoneTapped:(id)sender {
    [self doLogIn];
}

- (void) doLogIn{
    if (focusedField != nil)
    {
        [focusedField resignFirstResponder];
        [self slideFrameWhenUnFocus:focusedField];
    }
    
    currentStep = LoginStep;
    
    NSString *userName = userNameField.text;
    NSString *password = passwordField.text;
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildLoginBody:userName password:password];
    [self sendRequest];
}

- (IBAction)btnForgotPasswordTapped:(id)sender {
    RecoveryPasswordViewController *viewController = [[RecoveryPasswordViewController alloc] initWithNibName:@"RecoveryPasswordViewController" bundle:nil];
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNResponse *response = nil;
    switch (currentStep) {
        case LoginStep:
            response = [[SBNLoginResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case SubscribeDeviceStep:
            response = [[SBNSubscribeDeviceResponse alloc] init];
            [response parseResponseData:responseData];
        case GetClientConfigStep:
            response = [[SBNGetClientConfigurationResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetGameScoreOptionStep:
            response = [[SBNGameScoreRefreshOptions alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetSportStep:
            response = [[SBNSportResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetSubscribedTeamCount:
            response = [[SBNGetSubscribedTeamsCountResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case GetSubscribedGameCount:
            response = [[SBNGetSubscribedGamesCountResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        default:
            break;
    }
    
    if ([response.responseCode isEqualToString:@""]) return;
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]){
        switch (currentStep) {
            case LoginStep:
            {
                SBNLoginResponse *loginResponse = (SBNLoginResponse*) response;
                [[SharedComponent getInstance] setLoginToken: loginResponse.loginToken];
                [[SharedComponent getInstance] setCanCreateGame:[loginResponse.canCreateGame boolValue]];
                
                [CommonFunction saveData:loginResponse.loginToken withKey:kLoginToken];
                [CommonFunction saveData:userNameField.text withKey:kUser];
                [CommonFunction saveData:loginResponse.canCreateGame withKey:kCanCreateGame];
                
                currentStep = SubscribeDeviceStep;
                if (sbnRequest == nil){
                    sbnRequest = [[SBNRequest alloc] init];
                }
                
                NSString* deviceToken = [[SharedComponent getInstance] deviceToken];
                if (deviceToken.length > 0) {
                    [sbnRequest buildSubscribeDeviceBody:[[SharedComponent getInstance] loginToken] deviceToken:deviceToken];
                    [self sendRequest];
                }
                else {
                    //get client config
                    currentStep = GetClientConfigStep;
                    
                    NSString *loginToken = [[SharedComponent getInstance] loginToken];
                    if (sbnRequest == nil){
                        sbnRequest = [[SBNRequest alloc] init];
                    }
                    
                    [sbnRequest buildGetClientConfigurationBody:loginToken];
                    [self sendRequest];
                }
                break;
            }
            case SubscribeDeviceStep:
                {
                    //get client config
                    currentStep = GetClientConfigStep;
                    
                    NSString *loginToken = [[SharedComponent getInstance] loginToken];
                    if (sbnRequest == nil){
                        sbnRequest = [[SBNRequest alloc] init];
                    }
                    
                    [sbnRequest buildGetClientConfigurationBody:loginToken];
                    [self sendRequest];
                    break;
                }
            case GetClientConfigStep:
            {
                SBNGetClientConfigurationResponse *getClientConfigurationResponse = (SBNGetClientConfigurationResponse*) response;
                [[SharedComponent getInstance] setRefreshSeconds:getClientConfigurationResponse.refreshSeconds.intValue];
                
                currentStep = GetGameScoreOptionStep;
                
                NSString *loginToken = [[SharedComponent getInstance] loginToken];
                if (sbnRequest == nil){
                    sbnRequest = [[SBNRequest alloc] init];
                }
                
                [sbnRequest buildGameScoreRefreshOptionsBody:loginToken];
                [self sendRequest];
                break;
            }
            case GetGameScoreOptionStep:
            {
                SBNGameScoreRefreshOptions *gameScoreRefreshOptions = (SBNGameScoreRefreshOptions*) response;
                [[SharedComponent getInstance] setListGameScoreRefreshOption:[gameScoreRefreshOptions listGameScoreRefreshOption]];
                
                //set game score refresh time, if first time, set option 1
                //if from second lauch time, get from local data
                NSString *optionName = [CommonFunction getDataWithKey:Notification_ScoreUpdate];
                if (optionName == nil || [optionName isEqualToString:@""]){
                    GameScoreRefreshOption *firstOption = [[[SharedComponent getInstance] listGameScoreRefreshOption] objectAtIndex:0];
                    [CommonFunction saveData:firstOption.optionName withKey:Notification_ScoreUpdate];
                    [[SharedComponent getInstance] setGameScoreRefreshSeconds:firstOption.optionSeconds.intValue];
                }
                else{
                    BOOL isStop = NO;
                    for (GameScoreRefreshOption *option in [[SharedComponent getInstance] listGameScoreRefreshOption]){
                        if (!isStop){
                            if ([optionName isEqualToString:option.optionName]){
                                [[SharedComponent getInstance] setGameScoreRefreshSeconds:option.optionSeconds.intValue];
                                isStop = YES;
                            }   
                        }
                    }
                    
                    //if don't match to every option, set default value: 120 seconds
                    if (!isStop){
                        [[SharedComponent getInstance] setGameScoreRefreshSeconds:120];
                    }
                }
                
                currentStep = GetSportStep;
                
                NSString *loginToken = [[SharedComponent getInstance] loginToken];
                if (sbnRequest == nil){
                    sbnRequest = [[SBNRequest alloc] init];
                }
                
                [sbnRequest buildSportBody:loginToken];
                [self sendRequest];
                break;
            }
            case GetSportStep:
            {
                SBNSportResponse *sportResponse = (SBNSportResponse*) response;
                [[SharedComponent getInstance] setListSport:sportResponse.listSport];
                
                // synch teams
                currentStep = GetSubscribedTeamCount;
                
                NSString *loginToken = [[SharedComponent getInstance] loginToken];
                if (sbnRequest == nil){
                    sbnRequest = [[SBNRequest alloc] init];
                }
                
                [sbnRequest buildGetSubscribedTeamsCountBody:loginToken];
                [self sendRequest];
                
                break;
            }
            case GetSubscribedTeamCount:
            {
                SBNGetSubscribedTeamsCountResponse *countResponse = (SBNGetSubscribedTeamsCountResponse*) response;
                
                int teamCount = [Team getTeamsCount];
                if (countResponse.totalRecord > teamCount)
                    [self synchTeam:FALSE];
                
                // synch games
                currentStep = GetSubscribedGameCount;
                
                NSString *loginToken = [[SharedComponent getInstance] loginToken];
                if (sbnRequest == nil){
                    sbnRequest = [[SBNRequest alloc] init];
                }
                
                [sbnRequest buildGetSubscribedGamesCountBody:loginToken];
                [self sendRequest];
                break;
            }
            case GetSubscribedGameCount:
            {
                SBNGetSubscribedGamesCountResponse *countResponse = (SBNGetSubscribedGamesCountResponse*) response;
                
                int gameCount = [Game getGamesCount];
                if (countResponse.totalRecord > gameCount)
                    [self synchGame:FALSE];
                
                MyScoreBoardViewController *viewController = [[MyScoreBoardViewController alloc] initWithNibName:@"MyScoreBoardViewController" bundle:nil];
                [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
                [viewController release];
                break;
            }
            default:
                break;
        }
    }
    else
    {
        NSString *errorMessage = @"Error";
        if (![response.errorMessage isEqualToString:@""])
        {
            errorMessage = response.errorMessage;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
    [responseData release];
    [response release];
}

- (IBAction)btnFacebookTapped:(id)sender {
    [[SharedComponent getInstance] setBtnFacebook:sender];
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"offline_access", nil];
    if (![[SharedComponent getInstance].facebook isSessionValid]) {
        [[SharedComponent getInstance].facebook authorize:permissions];
    }
    else
    {
        [[SharedComponent getInstance].facebook logout];
    }
}

- (IBAction)btnTwitterTapped:(id)sender {
    //TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];
    
    if ([TWTweetComposeViewController canSendTweet])
    {
        if ([[SharedComponent getInstance] isLinkTwitter])
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your twitter account is unlinked." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];    
            [alert release];
            isLinkTwitter = NO;
            
            [[SharedComponent getInstance] setIsLinkTwitter:NO];
            [btnTwitter setBackgroundImage:[UIImage imageNamed:@"BtnTwitter.png"] forState:UIControlStateNormal];  
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Your twitter account is linked." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];    
            [alert release];
            isLinkTwitter = YES;
            
            [[SharedComponent getInstance] setIsLinkTwitter:YES];
            [btnTwitter setBackgroundImage:[UIImage imageNamed:@"BtnTwitter_Unlink.png"] forState:UIControlStateNormal];  
        }
    }
    else
    {
        TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];
        [self presentModalViewController:twitter animated:YES];
        
    }
    
}

@end
