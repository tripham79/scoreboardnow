
#import <UIKit/UIKit.h>
#import <sqlite3.h>

#import "ASIHTTPRequest.h"
#import "Config.h"

#import "SBNRequest.h"
#import "SBNResponse.h"
#import "SBNService.h"

#import "SharedComponent.h"
#import "CommonFunction.h"

#import "CSwitch.h"
#import "CTextField.h"
#import "CSearchField.h"
#import "GameBox.h"
#import "ScoreBoard.h"
#import "BaseScoreBoard.h"
#import "ScoreBoardFootball.h"
#import "ScoreBoardBaseball.h"

#import "CCheckboxField.h"
#import "DropDownList.h"
#import "MBProgressHUD.h"
#import "NSDate+Extensions.h"

@protocol SelectItemViewControllerDelegate <NSObject>
@optional
- (void) selectItemViewController:(id)sender didSelectedRowAtIndex:(int)index;
@end

@interface BaseViewController : UIViewController <ASIHTTPRequestDelegate, UITextFieldDelegate> {
    UIView *focusedField;
    int frameMovement;
    
    UIView* waitingView;
    BOOL isWaiting;
    
    // used for synchronized requests by showing a waiting view
    SBNRequest *sbnRequest;
    
    // requests in the background, not showing waiting view
    SBNRequest *sbnGetScoreRequest;
    SBNRequest *sbnSynchTeamRequest;
    SBNRequest *sbnSynchGameRequest;
    SBNRequest *sbnGetNewGameRequest;
    SBNRequest *sbnSubscribeNewGameRequest;
    
    int refreshSeconds;
    int currentStep;
    UITextField* activeField;
}

#pragma properties
@property (retain) IBOutlet UIScrollView *mainScrollView;
@property (retain) IBOutlet UIButton *btnHome;
@property (retain) IBOutlet UIButton *btnSetting;
@property (retain) IBOutlet UIButton *btnBack;
@property (retain) IBOutlet UIButton *btnRefresh;
@property (retain) IBOutlet UIButton *btnAbout;

#pragma Keyboard action
- (IBAction) textFieldBeginEditing:(id)sender;
- (IBAction) textFieldEndEditting:(id)sender;

- (void)registerForKeyboardNotifications;
- (void)keyboardWillShown:(NSNotification*)aNotification;
- (void)keyboardWillHide:(NSNotification*)aNotification;

- (void) slideFrameWhenFocus:(id)field;
- (void) slideFrameWhenUnFocus:(id)field;
- (void) slideFrame:(int)movement up:(BOOL)up;

#pragma WebService methods
- (void) sendRequest;
- (void) handleResponse:(NSString*)responseData;

- (void) sendGetScoreRequest;
- (void) handleGeScoreResponse;

- (void) handleFailRequest;

// called when new games downloaded
- (void) newGamesDownloaded:(int)count;

- (void) showWaiting:(NSString*)msg;
- (void) removeWaiting;

- (void) synchTeam:(BOOL)showWaiting;
- (void) synchGame:(BOOL)showWaiting;
- (void) synchNewGames:(BOOL)showWaiting;

- (void) sendAddTeams:(NSArray*)listTeamId;
- (void) sendRemoveTeams:(NSArray*)listTeamId;

- (void) sendAddGames:(NSArray*)listGameId;
- (void) sendRemoveGames:(NSArray*)listGameId;

// utility methods
- (BOOL)handleServiceError:(id)result error:(NSError*)error;

#pragma common action
- (IBAction) btnBackTapped:(id)sender;
- (IBAction) btnHomeTapped:(id)sender;
- (IBAction) btnSettingTapped:(id)sender;
- (IBAction) btnRefreshTapped:(id)sender;
- (IBAction) btnAboutTapped:(id)sender;

@end


#pragma import sub view controller
#import "SignInViewController.h"
#import "SetupViewController.h"
#import "RegistrationViewController.h"
#import "MyScoreBoardViewController.h"
#import "GameFeedViewController.h"
#import "UpdateScoreViewController.h"
#import "TwitterFeedViewController.h"
#import "ManageGameViewController.h"
#import "RecoveryPasswordViewController.h"
#import "AboutViewController.h"

