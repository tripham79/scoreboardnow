
#import "TwitterFeedViewController.h"

@implementation TwitterFeedViewController
@synthesize twitterFeedTable;
@synthesize tweets;
@synthesize hashTag;
@synthesize font;
@synthesize cachedImages;
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.font = [UIFont fontWithName:@"Helvetica" size:14];
    self.cachedImages = [[NSMutableDictionary alloc] init];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    [self loadTweetWithHashTag];
}

- (void) btnRefreshTapped:(id)sender{
    [self loadTweetWithHashTag];
}

- (void) loadTweetWithHashTag {
    NSLog(@"hash tag %@", self.hashTag);
    [self showWaiting:@""];
    
    /* // v1.0 is obsoletted now.
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://search.twitter.com/search.json?q=%@&rpp=50&with_twitter_user_id=true&result_type=recent", self.hashTag]];
	NSURLRequest *twitterRequest = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:twitterRequest delegate:self];
    responseData = [[NSMutableData alloc] init];
    [connection start];
    */
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setValue:self.hashTag forKey:@"q"];
    [params setValue:@"50" forKey:@"count"];
    [params setValue:@"recent" forKey:@"result_type"];
    
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json"];
    TWRequest *request = [[TWRequest alloc] initWithURL:url parameters:params requestMethod:TWRequestMethodGET];
    [request setAccount:[[SharedComponent getInstance] twitterAccount]];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData != nil) {
            //[self logResponse:responseData];
            
            NSError* err;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&err];
            self.tweets = [json objectForKey:@"statuses"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self removeWaiting];
                if (self.tweets)
                    [self.twitterFeedTable reloadData];
                else
                    NSLog(@"%@", err);
            });
        }
    }];
}

/*
-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [responseData appendData:data];
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection{
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableLeaves error:nil];
    self.tweets = [jsonObject objectForKey:@"results"];
    [twitterFeedTable reloadData];
    
    [connection release];
    [responseData release];
    [self removeWaiting];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
	NSLog(@"error~~~~~~~");
    [connection release];
    [responseData release];
    
    [self removeWaiting];
}
*/

- (void)viewDidUnload
{
    [self setTwitterFeedTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [self.tweets count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *cellIdentifier = @"UITableViewCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell == nil){
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease]; 
	}
	
	if([indexPath row] < [self.tweets count]){
        NSDictionary *tweet = [self.tweets objectAtIndex:indexPath.row];
        
        NSDictionary *userDic = [tweet objectForKey:@"user"];
        cell.textLabel.text = [userDic objectForKey:@"name"];
        
        NSString* url = [userDic objectForKey:@"profile_image_url"];
        //NSLog(@"%@", url);
        if (url != nil && ![url isEqualToString:@""])
        {
            UIImage *image = [cachedImages objectForKey:url];
            if (image == nil){
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
                [cachedImages setValue:image forKey:url];
                NSLog(@"download new image");
            }
            cell.imageView.image = image;
        }
        else
            cell.imageView.image = nil;

		cell.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
		cell.detailTextLabel.numberOfLines = 0;
		cell.detailTextLabel.text = [tweet objectForKey:@"text"];
        cell.detailTextLabel.font = self.font;
	}
    
	return cell;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if([indexPath row] < [self.tweets count]){
        NSDictionary *tweet = [self.tweets objectAtIndex:indexPath.row];
        NSString *text = [tweet objectForKey:@"text"];
        CGSize size = [text sizeWithFont:self.font constrainedToSize:CGSizeMake(200, 9999) lineBreakMode:UILineBreakModeWordWrap];
        return size.height + 50;
    }
    
    return 100;
}

- (void)dealloc {
    [cachedImages release];
    [twitterFeedTable release];
    [tweets release];
    [font release];
    [super dealloc];
}
@end
