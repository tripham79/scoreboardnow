//
//  CreateGameViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface CreateGameViewController : BaseViewController<UITextFieldDelegate, SelectItemViewControllerDelegate>

@property (nonatomic, retain) Game* game;
@property (nonatomic, retain) NSDate* scheduledStartTime;

- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnSaveClick:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblGameNumber;
@property (retain, nonatomic) IBOutlet UILabel *lblHomeTeam;
@property (retain, nonatomic) IBOutlet UILabel *lblGuestTeam;
@property (retain, nonatomic) IBOutlet UILabel *lblStadium;

@property (retain, nonatomic) IBOutlet UIButton *btnHomeTeam;
@property (retain, nonatomic) IBOutlet UIButton *btnGuestTeam;
@property (retain, nonatomic) IBOutlet UIButton *btnStadium;
- (IBAction)btnHomeTeamClick:(id)sender;
- (IBAction)btnGuestTeamClick:(id)sender;
- (IBAction)btnStadiumClick:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField *txtHomeTeam;
@property (retain, nonatomic) IBOutlet UITextField *txtGuestTeam;
@property (retain, nonatomic) IBOutlet UITextField *txtStadium;
- (IBAction)btnSearchHomeTeamClick:(id)sender;
- (IBAction)btnSearchGuestTeamClick:(id)sender;
- (IBAction)btnSearchStadiumClick:(id)sender;


@property (retain, nonatomic) IBOutlet UITextField *txtScheduledStartTime;
@property (retain, nonatomic) IBOutlet UITextField *txtDuration;
- (IBAction)btnCalendarCancel:(id)sender;
- (IBAction)btnCalendarDone:(id)sender;
@property (retain, nonatomic) IBOutlet UIDatePicker *dtpDatePicker;
@property (retain, nonatomic) IBOutlet UIView *dtpViewContainer;

@property (retain, nonatomic) IBOutlet UIButton *btnSave;
@property (retain, nonatomic) IBOutlet UIButton *btnDelete;
- (IBAction)btnDeleteClick:(id)sender;

@end
