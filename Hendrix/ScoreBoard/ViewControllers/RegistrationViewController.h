
#import "BaseViewController.h"

@interface RegistrationViewController : BaseViewController

@property (retain, nonatomic) IBOutlet CTextField *firstNameField;
@property (retain, nonatomic) IBOutlet CTextField *lastNameField;
@property (retain, nonatomic) IBOutlet CTextField *emailField;
@property (retain, nonatomic) IBOutlet CTextField *zipCodeField;
@property (retain, nonatomic) IBOutlet CTextField *passwordField;
@property (retain, nonatomic) IBOutlet CTextField *confirmPasswordField;

- (IBAction)btnCreateAccountTapped:(id)sender;

@end
