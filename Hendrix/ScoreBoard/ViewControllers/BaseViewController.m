#import "BaseViewController.h"

#import "SplashViewController.h"
#import "GameFeedViewController.h"
#import "Config.h"

@implementation BaseViewController {
    int totalTeams;
    int totalGames;
    int pageIndexTeam;
    int pageIndexGame;
    bool isSynchingTeams;
    bool isSynchingGames;
    
    int totalNewGames;
    bool hasMoreGame;
}

@synthesize mainScrollView;
@synthesize btnHome, btnSetting, btnBack, btnRefresh, btnAbout;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    }
    
    sbnRequest = nil;
    sbnGetScoreRequest = nil;
    
    currentStep = -1;
    
    [mainScrollView setContentSize: mainScrollView.frame.size];
    [mainScrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    if (btnHome != nil)
    {
        [btnHome setBackgroundImage:[UIImage imageNamed:@"btnHome.png"] forState:UIControlStateNormal];
        [btnHome setBackgroundImage:[UIImage imageNamed:@"btnHome_Active.png"] forState:UIControlStateHighlighted];
        [btnHome addTarget:self action:@selector(btnHomeTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (btnSetting != nil)
    {
        [btnSetting setBackgroundImage:[UIImage imageNamed:@"btnSetting.png"] forState:UIControlStateNormal];
        [btnSetting setBackgroundImage:[UIImage imageNamed:@"btnSetting_Active.png"] forState:UIControlStateHighlighted];
        [btnSetting addTarget:self action:@selector(btnSettingTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (btnBack != nil)
    {
        [btnBack setBackgroundImage:[UIImage imageNamed:@"btnBack.png"] forState:UIControlStateNormal];
        [btnBack setBackgroundImage:[UIImage imageNamed:@"btnBack_Active.png"] forState:UIControlStateHighlighted];
        [btnBack addTarget:self action:@selector(btnBackTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (btnRefresh != nil)
    {
        [btnRefresh setBackgroundImage:[UIImage imageNamed:@"btnRefresh.png"] forState:UIControlStateNormal];
        [btnRefresh setBackgroundImage:[UIImage imageNamed:@"btnRefresh_Active.png"] forState:UIControlStateHighlighted];
        [btnRefresh addTarget:self action:@selector(btnRefreshTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (btnAbout != nil)
    {
        [btnAbout setBackgroundImage:[UIImage imageNamed:@"btnAbout.png"] forState:UIControlStateNormal];
        [btnAbout setBackgroundImage:[UIImage imageNamed:@"btnAbout_Active.png"] forState:UIControlStateHighlighted];
        [btnAbout addTarget:self action:@selector(btnAboutTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
}

// Add this method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![self isKindOfClass: [SplashViewController class]]
        && ![self isKindOfClass:[GameFeedViewController class]])
    {
        [[UIDevice currentDevice] performSelector:NSSelectorFromString(@"setOrientation:") withObject:(id)UIInterfaceOrientationPortrait];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    // Return YES for supported orientations
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        [[SharedComponent getInstance] setInterfaceOrientation:toInterfaceOrientation];
        return YES;
    }
    return NO;
}

#pragma implement Keyboard action
- (IBAction) textFieldBeginEditing:(id)sender{
    [self slideFrameWhenFocus:sender];
}

- (IBAction) textFieldEndEditting:(id)sender{
    [self slideFrameWhenUnFocus:sender];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    //[self slideFrameWhenFocus:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
    //[self slideFrameWhenUnFocus:textField];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];   
    
}

// Called when the UIKeyboardWillShowNotification is sent.
- (void)keyboardWillShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGFloat height = activeField.inputAccessoryView.frame.size.height;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + height, 0.0);
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    //CGRect aRect = self.mainScrollView.frame;
    //aRect.origin.y = 0;
    //aRect.size.height -= (kbSize.height + height);
    // always scroll to view (calculating is not correct)
    //if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.mainScrollView scrollRectToVisible:activeField.frame animated:YES];
    //}
}


// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
}

- (void) slideFrameWhenFocus:(id)field{
    
    if (![field isKindOfClass:[UIView class]]) return;
    
    if (mainScrollView == nil) return;
    
    BOOL isShowingKeyboard = NO;
    
    if (focusedField != nil)
    {
        isShowingKeyboard = YES;
    }
    
    focusedField = (UIView*)field;
    
    int keyboardHeight = 0;
    
    if ([CommonFunction checkPortraitOrientation])
    {
        keyboardHeight = PortraitKeyboardHeight;
    }
    else if ([CommonFunction checkLandscapeOrientation])
    {
        keyboardHeight = LandscapeKeyboardHeight;
    }
    
    CGRect mainScrollFrame = mainScrollView.frame;
    CGRect focusedFieldFrame = focusedField.frame;
    int topPosition = focusedFieldFrame.origin.y + mainScrollFrame.origin.y - mainScrollView.contentOffset.y;
    
    if (!isShowingKeyboard)
    {
        //first: resize mainScroll   
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration:  0.3f];
        [mainScrollView setFrame:CGRectMake(mainScrollFrame.origin.x, mainScrollFrame.origin.y, mainScrollFrame.size.width, mainScrollFrame.size.height - keyboardHeight)];
        [UIView commitAnimations];
    }
    
    //second: adjust contentOffset if field is overlapped
    //check field is overlapped?
    if (topPosition + focusedFieldFrame.size.height > self.view.frame.size.height - keyboardHeight)
    {
        //overlapped
        int movement = topPosition + focusedFieldFrame.size.height - (self.view.frame.size.height - keyboardHeight) + 10;
        [mainScrollView setContentOffset:CGPointMake(mainScrollView.contentOffset.x, mainScrollView.contentOffset.y + movement) animated:YES];
    }
    
}

- (void) slideFrameWhenUnFocus:(id)field{
    focusedField = nil;
    
    int keyboardHeight = 0;
    
    if ([CommonFunction checkPortraitOrientation])
    {
        keyboardHeight = PortraitKeyboardHeight;
    }
    else if ([CommonFunction checkLandscapeOrientation])
    {
        keyboardHeight = LandscapeKeyboardHeight;
    }
    
    CGRect mainScrollFrame = mainScrollView.frame;
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:  0.3f];
    [mainScrollView setFrame:CGRectMake(mainScrollFrame.origin.x, mainScrollFrame.origin.y, mainScrollFrame.size.width, mainScrollFrame.size.height + keyboardHeight)];
    [UIView commitAnimations];
}

- (void) slideFrame:(int) movement up:(BOOL)up
{
    movement = (up? -movement: movement);
    
    [UIView beginAnimations: nil context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration:  0.3f];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)handleServiceError:(id)result error:(NSError*)error; {
    if (error != nil) {
        // this is a network error.
        NSString* msg = [error.userInfo objectForKey:@"message"];
        showAlert(@"", msg, nil);
        return TRUE;
    } else {
        return [self handleServiceResponseError:result];
    }
    return FALSE;
}

- (BOOL)handleServiceResponseError:(BaseResponse*)response {
    if (response == nil || response.isSuccess || isEmpty(response.errorMessage))
        return FALSE;
    
    if ([response.errorCode isEqualToString:ERROR_DUPLICATED_GAME])
        return FALSE;
    
    if ([response.errorMessage isEqualToString:INVALID_LOGIN_TOKEN]) {
        [CommonFunction saveData:@"" withKey:kLoginToken];
        [CommonFunction saveData:@"" withKey:kUser];
        [CommonFunction saveData:@"" withKey:kCanCreateGame];
        
        BOOL isPopToLogin = NO;
        
        UINavigationController *nav = [[SharedComponent getInstance] navigationController];
        for (UIViewController *viewController in [nav viewControllers])
        {
            if ([viewController isKindOfClass:[SignInViewController class]])
            {
                isPopToLogin = YES;
                [nav popToViewController:viewController animated:YES];
                break;
            }
        }
        
        if (!isPopToLogin){
            [nav popToRootViewControllerAnimated:NO];
            SignInViewController *viewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
            [[[SharedComponent getInstance] navigationController] pushViewController:viewController animated:YES];
            [viewController release];
        }
    }
    else {
        // show server message
        showAlert(@"", response.errorMessage, nil);
    }
    return TRUE;
}


#pragma  implement communication with service
- (void) showWaiting:(NSString*)msg
{
    if ([self isKindOfClass:[SplashViewController class]]) return;
    
    if (msg == nil || [msg isEqualToString:@""])
        msg = @"Processing";
    
    if (!isWaiting)
    {
        isWaiting = YES;
        waitingView = [[UIView alloc] init];
        [waitingView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5]];
        
        UILabel *label = [[UILabel alloc] init];
        [label setText:msg];
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] init];
        
        if ([CommonFunction checkPortraitOrientation])
        {
            [waitingView setFrame:CGRectMake(0, 0, 320, 480)];
            [activity setFrame:CGRectMake(145, 225, 30, 30)];
            
            [label setFrame:CGRectMake(0, 200, 320, 21)];
        }
        else if ([CommonFunction checkLandscapeOrientation])
        {
            [waitingView setFrame:CGRectMake(0, 0, 480, 320)];
            [activity setFrame:CGRectMake(225, 155, 30, 30)];
            
            [label setFrame:CGRectMake(0, 130, 320, 21)];
        }
        
        [activity startAnimating];
        [waitingView addSubview:activity];
        [waitingView addSubview:label];
        [self.view addSubview:waitingView];
    }
}

- (void) removeWaiting
{
    if ([self isKindOfClass:[SplashViewController class]]) return;
    
    if (isWaiting)
    {
        isWaiting = NO;
        [waitingView removeFromSuperview];
        [waitingView release];
    }
}

- (void) sendRequest
{
    if (sbnRequest == nil) return;
    
    [self showWaiting:nil];
    [sbnRequest.baseRequest setDelegate:self];
    [sbnRequest.baseRequest startAsynchronous];
}

- (void) sendGetScoreRequest
{
    if (sbnGetScoreRequest == nil) return;
    
    [sbnGetScoreRequest.baseRequest setDelegate:self];
    [sbnGetScoreRequest.baseRequest startAsynchronous];
}

- (void) request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data
{   
    if ([sbnRequest baseRequest] == request)
        [sbnRequest.receivedData appendData:data];
    else if ([sbnGetScoreRequest baseRequest] == request)
        [sbnGetScoreRequest.receivedData appendData:data];
    else if ([sbnSynchTeamRequest baseRequest] == request)
        [sbnSynchTeamRequest.receivedData appendData:data];
    else if ([sbnSynchGameRequest baseRequest] == request)
        [sbnSynchGameRequest.receivedData appendData:data];
    else if ([sbnGetNewGameRequest baseRequest] == request)
        [sbnGetNewGameRequest.receivedData appendData:data];
    else if ([sbnSubscribeNewGameRequest baseRequest] == request)
        [sbnSubscribeNewGameRequest.receivedData appendData:data];
}

- (void) requestFinished:(ASIHTTPRequest *)request
{   
    if ([sbnRequest baseRequest] == request)
    {
        NSLog(@"sbnRequest finish %@", [[NSString alloc] initWithData: sbnRequest.receivedData encoding:NSUTF8StringEncoding]);

        [self removeWaiting];
        
        NSString *responseData = @"";
        if ([sbnRequest.receivedData length] > 0) {
            responseData = [[NSString alloc] initWithData: sbnRequest.receivedData encoding:NSUTF8StringEncoding];
        }
        
        [sbnRequest release];
        sbnRequest = nil;
        
        [self handleResponse:responseData];
    }
    else if ([sbnGetScoreRequest baseRequest] == request)
    {
        NSLog(@"sbnGetScoreRequest finish %@", [[NSString alloc] initWithData: sbnGetScoreRequest.receivedData encoding:NSUTF8StringEncoding]);
        [self handleGeScoreResponse];
    }
    else if ([sbnSynchTeamRequest baseRequest] == request) {
        [self handleSynchTeamResponse];
    }
    else if ([sbnSynchGameRequest baseRequest] == request) {
        [self handleSynchGameResponse];
    }
    else if ([sbnGetNewGameRequest baseRequest] == request) {
        [self handleGetNewGamesResponse];
    }
    else if ([sbnSubscribeNewGameRequest baseRequest] == request) {
        [self handleSubscribeNewGamesResponse];
    }
}

- (void) requestFailed:(ASIHTTPRequest *)request{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Network has problem!!!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    [alert release];
    
    [self removeWaiting]; // hide waiting message if any
    [self handleFailRequest];
    
    if (sbnRequest != nil)
    {
        [sbnRequest release];
        sbnRequest = nil;        
    }    
    if (sbnGetScoreRequest != nil)
    {
        [sbnGetScoreRequest release];
        sbnGetScoreRequest = nil;        
    }    
    if (sbnSynchTeamRequest != nil)
    {
        [sbnSynchTeamRequest release];
        sbnSynchTeamRequest = nil;        
    }
    if (sbnSynchGameRequest != nil)
    {
        [sbnSynchGameRequest release];
        sbnSynchGameRequest = nil;        
    }
    if (sbnGetNewGameRequest != nil)
    {
        [sbnGetNewGameRequest release];
        sbnGetNewGameRequest = nil;
        
        [SharedComponent getInstance].isCheckingNewGame = NO;
    }
}


- (void) sendAddTeams:(NSArray*)listTeamId {
    sbnRequest = [[SBNRequest alloc] init];
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    [sbnRequest buildSubscribeTeamBody:loginToken listTeamId:listTeamId];
    
    [self showWaiting:nil];
    [sbnRequest.baseRequest setDelegate:self];
    [sbnRequest.baseRequest startAsynchronous];
}
- (void) sendRemoveTeams:(NSArray*)listTeamId {
    sbnRequest = [[SBNRequest alloc] init];
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    [sbnRequest buildUnSubscribeTeamBody:loginToken listTeamId:listTeamId];
    
    [self showWaiting:nil];
    [sbnRequest.baseRequest setDelegate:self];
    [sbnRequest.baseRequest startAsynchronous];
}

- (void) sendAddGames:(NSArray*)listGameId {
    sbnRequest = [[SBNRequest alloc] init];
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    [sbnRequest buildSubscribeGameBody:loginToken listGameId:listGameId];
    
    [self showWaiting:nil];
    [sbnRequest.baseRequest setDelegate:self];
    [sbnRequest.baseRequest startAsynchronous];
}
- (void) sendRemoveGames:(NSArray*)listGameId {
    sbnRequest = [[SBNRequest alloc] init];
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    [sbnRequest buildUnSubscribeGameBody:loginToken listGameId:listGameId];
    
    [self showWaiting:nil];
    [sbnRequest.baseRequest setDelegate:self];
    [sbnRequest.baseRequest startAsynchronous];
}

- (void) handleResponse:(NSString*)responseData {
    
}

- (void) handleGeScoreResponse {
    return;
}

- (void) handleFailRequest {
    return;
}

- (void) newGamesDownloaded:(int)count {
    
}

/*- (void) synchTeam:(BOOL)showWaiting {
    if (isSynchingTeams)
        return;
    
    totalTeams = 0;
    pageIndexTeam = 0;
    isSynchingTeams = true;
    
    if (showWaiting)
        [self showWaiting:@"Synchronizing"];
    
    [self downloadTeam:pageIndexTeam];
}*/

- (void) synchGame:(BOOL)showWaiting {
    if (isSynchingGames)
        return;
    
    totalGames = 0;
    pageIndexGame = 0;
    isSynchingGames = true;
    
    if (showWaiting)
        [self showWaiting:@"Synchronizing"];
    
    [self downloadGame:pageIndexGame];
}

- (void) synchNewGames:(BOOL)showWaiting {
    if ([SharedComponent getInstance].isCheckingNewGame)
        return;
    
    [SharedComponent getInstance].isCheckingNewGame = YES;
    totalNewGames = 0;
    
    if (showWaiting)
        [self showWaiting:@"Checking New Games"];
    
    [self downloadNewGames:0];
}

/*- (void) downloadTeam:(int)page {
    if (sbnSynchTeamRequest == nil)
        sbnSynchTeamRequest = [[SBNRequest alloc] init];
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    [sbnSynchTeamRequest buildGetSubscribedTeamsBody:loginToken pageIndex:page];
    
    [sbnSynchTeamRequest.baseRequest setDelegate:self];
    [sbnSynchTeamRequest.baseRequest startAsynchronous];
}*/

- (void) downloadGame:(int)page {
    if (sbnSynchGameRequest == nil)
        sbnSynchGameRequest = [[SBNRequest alloc] init];
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    [sbnSynchGameRequest buildGetSubscribedGamesBody:loginToken pageIndex:page];
    
    [sbnSynchGameRequest.baseRequest setDelegate:self];
    [sbnSynchGameRequest.baseRequest startAsynchronous];
}

- (void) downloadNewGames:(int)page {
    if (sbnGetNewGameRequest == nil)
        sbnGetNewGameRequest = [[SBNRequest alloc] init];
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    NSArray *listTeamID = [Team loadTeamIds];
    [sbnGetNewGameRequest buildGetNewGamesBody:loginToken listTeamId:listTeamID pageIndex:page];
    
    [sbnGetNewGameRequest.baseRequest setDelegate:self];
    [sbnGetNewGameRequest.baseRequest startAsynchronous];
}

- (void) handleSynchTeamResponse {
    if ([sbnSynchTeamRequest.receivedData length] <= 0)
        return;
    
    NSString *responseData = [[NSString alloc] initWithData: sbnSynchTeamRequest.receivedData encoding:NSUTF8StringEncoding];
    
    [sbnSynchTeamRequest release];
    sbnSynchTeamRequest = nil;
    
    SBNGetSubscribedTeamsResponse *response = [[SBNGetSubscribedTeamsResponse alloc] init];
    [response parseResponseData:responseData];
    
    totalTeams += response.listTeam.count;
    for (Team *t in response.listTeam) {
        t.user = [CommonFunction getDataWithKey:kUser];
        [t insertDatabase];
    }
    
    if (totalTeams < response.totalRecord)
    {
        // download next page
        pageIndexTeam += 1;
        [self downloadTeam:pageIndexTeam];
    }
    else {
        [self removeWaiting];
        isSynchingTeams = false;
    }
    
    [response release];
    response = nil;
}

- (void) handleSynchGameResponse {
    if ([sbnSynchGameRequest.receivedData length] <= 0) return;
    
    NSString *responseData = [[NSString alloc] initWithData: sbnSynchGameRequest.receivedData encoding:NSUTF8StringEncoding];
    
    [sbnSynchGameRequest release];
    sbnSynchGameRequest = nil;
    
    SBNGetSubscribedGamesResponse *response = [[SBNGetSubscribedGamesResponse alloc] init];
    [response parseResponseData:responseData];
    
    totalGames += response.listGame.count;
    for (Game *g in response.listGame) {
        if (![Team hasTeam:g.team1ID] && ![Team hasTeam:g.team2ID])
            g.addedManual = TRUE;
        
        [g insertDatabase];
    }
    
    if (totalGames < response.totalRecord) {
        // download next page
        pageIndexGame += 1;
        [self downloadGame:pageIndexGame];
    }
    else {
        [self removeWaiting];
        isSynchingGames = false;
    }
    
    [response release];
    response = nil;
}

- (void) handleGetNewGamesResponse {
    if ([sbnGetNewGameRequest.receivedData length] <= 0) return;
    NSString *responseData = [[NSString alloc] initWithData: sbnGetNewGameRequest.receivedData encoding:NSUTF8StringEncoding];
    
    [sbnGetNewGameRequest release];
    sbnGetNewGameRequest = nil;
    
    SBNGetNewGamesResponse *response = [[SBNGetNewGamesResponse alloc] init];
    [response parseResponseData:responseData];
    
    NSMutableArray *listNewGameId = [[NSMutableArray alloc] init];
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]) {
        hasMoreGame = response.listGame.count < response.totalRecord;
        totalNewGames += response.listGame.count;
        
        for (Game *g in response.listGame)
        {
            if (![Game hasGame:g.gameID])
                [g insertDatabase];
            
            // always subscribe back to server, otherwise a loop will occur
            [listNewGameId addObject:g.gameID];        
        }
    }
    else
        hasMoreGame = false;
    
    [response release];
    response = nil;
    
    // subscribe new games
    if (listNewGameId.count > 0) {
        if (sbnSubscribeNewGameRequest == nil)
            sbnSubscribeNewGameRequest = [[SBNRequest alloc] init];
        
        NSString *loginToken = [[SharedComponent getInstance] loginToken];
        [sbnSubscribeNewGameRequest buildSubscribeGameBody:loginToken listGameId:listNewGameId];
        [sbnSubscribeNewGameRequest.baseRequest setDelegate:self];
        [sbnSubscribeNewGameRequest.baseRequest startAsynchronous];
    }      
    else {
        [self removeWaiting];
        [SharedComponent getInstance].isCheckingNewGame = NO;
        
        // inform view to refresh
        [self newGamesDownloaded:totalNewGames];
    }
}

- (void) handleSubscribeNewGamesResponse {
    if ([sbnSubscribeNewGameRequest.receivedData length] <= 0)
        return;
    
    NSString *responseData = [[NSString alloc] initWithData: sbnSubscribeNewGameRequest.receivedData encoding:NSUTF8StringEncoding];
    
    [sbnSubscribeNewGameRequest release];
    sbnSubscribeNewGameRequest = nil;

    SBNSubscribeGameResponse *response = [[SBNSubscribeGameResponse alloc] init];
    [response parseResponseData:responseData];
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL])
    {
        if (hasMoreGame)
            [self downloadNewGames:0];
        else {
            [self removeWaiting];
            [SharedComponent getInstance].isCheckingNewGame = NO;
            
            // inform view to refresh
            [self newGamesDownloaded:totalNewGames];
        }
    }
    
    [response release];
    response = nil;
}

- (IBAction) btnBackTapped:(id)sender{
    [[[SharedComponent getInstance] navigationController] popViewControllerAnimated:YES];
}

- (IBAction)btnHomeTapped:(id)sender {
    UINavigationController *nav = [[SharedComponent getInstance] navigationController];
    for (UIViewController *viewController in [nav viewControllers])
    {
        if ([viewController isKindOfClass:[MyScoreBoardViewController class]])
        {
            [nav popToViewController:viewController animated:YES];
            break;
        }
    }
}

- (IBAction)btnSettingTapped:(id)sender{
    SetupViewController *viewController = [[SetupViewController alloc] initWithNibName:@"SetupViewController" bundle:nil];
    [[[SharedComponent getInstance] navigationController] pushViewController:viewController animated:YES];
    [viewController release];
}

- (IBAction)btnRefreshTapped:(id)sender{
    return;
}

- (IBAction)btnAboutTapped:(id)sender{
    AboutViewController *viewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    [[[SharedComponent getInstance] navigationController] pushViewController:viewController animated:YES];
    [viewController release];
}

- (void) dealloc{
    NSLog(@"%@ dealloc", [self class]);
    if (btnHome != nil)
        [btnHome release];
    if (btnSetting != nil)
        [btnSetting release];
    if (mainScrollView != nil)
        [mainScrollView release];
    if (btnBack != nil)
        [btnBack release];
    if (btnAbout != nil)
        [btnAbout release];
    
    [super dealloc];
}
@end
