
#import "RecoveryPasswordViewController.h"

@implementation RecoveryPasswordViewController
@synthesize emailTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setEmailTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [emailTextField release];
    [super dealloc];
}
- (IBAction)btnResetPasswordTapped:(id)sender {
    if (![CommonFunction validateEmail:emailTextField.text]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email is invalid" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        return;
    }
    
    [emailTextField resignFirstResponder];
    NSString *email = emailTextField.text;
    
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildForgotPasswordBody:email];
    [self sendRequest];
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNForgotPasswordResponse *response = [[SBNForgotPasswordResponse alloc] init];
    [response parseResponseData:responseData];
    
    if ([response.responseCode isEqualToString:@""]) return;
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"New password is sent to your email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    else
    {
        NSString *errorMessage = @"Error";
        if (![response.errorMessage isEqualToString:@""])
        {
            errorMessage = response.errorMessage;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
    [responseData release];
    [response release];
}
@end
