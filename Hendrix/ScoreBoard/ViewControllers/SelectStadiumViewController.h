//
//  SelectStadiumViewController.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "BaseViewController.h"

@interface SelectStadiumViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>

@property (assign) id<SelectItemViewControllerDelegate> delegate;
@property (assign) id sourceView;
@property (assign) NSString* searchText;
@property (retain, nonatomic) IBOutlet UITextField *txtSearchText;
@property (retain, nonatomic) IBOutlet UIButton *btnSearch;
- (IBAction)btnSearchClick:(id)sender;

@property (nonatomic, retain) NSMutableArray* stadiums;
// use to show & enable load more
@property int totalRecords;

@property (retain, nonatomic) IBOutlet UITableView *tableStadiums;
- (IBAction)btnBackClick:(id)sender;

@end
