typedef enum ManageGameStep{
    GetDivision = 0,
    GetTeam,
    GetGame,
    GetGameScore,
    GetLeague,
    SubscribeGame,
    SubscribeTeam
} ManageGameStep;

#import "ManageGameViewController.h"
#import "CheckboxCell.h"

@implementation ManageGameViewController {
    NSDateFormatter *dateFormatter;
}

//@synthesize delegate;
@synthesize btnAddTeam;
@synthesize tableGame;
@synthesize btnAddGame;
@synthesize seasonDropDownList;
@synthesize sportDropDownList;
//@synthesize leagueDropDownList;
//@synthesize divisionDropDownList;
@synthesize teamDropDownList;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super.mainScrollView setContentSize:CGSizeMake(320, 568)];
    
    // Do any additional setup after loading the view from its nib.
    [seasonDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];
    [sportDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];
    //[leagueDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];
    //[divisionDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];
    [teamDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnAddTeam addTarget:self action:@selector(btnAddTeamTapped:) forControlEvents:UIControlEventTouchUpInside];
    [btnAddGame addTarget:self action:@selector(btnAddGameTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    selectedSport = nil;
    selectedLeague = nil;
    selectedDivision = nil;
    selectedTeam = nil;
    listSeason = [[NSMutableArray alloc] init];
    
    
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    
    for (int i=currentYear-1; i <= currentYear + 1; i++) {
        //[listSeason addObject:[NSString stringWithFormat:@"Year %d - %d", i, i+1]];
        [listSeason addObject:[NSString stringWithFormat:@"Year %d", i]];
    }
    
    selectedSeason = [listSeason objectAtIndex:1];
    [seasonDropDownList setTitle:selectedSeason forState:UIControlStateNormal];
    
    listLeague = nil;
    listDivision = nil;
    listTeam = nil;
    listGame = [[NSMutableArray alloc] init];

    listSelectedGameId = [[NSMutableArray alloc] init];
    
    [tableGame setDelegate:self];
    [tableGame setDataSource:self];
    
    [tableGame setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if ([self.tableGame respondsToSelector:@selector(registerNib:forCellReuseIdentifier:)]) {
        [tableGame registerNib:[UINib nibWithNibName:@"CheckboxCell" bundle:nil] forCellReuseIdentifier:@"GameCell"];
    }

    UIImage* img = [[UIImage imageNamed:@"BackgroundTextView.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    self.gamesContainerBox.image = img;
}

- (void) showDropDownList:(id)sender {
    if (sender == seasonDropDownList) {
        DropDownList *dropDownList = [[DropDownList alloc] init];
        dropDownList.title = @"Select Season";
        [dropDownList setSource:listSeason];
        [dropDownList setDelegate:self];
        [dropDownList setOwner:seasonDropDownList];
        
        [[[SharedComponent getInstance] navigationController] pushViewController:dropDownList animated:YES];
        [dropDownList release];
    }
    else if (sender == sportDropDownList) {
        DropDownList *dropDownList = [[DropDownList alloc] init];
        dropDownList.title = @"Select Sport";
        
        NSMutableArray *listItem = [[NSMutableArray alloc] init];
        NSArray *listSport = [[SharedComponent getInstance] listSport];
        
        for (Sport *sport in listSport)
        {
            [listItem addObject: sport.sportName];
        }
        
        [dropDownList setSource:listItem];
        [dropDownList setDelegate:self];
        [dropDownList setOwner:sportDropDownList];
        
        [listItem release];
        
        [[[SharedComponent getInstance] navigationController] pushViewController:dropDownList animated:YES];
        [dropDownList release];
    }
    /*else if (sender == leagueDropDownList) {
        DropDownList *dropDownList = [[DropDownList alloc] init];
        NSMutableArray *listItem = [[NSMutableArray alloc] init];
        
        for (League *league in listLeague)
        {
            [listItem addObject: league.leagueName];
        }
        
        [dropDownList setSource:listItem];
        [dropDownList setDelegate:self];
        [dropDownList setOwner:leagueDropDownList];
        
        [listItem release];
        
        [[[SharedComponent getInstance] navigationController] pushViewController:dropDownList animated:YES];
        [dropDownList release];
    }
    else if (sender == divisionDropDownList){
        DropDownList *dropDownList = [[DropDownList alloc] init];
        NSMutableArray *listItem = [[NSMutableArray alloc] init];
        
        for (Division *division in listDivision)
        {
            [listItem addObject: division.divisionName];
        }
        
        [dropDownList setSource:listItem];
        [dropDownList setDelegate:self];
        [dropDownList setOwner:divisionDropDownList];
        
        [listItem release];
        
        [[[SharedComponent getInstance] navigationController] pushViewController:dropDownList animated:YES];
        [dropDownList release];
    }*/
    else if (sender == teamDropDownList){
        DropDownList *dropDownList = [[DropDownList alloc] init];
        dropDownList.title = @"Select Team";
        NSMutableArray *listItem = [[NSMutableArray alloc] init];
        
        for (Team *team in listTeam)
        {
            [listItem addObject: team.teamName];
        }
        
        [dropDownList setSource:listItem];
        [dropDownList setDelegate:self];
        [dropDownList setOwner:teamDropDownList];
        
        [listItem release];
        
        [[[SharedComponent getInstance] navigationController] pushViewController:dropDownList animated:YES];
        [dropDownList release];
    }
}

- (void) dropDownList:(id)owner didSelectedRowAtIndex:(int)index
{
    [btnAddTeam setEnabled:YES];
    
    if (owner == seasonDropDownList){
        selectedSeason = [listSeason objectAtIndex:index];
        [seasonDropDownList setTitle:selectedSeason forState:UIControlStateNormal];
    }
    else if (owner == sportDropDownList){
        NSArray *listSport = [[SharedComponent getInstance] listSport];
        selectedSport = [listSport objectAtIndex:index];
        
        [sportDropDownList setTitle:selectedSport.sportName forState:UIControlStateNormal];
        
        //currentStep = GetLeague;
        currentStep = GetTeam;
        
        NSString *loginToken = [[SharedComponent getInstance] loginToken];
        if (sbnRequest == nil){
            sbnRequest = [[SBNRequest alloc] init];
        }
        
        //[sbnRequest buildLeagueBody:loginToken sportId:selectedSport.sportId];
        [sbnRequest buildSearchTeamBody:loginToken sportId:selectedSport.sportId leagueId:@"0" divisionId:@"0"];
        [self sendRequest];

    }
    /*else if (owner == leagueDropDownList){
        selectedLeague = [listLeague objectAtIndex:index];
        
        [leagueDropDownList setTitle:selectedLeague.leagueName forState:UIControlStateNormal];
        
        currentStep = GetDivision;
        
        NSString *loginToken = [[SharedComponent getInstance] loginToken];
        if (sbnRequest == nil){
            sbnRequest = [[SBNRequest alloc] init];
        }
        
        [sbnRequest buildDivisionBody:loginToken leagueId:selectedLeague.leagueId];
        [self sendRequest];
    }
    else if (owner == divisionDropDownList){
        selectedDivision = [listDivision objectAtIndex:index];
        [divisionDropDownList setTitle:selectedDivision.divisionName forState:UIControlStateNormal];
        
        if (selectedLeague != nil && selectedDivision != nil) {
            currentStep = GetTeam;
            
            NSString *loginToken = [[SharedComponent getInstance] loginToken];
            if (sbnRequest == nil){
                sbnRequest = [[SBNRequest alloc] init];
            }
        
            [sbnRequest buildSearchTeamBody:loginToken sportId:selectedSport.sportId leagueId:selectedLeague.leagueId divisionId:selectedDivision.divisionID];
            [self sendRequest];
        }
    }*/
    else if (owner == teamDropDownList){
        selectedTeam = [listTeam objectAtIndex:index];
        
        if ([Team hasTeam:selectedTeam.teamID])
            [btnAddTeam setEnabled:NO];
        
        [teamDropDownList setTitle:selectedTeam.teamName forState:UIControlStateNormal];
        
        currentStep = GetGame;
        
        NSString *loginToken = [[SharedComponent getInstance] loginToken];
        if (sbnRequest == nil){
            sbnRequest = [[SBNRequest alloc] init];
        }
        
        NSInteger year = [self getSelectedYear];
        
        NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        formatter.timeStyle = NSDateFormatterNoStyle;
        formatter.dateFormat = @"MM/dd/yyyy";
        
        NSDate *fromDate = [formatter dateFromString:[NSString stringWithFormat:@"01/01/%d", year]];
        NSDate *toDate = [formatter dateFromString:[NSString stringWithFormat:@"12/31/%d", year]];
        
        [sbnRequest buildSearchGameBody:loginToken scheduledFromDate:fromDate scheduledToDate:toDate sportId:selectedSport.sportId teamId:selectedTeam.teamID stadiumId:selectedTeam.stadiumID];
        
        [self sendRequest];
    }
}

- (NSInteger) getSelectedYear {
    NSArray *words = [selectedSeason componentsSeparatedByString:@" "];; //[selectedSeason componentsSeparatedByString:@" - "];
    NSInteger year = ((NSString*)[words objectAtIndex:1]).intValue;
    return year;
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNResponse *response = nil;
    
    if (currentStep == GetLeague){
        response = [[SBNLeagueResponse alloc] init];
        [response parseResponseData:responseData];
    }
    else if (currentStep == GetDivision){
        response = [[SBNDivisionResponse alloc] init];
        [response parseResponseData:responseData];
    }
    else if (currentStep == GetTeam){
        response = [[SBNSearchTeamResponse alloc] init];
        [response parseResponseData:responseData];        
    }
    else if (currentStep == GetGame){
        response = [[SBNSearchGameResponse alloc] init];
        [response parseResponseData:responseData];                
    }
    else if (currentStep == GetGameScore){
        response = [[SBNGameScoreResponse alloc] init];
        [response parseResponseData:responseData];
    }
    else if (currentStep == SubscribeGame){
        response = [[SBNSubscribeGameResponse alloc] init];
        [response parseResponseData:responseData];
    }
    else if (currentStep == SubscribeTeam){
        response = [[SBNSubscribeTeamResponse alloc] init];
        [response parseResponseData:responseData];
    }
    
    if ([response.responseCode isEqualToString:@""]) return;
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL])
    {
        /*if (currentStep == GetLeague) {
            SBNLeagueResponse *leagueResponse = (SBNLeagueResponse*)response;
            
            [listLeague release];
            listLeague = [[leagueResponse listLeague] retain];
            
            selectedLeague = nil;
            selectedDivision = nil;
            selectedTeam = nil;
                    
            [leagueDropDownList setTitle:@"Select League" forState:UIControlStateNormal];
            [divisionDropDownList setTitle:@"Select Division" forState:UIControlStateNormal];
            [teamDropDownList setTitle:@"Select Team" forState:UIControlStateNormal];
            
            //currentStep = GetDivision;
            
            //NSString *loginToken = [[SharedComponent getInstance] loginToken];
            //if (sbnRequest == nil){
            //    sbnRequest = [[SBNRequest alloc] init];
            //}
            
            //[sbnRequest buildDivisionBody:loginToken leagueId:@"0"];
            //[self sendRequest];
        }
        else if (currentStep == GetDivision) {
            SBNDivisionResponse *divisionResponse = (SBNDivisionResponse*)response;
            
            [listDivision release];
            listDivision = [[divisionResponse listDivision] retain];
            
            selectedDivision = nil;
            selectedTeam = nil;
            
            [divisionDropDownList setTitle:@"Select Division" forState:UIControlStateNormal];
            [teamDropDownList setTitle:@"Select Team" forState:UIControlStateNormal];
            
            if (selectedLeague != nil && selectedDivision != nil) {
                currentStep = GetTeam;
            
                NSString *loginToken = [[SharedComponent getInstance] loginToken];
                if (sbnRequest == nil){
                    sbnRequest = [[SBNRequest alloc] init];
                }
            
                [sbnRequest buildSearchTeamBody:loginToken sportId:selectedSport.sportId leagueId:selectedLeague.leagueId divisionId:selectedDivision.divisionID];
                [self sendRequest];
            }
        }*/
        if (currentStep == GetTeam) {
            SBNSearchTeamResponse *searchTeamResponse = (SBNSearchTeamResponse*)response;
            
            [listTeam release];
            listTeam = [[searchTeamResponse listTeam] retain];
            
            selectedTeam = nil;
            [teamDropDownList setTitle:@"Select Team" forState:UIControlStateNormal];
        }
        else if (currentStep == GetGame)
        {
            SBNSearchGameResponse *searchGameResponse = (SBNSearchGameResponse*)response;
            
            [listGame removeAllObjects];
            for (Game* g in searchGameResponse.listGame) {
                if (![Game hasGame:g.gameID])
                    [listGame addObject:g];
            }
            
            //reload table game
            [tableGame reloadData];  
        }
        else if (currentStep == SubscribeTeam)
        {
            NSLog(@"SubscribeTeam succeeded.");
            [selectedTeam insertDatabase];
            
            // add all games of team
            [listSelectedGameId removeAllObjects];
            for (Game* g in listGame) {
                [listSelectedGameId addObject:g.gameID];
                g.addedManual = FALSE;
            }
            
            currentStep = SubscribeGame;
            [self sendAddGames:listSelectedGameId];
        }
        else if (currentStep == SubscribeGame)
        {
            NSLog(@"SubscribeGame succeeded.");
            
            // add selected games to database
            for (Game* g in listGame) {
                if ([listSelectedGameId containsObject:g.gameID])
                    [g insertDatabase];
            }
            
            // get latest score
            currentStep = GetGameScore;
            NSString *loginToken = [[SharedComponent getInstance] loginToken];
            
            if (sbnRequest == nil){
                sbnRequest = [[SBNRequest alloc] init];
            }
            
            [sbnRequest buildGetScoreBody:loginToken listGameId:listSelectedGameId];
            [self sendRequest];
        }
        else if (currentStep == GetGameScore)
        {
            SBNGameScoreResponse *gamScoreResponse = (SBNGameScoreResponse*)response;
            NSMutableArray *listGameScore = [gamScoreResponse listGameScore];
            
            for (GameScore *gameScore in listGameScore)
            {
                NSUInteger i = [listGame indexOfObjectPassingTest:^(id obj, NSUInteger idx, BOOL *stop) {
                    Game *g = (Game *)obj;
                    if ([g.gameID isEqualToString:gameScore.gameId])
                    {
                        *stop = YES;
                        return YES;
                    }
                    return  NO;
                }];
                
                if (i != NSNotFound) {
                    Game* g = [listGame objectAtIndex:i];
                    g.team1Score = gameScore.team1Score;
                    g.team2Score = gameScore.team2Score;
                    g.qtr = gameScore.qtr;
                    g.lastUpdated = gameScore.lastUpdated;
                    g.hours = gameScore.hours;
                    g.minutes = gameScore.minutes;
                    g.seconds = gameScore.seconds;
                    g.numberCheckedInUser = gameScore.numberCheckedInUser;
                    
                    [g updateDatabase];
                }
            }
            
            //go to MyScoreBoard
            [[[SharedComponent getInstance] navigationController] popViewControllerAnimated:YES];
        }
    }
    else
    {
        NSString *errorMessage = @"Error";
        if (![response.errorMessage isEqualToString:@""])
        {
            errorMessage = response.errorMessage;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
    [responseData release];
    [response release];
}


//table viewß
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listGame count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"GameCell";
    
	CheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // If there is no cell to reuse, create a new one
    if(cell == nil) {
        //cell = [[CheckboxCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        cell = (CheckboxCell*)[[[UINib nibWithNibName:@"CheckboxCell" bundle:nil] instantiateWithOwner:self  options:nil] objectAtIndex:0];
        
        [cell.labelField setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [cell.labelField setMinimumFontSize:10];
        [cell.labelField setTextColor:[UIColor blackColor]];
        [cell.labelField setAdjustsFontSizeToFitWidth:YES];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // Configure the cell before it is displayed...
    Game* game = [listGame objectAtIndex:indexPath.row];
    
    NSString *gameTitle = [NSString stringWithFormat:@"%@ - %@", game.team1Name, game.team2Name];
    cell.labelField.text = gameTitle;
    
    NSString* detailText = [dateFormatter stringFromDate:game.scheduledStartTime];
    cell.detailField.text = detailText;
    cell.isChecked = game.isChecked;
    
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckboxCell *cell = (CheckboxCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    Game *game = [listGame objectAtIndex:indexPath.row];
    game.isChecked = cell.isChecked;
}
///////////

- (void) btnAddTeamTapped:(id)sender
{
    if (selectedTeam == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please select a team." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        return;
    }
    
    currentStep = SubscribeTeam;
    
    // add on server first
    NSMutableArray *listTeamIds = [[[NSMutableArray alloc] init] autorelease];
    [listTeamIds addObject:selectedTeam.teamID];
    
    [self sendAddTeams:listTeamIds];
}

- (void) btnAddGameTapped:(id)sender
{
    [listSelectedGameId removeAllObjects];
    for (Game* g in listGame) {
        if (g.isChecked) {
            [listSelectedGameId addObject:g.gameID];
            g.addedManual = TRUE;
        }
    }
    
    if (listSelectedGameId.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please select a game." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        return;
    }
    
    currentStep = SubscribeGame;
    
    [self sendAddGames:listSelectedGameId];
}

- (void)viewDidUnload
{
    //[self setLeagueDropDownList:nil];
    //[self setDivisionDropDownList:nil];
    [self setTeamDropDownList:nil];
    [self setTableGame:nil];
    [self setBtnAddGame:nil];
    [self setBtnAddTeam:nil];
    [self setSportDropDownList:nil];
    [self setSeasonDropDownList:nil];
    [self setGamesContainerBox:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [sportDropDownList release];
    //[leagueDropDownList release];
    //[divisionDropDownList release];
    [teamDropDownList release];
    [tableGame release];
    [btnAddGame release];
    [btnAddTeam release];
    [seasonDropDownList release];
    [listSelectedGameId release];
    [_gamesContainerBox release];
    [super dealloc];
}

@end
