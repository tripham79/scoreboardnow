
#import "BaseViewController.h"

@interface RecoveryPasswordViewController : BaseViewController

@property (retain, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)btnResetPasswordTapped:(id)sender;
@end
