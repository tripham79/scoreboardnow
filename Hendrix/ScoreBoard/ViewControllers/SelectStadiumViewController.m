//
//  SelectStadiumViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SelectStadiumViewController.h"
#import "Stadium.h"
#import "SearchStadiumResponse.h"

@interface SelectStadiumViewController ()

@end

@implementation SelectStadiumViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableStadiums.delegate = self;
    self.tableStadiums.dataSource = self;
    
    self.txtSearchText.text = self.searchText;
    // search all by default
    [self doSearch:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [_tableStadiums release];
    [_txtSearchText release];
    [_btnSearch release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTableStadiums:nil];
    [self setTxtSearchText:nil];
    [self setBtnSearch:nil];
    [super viewDidUnload];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.stadiums count]) {
        NSString *cellIdentifier = @"UITableViewCell";
	
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
        if(cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
            [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.textLabel setMinimumFontSize:10];
            [cell.textLabel setTextColor:[UIColor blackColor]];
        }
    
        Stadium* obj = [self.stadiums objectAtIndex:indexPath.row];
        cell.textLabel.text = obj.Name;
    
        NSMutableString* detail = [[NSMutableString alloc] initWithString:obj.Address];
        if ([obj.City length] > 0) {
            [detail appendString:@", "];
            [detail appendString:obj.City];
        }
        cell.detailTextLabel.text = detail;
        return cell;
    }
    else {
        NSString *cellIdentifier = @"UITableViewCellLoadMore";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault    reuseIdentifier:cellIdentifier] autorelease];
            [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.textLabel setMinimumFontSize:10];
            [cell.textLabel setTextColor:[UIColor blackColor]];
            [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"Load More (%d/%d)", [self.stadiums count], self.totalRecords];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.stadiums count]) {
        if (self.delegate)
            [self.delegate selectItemViewController:self didSelectedRowAtIndex:indexPath.row];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        // load more
        [MBProgressHUD showHUDAddedTo:self.view text:@"Loading" animated:YES];
        int pageIndex = [self.stadiums count] / 15;
        
        [[SBNService sharedInstance] searchTeamsWithStadium:pageIndex pageSize:15 searchText:self.searchText withCallback:^(BaseRequest *request, id result, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            // if has error then stop
            if ([self handleServiceError:result error:error])
                return;
            
            SearchStadiumResponse* response = (id)result;
            [self.stadiums addObjectsFromArray:response.stadiums];
            self.totalRecords = response.totalRecords;
            
            [self.tableStadiums reloadData];
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.totalRecords > [self.stadiums count])
        return [self.stadiums count] + 1;
    else
        return [self.stadiums count];
}

- (IBAction)btnSearchClick:(id)sender {
    NSString* searchText = self.txtSearchText.text;
    [self doSearch:searchText];
}

- (void)doSearch:(NSString*)searchText {
    [MBProgressHUD showHUDAddedTo:self.view text:@"Searching" animated:YES];
    [[SBNService sharedInstance] searchStadiums:0 pageSize:15 searchText:searchText withCallback:^(BaseRequest *request, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        SearchStadiumResponse* response = (id)result;
        if ([response.stadiums count] == 0) {
            showAlert(@"No stadium found.", @"", nil);
            return;
        }
        
        self.stadiums = response.stadiums;
        self.totalRecords = response.totalRecords;
        
        [self.tableStadiums reloadData];
    }];
}

@end
