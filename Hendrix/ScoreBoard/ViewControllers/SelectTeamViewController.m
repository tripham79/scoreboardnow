//
//  SelectTeamViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "SelectTeamViewController.h"
#import "Team.h"
#import "SearchTeamResponse.h"

@interface SelectTeamViewController ()

@end

@implementation SelectTeamViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableTeams.delegate = self;
    self.tableTeams.dataSource = self;
    
    self.txtSearchText.text = self.searchText;
    // search all by default
    [self doSearch:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    [_tableTeams release];
    [_txtSearchText release];
    [_btnSearch release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTableTeams:nil];
    [self setTxtSearchText:nil];
    [self setBtnSearch:nil];
    [super viewDidUnload];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [self.teams count]) {
        NSString *cellIdentifier = @"UITableViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
        if(cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
            [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.textLabel setMinimumFontSize:10];
            [cell.textLabel setTextColor:[UIColor blackColor]];
        }
    
        Team* team = [self.teams objectAtIndex:indexPath.row];
        cell.textLabel.text = team.teamName;
    
        NSMutableString* detail = [[NSMutableString alloc] initWithString:team.sportName];
        if ([team.leagueName length] > 0) {
            [detail appendString:@"/"];
            [detail appendString:team.leagueName];
        }
        if ([team.divisionName length] > 0) {
            [detail appendString:@"/"];
            [detail appendString:team.divisionName];
        }
    
        cell.detailTextLabel.text = detail;
        return cell;
    }
    else {
        NSString *cellIdentifier = @"UITableViewCellLoadMore";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault    reuseIdentifier:cellIdentifier] autorelease];
            [cell.textLabel setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.textLabel setMinimumFontSize:10];
            [cell.textLabel setTextColor:[UIColor blackColor]];
            [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"Load More (%d/%d)", [self.teams count], self.totalRecords];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.teams count]) {
        Team* team = [self.teams objectAtIndex:indexPath.row];
        if (self.sportName.length > 0 && ![team.sportName isEqualToString:self.sportName]) {
            showAlert(@"", [NSString stringWithFormat:@"Teams must be of the same sport: %@", self.sportName], nil);
            return;
        }
        
        if (self.delegate)
            [self.delegate selectItemViewController:self didSelectedRowAtIndex:indexPath.row];
    
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        // load more
        [MBProgressHUD showHUDAddedTo:self.view text:@"Loading" animated:YES];
        int pageIndex = [self.teams count] / 15;
        
        [[SBNService sharedInstance] searchTeamsWithStadium:pageIndex pageSize:15 searchText:self.searchText sportId:0 withCallback:^(BaseRequest *request, id result, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            // if has error then stop
            if ([self handleServiceError:result error:error])
                return;
            
            SearchTeamResponse* response = (id)result;
            [self.teams addObjectsFromArray:response.teams];
            self.totalRecords = response.totalRecords;
            
            [self.tableTeams reloadData];
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.totalRecords > self.teams.count)
        return self.teams.count + 1;
    else
        return self.teams.count;
}

- (IBAction)btnSearchClick:(id)sender {
    NSString* searchText = self.txtSearchText.text;
    [self doSearch:searchText];
}

- (void)doSearch:(NSString*)searchText {
    [MBProgressHUD showHUDAddedTo:self.view text:@"Searching" animated:YES];
    
    [[SBNService sharedInstance] searchTeamsWithStadium:0 pageSize:15 searchText:searchText sportId:0 withCallback:^(BaseRequest *request, id result, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        SearchTeamResponse* response = (id)result;
        if ([response.teams count] == 0) {
            showAlert(@"No team found.", @"", nil);
            return;
        }
        
        self.teams = response.teams;
        self.totalRecords = response.totalRecords;
        
        [self.tableTeams reloadData];
        
    }];
}

@end
