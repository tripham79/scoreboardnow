//
//  GameListViewController.h
//  ScoreBoard
//
//  Created by Apple on 7/11/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GameListViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>
{
    
}
@property (retain, nonatomic) IBOutlet UITableView *tableTeams;
@property (retain, nonatomic) IBOutlet UITableView *tableGames;
@property (retain, nonatomic) IBOutlet UIButton *btnRemoveTeams;
@property (retain, nonatomic) IBOutlet UIButton *btnAddTeams;
@property (retain, nonatomic) IBOutlet UIButton *btnRemoveGames;
@property (retain, nonatomic) IBOutlet UIButton *btnAddGames;
@property (retain, nonatomic) IBOutlet UIButton *btnCheckNewGames;
@property (retain, nonatomic) IBOutlet UIButton *btnMyCreatedGames;

@property (retain, nonatomic) IBOutlet UIImageView *teamsContainerBox;
@property (retain, nonatomic) IBOutlet UIImageView *gamesContainerBox;

- (IBAction)btnRemoveTeamsTapped:(id)sender;
- (IBAction)btnAddTeamsTapped:(id)sender;
- (IBAction)btnRemoveGamesTapped:(id)sender;
- (IBAction)btnAddGamesTapped:(id)sender;
- (IBAction)btnCheckNewGamesTapped:(id)sender;
- (IBAction)btnMyCreatedGamesClick:(id)sender;

@end
