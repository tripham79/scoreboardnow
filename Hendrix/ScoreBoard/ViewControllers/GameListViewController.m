//
//  GameListViewController.m
//  ScoreBoard
//
//  Created by Apple on 7/11/13.
//  Copyright (c) 2013 YPVN. All rights reserved.
//

#import "GameListViewController.h"
#import "ManageGameViewController.h"
#import "MyCreatedGameViewController.h"
#import "CheckboxCell.h"

typedef enum GameListStep{
    RemoveTeams = 0,
    RemoveGames,
    CheckNewGames
} GameListStep;

@interface GameListViewController ()

@end

@implementation GameListViewController {
    NSMutableArray *listTeams;
    NSMutableArray *listGames;
    NSDateFormatter *dateFormatter;
    
    int currentStep;
    NSMutableArray *selectedIds;
}

//@synthesize tableGames, tableTeams;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [super.mainScrollView setContentSize:CGSizeMake(320, 568)];
    
    [self.tableTeams setDelegate:self];
    [self.tableTeams setDataSource:self];
    [self.tableGames setDelegate:self];
    [self.tableGames setDataSource:self];
    
    [self.tableTeams setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableGames setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIImage* img = [UIImage imageNamed:@"button_bg.png"];// [self.btnMyCreatedGames backgroundImageForState:UIControlStateNormal];
    img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    [self.btnMyCreatedGames setBackgroundImage:img forState:UIControlStateNormal];
    
    // iOS 5 and later
    if ([self.tableTeams respondsToSelector:@selector(registerNib:forCellReuseIdentifier:)]) {
        [self.tableTeams registerNib:[UINib nibWithNibName:@"CheckboxCell" bundle:nil] forCellReuseIdentifier:@"TeamCell"];
        [self.tableGames registerNib:[UINib nibWithNibName:@"CheckboxCell" bundle:nil] forCellReuseIdentifier:@"GameCell"];
    }
    
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    selectedIds = [[NSMutableArray alloc] init];
    
    if (![SharedComponent getInstance].canCreateGame)
        self.btnMyCreatedGames.hidden = true;
    
    UIImage* img2 = [[UIImage imageNamed:@"BackgroundTextView.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(8, 8, 8, 8)];
    self.teamsContainerBox.image = img2;
    self.gamesContainerBox.image = img2;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    listTeams = [Team loadTeams];
    listGames = [Game loadManuallyAddedGames];
    
    [_tableTeams reloadData];
    [_tableGames reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//table view
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableTeams)
        return listTeams.count;
    else
        return listGames.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableTeams) {
        // We are looking for cells with the Cell identifier
        // We should reuse unused cells if there are any
        static NSString *teamCellIdentifier = @"TeamCell";

        CheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier:teamCellIdentifier];
        
        // If there is no cell to reuse, create a new one
        if(cell == nil) {
            //cell = [[CheckboxCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:teamCellIdentifier];
            cell = (CheckboxCell*)[[[UINib nibWithNibName:@"CheckboxCell" bundle:nil] instantiateWithOwner:self  options:nil] objectAtIndex:0];
            
            [cell.labelField setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.labelField setMinimumFontSize:10];
            [cell.labelField setTextColor:[UIColor blackColor]];
            [cell.labelField setAdjustsFontSizeToFitWidth:YES];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        // Configure the cell before it is displayed...
        Team* team = [listTeams objectAtIndex:indexPath.row];
        int gamesCount = [team getGamesCount];
        
        cell.labelField.text = [NSString stringWithFormat:@"[%@] %@ (%d games)", team.teamNumber, team.teamName, gamesCount];
        
        if (team.sportName == nil)
            team.sportName = @"NA";
        if (team.leagueName == nil)
            team.leagueName = @"NA";
        if (team.divisionName == nil)
            team.divisionName = @"NA";
        
        cell.detailField.text = [NSString stringWithFormat:@"%@ / %@ / %@", team.sportName, team.leagueName, team.divisionName];
        cell.isChecked = team.isChecked;
        
        return cell;
    }
    else {
        static NSString *gameCellIdentifier = @"GameCell";
        
        CheckboxCell *cell = [tableView dequeueReusableCellWithIdentifier:gameCellIdentifier];
        
        // If there is no cell to reuse, create a new one
        if(cell == nil) {
            //cell = [[CheckboxCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:gameCellIdentifier];
            
            // works
            //cell = [[[NSBundle mainBundle] loadNibNamed:@"CheckboxCell" owner:self options:nil] objectAtIndex:0];
            
            // works too
            cell = (CheckboxCell*)[[[UINib nibWithNibName:@"CheckboxCell" bundle:nil] instantiateWithOwner:self  options:nil] objectAtIndex:0];
            
            [cell.labelField setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
            [cell.labelField setMinimumFontSize:10];
            [cell.labelField setTextColor:[UIColor blackColor]];
            [cell.labelField setAdjustsFontSizeToFitWidth:YES];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        // Configure the cell before it is displayed...
        Game* game = [listGames objectAtIndex:indexPath.row];
        NSString *gameTitle = [NSString stringWithFormat:@"[%@] %@ - %@", [dateFormatter stringFromDate:game.scheduledStartTime], game.team1Name, game.team2Name];
        cell.labelField.text = gameTitle;
        
        if (game.sportName == nil)
            game.sportName = @"NA";
        if (game.leagueName == nil)
            game.leagueName = @"NA";
        if (game.divisionName == nil)
            game.divisionName = @"NA";
        
        cell.detailField.text = [NSString stringWithFormat:@"%@ / %@ / %@", game.sportName, game.leagueName, game.divisionName];
        
        cell.isChecked = game.isChecked;
        
        return cell;
    }
    return  nil;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CheckboxCell *cell = (CheckboxCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if (tableView == self.tableTeams) {
        Team *team = [listTeams objectAtIndex:indexPath.row];
        team.isChecked = cell.isChecked;
    }
    else {
        Game *game = [listGames objectAtIndex:indexPath.row];
        game.isChecked = cell.isChecked;
    }
}

- (void)dealloc {
    [_tableTeams release];
    [_tableGames release];
    [_btnRemoveTeams release];
    [_btnAddTeams release];
    [_btnRemoveGames release];
    [_btnAddGames release];
    [_btnCheckNewGames release];
    [_btnMyCreatedGames release];
    [_teamsContainerBox release];
    [_gamesContainerBox release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTableTeams:nil];
    [self setTableGames:nil];
    [self setBtnRemoveTeams:nil];
    [self setBtnAddTeams:nil];
    [self setBtnRemoveGames:nil];
    [self setBtnAddGames:nil];
    [self setBtnCheckNewGames:nil];    
    [self setBtnMyCreatedGames:nil];
    [self setTeamsContainerBox:nil];
    [self setGamesContainerBox:nil];
    [super viewDidUnload];
}

- (IBAction)btnRemoveTeamsTapped:(id)sender {
    [selectedIds removeAllObjects];
    
    for (Team* t in listTeams) {
        if (t.isChecked)
            [selectedIds addObject:t.teamID];
    }
    
    if (selectedIds.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please select a team." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        return;
    }
    
    currentStep = RemoveTeams;
    [self sendRemoveTeams:selectedIds];
}

- (IBAction)btnRemoveGamesTapped:(id)sender {
    [selectedIds removeAllObjects];
    
    for (Game* g in listGames) {
        if (g.isChecked)
            [selectedIds addObject:g.gameID];
    }
    
    if (selectedIds.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please select a game." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        return;
    }
    
    currentStep = RemoveGames;
    [self sendRemoveGames:selectedIds];
}

- (IBAction)btnAddTeamsTapped:(id)sender {
    ManageGameViewController *viewController = [[ManageGameViewController alloc] initWithNibName:@"ManageGameViewController" bundle:nil];
   
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (IBAction)btnAddGamesTapped:(id)sender {
    ManageGameViewController *viewController = [[ManageGameViewController alloc] initWithNibName:@"ManageGameViewController" bundle:nil];
    //[viewController setDelegate:self];
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (IBAction)btnCheckNewGamesTapped:(id)sender {
    [self synchNewGames:TRUE];
}

- (IBAction)btnMyCreatedGamesClick:(id)sender {
    MyCreatedGameViewController *viewController = [[MyCreatedGameViewController alloc] initWithNibName:@"MyCreatedGameViewController" bundle:nil];
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
    [viewController release];
}

- (void) newGamesDownloaded:(int)count {
    NSString* msg = @"No new games available.";  //don't release msg, or BAD_ACCESS error will occurs
    if (count > 0) {
        msg = [NSString stringWithFormat:@"%d new games downloaded.", count];
        
        listTeams = [Team loadTeams];
        [_tableTeams reloadData];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    [alert release];
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNResponse *response = [[SBNResponse alloc] init];
    [response parseResponseData:responseData];
    
    // remove on server ok, remove local database
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL])
    {
        if (currentStep == RemoveTeams)
        {
            for (NSString* teamId in selectedIds)
                [Team deleteTeam:teamId];
            
            listTeams = [Team loadTeams];
            listGames = [Game loadManuallyAddedGames];
            
            [_tableTeams reloadData];
            [_tableGames reloadData];
        }
        if (currentStep == RemoveGames)
        {
            for (NSString* gameId in selectedIds)
                [Game deleteDatabase:gameId];
            
            listGames = [Game loadManuallyAddedGames];
            [_tableGames reloadData];
        }
    }
    
    [responseData release];
    [response release];
    response = nil;
}

@end
