
#import "UpdateScoreViewController.h"

@implementation UpdateScoreViewController
@synthesize fbCheckbox;
@synthesize twitterCheckbox;
@synthesize finalCheckbox;
@synthesize scoreBoard;//, selectedGameBox;
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[SharedComponent getInstance] setCurrentViewController:self];
    
    isEndGame = @"false";    
    //currentScore = [[NSMutableString alloc] init];
    
    [fbCheckbox setText:@"Facebook"];
    [twitterCheckbox setText:@"Twitter"];
    [twitterCheckbox setStatus:YES];
    [finalCheckbox setText:@"End Of Game / Final Score"];
    
    //score1 = nil;
    //score2 = nil;
    //scoreQtr = nil;
    
    //seconds = [[scoreBoard tempSeconds] retain];
    //minutes = [[scoreBoard tempMinutes] retain];
    
    //maxQtr = INT16_MAX;
    //maxQtrDurationSeconds = INT16_MAX;
    
    /*for (League *league in [[SharedComponent getInstance] listLeague]){
        if ([selectedGameBox.game.leagueId isEqualToString:league.leagueId]){
            maxQtr = league.qtr.intValue;
            maxQtrDurationSeconds = league.qtrDuration.intValue * 60;
            break;
        }
    }*/
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view addSubview:scoreBoard];
    [scoreBoard setFrame:CGRectMake(21, 45, 279, 135)];
    
    if ([CommonFunction checkPortraitOrientation])
    {
        [self.view setFrame:CGRectMake(0, 0, 320, 480)];
        [self.mainScrollView setFrame:CGRectMake(0, 0, 320, 480)];
        [scoreBoard setFrame:CGRectMake(21, 45, 279, 135)];
        [scoreBoard changeOrientation:Portrait];
    }

    
    //btnTeam1 = [scoreBoard getTeam1Button];
    //btnTeam2 = [scoreBoard getTeam2Button];
    //btnQtr = [scoreBoard getQtrButton];
    //btnSeconds = [scoreBoard getSecondsButton];
    //btnMinutes = [scoreBoard getMinutesButton];
    
    [scoreBoard setReadOnly:NO];
    //[btnTeam1 setUserInteractionEnabled:YES];
    //[btnTeam2 setUserInteractionEnabled:YES];
    //[btnQtr setUserInteractionEnabled:YES];
    //[btnSeconds setUserInteractionEnabled:YES];
    //[btnMinutes setUserInteractionEnabled:YES];
    
    /*[btnTeam1 addTarget:self action:@selector(btnTeam1Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnTeam2 addTarget:self action:@selector(btnTeam2Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnQtr addTarget:self action:@selector(btnQtrTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnSeconds addTarget:self action:@selector(btnSecondsTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnMinutes addTarget:self action:@selector(btnMinutesTapped) forControlEvents:UIControlEventTouchUpInside];
    */
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    /*
    [btnTeam1 removeTarget:self action:@selector(btnTeam1Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnTeam2 removeTarget:self action:@selector(btnTeam2Tapped) forControlEvents:UIControlEventTouchUpInside];
    [btnQtr removeTarget:self action:@selector(btnQtrTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnSeconds removeTarget:self action:@selector(btnSecondsTapped) forControlEvents:UIControlEventTouchUpInside];
    [btnMinutes removeTarget:self action:@selector(btnMinutesTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];   
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
     
    [scoreBoard activeScore:0];
     */
    
    [scoreBoard setReadOnly:YES];
}
/*
- (void) btnTeam1Tapped{
    [btnTeam1 setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
    
    [scoreBoard activeScore:1];
    
    isEditScore1 = YES;
    isEditScore2 = NO;  
    isEditQtr = NO;
    isEditTime = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnTeam2Tapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];   
    [btnTeam2 setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
    
    [scoreBoard activeScore:2];
    
    isEditScore1 = NO;
    isEditScore2 = YES;
    isEditQtr = NO;
    isEditTime = NO;
    
    [currentScore setString:@""];
    
    maxLength = 2;
}

- (void) btnQtrTapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];   
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];  
    
    [scoreBoard activeScore:3];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = YES;
    isEditTime = NO;
    
    [currentScore setString:@""];
    
    maxLength = 1;
}

- (void) btnMinutesTapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];   
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
    
    [scoreBoard activeScore:4];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditTime = YES;
    
    [currentScore setString:@""];
    
    maxLength = 4;
}

- (void) btnSecondsTapped{
    [btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];   
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
    
    [scoreBoard activeScore:4];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;
    isEditTime = YES;
    
    [currentScore setString:@""];
    
    maxLength = 4;
}
*/

- (void)viewDidUnload
{
    [self setFbCheckbox:nil];
    [self setTwitterCheckbox:nil];
    [self setFinalCheckbox:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [[SharedComponent getInstance] setCurrentViewController:nil];
    
    //[scoreBoard release];
    [fbCheckbox release];
    [twitterCheckbox release];
    [finalCheckbox release];
    [super dealloc];
}

/*
- (void) updateScore{
    if (currentScore.length > maxLength) return;
    
    NSString *newScore = currentScore;
    
    //if (currentScore.length == 1){
    //    newScore = [NSString stringWithFormat:@"0%@", currentScore];
    //}
    //else if (currentScore.length == 2)
    //{
    //    newScore = currentScore;
    //}
    
    //if (currentScore.length > 2) {
    //    newScore = [currentScore substringFromIndex:(currentScore.length - 2)];
    //    [currentScore setString:newScore];
    //}
    
    
    if (isEditScore1)
    {
        score1 = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        [scoreBoard updateTeam1Score:newScore.intValue];
    }
    else if (isEditScore2)
    {
        score2 = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        [scoreBoard updateTeam2Score:newScore.intValue];        
    }
    else if (isEditQtr)
    {
        scoreQtr = [[NSString stringWithFormat:@"%d", newScore.intValue] retain];
        
        
        // don't need in phase 1
        //if (scoreQtr.intValue > maxQtr){
        //    [scoreQtr release];
        //    scoreQtr = [[NSString stringWithFormat:@"%d", maxQtr] retain];
            
        //    NSString *message = [NSString stringWithFormat:@"Max Qtr of game is %d", maxQtr];
        //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        //    [alert show];
        //    [alert release];
            
        //    [currentScore setString:@""];
        //}
        
         
        [scoreBoard updateQtr:scoreQtr.intValue];
    }
    else if (isEditTime)
    {
        int loopTimes = maxLength - newScore.length;
        for (int i=0; i< loopTimes;i++){
            newScore = [newScore stringByAppendingString:@"0"];
        }
        
        int time = newScore.intValue;
        
        [minutes release];
        minutes = [[NSString stringWithFormat:@"%d", time/100] retain];
        
        [seconds release];
        seconds = [[NSString stringWithFormat:@"%d", time%100] retain];
        
        if (seconds.intValue >= 60){
            [seconds release];
            seconds = [[NSString stringWithFormat:@"%d", 59] retain];  
        }
        
        [scoreBoard updateMinutes:minutes.intValue];
        [scoreBoard updateSeconds:seconds.intValue];
    }
    
    //check max qtr duration
    if (isEditTime){
        int currentQtrDurationSeconds = minutes.intValue*60 + seconds.intValue;
        if (currentQtrDurationSeconds > maxQtrDurationSeconds){
            [scoreBoard updateTimeElapsed:maxQtrDurationSeconds];
            
            NSString *message = [NSString stringWithFormat:@"Max Qtr duration of game is %d:00", maxQtrDurationSeconds / 60];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            [alert release];
            
            [minutes release];
            minutes = [[NSString stringWithFormat:@"%d", maxQtrDurationSeconds/60] retain];
            [seconds release];
            seconds = [[NSString stringWithFormat:@"%d", 0] retain];
            
            [currentScore setString:@""];
        }
    }
}
*/

- (IBAction)btnUpdateScoreTapped:(id)sender {
    if ([finalCheckbox isChecked])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Ending Game will Disable Updates, Requests and Shouts. Are you sure?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"NO",@"YES", nil];
        [alert show];
        [alert release];
        return;
    }
    
    [self sendUpdateScore];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // user confirm end of game
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"YES"]){
        isEndGame = @"true";
        [self sendUpdateScore];
    }
}

- (void) sendUpdateScore{
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    //NSString *gameId = [scoreBoard currentGame].gameID;
    //NSString *score1 = [scoreBoard currentGame].team1Score;
    //NSString *score2 = [scoreBoard currentGame].team2Score;
    //NSString *scoreQtr = [scoreBoard currentGame].qtr;
    //NSString *minutes = [scoreBoard currentGame].minutes;
    //NSString *seconds = [scoreBoard currentGame].seconds;
    //NSString *outs = [scoreBoard currentGame].outs;
    //NSString *balls = [scoreBoard currentGame].balls;
    //NSString *strikes = [scoreBoard currentGame].strikes;
    //BOOL halfTime = [scoreBoard currentGame].halfTime;
    
    scoreBoard.currentGame.lastUpdated = [NSDate date]; //[CommonFunction toUTCTime:[NSDate date]];
    [scoreBoard showGame:scoreBoard.currentGame];
    
    if ([finalCheckbox isChecked])
        scoreBoard.currentGame.localStatusId = [NSString stringWithFormat:@"%d", End];
    
    [scoreBoard.currentGame updateDatabase];
    
    [sbnRequest buildUpdateScoreBody:loginToken game:scoreBoard.currentGame endGame:isEndGame];
    [self sendRequest];
    
    if ([fbCheckbox isChecked])
        [self PostShoutFB];
    else if ([twitterCheckbox isChecked])
        [self PostShoutTwitter];
}

- (NSString*) getShoutMessage {
    Game *game = scoreBoard.currentGame;
    Sport* sport = [[SharedComponent getInstance] getSport:game.sportID];
    SportTypeEnum sportType = [[SharedComponent getInstance] getSportType:game.sportID];    
    NSString *messageShout = @"";
    
    NSString *szTopBottom = @"Top";
    if (!game.isTop)
        szTopBottom = @"Bottom";
    
    switch (sportType) {
        case SportBaseball:
            messageShout = [NSString stringWithFormat:@"%@ %@%@ %@: %@ %02d - %02d %@, Inning %02d %@, Balls %d, Strikes %d, Outs %d.", hashTagSBN, hashTagPrefix2, game.gameNumber, sport.sportName, game.team1Name, game.team1Score.intValue, game.team2Score.intValue, game.team2Name, game.qtr.intValue, szTopBottom, game.balls.intValue, game.strikes.intValue, game.outs.intValue];
            break;
        case SportSoccer:
            messageShout = [NSString stringWithFormat:@"%@ %@%@ %@: %@ %02d - %02d %@, Half %d, Time %02d:%02d.", hashTagSBN, hashTagPrefix2, game.gameNumber, sport.sportName, game.team1Name, game.team1Score.intValue, game.team2Score.intValue, game.team2Name, game.qtr.intValue, game.minutes.intValue, game.seconds.intValue];
            break;
        case SportFootball:
        case SportBasketball:
        case SportUnknown:
            messageShout = [NSString stringWithFormat:@"%@ %@%@ %@: %@ %02d - %02d %@, Qtr %d, %02d:%02d Remain.", hashTagSBN, hashTagPrefix2, game.gameNumber, sport.sportName, game.team1Name, game.team1Score.intValue, game.team2Score.intValue, game.team2Name, game.qtr.intValue, game.minutes.intValue, game.seconds.intValue];
            break;
    }
    return messageShout;
}

- (void) PostShoutFB {
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"offline_access", nil];
    NSString *messageShout = [self getShoutMessage];
    
    NSMutableDictionary *params =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     @"Score Board Now", @"name",
     messageShout, @"caption",
     @"", @"description",
     @"http://scoreboardnow.com", @"link",
     @"http://scoreboardnow.com/images/SBNLogo.png", @"picture",
     nil];
    
    if (![[[SharedComponent getInstance] facebook] isSessionValid]) {
        [[[SharedComponent getInstance] facebook] authorize:permissions];
        [[[SharedComponent getInstance] facebook] dialog:@"feed"
                                               andParams:params
                                             andDelegate:self];
    }
    else
    {
        [[[SharedComponent getInstance] facebook] dialog:@"feed"
                                               andParams:params
                                             andDelegate:self];
    }
}

- (void) PostShoutTwitter {
    TWTweetComposeViewController *twitter = [[TWTweetComposeViewController alloc] init];
    NSString *postData = [self getShoutMessage];
    
    if ([postData length] > 140)
    {
        postData = [postData substringToIndex:140];
    }
    
    [twitter setInitialText:[NSString stringWithFormat:@"%@", postData]];
    
    [self presentViewController:twitter animated:YES completion:nil];
    twitter.completionHandler = ^(TWTweetComposeViewControllerResult res) {
        if(res == TWTweetComposeViewControllerResultDone) {
            [twitter dismissModalViewControllerAnimated:YES];
        }
        if(res == TWTweetComposeViewControllerResultCancelled) {
            [twitter dismissModalViewControllerAnimated:YES];
        }
    };
}

- (void) dialogDidComplete:(FBDialog *)dialog{
    NSLog(@"Post FB completed.");
    
    if ([twitterCheckbox isChecked])
        [self PostShoutTwitter];
}
- (void) dialogDidNotComplete:(FBDialog *)dialog{
    NSLog(@"Post FB not completed.");
    
    if ([twitterCheckbox isChecked])
        [self PostShoutTwitter];
}

- (IBAction)numberPad1Tapped:(id)sender {
    //[currentScore appendString:@"1"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad1];
}

- (IBAction)numberPad2Tapped:(id)sender {
    //[currentScore appendString:@"2"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad2];
}

- (IBAction)numberPad3Tapped:(id)sender {
    //[currentScore appendString:@"3"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad3];
}

- (IBAction)numberPad4Tapped:(id)sender {
    //[currentScore appendString:@"4"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad4];
}

- (IBAction)numberPad5Tapped:(id)sender {
    //[currentScore appendString:@"5"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad5];
}

- (IBAction)numberPad6Tapped:(id)sender {
    //[currentScore appendString:@"6"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad6];
}

- (IBAction)numberPad7Tapped:(id)sender {
    //[currentScore appendString:@"7"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad7];
}

- (IBAction)numberPad8Tapped:(id)sender {
    //[currentScore appendString:@"8"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad8];
}

- (IBAction)numberPad9Tapped:(id)sender {
    //[currentScore appendString:@"9"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad9];
}

- (IBAction)numberPad0Tapped:(id)sender {
    //[currentScore appendString:@"0"];
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPad0];
}

- (IBAction)delPadTapped:(id)sender {
    //[currentScore setString:@""];
    
    //[self updateScore];
    [scoreBoard keyPadPressed:KeyPadDel];
}

- (IBAction)enterPadTapped:(id)sender {
    /*[btnTeam1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnTeam2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnQtr setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];  
    
    [scoreBoard activeScore:NSNotFound];
    
    isEditScore1 = NO;
    isEditScore2 = NO;
    isEditQtr = NO;    isEditTime = NO;*/
    [scoreBoard keyPadPressed:KeyPadEnter];
}

@end
