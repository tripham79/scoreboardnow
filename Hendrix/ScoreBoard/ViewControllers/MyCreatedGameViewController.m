//
//  MyCreatedGameViewController.m
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//

#import "MyCreatedGameViewController.h"
#import "CreateGameViewController.h"
#import "GameInfoCell.h"
#import "GetMyCreatedGameResponse.h"

@interface MyCreatedGameViewController ()

@end

@implementation MyCreatedGameViewController {
    NSMutableArray *listGames;
    NSDateFormatter *dateFormatter;
    int currentFromIndex;
    int totalGameRecords;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableGames setDelegate:self];
    [self.tableGames setDataSource:self];
    
    UIImage* img = [self.btnAdd backgroundImageForState:UIControlStateNormal];
    img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [self.btnAdd setBackgroundImage:img forState:UIControlStateNormal];
    
    // iOS 5 and later
    if ([self.tableGames respondsToSelector:@selector(registerNib:forCellReuseIdentifier:)]) {
        [self.tableGames registerNib:[UINib nibWithNibName:@"GameInfoCell" bundle:nil] forCellReuseIdentifier:@"GameInfoCell"];
    }
    
    listGames = [[NSMutableArray alloc] init];
    currentFromIndex = 0;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    // load games from server
    [self loadGames];
}

- (void)loadGames {
    [MBProgressHUD showHUDAddedTo:self.view text:@"Loading" animated:YES];
    
    [[SBNService sharedInstance] getMyCreatedGames:currentFromIndex maxRecords:PAGING_RECORD withCallback:^(BaseRequest *request, id result, NSError *error)
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        // if has error then stop
        if ([self handleServiceError:result error:error])
            return;
        
        // reload game list
        GetMyCreatedGameResponse* response = (id)result;
        totalGameRecords = response.totalRecords;
        currentFromIndex += [response.games count];
        
        [listGames addObjectsFromArray:response.games];
        [self.tableGames reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackClick:(id)sender {
    [[[SharedComponent getInstance] navigationController] popViewControllerAnimated:YES];
}

- (void)dealloc {
    [_tableGames release];
    [_btnAdd release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setTableGames:nil];
    [self setBtnAdd:nil];
    [super viewDidUnload];
}

- (IBAction)btnAddClick:(id)sender {
    // add new game
    CreateGameViewController *viewController = [[CreateGameViewController alloc] initWithNibName:@"CreateGameViewController" bundle:nil];
    viewController.game = [[Game alloc] init];
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listGames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *gameCellIdentifier = @"GameInfoCell";
    GameInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:gameCellIdentifier];
    
    // If there is no cell to reuse, create a new one
    if(cell == nil) {
        //cell = [[CheckboxCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:gameCellIdentifier];
        
        // works
        //cell = [[[NSBundle mainBundle] loadNibNamed:@"CheckboxCell" owner:self options:nil] objectAtIndex:0];
        
        // works too
        cell = (GameInfoCell*)[[[UINib nibWithNibName:@"GameInfoCell" bundle:nil] instantiateWithOwner:self  options:nil] objectAtIndex:0];
        
        [cell.lblTitle setFont:[UIFont fontWithName:@"Trebuchet MS" size:15]];
        [cell.lblTitle setMinimumFontSize:10];
        [cell.lblTitle setTextColor:[UIColor blackColor]];
        [cell.lblTitle setAdjustsFontSizeToFitWidth:YES];
    }
   
    // Configure the cell before it is displayed...
    Game* game = [listGames objectAtIndex:indexPath.row];
    NSDate* localDate = [game.scheduledStartTime toLocalDate];
    
    NSString *gameTitle = [NSString stringWithFormat:@"[%@] %@ - %@", [dateFormatter stringFromDate:localDate], game.team1Name, game.team2Name];
    cell.lblTitle.text = gameTitle;
    
    if (game.sportName == nil)
        game.sportName = @"NA";
    if (game.leagueName == nil)
        game.leagueName = @"NA";
    if (game.divisionName == nil)
        game.divisionName = @"NA";
    
    cell.lblDetail.text = [NSString stringWithFormat:@"%@ / %@ / %@", game.sportName, game.leagueName, game.divisionName];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Game* game = [listGames objectAtIndex:indexPath.row];
    CreateGameViewController *viewController = [[CreateGameViewController alloc] initWithNibName:@"CreateGameViewController" bundle:nil];
    viewController.game = game;
    
    [[[SharedComponent getInstance] navigationController] pushViewController: viewController animated:YES];
}

- (void)gameSaved:(Game*)game isAdded:(BOOL)isAdded {
    if (isAdded) {
        [listGames insertObject:game atIndex:0];
        NSIndexPath* firstRowIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableGames insertRowsAtIndexPaths:[NSArray arrayWithObject:firstRowIndex] withRowAnimation:YES];
    }
    else {
        NSIndexPath* selectedIndex = [self.tableGames indexPathForSelectedRow];
        [listGames setObject:game atIndexedSubscript:selectedIndex.row];
        [self.tableGames reloadRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)gameDeleted:(Game*)game {
    NSIndexPath* selectedIndex = [self.tableGames indexPathForSelectedRow];
    [listGames removeObjectAtIndex:selectedIndex.row];
    [self.tableGames deleteRowsAtIndexPaths:[NSArray arrayWithObject:selectedIndex] withRowAnimation:YES];
}

@end
