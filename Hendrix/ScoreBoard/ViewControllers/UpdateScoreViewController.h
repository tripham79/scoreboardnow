
#import "BaseViewController.h"
#import "FBConnect.h"
#include <Twitter/Twitter.h>

@interface UpdateScoreViewController : BaseViewController<FBDialogDelegate, UIAlertViewDelegate>{
    //BOOL isGettingScore;
    
    //UIButton *btnTeam1;
    //UIButton *btnTeam2;
    //UIButton *btnQtr;
    //UIButton *btnSeconds;
    //UIButton *btnMinutes;
    
    //NSString *score1;
    //NSString *score2;
    //NSString *scoreQtr;
    //NSString *seconds;
    //NSString *minutes;
    
    //NSMutableString *currentScore;
    //BOOL isEditScore1;
    //BOOL isEditScore2;
    //BOOL isEditQtr;
    
    //BOOL isEditTime;
    
    //int maxQtr;
    //int maxQtrDurationSeconds;
    
    //int maxLength;
    
    NSString *isEndGame;
}

@property (assign) BaseScoreBoard *scoreBoard;
// use to update latest game score 
//@property (retain) GameBox *selectedGameBox;

@property (retain, nonatomic) IBOutlet CCheckboxField *fbCheckbox;
@property (retain, nonatomic) IBOutlet CCheckboxField *twitterCheckbox;
@property (retain, nonatomic) IBOutlet CCheckboxField *finalCheckbox;

//- (void) getGameScore;
//- (void) updateScore;
- (IBAction)btnUpdateScoreTapped:(id)sender;
- (void) sendUpdateScore;

- (IBAction)numberPad1Tapped:(id)sender;
- (IBAction)numberPad2Tapped:(id)sender;
- (IBAction)numberPad3Tapped:(id)sender;
- (IBAction)numberPad4Tapped:(id)sender;
- (IBAction)numberPad5Tapped:(id)sender;
- (IBAction)numberPad6Tapped:(id)sender;
- (IBAction)numberPad7Tapped:(id)sender;
- (IBAction)numberPad8Tapped:(id)sender;
- (IBAction)numberPad9Tapped:(id)sender;
- (IBAction)numberPad0Tapped:(id)sender;
- (IBAction)delPadTapped:(id)sender;
- (IBAction)enterPadTapped:(id)sender;

@end
