
#import "BaseViewController.h"
#import "FBConnect.h"
#include <Twitter/Twitter.h>

@interface GameFeedViewController : BaseViewController<UITextViewDelegate, FBDialogDelegate, BaseScoreBoardDelegate>{
    BOOL isGettingScore;
    
    BOOL isShouldPostToFB;
    BOOL isShouldPostToTwitter;
    NSString *messageShout;
}

//@property (retain) GameBox *selectedGameBox;
@property (retain) Game *selectedGame;
@property (retain) BaseScoreBoard *scoreBoard;
@property (retain) IBOutlet UIButton *btnRefreshNow;
@property (retain) IBOutlet UIButton *btnCheckIn;
@property (retain, nonatomic) IBOutlet UIButton *btnCheckOut;

@property (retain) IBOutlet UIButton *btnVewiGameFeed;
@property (retain) IBOutlet UIButton *btnPostShout;
@property (retain, nonatomic) IBOutlet UILabel *numberCheckedInUser;

@property (retain) IBOutlet UIButton *btnRequestScore;
@property (retain) IBOutlet UIButton *btnUpdateScore;

@property (retain) IBOutlet UITextView *shoutMessage;

@property (retain) IBOutlet CCheckboxField *fbCheckbox;
@property (retain) IBOutlet CCheckboxField *twitterCheckbox;
- (IBAction)btnRefreshTapped:(id)sender;

- (IBAction)hideKeyboard:(id)sender;
- (IBAction)btnTwitterFeedTapped:(id)sender;

- (IBAction)btnPostShoutTapped:(id)sender;
- (IBAction)checkInGameTapped:(id)sender;
- (IBAction)btnCheckOutTapped:(id)sender;


//- (void) changeOrientation;
- (void) getGameScore;
@end
