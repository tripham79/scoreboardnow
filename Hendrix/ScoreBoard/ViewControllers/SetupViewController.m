typedef enum SetupScreenStep{
    SetConfig = 0,
    Logout
}SetupScreenStep;

#import "SetupViewController.h"

@implementation SetupViewController
@synthesize startGameSetting;
@synthesize endGameSetting;
@synthesize scoreUpdateDropDownList;
@synthesize scoreRequestSetting;
@synthesize selectSportDropDownList;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[CommonFunction getDataWithKey:Notification_StartGame] isEqualToString:@"YES"]){
        [startGameSetting setStatus:YES];
    }
    else
    {
        [startGameSetting setStatus:NO];
    }
    
    if ([[CommonFunction getDataWithKey:Notification_EndGame] isEqualToString:@"YES"]){
        [endGameSetting setStatus:YES];
    }
    else
    {
        [endGameSetting setStatus:NO];
    }
    
    if ([[CommonFunction getDataWithKey:Notification_SocreRequest] isEqualToString:@"YES"]){
        [scoreRequestSetting setStatus:YES];
    }
    else
    {
        [scoreRequestSetting setStatus:NO];
    }
    
    if ([CommonFunction getDataWithKey:Notification_ScoreUpdate] == nil || [[CommonFunction getDataWithKey:Notification_ScoreUpdate] isEqualToString:@""])
    {
        [scoreUpdateDropDownList setTitle:@"Never" forState:UIControlStateNormal];
        selectedScoreUpdateId = @"0";
    }
    else
    {
        NSString *optionName = [CommonFunction getDataWithKey:Notification_ScoreUpdate];
        [scoreUpdateDropDownList setTitle:optionName forState:UIControlStateNormal];
        
        for (GameScoreRefreshOption *option in [[SharedComponent getInstance] listGameScoreRefreshOption])
        {
            if ([option.optionName isEqualToString:optionName]){
                selectedScoreUpdateId = option.optionId;
                break;
            }
        }
    }
    [scoreUpdateDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];
    
    // sport displayed in my score board
    selectedSportId = [CommonFunction getDataWithKey:MyScoreBoard_ShowSport];
    if (selectedSportId == nil || [selectedSportId isEqualToString:@""])
    {
        [selectSportDropDownList setTitle:@"All" forState:UIControlStateNormal];
    }
    else
    {
        NSString *selectedSportName = @"All";        
        for (Sport *sport in [[SharedComponent getInstance] listSport])
        {
            if ([sport.sportId isEqualToString:selectedSportId]){
                selectedSportName = sport.sportName;
                break;
            }
        }        
        [selectSportDropDownList setTitle:selectedSportName forState:UIControlStateNormal];
    }
    
    [selectSportDropDownList addTarget:self action:@selector(showDropDownList:) forControlEvents:UIControlEventTouchUpInside];

}

- (void) showDropDownList:(id)sender
{
    DropDownList *dropDownList = [[DropDownList alloc] init];
    
    NSMutableArray *listItem = [[NSMutableArray alloc] init];
    
    if (sender == scoreUpdateDropDownList)
    {
        for (GameScoreRefreshOption *option in [[SharedComponent getInstance] listGameScoreRefreshOption])
        {
            [listItem addObject:option.optionName];
        }
        [dropDownList setOwner:scoreUpdateDropDownList];
    }
    else if (sender == selectSportDropDownList)
    {
        [listItem addObject:@"All"];
        for (Sport *sport in [[SharedComponent getInstance] listSport])
        {
            [listItem addObject:sport.sportName];
        }
        [dropDownList setOwner:selectSportDropDownList];
    }
    
    [dropDownList setSource:listItem];
    [dropDownList setDelegate:self];

    [listItem release];
    
    [[[SharedComponent getInstance] navigationController] pushViewController:dropDownList animated:YES];
    [dropDownList release];    
}

- (void) dropDownList:(id)owner didSelectedRowAtIndex:(int)index {
    if (owner == scoreUpdateDropDownList)
    {
        GameScoreRefreshOption *option = [[[SharedComponent getInstance] listGameScoreRefreshOption] objectAtIndex:index];
        
        [scoreUpdateDropDownList setTitle:option.optionName forState:UIControlStateNormal];
        selectedScoreUpdateId = option.optionId;
    }
    else if (owner == selectSportDropDownList) {
        if (index > 0) {
            Sport *sport = [[[SharedComponent getInstance] listSport] objectAtIndex:index-1];
            selectedSportId = sport.sportId;
            [selectSportDropDownList setTitle:sport.sportName forState:UIControlStateNormal];
        }
        else {
            selectedSportId = @"";
            [selectSportDropDownList setTitle:@"All" forState:UIControlStateNormal];
        }
    }
}

- (void)viewDidUnload
{
    [self setStartGameSetting:nil];
    [self setEndGameSetting:nil];
    [self setScoreUpdateDropDownList:nil];
    [self setScoreRequestSetting:nil];
    [self setSelectSportDropDownList:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    [startGameSetting release];
    [endGameSetting release];
    [scoreUpdateDropDownList release];
    [scoreRequestSetting release];
    [selectSportDropDownList release];
    [super dealloc];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //Log config
    NSLog(@"refreshSeconds %d", [[SharedComponent getInstance] refreshSeconds]);
    NSLog(@"gameScoreRefreshSeconds %d", [[SharedComponent getInstance] gameScoreRefreshSeconds]);
}

- (IBAction)btnDoneTapped:(id)sender {
    if ([startGameSetting status]){
        [CommonFunction saveData:@"YES" withKey:Notification_StartGame];    
    }
    else
    {
        [CommonFunction saveData:@"NO" withKey:Notification_StartGame];    
    }
    
    if ([endGameSetting status]){
        [CommonFunction saveData:@"YES" withKey:Notification_EndGame];    
    }
    else
    {
        [CommonFunction saveData:@"NO" withKey:Notification_EndGame];    
    }
    
    if ([scoreRequestSetting status]){
        [CommonFunction saveData:@"YES" withKey:Notification_SocreRequest];    
    }
    else
    {
        [CommonFunction saveData:@"NO" withKey:Notification_SocreRequest];    
    }
    
    [CommonFunction saveData:selectedSportId withKey:MyScoreBoard_ShowSport];
    
    currentStep = SetConfig;
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildSetClientConfigurationBody:loginToken scoreUpateOptionId:selectedScoreUpdateId];
    [self sendRequest];
}

- (IBAction)btnLogoutTapped:(id)sender {
    currentStep = Logout;
    NSString *loginToken = [[SharedComponent getInstance] loginToken];
    if (sbnRequest == nil){
        sbnRequest = [[SBNRequest alloc] init];
    }
    
    [sbnRequest buildLogoutBody:loginToken];
    [self sendRequest];
}

- (void) handleResponse:(NSString*)responseData
{
    if (responseData == nil || [responseData isEqualToString:@""])
        return;
    
    SBNResponse *response;
    
    switch (currentStep) {
        case Logout:
            response = [[SBNLogoutResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        case SetConfig:
            response = [[SBNSetClientConfigurationResponse alloc] init];
            [response parseResponseData:responseData];
            break;
        default:
            break;
    }
    
    if ([response.responseCode isEqualToString:@""]) return;
    
    if ([response.responseCode isEqualToString: RESPONSE_SUCCESSFULL]){
        switch (currentStep) {
            case Logout:
                {    
                    [CommonFunction saveData:@"" withKey:kLoginToken];
                    [CommonFunction saveData:@"" withKey:kUser];
                    [CommonFunction saveData:@"" withKey:kCanCreateGame];
                    
                    BOOL isPopToLogin = NO;
                    
                    UINavigationController *nav = [[SharedComponent getInstance] navigationController];
                    for (UIViewController *viewController in [nav viewControllers])
                    {
                        if ([viewController isKindOfClass:[SignInViewController class]])
                        {
                            isPopToLogin = YES;
                            [nav popToViewController:viewController animated:YES];
                            break;
                        }
                    }
                    
                    if (!isPopToLogin){
                        [nav popToRootViewControllerAnimated:NO];
                        SignInViewController *viewController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
                        [[[SharedComponent getInstance] navigationController] pushViewController:viewController animated:YES];
                        [viewController release];
                    }
                    break;
                }   
            case SetConfig:
                {
                    [CommonFunction saveData:[scoreUpdateDropDownList titleLabel].text withKey:Notification_ScoreUpdate];
                    
                    NSString *optionName = [scoreUpdateDropDownList titleLabel].text;
                    BOOL isStop = NO;
                    for (GameScoreRefreshOption *option in [[SharedComponent getInstance] listGameScoreRefreshOption]){
                        if (!isStop){
                            if ([optionName isEqualToString:option.optionName]){
                                [[SharedComponent getInstance] setGameScoreRefreshSeconds:option.optionSeconds.intValue];
                                isStop = YES;
                            }   
                        }
                    }
                    [[[SharedComponent getInstance] navigationController] popViewControllerAnimated:YES];
                    break;
                }
            default:
                break;
        }
    }
    else
    {
        NSString *errorMessage = @"Error";
        if (![response.errorMessage isEqualToString:@""])
        {
            errorMessage = response.errorMessage;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
    [responseData release];
    [response release];
}
@end
