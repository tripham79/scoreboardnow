

#import "BaseViewController.h"
#import "FBConnect.h"

@interface SignInViewController : BaseViewController{
    BOOL isLoginStep;
    BOOL isSubscribeDeviceStep;
    BOOL isGetLeagueStep;
    
    BOOL isLinkTwitter;
}

@property (retain, nonatomic) IBOutlet UITextField *userNameField;
@property (retain, nonatomic) IBOutlet UITextField *passwordField;
@property (retain, nonatomic) IBOutlet UIButton *btnFacebook;
@property (retain, nonatomic) IBOutlet UIButton *btnTwitter;

- (IBAction)btnFacebookTapped:(id)sender;
- (IBAction)btnTwitterTapped:(id)sender;

#pragma user action
- (IBAction)btnRegistrationTapped:(id)sender;

- (IBAction)btnDoneTapped:(id)sender;
- (IBAction)btnForgotPasswordTapped:(id)sender;


- (void) doLogIn;
@end
