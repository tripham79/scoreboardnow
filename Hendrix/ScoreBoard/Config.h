//
//  Config.h
//  ScoreBoard
//
//  Created by Tri Pham Minh on 3/24/14.
//  Copyright (c) 2014 YPVN. All rights reserved.
//
#import "UIAlertView+Extensions.h"

#define SBNEnpoint @"http://hendrix.scoreboardnow.com/Services/SBNService.svc"
//#define SBNEnpoint @"http://192.168.0.100/Hendrix/Services/SBNService.svc"
//#define SBNEnpoint @"http://10.0.0.190/Hendrix/Services/SBNService.svc"

#define INVALID_LOGIN_TOKEN @"INVALID_LOGIN_TOKEN"
#define PAGING_RECORD 15

// Check if the "thing" pass'd is empty
static inline BOOL isEmpty(id thing) {
    return thing == nil
    || [thing isKindOfClass:[NSNull class]]
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

inline static void showAlert(NSString* title, NSString *message, id delegate)
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

inline static void showQuestion(NSString* title, NSString *message, id delegate)
{
    NSString* yesText = @"Yes";
    NSString* noText = @"No";
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate  cancelButtonTitle:noText otherButtonTitles:yesText, nil];
    [alert show];
}

inline static void showAlertWithBlock(NSString* title, NSString *message, void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex))
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [alert showWithCompletion:completionBlock];
}


inline static void showQuestionWithBlock(NSString* title, NSString *message, void(^completionBlock)(UIAlertView *alertView, NSInteger buttonIndex))
{
    NSString* yesText = @"Yes";
    NSString* noText = @"No";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:noText
                                              otherButtonTitles:yesText, nil];
    
    [alertView showWithCompletion:completionBlock];
}
